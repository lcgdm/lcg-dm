/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-modifyfs.c,v 1.2 2007/07/19 15:53:24 grodid Exp $

// Modified by GG (25/10/2004)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"

#ifndef SIGCHLD
#define SIGCHLD SIGCLD
#endif


//********************************************************//
int main(int argc, char *argv[])
{

  //struct dpm_filestatus *filestatuses;
  //struct dpm_tokeninfo *tokeninfos;
	int i;
	int nbfiles;
	int nbprotocols;
	int nbreplies;
	static char *protocols[] = {"rfio"};
	int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;

	int nbprotos;
	char **lprotos;
	int nbsurls;
	//char **lsurls;

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 4) {
		fprintf (stderr, "usage: %s servername(str) fsname(str) status(int)\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	printf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	printf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( nbargs != 3 ) {
    perror (" dpm-modifyfs: 3 args must be provided, see --help");
    exit(1);
  }
  nbfiles = nbargs;
  printf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  printf (" \n");

  /*
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROT)
  */

  // Setting defaults

/*    if ( ! dpm_rtoken ) { */
/*      perror (" Reqid token option is mandatory: --dpmrtoken=request_token "); */
/*      exit(1); */
/*    } */
 nbsurls = nbargs;
 status = atoi(remargs[2]);

  printf (" \n");

	if ((status = dpm_modifyfs (remargs[0],
				    remargs[1], 
				    status
				    )) < 0) {
		sperror ("dpm_modifyfs");
		exit (1);
	}

	printf ("request state %s\n", status == DPM_DONE ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

	return 0;

}
