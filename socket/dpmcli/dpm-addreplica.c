/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-addreplica.c,v 1.2 2007/07/19 15:53:24 grodid Exp $

// Created by GG (07/03/2006)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
//#include "Cns.h"
//#include "Cns_api.h"
//#include "dpns.h"
//#include "dpm.h"
#include "dpns_api.h"
#include "dpm_api.h"
#include "serrno.h"
#include "u64subr.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"

extern int dpns_addreplica(char const *,struct dpns_fileid *,char const *,char const *,char const,char const,char const *,char const *);

//********************************************************//
int main(int argc, char *argv[])
{

#if 0
  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
#include "dpmestat.h"
	struct dpm_putfilestatus *filestatuses;
	int i;
	int nbfiles;
	int nbprotocols;
	int nbreplies;
	static char *protocols[] = {"rfio"};
	int r = 0;
	char r_token[CA_MAXDPMTOKENLEN+1];
	//char u_token[CA_MAXDPMTOKENLEN+1];
	struct dpm_putfilereq *reqfiles;
	//int status;
#endif

	char *guid = NULL;
	struct dpns_fileid file_uniqueid;
	char *server = NULL;
	char *sfn = NULL;
	char f_status = '-';
	char f_type;
	char *poolname = NULL;
	char *fs = NULL;

	int thisarg;
	int nbargs;
	char **remargs;
	int i;
	int status;

#include "dpmgdebug.h"

	if (argc < 5) {
		fprintf (stderr, "usage: %s [--dpmftype=[V|D|P]] guid server SURLorPFN poolname mountpoint\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    perror ("dpm-put: at least one SURL must be provided ");
    exit(1);
  }
  //nbfiles = nbargs;
  //Pprintf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  //Pprintf (" \n");

#if 0
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROTL)
#endif

  // Setting defaults

 if ( dpm_ftype == 'Z' || dpm_ftype == '0' ) {
   dpm_ftype = 'V';
 }

#if 0
 for (i = 0; i < nbsurls; i++) {
   reqfiles[i].lifetime = dpm_lifet;
   reqfiles[i].to_surl = remargs[i];
   reqfiles[i].f_type = dpm_ftype;  //  pb with 'V' ?
   //reqfiles[i].f_type = 'V';
   strcpy(reqfiles[i].s_token, dpm_stoken);
   reqfiles[i].requested_size = dpm_reqsize*1024*1024;
   printf (" File: %s %d %s %c \n", reqfiles[i].to_surl, reqfiles[i].lifetime, reqfiles[i].s_token, reqfiles[i].f_type);
   //printf (" File: %s \n", reqfiles[i].to_surl);
 }

  printf (" \n");

	nbprotocols = sizeof(protocols) / sizeof(char *);
#endif

	//memset (file_uniqueid, 0, sizeof(struct dpns_fileid));
	memset (&file_uniqueid, 0, sizeof(file_uniqueid));
	strcpy(file_uniqueid.server, remargs[1]);
	printf ("dpm-addreplica got fileid: %s\n", remargs[0]);
	file_uniqueid.fileid = strutou64(remargs[0]);
	printf ("dpm-addreplica got fileid: %lld\n", file_uniqueid.fileid);
	//exit(0);

	if ((status = dpns_addreplica (NULL, 
			       &file_uniqueid, 
			       remargs[1], 
			       remargs[2], 
			       f_status, 
			       dpm_ftype, 
			       remargs[3], 
			       remargs[4] 
			       )) < 0) {
		sperror ("dpns_addreplica");
		exit (1);
	}

#if 0
	printf ("dpm_put returned r_token: %s\n", r_token);

	/* wait for request status "Done" or "Failed" */

	while (status == DPM_QUEUED || status == DPM_ACTIVE) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->to_surl)
				free ((filestatuses+i)->to_surl);
			if ((filestatuses+i)->turl)
				free ((filestatuses+i)->turl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
		printf("request state Pending\n");
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if ((status = dpm_getstatus_putreq (r_token, 0, NULL,
		    &nbreplies, &filestatuses)) < 0) {
			sperror ("dpm_getstatus_putreq");
			//exit (1);
                        if (status == DPM_FAILED)
                               break;
		}
	}
#endif
	printf ("request state %d\n", status);
	printf ("request state %s\n", status == DPM_DONE ? "Done" : "Failed");
/*
	if (status == DPM_FAILED)
		exit (1);
*/
	return 0;

}
