/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpmutils.h,v 1.1 2005/01/20 09:47:19 grodid Exp $

/* Some common utils for the DPM CLI suite */
// Created by GG (19/10/2004)

/* dpmt/dpm_utils.c */
extern void signal_handler(int signo);
extern void store_cmd(const char *s, int n, sel_union *data, int *nb, char **store);
extern void item_store(int typecsel, int *nb, char ***pstore);
extern void p_array(const char *title, int nl, char **files);
















/*  Templates */
#if 0
/* global.c */
extern void self_info(void);

/* parser.c */
extern int arg_parse(int argc, char *argv[]);
#endif
