/*
 * Copyright (C) 2005-2006 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-listReplicas.c,v $ $Revision: 1.2 $ $Date: 2006/12/19 20:17:29 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*      dpm-listReplicas - drain a component of the Light Weight Disk Pool Manager */

/*
 * ../../dpm/dpm-listReplicas --server lxb1904.cern.ch --fs /data02
 */

#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "dpns_api.h"
#include "rfio_api.h"
#include "serrno.h"
#define DEFPOLLINT 5

//********************************************************//
static int
rfstat(dest) 
     char *dest;
{
        struct stat statbuf;

        if (rfio_stat (dest, &statbuf) < 0) {
                perror ("rfio_stat");
                //exit (1);
		printf ("rfio_stat failed for: %s\n", dest);
		return -1;
        }

        //printf ("rfio_stat successful for: %s PHYSICAL FILE SIZE ====> %ld\n", dest, statbuf.st_size);
        printf ("rfio_stat successful: PHYSICAL FILE SIZE ====> %ld\n", statbuf.st_size);
	/*
        printf ("mode = %o\n", statbuf.st_mode);
        printf ("nlink = %d\n", statbuf.st_nlink);
        printf ("uid = %d\n", statbuf.st_uid);
        printf ("gid = %d\n", statbuf.st_gid);
        printf ("size = %ld\n", statbuf.st_size);

  printf (" \n");
	*/

	return 0;
}

//********************************************************//
static int
listreplica(surl)
     char *surl;
{

            int flags;
            Cns_list list;
            struct Cns_filereplica *lp;
	    struct Cns_filestatg statbufg;
	    int rc = 0;

            flags = CNS_LIST_BEGIN;
            while ((lp = dpns_listreplica (surl, NULL, flags, &list)) != NULL) {
                 flags = CNS_LIST_CONTINUE;
                 /* process the entry */
                 printf (" SFN = %s, FS = %s\n", lp->sfn, lp->fs);
		 //lpsfn = strdup(lp->sfn);
		 if (Cns_statr (lp->sfn, &statbufg) < 0) {
		   rc = -1;
		   break;
		   //return (-1);
		 }
		 if ( ! statbufg.filesize ) {
		   printf (" SFN = %s, FS = %s <======= NULL SIZE \n", lp->sfn, lp->fs);
		   if (rfstat(lp->sfn) < 0) {
		     rc = -1;
		     break;
		     //return -1;
		   }
		 }
            }
            (void) dpns_listreplica (surl, NULL, CNS_LIST_END, &list);

	    return rc;
}

//********************************************************//
static int
greplicate(const char *path, char f_type)
{
#if 0
	struct dpm_filestatus *filestatuses = NULL;
	char *from_turl;
	struct dpm_getfilereq getfilereq;
	struct dpm_getfilestatus *getfilestatus;
	int i;
	int nbreplies;
	char *p;
	static char *protocols[] = {"gsiftp"};
	struct dpm_putfilereq putfilereq;
	struct dpm_putfilestatus *putfilestatus;
	int r = 0;
	char r_tokeng[CA_MAXDPMTOKENLEN+1];
	char r_tokenp[CA_MAXDPMTOKENLEN+1];
	char sfn[CA_MAXSFNLEN+1];
	struct Cns_filestatg statbuf;
	int status;
	char *surl;
	char *to_turl;
	char turl[CA_MAXSFNLEN+1];
#endif

	int i;
	struct Cns_filestatg statbuf;
	char sfn[CA_MAXSFNLEN+1];
	char *surl;
	struct Cns_filereplica *lp = NULL;
	int rc=0;
	int nbentries;

#if 0
	if (strncmp (path, "srm://", 6) == 0 ||
	    rfio_HsmIf_IsHsmFile (path)) {
		surl = (char *) path;
		memset (&getfilereq, 0, sizeof(getfilereq));
		getfilereq.from_surl = surl;
		if ((status = dpm_get (1, &getfilereq, 1, protocols, NULL,
		    0, r_tokeng, &nbreplies, &getfilestatus)) < 0)
			return (-1);

		/* wait for request status "Done" or "Failed" */

		while (status == DPM_QUEUED || status == DPM_ACTIVE) {
			if (getfilestatus->from_surl)
				free (getfilestatus->from_surl);
			if (getfilestatus->turl)
				free (getfilestatus->turl);
			if (getfilestatus->errstring)
				free (getfilestatus->errstring);
			free (getfilestatus);
			sleep ((r++ == 0) ? 1 : DEFPOLLINT);
			if ((status = dpm_getstatus_getreq (r_tokeng, 0, NULL,
			    &nbreplies, &getfilestatus)) < 0)
				return (-1);
		}
		if (getfilestatus->turl)
			from_turl = getfilestatus->turl;
		if (getfilestatus->from_surl)
			free (getfilestatus->from_surl);
		if (getfilestatus->errstring)
			free (getfilestatus->errstring);
		free (getfilestatus);
	} else {
#endif
		if (Cns_statr (path, &statbuf) < 0)
			return (-1);
		//OKprintf(" Cns_statr path: %s statbuf.fileid: %d\n", path, statbuf.fileid);
#if 0
		p = strstr (path, ":/");
		strcpy (turl, "gsiftp://");
		strncpy (turl + 9, path, p - path);
		*(turl + (p - path + 9)) = '/';
		strcpy (turl + (p - path + 10), path);
		from_turl = turl;
#endif
		if (Cns_getpath (NULL, statbuf.fileid, sfn) < 0)
			return (-1);
		surl = sfn;
		//BADprintf(" Cns_getpath statbuf.fileid: %d surl: %s\n", statbuf.fileid, surl);
		//OKprintf(" Cns_getpath surl: %s statbuf.fileid: %d\n", surl, statbuf.fileid);
#if 0
	}
#endif

	/* get turl for replica */

	printf (" Input: %s\n", surl);
#if 1
	/*
	if ( listreplica(surl) < 0 )
	  return (-1);
	*/
	rc = Cns_getreplica (surl, NULL, NULL, &nbentries, &lp);
	//if ( nbentries > 1 )
	//OKprintf ("listing %s %d %d %x %s %s %s %s\n", surl, nbentries, rc, lp, lp->poolname, lp->host, lp->fs, lp->sfn);
	  printf ("listing %s %d %d %x %s %s %s\n", surl, nbentries, rc, lp, lp->poolname, lp->host, lp->fs);
	  for (i=0; i<nbentries; i++)
	    printf (" SFN = %s, FS = %s, status: %c, f_type: %c\n", (lp+i)->sfn, (lp+i)->fs, (lp+i)->status, (lp+i)->f_type);
	  //dpm_free_filest (nbentries, lp);
	  if ( nbentries == 1 )
	    free(lp);
	  (void) fflush(NULL);
	return (0);	
#else
	listreplica(surl);
	  (void) fflush(NULL);
	return (0);

#endif
}

#if 0
//********************************************************//
static int
listreplicad(surl)
     char *surl;
{

            int flags;
            Cns_list list;
            struct Cns_filereplica *lp;

            flags = CNS_LIST_BEGIN;
            while ((lp = dpns_listreplica (surl, NULL, flags, &list)) != NULL) {
                 flags = CNS_LIST_CONTINUE;
                 /* process the entry */
                 printf (" SFN = %s, FS = %s\n", lp->sfn, lp->fs);
		 //lpsfn = strdup(lp->sfn);
            }
            (void) dpns_listreplica (surl, NULL, CNS_LIST_END, &list);

	    return 0;
}
#endif

//********************************************************//
main(argc, argv)
int argc;
char **argv;
{
	int c;
	struct dpm_fs *dpm_fs;
	struct dpm_pool *dpm_pools;
	int errflg = 0;
	struct Cns_fileid file_uniqueid;
	int flags;
	char *fs = NULL;
	int i;
	int j;
	Cns_list list;
	static struct Coptions longopts[] = {
		{"fs", REQUIRED_ARGUMENT, 0, OPT_FS},
		{"poolname", REQUIRED_ARGUMENT, 0, OPT_POOL_NAME},
		{"server", REQUIRED_ARGUMENT, 0, OPT_FS_SERVER},
		{"count", REQUIRED_ARGUMENT, 0, 9},
		{0, 0, 0, 0}
	};
	struct Cns_filereplica *lp;
	//struct Cns_filereplica **lpa;
	int nbentries;
	int nbfs;
	int nbpools;
	char *poolname = NULL;
	int rc = 0;
	char *server = NULL;
	int status;
	int count = 5;
	int gount = 0;

	char *path = "/dpm/cern.ch/home/dteam/srmv2_Tfile.fil1180A0FX";
	//char *path = "/dpm/roma1.infn.it/home/cms/PhEDEx_LoadTest/LoadTest_T2_Rome_026";

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case 9:
		  /*
			if (strlen (Coptarg) > 79) {
				fprintf (stderr,
				    "filesystem name too long: %s\n", Coptarg);
				errflg++;
			} else
		  */
				count = atoi(Coptarg);
			break;
		case OPT_FS:
			if (strlen (Coptarg) > 79) {
				fprintf (stderr,
				    "filesystem name too long: %s\n", Coptarg);
				errflg++;
			} else
				fs = Coptarg;
			break;
		case OPT_FS_SERVER:
			if (strlen (Coptarg) > CA_MAXHOSTNAMELEN) {
				fprintf (stderr,
				    "server name too long: %s\n", Coptarg);
				errflg++;
			} else
				server = Coptarg;
			break;
		case OPT_POOL_NAME:
			if (strlen (Coptarg) > CA_MAXPOOLNAMELEN) {
				fprintf (stderr,
				    "pool name too long: %s\n", Coptarg);
				errflg++;
			} else
				poolname = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc || (poolname == NULL && server == NULL && fs == NULL)) {
		errflg++;
	}
	if (fs && server == NULL)
		errflg++;
	if (errflg) {
		fprintf (stderr, "usage: %s %s\t%s %s\t%s %s",
		    argv[0], "--poolname pool_name\n",
		    argv[0], "--server fs_server\n",
		    argv[0], "--server fs_server --fs fs_name\n");
		exit (USERR);
	}

	/* Set the filesystem(s) status to FS_RDONLY */

#if 0
	if (fs) {
		if (dpm_modifyfs (server, fs, FS_RDONLY) < 0) {
			fprintf (stderr, "dpm_modifyfs %s %s: %s\n", server, fs,
			    sstrerror (serrno));
			exit (USERR);
		}
	} else if (! poolname) {	/* all filesystems on a given server */
		if (dpm_getpools (&nbpools, &dpm_pools) < 0) {
			fprintf (stderr, "dpm_getpools: %s\n", sstrerror (serrno));
			exit (USERR);
		}
		for (i = 0; i < nbpools; i++) {
			if (dpm_getpoolfs ((dpm_pools + i)->poolname, &nbfs, &dpm_fs) < 0) {
				fprintf (stderr, "dpm_getpoolfs %s: %s\n",
				    (dpm_pools + i)->poolname, sstrerror (serrno));
				errflg++;
			}
			for (j = 0; j < nbfs; j++) {
				if (strcmp ((dpm_fs + j)->server, server)) continue;
				if (dpm_modifyfs (server, (dpm_fs + j)->fs, FS_RDONLY) < 0) {
					fprintf (stderr, "dpm_modifyfs %s %s: %s\n",
					    server, (dpm_fs + j)->fs, sstrerror (serrno));
					errflg++;
				}
			}
		}
	} else {
		if (dpm_getpoolfs (poolname, &nbfs, &dpm_fs) < 0) {
			fprintf (stderr, "dpm_getpoolfs %s: %s\n",
			    poolname, sstrerror (serrno));
			errflg++;
		}
		for (j = 0; j < nbfs; j++) {
			if (server == NULL || strcmp ((dpm_fs + j)->server, server))
				continue;
			if (dpm_modifyfs (server, (dpm_fs + j)->fs, FS_RDONLY) < 0) {
				fprintf (stderr, "dpm_modifyfs %s %s: %s\n",
				    server, (dpm_fs + j)->fs, sstrerror (serrno));
				errflg++;
			}
		}
	}
#endif
	if (errflg)
		exit (USERR);
	memset (&file_uniqueid, 0, sizeof(file_uniqueid));

#if 0	
	count = 200;
	flags = CNS_LIST_BEGIN;
	while ((lp = Cns_listreplicax (poolname, server, fs, flags, &list)) != NULL) {
		if ( ! count-- )
		  break;
		flags = CNS_LIST_CONTINUE;
		printf ("listing %s\n", lp->sfn);
	}
	(void) Cns_listreplicax (poolname, server, fs, CNS_LIST_END, &list);
	exit (0);
#endif

#if 0
	rc = 0;
	// The 'path' here is a SURL ...
	rc = Cns_getreplica (path, NULL, NULL, &nbentries, &lp);
	printf ("listing %s %d %d %x %s %s %s %s\n", path, nbentries, rc, lp, lp->poolname, lp->host, lp->fs, lp->sfn);
	exit (0);
#endif	
	
	//OPTcount = 5;
	gount = 0;
	printf ("Server, Fs: %s %s\n", server, fs);
	flags = CNS_LIST_BEGIN;
	while ((lp = Cns_listreplicax (poolname, server, fs, flags, &list)) != NULL) {
		if ( ! count-- )
		  break;
		flags = CNS_LIST_CONTINUE;
		file_uniqueid.fileid = lp->fileid;
		if (lp->status != '-') {	/* file is being populated/deleted */
		  /*
			printf ("%s is being %s\n", lp->sfn,
			    (lp->status == 'P') ? "populated" : "deleted");
		  */
			count++;
			rc = 1;
			continue;
		}
#if 0	       
		if (lp->ptime > time (0)) {	/* file is pinned */
		  //printf ("\n%s pinned until %s", lp->sfn, ctime (&lp->ptime));
			printf ("\n%s %c pinned until %s", lp->sfn, lp->f_type, ctime (&lp->ptime));
			count++;
			rc = 1;
			continue;
		}
#endif
		if (lp->f_type != 'Z') {
		  //listreplicad(lp->sfn);
			printf ("\nlistreplicating %s\n", lp->sfn);
			if ( gount++ < 0 ) {
			  printf ("skipping %s\n", lp->sfn);
			  count++;
			  rc = 1;
			  continue;
			}
		
			if (greplicate (lp->sfn, lp->f_type) < 0) {
			  /*
				fprintf (stderr, "dpm_replicate %s: %s\n", 
				    lp->sfn, sstrerror(serrno));
			  */
			  printf ("greplicating %s FAILED\n", lp->sfn);
			  count++;
				rc = 1;
				continue;
			}
		
		} else {
		  count++;
		}

#if 0
		//listreplicad(lp->sfn);
		printf ("deleting %s\n", lp->sfn);

		dpm_delreplica (lp->sfn);
		//listreplicad(lp->sfn);
#endif
		
	}
	(void) Cns_listreplicax (poolname, server, fs, CNS_LIST_END, &list);
	exit (rc);
}
