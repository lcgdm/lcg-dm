/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpmgopts.h,v 1.1 2005/01/20 09:47:19 grodid Exp $

// Created by GG (26/10/2004)

// gnu opts
#include "gop_comm.h"
#include "gop_verf.h"
#include "gop_synfo.h"
#include "gop_sig.h"
