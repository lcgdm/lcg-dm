/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-rm.c,v 1.4 2007/07/19 15:53:25 grodid Exp $

// Created by GG (22/10/2004)
// Modified by GG (08/07/2005)

/*	dpm_rm - to delete a set of files */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

	struct dpm_filestatus *filestatuses = NULL;
	int i = 0;
	int nbfiles;
	int nb_file_err = 0;
	int nbreplies;
	int status;

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s list-of-SURLs\n", argv[0]);
		exit (USERR);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//Pprintf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//Pprintf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    perror (" dpm-rm: at least one SURL must be provided ");
    exit(USERR);
  }
  nbfiles = nbargs;

	if ((status = dpm_rm (	   nbfiles, 
				   remargs,
				   &nbreplies, 
				   &filestatuses
				   )) < 0) {
		fprintf (stderr, "dpm-rm: %s\n", sstrerror (serrno));
		//fprintf (stderr, " dpm_rm: %s %d\n", sstrerror(serrno), serrno);
		nb_file_err++;
	}

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");

	for (i = 0; i < nbreplies; i++) {
		if ((filestatuses+i)->status) {
			fprintf (stderr, "dpm-rm: file %s: %s\n", (filestatuses+i)->surl,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring :
			    sstrerror ((filestatuses+i)->status & 0xFFF));
			nb_file_err++;
		}
		if ((filestatuses+i)->surl)
			free ((filestatuses+i)->surl);
		if ((filestatuses+i)->errstring)
			free ((filestatuses+i)->errstring);
	}

	if (filestatuses)
		free (filestatuses);

	exit (nb_file_err ? USERR : 0);

}
