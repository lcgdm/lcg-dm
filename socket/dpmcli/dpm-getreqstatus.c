/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm-getreqstatus.c,v 1.5 2007/07/19 15:53:24 grodid Exp $

// Created by GG (03/11/2004)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include "dpm_api.h"
#include "serrno.h"
#include "u64subr.h"
#define DEFPOLLINT 10

#include <signal.h>   /* catch signals */

// gnu opts
#include "dpmgopts.h"

#include "dpmutils.h"


//********************************************************//
int main(int argc, char *argv[])
{

  //static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
#include "dpmestat.h"
	struct dpm_putfilestatus *pfilestatuses;
	struct dpm_getfilestatus *gfilestatuses;
	struct dpm_reqsummary *summaries;
	int i, is;
	int nbfiles;
	//int nbprotocols;
	int nbreplies;
	int pnbreplies;
	int gnbreplies;
	//static char *protocols[] = {"rfio"};
	//int r = 0;
	//char r_token[CA_MAXDPMTOKENLEN+1];
	//struct dpm_putfilereq *reqfiles;
	int status;

/*  	int nbprotos; */
/*  	char **lprotos; */
	int nbsurls;
	//char **lsurls;
	char tmpbuf[21];
	char tmpbup[25];

	int thisarg;
	int nbargs;
	char **remargs;

#include "dpmgdebug.h"

	if (argc < 2) {
		fprintf (stderr, "usage: %s list-of-r_tokens\n", argv[0]);
		exit (1);
	}

	// gnu opts

	reset_global();  /* must be before parser */

	prog_gname = strdup(argv[0]);

	//printf (" before arg_parse \n");
	thisarg = arg_parse(argc, argv);
	//printf ("\n after arg_parse %s %d %d \n", argv[thisarg], argc, thisarg);

	remargs = NULL;
	nbargs = 0;
	if ( argc > thisarg ) {
	  remargs = realloc(remargs, sizeof(char*)*(argc-thisarg));
	}
	while ( thisarg < argc ) {
	  remargs[nbargs++] = strdup(argv[thisarg++]);
	}

  if ( ! nbargs ) {
    perror (" dpm-getreqstatus: at least one reqid_token must be provided ");
    exit(1);
  }
  nbfiles = nbargs;
  //printf( " Last arg: %d %s %s \n\n", nbargs, remargs[0], remargs[nbargs-1]);

	// Loading inputs

  printf (" \n");

  /*
#define V_LSTR(NAME, UNAME) \
  nb ## NAME = 0; \
  l ## NAME = NULL; \
  item_store(DPM_ ## UNAME, &nb ## NAME, &l ## NAME); \
  p_array( #UNAME , nb ## NAME, l ## NAME);

V_LSTR(protos, PROT)
  */

  // Setting defaults

/*    if ( ! dpm_rtoken ) { */
/*      perror (" Reqid token option is mandatory: --dpmrtoken=request_token "); */
/*      exit(1); */
/*    } */
 nbsurls = nbargs;

  printf (" \n");

	if ((status = dpm_getreqsummary (	   nbfiles, 
				   remargs,
				   &nbreplies, 
				   &summaries
				   )) < 0) {
	        sperror ("dpm_getreqsummary");
		printf ("==> Failure in dpm_getreqsummary called from %s\n", argv[0]);
		exit (1);
	}

	printf ("request state %s\n", status == DPM_SUCCESS ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);

        for (is = 0; is < nbreplies; is++) {
	  printf ("\nstate[%d] => r_token %s, r_type = %c, Q/P/F= %d/%d/%d \n", 
		  is,
		  (summaries+is)->r_token,
		  (summaries+is)->r_type,
		  (summaries+is)->nb_queued,
		  (summaries+is)->nb_progress,
		  (summaries+is)->nb_finished);

	  if ( (summaries+is)->r_type == 'P' ) {
	    // PUT state
	    pnbreplies = 0;
		if ((status = dpm_getstatus_putreq ((summaries+is)->r_token, 
						    0, 
						    NULL,
						    &pnbreplies, 
						    &pfilestatuses
						    )) < 0) {
			sperror ("dpm_getstatus_putreq");
			//exit (1);
			//P printf (" nbreplies: %d\n", pnbreplies);
			if ( ! pnbreplies )
			  continue;
		}
        for (i = 0; i < pnbreplies; i++) {
	  if ((pfilestatuses+i)->turl) {
		strncpy(&tmpbup[0], ctime(&((pfilestatuses+i)->pintime)), 24);
		tmpbup[24] = '\0';
                        printf ("PUT request, state[%d] = %s, SURL = %s, TURL = %s, fsz = %s, pin = %s \n", i,
                            f_stat[(pfilestatuses+i)->status >> 12],
				(pfilestatuses+i)->to_surl,
				(pfilestatuses+i)->turl,
				u64tostru((pfilestatuses+i)->filesize, tmpbuf, 0),
				//(pfilestatuses+i)->filesize,
				//ctime(&((pfilestatuses+i)->pintime)));
				tmpbup);
		//(pfilestatuses+i)->pintime);
	  } 
	        else if (((pfilestatuses+i)->status & DPM_FAILED) == DPM_FAILED) {
		  if ( (pfilestatuses+i)->to_surl )
                        printf ("state[%d] = %s, serrno = %d, SURL = %s, errstring = <%s>\n", i,
                            f_stat[(pfilestatuses+i)->status >> 12],
                            (pfilestatuses+i)->status & 0xFFF,
				(pfilestatuses+i)->to_surl,
                            (pfilestatuses+i)->errstring ? (pfilestatuses+i)->errstring : ""
				);
		  else
                        printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
                            f_stat[(pfilestatuses+i)->status >> 12],
                            (pfilestatuses+i)->status & 0xFFF,
                            (pfilestatuses+i)->errstring ? (pfilestatuses+i)->errstring : "");
		  
	        }
                else
                        printf ("state[%d] = %s\n", i,
                            f_stat[(pfilestatuses+i)->status >> 12]);
                if ((pfilestatuses+i)->to_surl)
                        free ((pfilestatuses+i)->to_surl);
                if ((pfilestatuses+i)->turl)
                        free ((pfilestatuses+i)->turl);
                if ((pfilestatuses+i)->errstring)
                        free ((pfilestatuses+i)->errstring);
        }
	free (pfilestatuses);
	  }
	  else if ( (summaries+is)->r_type == 'G' ) {
	    // GET state
	    gnbreplies = 0;
		if ((status = dpm_getstatus_getreq ((summaries+is)->r_token, 
						    0, 
						    NULL,
						    &gnbreplies, 
						    &gfilestatuses
						    )) < 0) {
			sperror ("dpm_getstatus_getreq");
			//exit (1);
			if ( ! gnbreplies )
			  continue;
		}
	for (i = 0; i < gnbreplies; i++) {
		if ((gfilestatuses+i)->status != DPM_READY)
			printf ("state[%d] = %d %x %s, SURL = %s, errstring = <%s>\n", i,
			    (gfilestatuses+i)->status,
			    (gfilestatuses+i)->status,
				f_stat[(gfilestatuses+i)->status >> 12],
				(gfilestatuses+i)->from_surl,
			    (gfilestatuses+i)->errstring);
		else {
#if 0
			printf ("GET request, state[%d] = %d, TURL = %s\n", i,
			    (gfilestatuses+i)->status,
			    (gfilestatuses+i)->turl);
#else
		u64tostru((gfilestatuses+i)->filesize, tmpbuf, 0);
		strncpy(&tmpbup[0], ctime(&((gfilestatuses+i)->pintime)), 24);
		tmpbup[24] = '\0';
                        printf ("GET request, state[%d] = %s, SURL = %s, TURL = %s, fsz = %s, pin = %s \n", i,
                            f_stat[(gfilestatuses+i)->status >> 12],
				(gfilestatuses+i)->from_surl,
				(gfilestatuses+i)->turl,
				tmpbuf,
				//(gfilestatuses+i)->filesize,
				//ctime(&((gfilestatuses+i)->pintime)));
				tmpbup);
#endif
		}
	}
	free (gfilestatuses);
	  }
	  else if ( (summaries+is)->r_type == 'C' ) {
	    // COPY state
	    printf ("COPY request, nothing implemented yet\n");
	  }
	  else {
	    // Undefined
	    printf ("UNKNOWN request, should not occur\n");
	  }
	}

	free (summaries);
	return 0;

}
