/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: dpm_utils.c,v 1.1 2005/01/20 09:47:20 grodid Exp $

/* Some common utils for the DPM CLI suite */
// Created by GG (19/10/2004)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

/* major/minor number */
#include <sys/sysmacros.h>

#include <signal.h>   /* catch signals */

#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

#include <regex.h>
#define DEFREGMAX  10

#if 0
// old opts
extern char* optarg;
extern int optind, opterr, optopt;
#else
// gnu opts
#include "dpmgopts.h"
#endif

#include "gop_alloc.h"

#include "dpmutils.h"

#ifndef SIGCHLD
#define SIGCHLD SIGCLD
#endif

/* just reports a crash */
//static void signal_handler(int signo){
void signal_handler(int signo){
  if(signo==SIGPIPE) _exit(0);  /* "ps | head" will cause this */
  /* fprintf() is not reentrant, but we _exit() anyway */
  fprintf(stderr,
    "\n\n"
	  //"Signal %d (%s) caught by dpm signal handler (%s).\n"
    "Signal %d (%s) caught by %s (%s).\n"
    "Please send bug reports to <grodid@mail.cern.ch>\n",
    signo,
    signal_number_to_name(signo),
    prog_gname,
    procps_version
  );
  _exit(signo+128);
}

//********************************************************//
//static void store_cmd(const char *s, int n, sel_union *data, int *nb, char **store){
void store_cmd(const char *s, int n, sel_union *data, int *nb, char **store){
  //printf("%s %d ", s, *nb);
  while(--n){
    store[*nb] = strdup(data[n].cmd);
    (*nb)++;
    //printf("%.8s,", data[n].cmd);
  }
  store[*nb] = strdup(data[0].cmd);
  //printf("%.8s\n", data[0].cmd);
  //printf(" V: %p %d %.8s\n", store, sizeof(*store), store[*nb]);
  (*nb)++;
}

//********************************************************//
//static void item_store(int typecsel, int *nb, char ***pstore){
void item_store(int typecsel, int *nb, char ***pstore){
  selection_node *walk = selection_list;
  while(walk){
    if ( walk->typecode == typecsel ) {
      //printf("typecode value agreed! %d nbprot: %d new: %d realloc: %d store: %p \n", walk->typecode, *nb, walk->n, sizeof(int)*((*nb)+walk->n), (*pstore));
      (*pstore) = realloc((*pstore), sizeof(int)*((*nb)+walk->n));
      store_cmd("GETn", walk->n, walk->u, nb, (*pstore));
      //} else {
      //printf("typecode value ignored! %d %d %p \n", walk->typecode, *nb, (*pstore));
    }
    walk = walk->next;
  }
}

//********************************************************//
//static void p_array(const char *title, int nl, char **files) {
void p_array(const char *title, int nl, char **files) {
  int ftest;
  for (ftest=0; ftest<nl; ftest++)
    printf ( " %s %d: %s \n", title, ftest, files[ftest] );
  printf (" \n");
}
