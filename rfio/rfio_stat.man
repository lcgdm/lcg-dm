.\"
.\" $Id: rfio_stat.man,v 1.1 2005/03/31 13:13:03 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_stat.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:03 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_STAT 3 "$Date: 2005/03/31 13:13:03 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_stat \- get information about a file or directory
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_stat (const char *" path ", struct stat *" statbuf ");"
.br
.BI "int rfio_fstat (int " s ", struct stat *" statbuf ");"
.br
.BI "int rfio_lstat (const char *" path ", struct stat *" statbuf ");"
.br
.BI "int rfio_mstat (const char *" path ", struct stat *" statbuf ");"
.br
.BI "int rfio_mstat_reset ();"
.br
.BI "int rfio_end ();"
.br
.br
.sp
Under Linux, for large files:
.br
.B #define _LARGEFILE64_SOURCE
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_stat64 (const char *" path ", struct stat64 *" statbuf ");"
.br
.BI "int rfio_fstat64 (int " s ", struct stat64 *" statbuf ");"
.br
.BI "int rfio_lstat64 (const char *" path ", struct stat64 *" statbuf ");"
.br
.BI "int rfio_mstat64 (const char *" path ", struct stat64 *" statbuf ");"
.sp
For large files, under other systems:
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_stat64 (const char *" path ", struct stat64 *" statbuf ");"
.br
.BI "int rfio_fstat64 (int " s ", struct stat64 *" statbuf ");"
.br
.BI "int rfio_lstat64 (const char *" path ", struct stat64 *" statbuf ");"
.br
.BI "int rfio_mstat64 (const char *" path ", struct stat64 *" statbuf ");"
.SH DESCRIPTION
.B rfio_stat
gets information about a file or directory.
.LP
.B rfio_lstat
is identical to
.B rfio_stat
except for symbolic links. In this case, the link itself is statted and not
followed.
.LP
.B rfio_fstat
is identical to
.B rfio_stat
but works on the file descriptor
.B s
returned by
.BR rfio_open .
.LP
.B rfio_mstat
is identical to
.B rfio_stat
but keeps the connection open to the server unless there are more than MAXMCON
connections already opened. This is useful when issuing a series of stat calls.
The last
.B rfio_mstat
call should be followed by a call to
.BR rfio_end .
.LP
.B rfio_mstat_reset
is to be used when your program is forking. In such a case the permanent connections opened with
.B rfio_mstat
become shared between the parent and the child. Use
.B rfio_mstat_reset
to perform the necessary reset and close of the socket file descriptor in the parent or the child in order to be sure that only of them will receice an answer from the RFIO daemon. 
.LP
See NOTES section below.
.TP
.I path
specifies the logical pathname relative to the current directory or
the full pathname.
.TP
.I statbuf
is a pointer to a
.B stat
structure, receiving result of your query.
.P
The 64 bits functions must be used for large files. They have the same syntax as the normal stat functions except that they use a 
.B stat64
structure. 
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH NOTES
Multiple connections using rfio_mstat are thread-safe but not process-wide, therefore a forked child can share file descriptors opened with rfio_mstat by its parent. Use
.B rfio_mstat_reset
in such case.
.P
Multiple connections behaviour is undefined if you work in a multi-threaded environment and with threads \fBnot\fP created using the CASTOR's \fBCthread\fP interface.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file/directory does not exist or is a null pathname.
.TP
.B EBADF
.I s
is not a valid file descriptor.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix.
.TP
.B EFAULT
.I path
or
.I statbuf
is a NULL pointer.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR rfio_chmod(3) ,
.BR rfio_chown(3) ,
.BR Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
