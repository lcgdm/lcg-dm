.\"
.\" $Id: rfio_open.man,v 1.1 2005/03/31 13:13:03 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_open.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:03 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_OPEN 3 "$Date: 2005/03/31 13:13:03 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_open \- opens a file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_open (const char *" path ", int " flags ", int " mode ");"
.br
.sp
Under Linux, for large files:
.br
.B #define _LARGEFILE64_SOURCE
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_open64 (const char *" path ", int " flags ", int " mode ");"
.sp
For large files, under other systems:
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_open64 (const char *" path ", int " flags ", int " mode ");"
.SH DESCRIPTION
.B rfio_open
opens the file whose name is the string pointed to by
.BR path ,
a character string containing the filename specification, and associates a FILE stream with it.
.br
.BI flags
are formed by ORing the following values:
.ft CW
.nf
.sp
        O_RDONLY        Open for reading only
        O_WRONLY        Open for writing only
        O_RDWR          Open for reading and writing
        O_NDELAY        Do not block on open
        O_APPEND        Append on each write
        O_CREAT         Create file if it does not exist
        O_TRUNC         Truncate size to 0
        O_EXCL          Error if create and file exists
        O_LARGEFILE     When size can be superior to 2GB\-1. 
                        See NOTES
.ft
.LP
.fi

.br
.BI mode
specifies the permission bits to be set if the file is created.
.P
Opening a file with O_APPEND set causes each write on the file to be appended to the end.  If O_TRUNC is specified and the file exists, the file is truncated to zero length.  If O_EXCL is set with O_CREAT, then if the file already exists, the open returns an error.  This can be used to implement a simple exclusive access locking mechanism.  If O_EXCL is set and the last component of the pathname is a symbolic link, the open will succeed even if the symbolic link points to an existing name.  If the O_NDELAY flag is specified and the open call would result in the process being blocked for some reason (for example waiting for a carrier on a dial-up line), the open returns immediately. The first time the process attempts to perform \IO\ on the open file, it will block (not currently implemented). On systems that support the Large Files, O_LARGEFILE in rfio_open allows files whose sizes cannot be represented in 31 bits to be opened.

.SH RETURN VALUE
If successful, returns a non-negative integer which may be used to refer to the
file on subsequent \fBrfio_write\fP, \fBrfio_read\fP and \fBrfio_close\fP calls.
On failure, \fBrfio_open\fP returns -1 and the error code is stored in serrno.

.SH ERRORS
.TP 1.3i
.B ENOENT
The named file/directory does not exist or is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix.
.TP
.B EBUSY
Device or resource busy. Happens if you try to open in write mode a CASTOR file that is in an active migration stream.
.TP
.B EFAULT
.I path
is a NULL pointer.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR Castor_limits(4) , rfio_write(3) , rfio_read(3) , rfio_close(3)
.SH NOTES
On Irix, Tru64 and IA64 the 64 bit mode is the default one, open and open64 are identical. The 
.I flag 
O_LARGEFILE is accepted on Irix and IA64 (where it has no effect) but not on Tru64.
On all other platforms, rfio_open with O_LARGEFILE is equivalent to a call to rfio_open64.
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>

