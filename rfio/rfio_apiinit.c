/*
 * Copyright (C) 2005 by CERN/IT/GD/CT
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: rfio_apiinit.c,v $ $Revision: 1.1 $ $Date: 2005/05/20 14:34:36 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	rfio_apiinit - allocate thread specific or global structures */

#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include "Cglobals.h"
#include "rfio_api.h"
#include "serrno.h"
static int rfio_api_key = -1;

int DLL_DECL
rfio_apiinit(thip)
struct rfio_api_thread_info **thip;
{
	Cglobals_get (&rfio_api_key,
	    (void **) thip, sizeof(struct rfio_api_thread_info));
	if (*thip == NULL) {
		serrno = ENOMEM;
		return (-1);
	}
	return (0);
}
