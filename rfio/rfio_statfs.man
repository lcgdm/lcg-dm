.\"
.\" $Id: rfio_statfs.man,v 1.2 2005/09/20 12:07:09 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_statfs.man,v $ $Revision: 1.2 $ $Date: 2005/09/20 12:07:09 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2005 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_STATFS 3 "$Date: 2005/09/20 12:07:09 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_statfs \- get information about a mounted filesystem
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_statfs (const char *" path ", struct rfstatfs *" statfsbuf ");"
.sp
Under Linux, for large files:
.br
.B #define _LARGEFILE64_SOURCE
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_statfs64 (const char *" path ", struct rfstatfs64 *" statfsbuf ");"
.sp
For large files, under other systems:
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_statfs64 (const char *" path ", struct rfstatfs64 *" statfsbuf ");"
.SH DESCRIPTION
.B rfio_statfs
gets information about a mounted filesystem.
.TP 0.8i
.I path
specifies the logical pathname relative to the current directory or
the full pathname of any file in this filesystem.
.TP
.I statfsbuf
is a pointer to a rfstatfs structure:
.nf
.ft CW
struct rfstatfs {
        long totblks  ;      /* Total number of blocks       */
        long freeblks ;      /* Number of free blocks        */
        long bsize    ;      /* Block size                   */
        long totnods  ;      /* Total number of inodes       */
        long freenods ;      /* Number of free inodes        */
};
struct rfstatfs64 {
        signed64 totblks  ;  /* Total number of blocks       */
        signed64 freeblks ;  /* Number of free blocks        */
        long bsize        ;  /* Block size                   */
        signed64 totnods  ;  /* Total number of inodes       */
        signed64 freenods ;  /* Number of free inodes        */
};
.ft
.fi
.P
The 64 bits function must be used for large filesystems. It has the same
syntax as the normal statfs function except that it uses a
.B rfstatfs64
structure.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file/directory does not exist or is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix.
.TP
.B EFAULT
.I path
or
.I statfsbuf
is a NULL pointer.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR Castor_limits(4)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
