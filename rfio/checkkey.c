/*
 * $Id$
 */

/*
 * Copyright (C) 1993-2010 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: checkkey.c,v $ $Revision$ $Date$ CERN/IT/PDP/DM Felix Hassine";
#endif /* not lint */

#include <stdio.h>
#include <osdep.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#endif
#include <log.h>
#include <string.h>
#include <errno.h>
#include <serrno.h>
#include <marshall.h>
#include <socket_timeout.h>
#include <Cnetdb.h>

#define RFIO2TPREAD_MAGIC 0X0110
#define OK 1

extern int (*recvfunc)();       /* Network receive function */
extern int (*sendfunc)();	/* Network send function */

extern int DLL_DECL isremote_sa _PROTO((struct sockaddr *, char *));

#define netread         (*recvfunc)
#define netwrite        (*sendfunc)

#ifndef RFIO_CTRL_TIMEOUT
#define RFIO_CTRL_TIMEOUT 10
#endif

int connecttpread(host,aport)
        char * host ;
	u_short aport ;
{
        struct sockaddr_storage sin ;           /* An Internet socket address.  */
        int                    sock ;           /* Socket descriptor.           */
        extern char      * getenv() ;           /* Getting env variables        */
        char                  * env ;           /* To store env variables       */
        struct addrinfo hints, *ai, *aitop ;    /* input/output from getaddrinfo*/
        int                gaierrno ;           /* return code from lookups     */
        char    strport[NI_MAXSERV] ;           /* service name or port number  */

	log(LOG_DEBUG,"Connecting tpread@%s to check key on port %d\n",host,aport);

        memset (&hints, '\0', sizeof(struct addrinfo));
        hints.ai_family = PF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
#ifdef AI_ADDRCONFIG
        hints.ai_flags |= AI_ADDRCONFIG;
#endif

        /*
         * Building Daemon Internet address.
         */
        if ( (env=getenv("RFIO2TPREAD")) == NULL ) 
                snprintf(strport, sizeof(strport), "%u", aport);
        else    {
                snprintf(strport, sizeof(strport), "%s", env);
        }

        gaierrno = Cgetaddrinfo(host, strport, &hints, &aitop);
        if (gaierrno != 0) {
                serrno = SENOSHOST;
                log(LOG_ERR,"Cgetaddrinfo(): returned %d, errno %d\n",gaierrno,errno);
                return -1;
        }

        sock = -1;
        errno = 0;
        for(ai=aitop; ai; ai=ai->ai_next) {
                /*
                 * Creating socket.
                 */
                if (( sock= socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol)) == -1 ) {
                        continue;
                }

                /*
                 * Connecting the socket.
                 */
                if ( connect(sock, ai->ai_addr, ai->ai_addrlen)  == -1 ) {
                        (void) close(sock);
                        sock = -1;
                        continue;
                } else {
                        memcpy(&sin, ai->ai_addr, ai->ai_addrlen);
                        break;
                }
        }
        freeaddrinfo(aitop);

        if (sock<0) {
                if (errno)
                        log(LOG_ERR,"Could not connect to %s: Last error during socket() or connect(): %s\n",
                               host,sstrerror(errno));
                else
                        log(LOG_ERR,"Could not connect to %s\n",host);

                return -1;
        }

	log(LOG_DEBUG,"Checking that key replier is in site\n");
	if ( isremote_sa((struct sockaddr *)&sin, host) ) {
		log(LOG_INFO,"Attempt to give key from outside site rejected\n");
		return  -1 ;
	}
        if ( setnetio(sock) == -1 ) {
                log(LOG_ERR,"error in setnetio()\n") ;
                (void) close(sock) ;
                return -1 ;
        }
        return sock ;
}

/* 
 * Returns 1 if key is valid, 0 otherwise.
 * returns -1 if failure
 */
int checkkey( sock, key )
int sock;
u_short  key;
{
	int rcode ;
	int magic ;
	int answer;
	char marsh_buf[64] ;
	char *ptr;
	ptr = marsh_buf ;

	marshall_LONG(ptr,RFIO2TPREAD_MAGIC);
	marshall_LONG(ptr,(LONG)key);
	marshall_LONG(ptr, 0);
        /*
         * Sending key.
         */
        if ( netwrite_timeout(sock,marsh_buf,3*LONGSIZE,RFIO_CTRL_TIMEOUT) != (3*LONGSIZE) ) {
                log(LOG_ERR,"netwrite(): %s\n", strerror(errno)) ;
                return -1 ;
        }
	/*
	 * Waiting for ok akn.
	 */
	if ( (rcode= netread_timeout(sock,marsh_buf,LONGSIZE*3,RFIO_CTRL_TIMEOUT)) != (LONGSIZE*3) ) {
                log(LOG_ERR,"netread(): %s\n",strerror(errno)) ;
                (void) close(sock) ;
                return -1 ;
	}
	ptr = marsh_buf ;
	if ( rcode == 0 ) {
                log(LOG_ERR,"connection closed by remote end\n") ;
                (void) close(sock) ;
                return -1 ;
        }
	unmarshall_LONG(ptr,magic);
	if ( magic != RFIO2TPREAD_MAGIC ) {
		log(LOG_ERR,"Magic inconsistency. \n");
		return -1 ;
	}
	unmarshall_LONG(ptr,answer);
	if ( answer==OK ) {
		log(LOG_DEBUG,"Key is correct.\n");
		return 1 ;
	}
	else
		return 0 ;
}

