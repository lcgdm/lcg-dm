.\"
.\" $Id$
.\"
.\" @(#)$RCSfile: rfstat.man $ $Revision$ $Date$ CERN IT-GT Jean-Philippe Baud
.\" Copyright (C) 1998-2011 by CERN/IT/GT
.\" All rights reserved
.\"
.TH RFSTAT 1 "$Date$" CASTOR "Rfio User Commands"
.SH NAME
rfstat \- get information about a file or directory
.SH SYNOPSIS
.B rfstat
.IR directory
.br
.B rfstat
.IR filename
.SH DESCRIPTION
.IX "\fLrfstat\fR"
The rfstat program provides an interface to the
.B shift
remote file I/O daemon (rfiod) for getting information about a remote directory
or file.
The
.IR filename
or
.IR directory
argument is either a remote file name of the form:
.IP
.IB hostname : path
.LP
or a local file name (not containing the :/ character combination).
The output from the 
.BR rfstat 
command gives information similar to that of the
.BR "ls -il"
command for listing local files or directories, one field per line.
.SH EXAMPLE
.nf
.ft CW
rfstat /tmp

Device          : 802
Inode number    : 2
Nb blocks       : 16
Protection      : drwxrwxrwt (41777)
Hard Links      : 13
Uid             : 0 (root)
Gid             : 0 (root)
Size (bytes)    : 4096
Last access     : Wed Jun 15 07:18:10 2011
Last modify     : Wed Jun 15 07:18:10 2011
Last stat. mod. : Wed Jun 15 07:18:10 2011
.ft
.fi
.SH "SEE ALSO"
.BR rfio_stat(3), 
.BR rfiod(1)
.SH "NOTES"
.B rfstat
does not support regular expressions
.BR (regexp(5)) 
in the
.IR directory
or
.IR filename
argument.
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
