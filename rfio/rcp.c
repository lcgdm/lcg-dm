/*
 * $Id$
 */

/*
 * Copyright (C) 2009-2010 by CERN/IT/GS
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: rcp.c $ $Revision$ $Date$ CERN/IT/GS Jean-Philippe Baud";
#endif /* not lint */

/* rfio_rcp.c       Remote File I/O - third party copy of a file        */

#define RFIO_KERNEL     1       /* KERNEL part of the routines          */
#include "rfio.h"               /* Remote File I/O general definitions  */

int  DLL_DECL rfio_rcp(source, destination, timeout)
char *source;
char *destination;
int timeout;
{
   char buf[BUFSIZ];	/* General input/output buffer */
   mode_t curmask;
   char *host;
   int len;
   char *p=buf;
   char *path;
   int rcode;
   int rpd;
   int rps;
   int rt;
   int s;		/* socket descriptor           */
   char server[CA_MAXHOSTNAMELEN+1];
   int status;		/* remote method status        */

   (void) umask(curmask=umask(0));
   INIT_TRACE("RFIO_TRACE");
   TRACE(1, "rfio", "rfio_rcp(%s, %s)", source, destination);

   *server = '\0';
   if ((rps = rfio_parse(source, &host, &path)) < 0) {
      END_TRACE();
      return(-1);
   }
   if (rps == 0 && host) {	/* sfn is currently not supported */
      serrno = SEOPNOTSUP;
      END_TRACE();
      return(-1);
   }
   if (rps)
      strcpy(server, host);

   if ((rpd = rfio_parse(destination, &host, &path)) < 0) {
      END_TRACE();
      return(-1);
   }
   if (rpd == 0 && host) {	/* sfn is currently not supported */
      serrno = SEOPNOTSUP;
      END_TRACE();
      return(-1);
   }
   if (rpd && ! *server)
      strcpy(server, host);

   if (! *server)
      gethostname(server, sizeof(server));

   s = rfio_connect(server,&rt);
   if (s < 0)      {
      END_TRACE();
      return(-1);
   }

   len = strlen(source) + strlen(destination) + 2;
   if ( RQSTSIZE+len > BUFSIZ ) {
     TRACE(2,"rfio","rfio_rcp: request too long %d (max %d)",
           RQSTSIZE+len,BUFSIZ);
     END_TRACE();
     (void) netclose(s);
     serrno = E2BIG;
     return(-1);
   }
   marshall_WORD(p, RFIO_MAGIC);
   marshall_WORD(p, RQST_RCP);
   marshall_WORD(p, geteuid());
   marshall_WORD(p, getegid());
   marshall_WORD(p, curmask);
   marshall_LONG(p, timeout);
   marshall_LONG(p, len);
   p= buf + RQSTSIZE;
   marshall_STRING(p, source);
   marshall_STRING(p, destination);
   TRACE(1,"rfio","rfio_rcp: source %s, destination %s", source, destination);
   TRACE(2,"rfio","rfio_rcp: sending %d bytes",RQSTSIZE+len) ;
   if (netwrite_timeout(s,buf,RQSTSIZE+len,RFIO_CTRL_TIMEOUT) != (RQSTSIZE+len)) {
      TRACE(2, "rfio", "rfio_rcp: write(): ERROR occured (errno=%d)", errno);
      (void) netclose(s);
      END_TRACE();
      return(-1);
   }
   p = buf;
   TRACE(2, "rfio", "rfio_rcp: reading %d bytes", LONGSIZE);
   if (netread_timeout(s, buf, 2 * LONGSIZE, timeout) != (2 * LONGSIZE))  {
      TRACE(2, "rfio", "rfio_rcp: read(): ERROR occured (errno=%d)", errno);
      (void) netclose(s);
      END_TRACE();
      return(-1);
   }
   unmarshall_LONG(p, status);
   unmarshall_LONG(p, rcode);
   TRACE(1, "rfio", "rfio_rcp: return %d",status);
   rfio_errno = rcode;
   (void) netclose(s);
   if (status)     {
      END_TRACE();
      return(-1);
   }
   END_TRACE();
   return (0);
}
