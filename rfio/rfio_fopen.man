.\"
.\" $Id: rfio_fopen.man,v 1.1 2005/03/31 13:13:02 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_fopen.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:02 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_FOPEN 3 "$Date: 2005/03/31 13:13:02 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_fopen \- opens a file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "FILE *rfio_fopen (char *" path ", char *" mode ");"
.br
.sp
Under Linux, for large files:
.br
.B #define _LARGEFILE64_SOURCE
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "FILE *rfio_fopen64 (char *" path ", char " *mode ");"
.sp
For large files, under other systems:
.br
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "FILE *rfio_fopen64 (char *" path ", char " *mode ");"
.SH DESCRIPTION
.B rfio_fopen
opens the file whose name is the string pointed to by
.BI path
, a character string containing the filename specification, and associates a FILE stream with it.
.P
.BI mode
is an access mode indicator. One of the characters "r" or "w".
.sp
.B rfio_fopen64
allows to open large files (see NOTES).
.SH RETURN VALUE
This routine returns NULL if the operation failed or a non-NULL pointer to a FILE structure if the operation was successful. If it fails, \fBserrno\fP variable is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file/directory does not exist or is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix.
.TP
.B EFAULT
path is NULL
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B EINVAL
The mode provided to local open is invalid (see \fBfopen\fP(3))
.TP
.B EMFILE
Too many open files
.TP
.B ECONNRESET
Connection reset by peer
.TP
.B ETIMEDOUT
Connection timed out
.TP
.B ECONNREFUSED
Connection refused
.TP
.B EHOSTUNREACH
No route to host
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SEBADVERSION
Version ID mismatch
.TP
.B SECOMERR
Communication error.
.TP
.B SERTYEXHAUST
Retry count exhausted
.TP
.B SENORCODE
Host did not return error number
.TP
.B SEHOSTREFUSED
Host is not on local network and no mapping found
.SH NOTES
On Irix, Tru64 and IA64 the 64 bit mode is the default one, rfio_fopen and rfio_fopen64 are identical.
.SH SEE ALSO
.BR fopen(3) ,
.BR rfio_serror(3) ,
.BR rfio_perror(3) ,
.BR serrno(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
