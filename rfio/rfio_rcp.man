.\"
.\" $Id$
.\"
.\" @(#)$RCSfile: rfio_rcp.man $ $Revision$ $Date$ CERN IT-GT/DMS Jean-Philippe Baud
.\" Copyright (C) 2009-2010 by CERN/IT/GT/DMS
.\" All rights reserved
.\"
.TH RFIO_RCP 3 "$Date$" CASTOR "Rfio Library Functions"
.SH NAME
rfio_rcp \- third party copy of a file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_rcp (char *" source ", char *" destination ", int " timeout ");"
.SH DESCRIPTION
.B rfio_rcp
triggers a third party copy of a file.
.TP
.I source
specifies the physical path of the source file.
.TP
.I destination
specifies the physical path of the file at destination.
.TP
.I timeout
specifies the time after which the copy is cancelled if not completed.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The source file does not exist or a component of
.I destination
prefix does not exist.
.TP
.B EACCES
Search permission is denied on a component of the
.I source
or
.I destination
prefix or read permission is not granted on the source file or write permission
on the destination parent directory is denied.
.TP
.B EFAULT
.I source
or
.I destination
is a NULL pointer.
.TP
.B ENOSPC
No space to store the new file copy.
.TP
.B ENAMETOOLONG
The length of
.I source
or
.I destination
exceeds
.B CA_MAXPATHLEN
or the length of a path component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SETIMEDOUT
Has timed out.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.B Castor_limits(4)
