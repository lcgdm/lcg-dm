.\"
.\" $Id: rfcp.man,v 1.1 2005/03/31 13:13:01 baud Exp $
.\"
.\" @(#)$RCSfile: rfcp.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:01 $ CERN IT-PDP/DM Olof Barring, Jean-Damien Durand
.\" Copyright (C) 1998-2011 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFCP 1 "$Date: 2005/03/31 13:13:01 $" CASTOR "Rfio User Commands"
.SH NAME
rfcp \- Remote file copy
.SH SYNOPSIS
.B rfcp
[
.BI -s " size"
] [
.BI -v2
]
.IR filename1
.IR filename2
.br
.B rfcp
[
.BI -s " size"
] [
.BI -v2
]
.IR filename
.IR directory
.br
.P
On Windows only:
.br
.B rfcp
[
.BI -a
] [
.BI -b
] [
.BI -s " size"
] [
.BI -v2
]
.BI @ command_file
.SH DESCRIPTION
.IX "\fLrfcp\fR"
The remote file I/O copy program provides an interface to the
.B shift
remote file I/O daemon (rfiod) for transferring files between remote and/or
local hosts. Each
.IR filename
or
.IR directory
argument is either a remote file name of the form:
.IP
.IB hostname : path
.LP
or a local file name (not containing the :/ character combination). The standard input is supported with the
.BI \-
character. Then directory in output is not supported.
.SH OPTIONS
.TP
.BI \-a
tells
.B rfcp
that the source file is ASCII text (Windows/NT only)
.TP
.BI \-b
tells
.B rfcp
that the source is a binary file (Windows/NT only)
.TP
.BI \-s " size"
If specified, only
.I size
bytes will be copied
.TP
.BI \-v2
If specified, forces the RFIO V.2 protocol. Otherwise, V.3 (streaming mode) is used.
.SH RETURN CODES
\
.br
0	Ok.
.br
1	Bad parameter.
.br
2	System error.
.br
3	Unknown error.
.br
16	Device or resource busy.
.br
28	No space left on device.
.br
196	Request killed.
.br
198	Stager not active.
.br
200	Bad checksum.

.SH SEE ALSO
.BR rcp(1), 
.BR rfiod(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
