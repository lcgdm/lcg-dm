/*
 * $Id$
 */

/*
 * Copyright (C) 1990-2009 by CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: setopt.c,v $ $Revision$ $Date$ CERN/IT/PDP/DM Felix Hassine";
#endif /* not lint */
#include <stdlib.h>
#define RFIO_KERNEL 1
#include "rfio.h"               /* remote file I/O definitions          */
#include "rfio_rfilefdt.h"

/*
 * By default RFIO are buffered. 
 */
static int rfio_opt= RFIO_READBUF ;
/*
 * By default rfio_connect()  reads NET entries 
 */
static int rfio_net= RFIO_NET ;
static int rfio_connretry = RFIO_RETRYIT ;
/*
 * Connect retry option
 */
static int rfio_connect_retry_counter = 0 ;
static int rfio_connect_retry_interval = 0 ;
/*
 * Force local I/O access. Required in some cases.
 */ 
static int rfio_forcelocal = RFIO_NOLOCAL ;

/*
 * User can set option through this function.
 */
int DLL_DECL rfiosetopt(opt,pval,len) 
	int	opt ; 
	int  * pval ;		
	int	len ;
{
	switch(opt) {
		case RFIO_READOPT:
			rfio_opt= *pval ;
			return 0 ; 
		case RFIO_NETOPT:
			rfio_net= *pval ;
			return 0 ;
		case RFIO_NETRETRYOPT:
			rfio_connretry= *pval ;
			return 0 ;
		case RFIO_CONNECTOPT:
			rfio_forcelocal= *pval ;
			return 0 ;
		case RFIO_CONNECT_RETRY_COUNT_OPT:
			rfio_connect_retry_counter = *pval ;
			return 0 ;
		case RFIO_CONNECT_RETRY_INT_OPT:
			rfio_connect_retry_interval = *pval ;
			return 0 ;
		default:
			errno= EINVAL ;
			return -1 ;
	}
}
/*
 * User can read an option through this function
 */

int	rfioreadopt(opt)
int 	opt ;
{
	switch(opt) {
		case RFIO_READOPT:
			return ( rfio_opt ) ;
		case RFIO_NETOPT:
			return ( rfio_net ) ;
		case RFIO_NETRETRYOPT:
			return ( rfio_connretry ) ;
		case RFIO_CONNECTOPT:
			return ( rfio_forcelocal ) ;
		case RFIO_CONNECT_RETRY_COUNT_OPT:
			return ( rfio_connect_retry_counter ) ;
		case RFIO_CONNECT_RETRY_INT_OPT:
			return ( rfio_connect_retry_interval ) ;
		default:
			errno= EINVAL ;
			return -1 ;
	}
}

int DLL_DECL rfio_setbufsize(s, bufsize)
int s;
int bufsize;
{
	char *p;
	int s_index;

	if ((s_index = rfio_rfilefdt_findentry(s,FINDRFILE_WITHOUT_SCAN)) == -1)
		return (0);	/* the file is local */
	if (rfilefdt[s_index]->version3 == 1)
		return (0);	/* no intermediate buffering */

	if (bufsize < 0) {
		serrno = EINVAL;
		return (-1);
	}
	if (rfilefdt[s_index]->offset ||
	    rfilefdt[s_index]->socset ||
	    rfilefdt[s_index]->preseek) {
		serrno = EINVAL;	/* cannot change buffer size once I/O has started */
		return (-1);
	}
	if (rfilefdt[s_index]->_iobuf.base) {
		if (bufsize == rfilefdt[s_index]->_iobuf.hsize + rfilefdt[s_index]->_iobuf.dsize)
			return (0);
		if (bufsize == 0) {
			free (rfilefdt[s_index]->_iobuf.base);
			memset (&rfilefdt[s_index]->_iobuf, 0, sizeof(struct iobuf));
		} else {
			p = realloc (rfilefdt[s_index]->_iobuf.base, bufsize);
			if (p == NULL) {
				serrno = errno;
				return (-1);
			}
			rfilefdt[s_index]->_iobuf.base = p;
			rfilefdt[s_index]->_iobuf.dsize = bufsize - rfilefdt[s_index]->_iobuf.hsize;
			rfilefdt[s_index]->_iobuf.ptr = iodata(rfilefdt[s_index]);
			rfilefdt[s_index]->_iobuf.count = 0;
		}
	} else {
		if (bufsize == 0)
			return (0);
		p = malloc (bufsize);
		if (p == NULL) {
			serrno = errno;
			return (-1);
		}
		rfilefdt[s_index]->_iobuf.base = p;
		rfilefdt[s_index]->_iobuf.hsize = 3 * LONGSIZE + WORDSIZE;
		rfilefdt[s_index]->_iobuf.dsize = bufsize - rfilefdt[s_index]->_iobuf.hsize;
		rfilefdt[s_index]->_iobuf.ptr = iodata(rfilefdt[s_index]);
		rfilefdt[s_index]->_iobuf.count = 0;
	}
	return (0);
}
