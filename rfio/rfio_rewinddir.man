.\"
.\" $Id: rfio_rewinddir.man,v 1.1 2005/03/31 13:13:03 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_rewinddir.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:03 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_REWINDDIR 3 "$Date: 2005/03/31 13:13:03 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_rewinddir \- reset position to the beginning of a directory opened by
.B rfio_opendir
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "void rfio_rewinddir (RDIR *" dirp ");"
.SH DESCRIPTION
.B rfio_rewinddir
resets the position to the beginning of a directory opened by
.BR rfio_opendir .
.TP
.I dirp
specifies the pointer value returned by
.BR rfio_opendir .
.SH NOTES
For CASTOR directories, a multi-threaded application will need to initialize itself the Cthread (CASTOR Thread Interface) library ; this is done by including "shift/Cthread_api.h" and calling the function Cthread_init() at the beginning. Otherwise accessing CASTOR directories will not be thread-safe. See \fBCthread\fP(3).
.SH SEE ALSO
.BR rfio_closedir(3) ,
.BR rfio_opendir(3) ,
.BR rfio_readdir(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
