/*
 * $Id: chkserv.c,v 1.1 2005/03/31 13:13:00 baud Exp $
 */

/*
 * Copyright (C) 1994-1999 CERN/IT/PDP/DM
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: chkserv.c,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:00 $ CERN/IT/PDP/DM Felix Hassine";
#endif /* not lint */

/* chkserv.c    Remote File I/O - check availability of daemon  */

#define RFIO_KERNEL     1
#include <rfio.h>

int rfio_chkserv (host)
char *host ;
{
	int rcode , rt;
	char *p ;

        rcode=RFIO_NONET ;
        rfiosetopt(RFIO_NETOPT, &rcode , 4);

	rcode = rfio_connect (host,&rt);
        if (rcode <0 ) 
		return rcode ;

	p = rfio_buf ;
        marshall_WORD(p, RFIO_MAGIC );
        marshall_WORD(p, RQST_CHKCON ) ;
	
	if (netwrite_timeout(rcode, rfio_buf, RQSTSIZE, RFIO_CTRL_TIMEOUT)!=  RQSTSIZE ) {
		INIT_TRACE("RFIO_TRACE");
		TRACE(2,"rfio","rfio_chkserv() : write() : ERROR occured (errno=%d)", errno) ;
		END_TRACE() ;
		return -1 ;
	}
	if ( netread_timeout( rcode , rfio_buf, LONGSIZE, RFIO_CTRL_TIMEOUT) != LONGSIZE ) {
		INIT_TRACE("RFIO_TRACE");
		TRACE(2,"rfio","rfio_chkserv() : read() : ERROR occured (errno=%d)", errno) ;
		END_TRACE() ;
                return -1 ;
        }

	(void) close(rcode) ;
	return 0 ;
}



