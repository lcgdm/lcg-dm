.\"
.\" $Id: rfio_fflush.man,v 1.1 2005/03/31 13:13:02 baud Exp $
.\"
.\" @(#)$RCSfile: rfio_fflush.man,v $ $Revision: 1.1 $ $Date: 2005/03/31 13:13:02 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_FFLUSH 3 "$Date: 2005/03/31 13:13:02 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_fflush \- flush a file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_fflush (FILE *" fp ");"
.SH DESCRIPTION
.B rfio_fflush
forces a write of all buffered data for the file whose descriptor \fBfp\fP is
the one returned by
.B rfio_fopen.
The file remains open.
.SH RETURN VALUE
This routine returns 0 if successful, -1 if the operation failed and
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EBADF
.I fp
is not a valid file descriptor.
.TP
.B SEBADVERSION
Version ID mismatch.
.SH SEE ALSO
.BR rfio_fopen(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
