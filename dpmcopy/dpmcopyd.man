.\" @(#)$RCSfile: dpmcopyd.man,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007-2011 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH DPMCOPYD 8 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dpmcopyd \- start the Disk Pool Manager Copy backend
.SH SYNOPSIS
.B dpmcopyd
[
.BI -c " config_file"
] [
.BI -l " log_file"
] [
.BI -4
|
.BI -6
]
.SH DESCRIPTION
.LP
The
.B dpmcopyd
command starts the Disk Pool Manager Copy backend.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
This will read the Disk Pool Manager "request" database configuration file and
look for requests.
Each of them is processed in a child process which opens a connection to the
database server if necessary.
.LP
All error messages and statistical information are kept in a log.
.LP
The maximum number of child processes is set by default to 20.
If there are more concurrent requests, they remain in the queue until a
worker process is available.
The maximum number of child processes can be changed by putting an entry in
.B /etc/shift.conf
for example:
.RS
.HP
DPMCOPYD	MAX_WORKERS	30
.RE
.LP
The Disk Pool Manager Copy backend listen port number can be defined on client
hosts and on the server itself in either of the following ways:
.RS
.LP
setting an environment variable DPMCOPYD_PORT
.RS
.HP
setenv DPMCOPYD_PORT 5017
.RE
.LP
an entry in
.B /etc/shift.conf
like:
.RS
.HP
DPMCOPYD	PORT	5017
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of DPMCOPYD_PORT in dpmcopy_constants.h.
.LP
The Disk Pool Manager Copy backend host name can be defined on client hosts
in either of the following ways:
.RS
.LP
setting an environment variable DPMCOPYD_HOST, for example:
.RS
.HP
setenv DPMCOPYD_HOST sehost
.RE
.LP
an entry in
.B /etc/shift.conf
for example:
.RS
.HP
DPMCOPYD	HOST	sehost
.RE
.RE
.LP
If none of these methods is used, the default host is taken from the
definition of DpmHost in site.def.
.LP
The Disk Pool Manager Copy backend "request" database keeps the requests and
their status even after completion.
.LP
The Disk Pool Manager configuration file contains password information for the
database and must be readable/writable only by root.
It contains a single line in the format:
.HP
.RS
username/password@server
.RE
or
.RS
username/password@server/dbname
.RE
.sp
where 'username' and 'password' are the credentials to login to the database
instance identified by 'server'. If 'dbname' is not specified, "dpm_db" is used.
.LP
In the log each entry has a timestamp.
All entries corresponding to one request have the same process id.
For each user command there is one message giving information about
the requestor DN and several lines when getting the TURLs for the
source/destination files and when doing the copy itself.
The completion code of the command is also logged.
.SH OPTIONS
.TP
.BI -c " config_file"
Specifies a different path for the Disk Pool Manager configuration file.
.TP
.BI -l " log_file"
Specifies a different path for the Disk Pool Manager log file.
The special value
.B syslog
will send the log messages to the system logger syslogd.
.TP
.BI -4
only try to listen on IPv4 addresses
.TP
.BI -6
only try to listen on IPv6 addresses
.SH FILES
.TP 1.8i
.B /etc/DPMCONFIG
configuration file
.TP
.B /var/log/dpmcopy/log
.SH EXAMPLES
.TP
Here is a small log:
.nf
03/27 09:34:30 29631 dpmcopyd: started
03/27 17:04:36 21024 dpmcopyd: inc_reqctr request from lxb6133v1.cern.ch
03/27 17:04:36 21024 dpmcopyd: decrementing reqctr
03/27 17:04:36 21129 dpmcopyd: processing request d3cc74d5-f9c0-4e21-83d2-a29fd8e692af from /DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=baud/CN=373165/CN=Jean-Philippe Baud
03/27 17:04:36 21129 dpmcopy_push: calling dpm_get
03/27 17:04:36 21129 dpmcopy_push: dpm_get returned request token 25bd8353-6ee1-4d60-b7cb-a5120dfa1069
03/27 17:04:37 21129 dpmcopy_push: calling srmv2_put
03/27 17:04:37 21129 dpmcopy_push: srmv2_put returned request token 85872006-11d6-404b-8ebe-0b9fd85031b8
03/27 17:04:42 21129 dpmcopy_push: copying gsiftp://lxb6133v1.cern.ch/lxb6133v1.cern.ch:/dpm_data/dteam/2008-03-27/jpbt1.1.0 to gsiftp://lxdpm102.cern.ch/lxdpm102.cern.ch:/data01/dteam/2008-03-27/jpbt9.478949.0
03/27 17:04:43 21129 dpmcopy_push: dpm_copyfile returned 0, serrno = 0
03/27 17:04:43 21129 dpmcopy_push: calling srmv2_putdone
03/27 17:04:44 21024 dpmcopyd: process 21129 exiting with status 0
.fi
.SH SEE ALSO
.B Clogit(3)
