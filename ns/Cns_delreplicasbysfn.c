/*
 * Copyright (C) 2009-2010 by CERN/IT/GS
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Cns_delreplicasbysfn.c,v $ $Revision$ $Date$ CERN IT-GS Jean-Philippe Baud";
#endif /* not lint */

/*	Cns_delreplicasbysfn - delete entries associated with a list of sfns 
	and corresponding lfn if last replica */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "marshall.h"
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"

int DLL_DECL
Cns_delreplicasbysfn(int nbfiles, const char **sfns, const char **guids, int *nbstatuses, int **statuses)
{
	int c;
	char func[21];
	gid_t gid;
	int i;
	int msglen;
	char *q;
	char *rbp;
	char repbuf[4];
	char *sbp;
	char *sendbuf;
	struct Cns_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "Cns_delreplicasbysfn");
	if (Cns_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		Cns_errmsg (func, NS053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (nbfiles <= 0) {
		serrno = EINVAL;
		return (-1);
	}
	if (! sfns || ! nbstatuses || ! statuses) {
		serrno = EFAULT;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 6 * LONGSIZE;
	for (i = 0; i < nbfiles; i++) {
		msglen += strlen (sfns[i]) + 1;
		if (guids)
			msglen += strlen (guids[i]) + 1;
		else
			msglen++;
	}

	/* Allocate send buffer */

	if ((sendbuf = malloc (msglen)) == NULL) {
		serrno = ENOMEM;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, CNS_MAGIC);
	marshall_LONG (sbp, CNS_DELREPBYSFN);
	q = sbp;        /* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */
 
	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_LONG (sbp, nbfiles);
	for (i = 0; i < nbfiles; i++) {
		marshall_STRING (sbp, sfns[i]);
		if (guids) {
			marshall_STRING (sbp, guids[i]);
		} else {
			marshall_STRING (sbp, "");
		}
	}
 
	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2nsdx (NULL, NULL, sendbuf,
	    msglen, repbuf, sizeof(repbuf), (void **)statuses, nbstatuses);
	free (sendbuf);

	if (c == 0) {
		rbp = repbuf;
		unmarshall_LONG (rbp, *nbstatuses);
		if (*nbstatuses == 0) {
			*statuses = NULL;
			return (0);
		}
	}
	return (c);
}
