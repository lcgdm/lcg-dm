.\" @(#)$RCSfile: Cns_setcomment.man,v $ $Revision$ $Date$ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 2000-2010 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH CNS_SETCOMMENT 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_setcomment \- add/replace a comment associated with a CASTOR file/directory in the name server
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_setcomment (const char *" path ,
.BI "char *" comment )
.SH DESCRIPTION
.B Cns_setcomment
adds/replaces a comment associated with a CASTOR file/directory in the
name server.
.TP
.I path
specifies the logical pathname relative to the current CASTOR directory or
the full CASTOR pathname.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named directory does not exist or is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix or the caller effective user ID does not match the owner ID of the file
or write permission on the file/directory itself is denied.
.TP
.B EFAULT
.I path
or
.I comment
is a NULL pointer.
.TP
.B EINVAL
The length of
.I comment
exceeds
.BR CA_MAXCOMMENTLEN .
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_chdir(3) ,
.B Cupvlist(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
