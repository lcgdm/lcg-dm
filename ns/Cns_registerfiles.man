.\" @(#)$RCSfile: Cns_registerfiles.man,v $ $Revision$ $Date$ CERN IT-GS Jean-Philippe Baud
.\" Copyright (C) 2009-2010 by CERN/IT/GS
.\" All rights reserved
.\"
.TH CNS_REGISTERFILES 3 "$Date$" CASTOR "Cns Library Functions"
.SH NAME
Cns_registerfiles \- register list of files with their corresponding replica entry
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_registerfiles (int " nbfiles ,
.BI "struct Cns_filereg *" files ,
.BI "int *" nbstatuses ,
.BI "int **" statuses )
.SH DESCRIPTION
.B Cns_registerfiles
registers a list of files with their corresponding replica entry.
If the lfn is already registered, the guid is optional and only the 
replica is added (after checking that filesize and possibly checksum match).
If the lfn is not registered yet, the guid is mandatory for the LFC.
The lfn and the replica get registered.
.TP
.I nbfiles
specifies the number of files in the array
.IR files .
.TP
.I files
is a pointer to an array of Cns_filereg structures provided by the application.
.PP
.nf
.ft CW
struct Cns_filereg {
	char		*lfn;
	char		*guid;
	mode_t		mode;
	u_signed64	size;
	char		*csumtype;
	char		*csumvalue;
	char		*server;
	char		*sfn;
};
.ft
.fi
.TP
.I nbstatuses
will be set to the number of replies in the array of statuses.
.TP
.I statuses
will be set to the address of an array of integer statuses allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
Individual statuses are 0 if the operation was successful or set to the
serrno value in case of error.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
A component of
.I lfn
prefix does not exist or
.I lfn
is a null pathname.
.TP
.B E2BIG
Request too large (max 1 MB).
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request or unmarshalling
the reply.
.TP
.B EACCES
Search permission is denied on a component of the
.I lfn
prefix or the file does not exist and write permission on the parent directory
is denied or write permission on the file itself is denied.
.TP
.B EFAULT
.IR files ,
.I nbstatuses
or
.I statuses
is a NULL pointer.
.TP
.B EEXIST
The
.I sfn
exists already.
.TP
.B ENOTDIR
A component of
.I lfn
prefix is not a directory.
.TP
.B EISDIR
The
.I lfn
exists already and is not a regular file.
.TP
.B EINVAL
.I nbfiles
is not strictly positive, the length of one of the
.I guids
exceeds
.B CA_MAXGUIDLEN
or the length of
.I server
exceeds
.B CA_MAXHOSTNAMELEN
or lfn and guid are both given and they point at a different file.
.TP
.B ENOSPC
The name server database is full.
.TP
.B ENAMETOOLONG
The length of
.I lfn
exceeds
.B CA_MAXPATHLEN
or the length of an
.I lfn
component exceeds
.BR CA_MAXNAMELEN
or the length of
.I sfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSSERV
Service unknown.
.TP 
.B SEINTERNAL 
Database error.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_addreplica(3) ,
.BR Cns_creat(3) ,
.BR Cns_setfsize(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
