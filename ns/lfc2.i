/*
 * @(#)$RCSfile: lfc2.i,v $ $Revision: 1.3 $ $Date: 2009/12/08 16:19:03 $ CERN IT-DM/SMD Remi Mollon
 */

/*********************************************
    SWIG input file for LFC
*********************************************/

%module lfc2

%{
#include "lfc_api.h"
%}

%include "lfc_api.h"
%include "Cns.i"

%{
static char serrbuf[ERRORLEN_MAX] = "";
%}

%init %{
    lfc_seterrbuf (serrbuf, ERRORLEN_MAX);
%}
