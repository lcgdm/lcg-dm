.\" @(#)$RCSfile: nsping.man,v $ $Revision: 1.1 $ $Date: 2007/05/09 06:38:31 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH NSPING 1 "$Date: 2007/05/09 06:38:31 $" CASTOR "Cns Administrator Commands"
.SH NAME
nsping \- check if name server is alive and print its version number
.SH SYNOPSIS
.B nsping
[
.BI -h " server"
]
.SH DESCRIPTION
.B nsping
checks if the name server is alive and prints its version number.
.SH OPTIONS
.TP
.BI -h " server"
specify the name server hostname.
This can also be set thru the CNS_HOST environment variable.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
