.\" @(#)$RCSfile: Cns_setratime.man,v $ $Revision: 1.1 $ $Date: 2005/04/27 09:11:24 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2005 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH CNS_SETRATIME 3 "$Date: 2005/04/27 09:11:24 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_setratime \- set replica last access (read) date
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_setratime (const char *" sfn )
.SH DESCRIPTION
.B Cns_setratime
set the replica last access (read) date to the current time and increments
the counter of accesses to the replica.
This function should be called everytime a file replica is accessed.
.TP
.I sfn
is the Physical File Name for the replica.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named replica does not exist.
.TP
.B EACCES
Search permission is denied on a component of the file prefix or
the caller effective user ID does not match the owner ID of the file
or read permission on the file itself is denied.
.TP
.B EFAULT
.I sfn
is a NULL pointer.
.TP
.B ENAMETOOLONG
The length of
.I sfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.B Cns_listreplica(3)
