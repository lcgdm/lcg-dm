/*
 * Copyright (C) 2007-2008 by CERN/IT/GD/ITR
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: nsping.c,v $ $Revision: 1.4 $ $Date: 2008/09/24 11:25:01 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	nsping - check name server alive and return version number */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#endif
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"
extern	char	*optarg;
extern	int	optind;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	int errflg = 0;
	char info[256];
	static char retryenv[16];
	char *server = NULL;
#if defined(_WIN32)
	WSADATA wsadata;
#endif

	while ((c = getopt (argc, argv, "h:")) != EOF) {
		switch (c) {
		case 'h':
			server = optarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (optind < argc) {
		errflg++;
	}
	if (errflg) {
		fprintf (stderr, "usage: %s [-h server]\n", argv[0]);
		exit (USERR);
	}

#if defined(_WIN32)             
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, NS052);
		exit (SYERR);
	}       
#endif
	sprintf (retryenv, "%s=0", CNS_CONRETRY_ENV);
	putenv (retryenv);
	if (Cns_ping (server, info) < 0) {
		fprintf (stderr, "nsping: %s\n", sstrerror(serrno));
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR);
	}
	printf ("%s\n", info);
#if defined(_WIN32)
		WSACleanup();
#endif
	exit (0);
}
