.\" @(#)$RCSfile: nsarguspoll.man$ $Revision$ $Date$ CERN IT-GT Jean-Philippe Baud
.\" Copyright (C) 2010-2011 by CERN/IT/GT
.\" All rights reserved
.\"
.TH NSARGUSPOLL 1 "$Date$" CASTOR "Cns Administrator Commands"
.SH NAME
nsarguspoll \- set banned status in the Virtual Id tables according to Argus information.
.SH SYNOPSIS
.B nsarguspoll
.I resourceid
.IR pep_url ...
.SH DESCRIPTION
.B nsarguspoll
sets the banned status in the Virtual Id tables according to the Argus information.
For every entry in the Virtual Id tables (DN or FQAN), the Argus Autorization
Service Policy Enforcement Point is contacted and the status is updated if needed.
.LP
If the PEP daemon returns XACML_DECISION_DENY, the banned field is set to ARGUS_BAN
and every subsequent request submitted with this DN or primary FQAN will fail
with "Permission denied". Banned secondary FQANs are not taken into account
when processing a request.
.LP
If the PEP daemon returns XACML_DECISION_PERMIT or XACML_DECISION_NOT_APPLICABLE,
the banned field is reset.
.TP
.I resourceid
specifies the XACML Resource identifier.
.TP
.I pep_url
specifies the endpoint URL for the PEP server to be contacted.
Several pep_urls can be specified for failover reason.
.LP
This command requires ADMIN privilege.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH EXAMPLE
nsarguspoll my_resource_id https://argus.example.org:8154/authz
.SH SEE ALSO
.BR nsmodifygrpmap(1) ,
.B nsmodifyusrmap(1)
