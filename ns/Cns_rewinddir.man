.\" @(#)$RCSfile: Cns_rewinddir.man,v $ $Revision: 1.1.1.1 $ $Date: 2001/10/04 12:12:49 $ CERN IT-PDP/DM Jean-Philippe Baud
.\" Copyright (C) 1999-2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH CNS_REWINDDIR 3 "$Date: 2001/10/04 12:12:49 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_rewinddir \- reset position to the beginning of a CASTOR directory opened by
.B Cns_opendir
in the name server
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "void Cns_rewinddir (Cns_DIR *" dirp )
.SH DESCRIPTION
.B Cns_rewinddir
resets the position to the beginning of a CASTOR directory opened by
.B Cns_opendir
in the name server.
.TP
.I dirp
specifies the pointer value returned by
.BR Cns_opendir .
.SH SEE ALSO
.BR Cns_closedir(3) ,
.BR Cns_opendir(3) ,
.BR Cns_readdir(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
