.\" @(#)$RCSfile: lfc_perl.man,v $ $Revision: 1.3 $ $Date: 2007/02/23 10:03:07 $ CERN IT-GD/CT David Smith
.\" Copyright (C) 2004-2005 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH lfc_perl 3 "$Date: 2007/02/23 10:03:07 $" LFC "Perl Programmers Reference Guide"
.SH NAME
lfc \- Perl interface to the LFC
.SH SYNOPSIS
.B use lfc;
.br
\fBprintf "CNS_LIST_BEGIN is %d\en", $lfc::CNS_LIST_BEGIN;\fP
.SH DESCRIPTION
The lfc module permits you to access the LFC client interface from perl
programs. The lfc module is a swig wrapping of the standard C interface.
For detailed descriptions of each function see the individual man page of
each function.

There follows a series of examples of how to use selected functions and how
to retrieve the information returned by them: Examples are finding the
GUID of an existing entry, listing the replicas of a given GUID and
setting and retrieving the comment associated with an entry.
.SH EXAMPLE
.nf
#!/usr/bin/perl -w
use strict;
use lfc;

# stat an existing entry in the LFC and print the GUID
my ($name,$stat,$guid,$res);
$name = "/grid/dteam/my.test";

$stat = lfcc::new_lfc_filestatg();
$res = lfc::lfc_statg($name,undef,$stat);

if ($res == 0) {
  $guid = lfcc::lfc_filestatg_guid_get($stat);
  print "The GUID for $name is $guid\en";
} else {
  my $err_num = $lfc::serrno;
  my $err_string = lfc::sstrerror($err_num);
  print "There was an error while looking for $name: Error $err_num ($err_string)\en";
  exit(1);
}
lfcc::delete_lfc_filestatg($stat);
.fi
.SH EXAMPLE
.nf
#!/usr/bin/perl -w
use strict;
use lfc;

# list the replicas of a given entry, starting from the GUID
my ($guid,$listp,$flag,$num_replicas);
$guid = "6a3164e0-a4d7-4abe-9f76-e3b8882735d1";

$listp = lfcc::new_lfc_list();
$flag =  $lfc::CNS_LIST_BEGIN;

print "Listing replicas for GUID $guid:\en";

$num_replicas=0;

while(1) {
  my $res = lfc::lfc_listreplica(undef,$guid,$flag,$listp);
  $flag = $lfc::CNS_LIST_CONTINUE;

  if (!defined $res) {
    last;
  } else {
    my $rep_name = lfcc::lfc_filereplica_sfn_get($res);
    print "Replica: $rep_name\en";
    $num_replicas++;
  }
}
lfc::lfc_listreplica(undef,$guid,$lfc::CNS_LIST_END,$listp);
lfcc::delete_lfc_list($listp);

print "Found $num_replicas replica(s)\en";
.fi
.SH EXAMPLE
.nf
#!/usr/bin/perl -w
use strict;
use lfc;

# setting and retrieving a comment on a file
my ($file,$res,$bufspec,$buffer,$comment);
$file = "/grid/dteam/my.test";

$comment = "MyComment";
$res = lfc::lfc_setcomment($file,$comment);

if ($res != 0) {
  my $err_num = $lfc::serrno;
  my $err_string = lfc::sstrerror($err_num);
  print "Problem while setting comment for $file: Error $err_num ($err_string)\en";
  exit(1);
}
  
$bufspec =  "x".($lfc::CA_MAXCOMMENTLEN+1);
$buffer = pack($bufspec);
$res = lfc::lfc_getcomment($file,$buffer);

if ($res != 0) {
  my $err_num = $lfc::serrno;
  my $err_string = lfc::sstrerror($err_num);
  print "Problem while reading the comment for $file: Error $err_num ($err_string)\en";
  exit(1);
}

$comment = unpack("Z*", $buffer);
print "Read back comment $comment\en";
.fi
.SH NOTES
The current interface to the \fBlfc_getcomment(3)\fP, \fBlfc_getcwd(3)\fP,
\fBlfc_readlink(3)\fP, \fBlfc_seterrbuf(3)\fP requires the passing of a suitably allocated
buffer (in a similar way to the C functions). However this is rather non standard
in PERL. A future version of lfc perl interface may do away with the need to setup the
buffer before the call and to explicitly unpack the result afterwards.
.SH SEE ALSO
.B LFC C interface man pages
