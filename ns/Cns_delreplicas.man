.\" @(#)$RCSfile: Cns_delreplicas.man,v $ $Revision: 1.1 $ $Date: 2007/12/13 11:59:47 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_DELREPLICAS 3 "$Date: 2007/12/13 11:59:47 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_delreplicas \- delete replica entries associated with a list of GUIDs and a particular se
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_delreplicas (int " nbguids ,
.BI "const char **" guids ,
.BI "char *" se ,
.BI "int *" nbstatuses ,
.BI "int **" statuses )
.SH DESCRIPTION
.B Cns_delreplicas
deletes replica entries associated with a list of GUIDs and a particular se.
.TP
.I nbguids
specifies the number of guids in the array
.IR guids .
.TP
.I guids
specifies the list of Grid Unique IDentifiers.
.TP
.I se
allows to restrict the replica entries to a given SE.
.TP
.I nbstatuses
will be set to the number of replies in the array of statuses.
.TP
.I statuses
will be set to the address of an array of integer statuses allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named guid does not exist.
.TP
.B EACCES
Search permission is denied on a component of the parent directory or
write permission is denied on the parent directory or
the parent has the sticky bit S_ISVTX set and
.RS 1.5i
.LP
the effective user ID of the requestor does not match the owner ID of the file and
.LP
the effective user ID of the requestor does not match the owner ID of the
directory and
.LP
the file is not writable by the requestor and
.LP
the requestor does not have ADMIN privilege in the Cupv database.
.RE
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request or unmarshalling
the reply.
.TP
.B EFAULT
.IR guids ,
.IR se ,
.I nbstatuses
or
.I statuses
is a NULL pointer.
.TP
.B EINVAL
.I nbguids
is not strictly positive, the length of one of the
.I guids
exceeds
.B CA_MAXGUIDLEN
or the length of
.I se
exceeds
.BR CA_MAXHOSTNAMELEN .
.TP
.B SENOSSERV
Service unknown.
.TP 
.B SEINTERNAL 
Database error.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_delreplica(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
