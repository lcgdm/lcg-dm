.\" @(#)$RCSfile: nsmodifyusrmap.man,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005-2010 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH NSMODIFYUSRMAP 1 "$Date$" CASTOR "Cns Administrator Commands"
.SH NAME
nsmodifyusrmap \- modify user entry corresponding to a given virtual uid
.SH SYNOPSIS
.B nsmodifyusrmap
.BI --uid " uid"
.BI --user " newname"
[
.BI --status " status"
]
.SH DESCRIPTION
.B nsmodifyusrmap
modifies the user entry corresponding to a given virtual uid.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.BI --uid " uid"
specifies the Virtual User Id.
.TP
.BI --user " newname"
specifies the new user name.
It must be at most 255 characters long.
.TP
.BI --status " status"
status can be set to 0 or a combination of ARGUS_BAN and LOCAL_BAN.
This can be either alphanumeric or the corresponding numeric value.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.B Cns_modifyusrmap(3)
