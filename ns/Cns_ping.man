.\" @(#)$RCSfile: Cns_ping.man,v $ $Revision: 1.1 $ $Date: 2007/05/09 06:38:31 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH CNS_PING 3 "$Date: 2007/05/09 06:38:31 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_ping \- check if name server is alive and return its version number
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_ping (char *" server ,
.BI "char *" info )
.SH DESCRIPTION
.B Cns_ping
checks if the name server is alive and returns its version number.
.SH OPTIONS
.TP
.I server
specifies the CASTOR Name Server hostname.
If NULL, the CNS_HOST environment variable is used.
.TP
.I info
points at a buffer to receive the information (currently the version number).
The buffer must be at least 256 characters long.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I info
is a NULL pointer.
.TP
.B SENOSHOST
Host unknown.
.TP 
.B SENOSSERV
Service unknown.
.TP 
.B SECOMERR
Communication error.
.TP 
.B ENSNACT
Name server is not running or is being shutdown.
