%{!?perl_vendorarch: %global perl_vendorarch %(eval "`%{__perl} -V:installvendorarch`"; echo $installvendorarch)}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; import sys; sys.stdout.write(get_python_lib(1))")}

%if %{?rhel}%{!?rhel:0} == 5
%global _with_swig %(echo "--py-dontrunswig")
%global altpython python26
%global __altpython %{_bindir}/python2.6
# Disable the default python byte compilation
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%endif

%if %{?fedora}%{!?fedora:0} >= 13
%global altpython python3
%global __altpython %{_bindir}/python3
%endif

%if %{?altpython:1}%{!?altpython:0}
%global altpython_sitearch %(%{__altpython} -c "from distutils.sysconfig import get_python_lib; import sys; sys.stdout.write(get_python_lib(1))")
%endif

%if %{?filter_setup:1}%{!?filter_setup:0}
%filter_provides_in %{_libdir}/lcgdm/.*\.so$
%filter_provides_in %{perl_vendorarch}/.*\.so$
%filter_provides_in %{python_sitearch}/.*\.so$
%if %{?altpython:1}%{!?altpython:0}
%filter_provides_in %{altpython_sitearch}/.*\.so$
%endif
%filter_setup
%endif

%if %{?fedora}%{!?fedora:0} >= 17 || %{?rhel}%{!?rhel:0} >= 7
%global systemd 1
%else
%global systemd 0
%endif

# by default, argus support should not be included
%{!?_with_argus: %{!?without_argus: %define _without_argus --without-argus}}
# make sure only one option is defined
%{?_with_argus: %{?_without_argus: %{error: both _with_argus and _without_argus}}}

Name:		lcgdm
Version:	1.13.0
Release:        1%{?dist}
Summary:	LHC Computing Grid Data Management

Group:		Applications/Internet
License:	ASL 2.0
URL:		http://glite.web.cern.ch/glite/
#		LANG=C svn co http://svnweb.cern.ch/guest/lcgdm/lcg-dm/tags/LCG-DM_R_1_9_2 lcgdm-1.9.2
#		tar --exclude .svn -z -c -f lcgdm-1.9.2.tar.gz lcgdm-1.9.2
Source0:	%{name}-%{version}.tar.gz
#		Systemd unit files
Source1:	%{name}-unitfiles.tar.gz
#		Remove deprecated python function:
#		https://savannah.cern.ch/bugs/?69232
Patch0:		%{name}-python-exception.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{?rhel}%{!?rhel:0} == 5
BuildRequires:	buildsys-macros
%endif
%if %{?fedora}%{!?fedora:0} >= 5 || %{?rhel}%{!?rhel:0} >= 5
BuildRequires:	imake
%else
BuildRequires:	xorg-x11-devel
%endif
BuildRequires:	globus-gssapi-gsi-devel%{?_isa}
BuildRequires:	globus-gss-assist-devel%{?_isa}
BuildRequires:	globus-gsi-credential-devel%{?_isa}
BuildRequires:	globus-gsi-callback-devel%{?_isa}
BuildRequires:	globus-gass-copy-devel%{?_isa}
BuildRequires:	globus-ftp-client-devel%{?_isa}
BuildRequires:	globus-common-devel%{?_isa}
BuildRequires:	voms-devel%{?_isa}
BuildRequires:	gsoap-devel%{?_isa}
BuildRequires:	CGSI-gSOAP-devel%{?_isa} >= 1.3.4.0
BuildRequires:	mysql-devel%{?_isa}
%if %{?fedora}%{!?fedora:0} >= 12 || %{?rhel}%{!?rhel:0} >= 6
BuildRequires:	libuuid-devel%{?_isa}
%else
BuildRequires:	e2fsprogs-devel%{?_isa}
%endif
BuildRequires:	swig
%if %{?fedora}%{!?fedora:0} >= 7 || %{?rhel}%{!?rhel:0} >= 6
BuildRequires:	perl-devel%{?_isa}
%else
BuildRequires:	perl
%endif
BuildRequires:	python-devel%{?_isa}
%if %{?altpython:1}%{!?altpython:0}
BuildRequires:	%{altpython}-devel%{?_isa}
%endif
%if %systemd
BuildRequires:	systemd-units
%endif

BuildRequires:	groff

# This cannot be executed in the case of pure EPEL builds
# as Argus is not available there
%{?_with_argus:BuildRequires:  argus-pep-api-c >= 2.2.0}
%{?_with_argus:BuildRequires:  argus-pep-api-c-devel >= 2.2.0}

%description
The lcgdm package provides the LCG Data Management components: the LFC
(LCG File Catalog) and the DPM (Disk Pool Manager).

%package libs
Summary:	LHC Computing Grid Data Management common libraries
Group:		System Environment/Libraries
Provides:	%{name} = %{version}-%{release}
Obsoletes:	%{name} < %{version}-%{release}

%description libs
The lcgdm-libs package contains common libraries for the LCG Data Management
components: the LFC (LCG File Catalog) and the DPM (Disk Pool Manager).

%package devel
Summary:	LCG Data Management common development files
Group:		Development/Libraries
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}
%if 0%{?el5}
%ifarch %{ix86}
Provides:   %{name}-devel(x86-32)
%endif
%ifnarch %{ix86} ppc ppc64 s390 s390x sparc sparc64
# why doesn't ifarch x86_64 work?
Provides:   %{name}-devel(x86-64)
%endif
%else
Provides:   %{name}-devel%{?_isa}
%endif

%description devel
This package contains common development libraries and header files
for LCG Data Management.

%package -n lfc-libs
Summary:	LCG File Catalog (LFC) libraries
Group:		System Environment/Libraries
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}
Obsoletes:	lfc < %{version}-%{release}

%description -n lfc-libs
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package contains the run time LFC client library.

%package -n lfc-devel
Summary:	LFC development libraries and header files
Group:		Development/Libraries
Requires:	lfc-libs%{?_isa} = %{version}-%{release}
Requires:	%{name}-devel%{?_isa} = %{version}-%{release}

%description -n lfc-devel
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package contains the development libraries and header files for LFC.

%package -n lfc
Summary:	LCG File Catalog (LFC) client
Group:		Applications/Internet
Requires:	lfc-libs%{?_isa} = %{version}-%{release}
Provides:	lfc-client = %{version}-%{release}
Obsoletes:	lfc-client < %{version}-%{release}

%description -n lfc
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package contains the command line interfaces for the LFC.

%package -n lfc-perl
Summary:	LCG File Catalog (LFC) perl bindings
Group:		Applications/Internet
Provides:	perl-lfc = %{version}-%{release}
Obsoletes:	perl-lfc <= %{version}-%{release}
Requires:	lfc-libs%{?_isa} = %{version}-%{release}
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description -n lfc-perl
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package provides Perl bindings for the LFC client library.

%package -n lfc-python
Summary:	LCG File Catalog (LFC) python bindings
Group:		Applications/Internet
Provides:	python-lfc = %{version}-%{release}
Obsoletes:	python-lfc <= %{version}-%{release}
Requires:	lfc-libs%{?_isa} = %{version}-%{release}

%description -n lfc-python
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package provides Python bindings for the LFC client library.

%if %{?altpython:1}%{!?altpython:0}
%package -n lfc-%{altpython}
Summary:	LCG File Catalog (LFC) python bindings
Group:		Applications/Internet
Requires:	lfc-libs%{?_isa} = %{version}-%{release}
%if %{?rhel}%{!?rhel:0} == 5
Requires:	python(abi) = 2.6
%endif
%if %{altpython} == python26
Provides:	python26-lfc
Obsoletes:	python26-lfc
%endif

%description -n lfc-%{altpython}
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package provides Python bindings for the LFC client library.
%endif

%package -n lfc-server-mysql
Summary:	LCG File Catalog (LFC) server with MySQL database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Provides:	lfcdaemon = %{version}-%{release}
Requires:	lfc-libs%{?_isa} = %{version}-%{release}
Provides:	lfc-mysql = %{version}-%{release}
Obsoletes:	lfc-mysql < %{version}-%{release}
Provides:	LFC-server-mysql = %{version}-%{release}
Obsoletes:	LFC-server-mysql < %{version}-%{release}

Requires(pre):		shadow-utils
Requires(post):		mysql
%if %systemd
Requires(post):		systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units
%else
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts
%endif

%description -n lfc-server-mysql
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package provides an LFC server that uses MySQL as its database
back-end.

%package -n lfc-dli
Summary:	LCG File Catalog (LFC) data location interface (dli) server
Group:		Applications/Internet
Requires:	lfcdaemon = %{version}-%{release}

Requires(pre):		shadow-utils
Requires(post):         finger
%if %systemd
Requires(post):		systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units
%else
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts
%endif

%description -n lfc-dli
The LCG File Catalog (LFC) keeps track of the locations of the physical
replicas of the logical files in a distributed storage system.
This package provides the data location interface (dli) server for the LFC.

%package -n dpm-libs
Summary:	Disk Pool Manager (DPM) libraries
Group:		System Environment/Libraries
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}
Obsoletes:	dpm < %{version}-%{release}

%description -n dpm-libs
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package contains the run time DPM client library.

%package -n dpm-devel
Summary:	DPM development libraries and header files
Group:		Development/Libraries
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
Requires:	%{name}-devel%{?_isa} = %{version}-%{release}

%description -n dpm-devel
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package contains the development libraries and header files for DPM.

%package -n dpm
Summary:	Disk Pool Manager (DPM) client
Group:		Applications/Internet
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
Provides:	dpm-client = %{version}-%{release}
Obsoletes:	dpm-client < %{version}-%{release}

%description -n dpm
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package contains the command line interfaces for the DPM.

%package -n dpm-perl
Summary:	Disk Pool Manager (DPM) perl bindings
Group:		Applications/Internet
Provides:	perl-dpm = %{version}-%{release}
Obsoletes:	perl-dpm < %{version}-%{release}
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
Requires:	perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description -n dpm-perl
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides Perl bindings for the DPM client library.

%package -n dpm-python
Summary:	Disk Pool Manager (DPM) python bindings
Group:		Applications/Internet
Provides:	python-dpm = %{version}-%{release}
Obsoletes:	python-dpm < %{version}-%{release}
Requires:	dpm-libs%{?_isa} = %{version}-%{release}

%description -n dpm-python
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides Python bindings for the DPM client library.

%if %{?altpython:1}%{!?altpython:0}
%package -n dpm-%{altpython}
Summary:	Disk Pool Manager (DPM) python bindings
Group:		Applications/Internet
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
%if %{?rhel}%{!?rhel:0} == 5
Requires:	python(abi) = 2.6
%endif
%if %{altpython} == python26
Provides:	python26-dpm
Obsoletes:	python26-dpm
%endif

%description -n dpm-%{altpython}
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides Python bindings for the DPM client library.
%endif

%package -n dpm-server-mysql
Summary:	Disk Pool Manager (DPM) server with MySQL database back-end
Group:		Applications/Internet
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
Requires:	finger%{?_isa}
Requires:	MySQL-python
Requires:	python-lxml
Requires:	python-uuid
Provides:	dpm-mysql = %{version}-%{release}
Obsoletes:	dpm-mysql < 1.8.1.2-2
Provides:	DPM-server-mysql = %{version}-%{release}
Obsoletes:	DPM-server-mysql < %{version}-%{release}

Requires(pre):		shadow-utils
%if %systemd
Requires(post):		systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units
%else
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts
%endif

%description -n dpm-server-mysql
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a DPM server that uses MySQL as its database
back-end.

%package -n dpm-name-server-mysql
Summary:	DPM name server with MySQL database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
Provides:	dpm-mysql-nameserver = %{version}-%{release}
Obsoletes:	dpm-mysql-nameserver < %{version}-%{release}
Provides:	DPM-name-server-mysql = %{version}-%{release}
Obsoletes:	DPM-name-server-mysql < %{version}-%{release}

Requires(pre):		shadow-utils
Requires(post):		mysql
%if %systemd
Requires(post):		systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units
%else
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts
%endif

%description -n dpm-name-server-mysql
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a DPM name server that uses MySQL as its database
back-end.

%package -n dpm-copy-server-mysql
Summary:	DPM copy server with MySQL database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
Provides:	dpm-mysql-copyd = %{version}-%{release}
Obsoletes:	dpm-mysql-copyd < %{version}-%{release}
Provides:	DPM-copy-server-mysql = %{version}-%{release}
Obsoletes:	DPM-copy-server-mysql < %{version}-%{release}

Requires(pre):		shadow-utils
%if %systemd
Requires(post):		systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units
%else
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts
%endif

%description -n dpm-copy-server-mysql
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a DPM copy server that uses MySQL as its
database back-end.

%package -n dpm-srm-server-mysql
Summary:	DPM SRM server with MySQL database back-end
Group:		Applications/Internet
Requires:	finger%{?_isa}
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
Provides:	dpm-mysql-srmv1 = %{version}-%{release}
Obsoletes:	dpm-mysql-srmv1 < %{version}-%{release}
Provides:	dpm-mysql-srmv2 = %{version}-%{release}
Obsoletes:	dpm-mysql-srmv2 < %{version}-%{release}
Provides:	dpm-mysql-srmv22 = %{version}-%{release}
Obsoletes:	dpm-mysql-srmv22 < %{version}-%{release}
Provides:	DPM-srm-server-mysql = %{version}-%{release}
Obsoletes:	DPM-srm-server-mysql < %{version}-%{release}

Requires(pre):		shadow-utils
%if %systemd
Requires(post):		systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units
%else
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts
%endif

%description -n dpm-srm-server-mysql
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a DPM SRM server that uses MySQL as its
database back-end.

%package -n dpm-rfio-server
Summary:	DPM RFIO server
Group:		Applications/Internet
Requires:	finger%{?_isa}
Requires:	dpm-libs%{?_isa} = %{version}-%{release}
Provides:	dpm-rfiod = %{version}-%{release}
Obsoletes:	dpm-rfiod < %{version}-%{release}
Provides:	DPM-rfio-server = %{version}-%{release}
Obsoletes:	DPM-rfio-server < %{version}-%{release}

%if %systemd
Requires(post):		systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units
%else
Requires(post):		chkconfig
Requires(preun):	chkconfig
Requires(preun):	initscripts
Requires(postun):	initscripts
%endif

%description -n dpm-rfio-server
The LCG Disk Pool Manager (DPM) creates a storage element from a set
of disks. It provides several interfaces for storing and retrieving
data such as RFIO and SRM version 1, version 2 and version 2.2.
This package provides a Remote File IO (RFIO) server for DPM.

%prep
%setup -T -q -c
%setup -q -c -n %{name}-%{version}/lfc-mysql
%setup -q -c -n %{name}-%{version}/dpm-mysql
%setup -D -T -q -a 1
%setup -D -T -q

for d in lfc-mysql dpm-mysql ; do

pushd $d/%{name}-%{version}

%patch0 -p1

chmod 644 security/globus_gsi_gss_constants.h \
	  security/globus_i_gsi_credential.h \
	  security/gssapi_openssl.h
chmod 644 doc/lfc/INSTALL-*

# The code violates the strict aliasing rules all over the place...
# Need to use -fnostrict-aliasing so that the -O2 optimization in
# optflags doesn't try to use them.
sed 's/^CC +=/& %{optflags} -fno-strict-aliasing -fno-tree-sink/' -i config/linux.cf
sed "s/i386/'i386'/" -i config/linux.cf

popd

done

%if %{?_with_argus:1}%{!?_with_argus:0}
%package -n dpm-argus
Summary:	DPM Argus support
Group:		Applications/Internet
Requires:	dpm-mysql-nameserver > 1.8.7
Provides:	dpm-argus = %{version}-%{release}

%description -n dpm-argus
Argus support for the Disk Pool Manager (DPM).
%endif

%build
gsoapversion=`soapcpp2 -v 2>&1 | grep C++ | sed 's/.* //'`

pushd lfc-mysql/%{name}-%{version}

./configure lfc %{?_with_swig} \
	%{?_without_argus} \
	--with-mysql \
	--libdir=%{_lib} \
	--with-gsoap-version=$gsoapversion \
	--with-id-map-file=%{_sysconfdir}/lcgdm-mapfile \
	--with-ns-config-file=%{_sysconfdir}/NSCONFIG \
	--with-etc-dir='$(prefix)/../etc' \
	--with-emi 

make -f Makefile.ini Makefiles

make %{?_smp_mflags} SOAPFLG="`pkg-config --cflags gsoap`"

popd

pushd dpm-mysql/%{name}-%{version}

./configure dpm \
	%{?_without_argus} \
	--with-mysql \
	--libdir=%{_lib} \
	--with-gsoap-version=$gsoapversion \
	--with-dpm-config-file=%{_sysconfdir}/DPMCONFIG \
	--with-id-map-file=%{_sysconfdir}/lcgdm-mapfile \
	--with-ns-config-file=%{_sysconfdir}/NSCONFIG \
	--with-etc-dir='$(prefix)/../etc' \
	--with-emi 

make -f Makefile.ini Makefiles

make %{?_smp_mflags} SOAPFLG="`pkg-config --cflags gsoap`"

popd

%if %{?altpython:1}%{!?altpython:0}
mkdir %{altpython}
pushd %{altpython}

INCLUDE_PYTHON=`%{__altpython} \
    -c "from distutils import sysconfig; \
	import sys; \
	sys.stdout.write('-I' + sysconfig.get_python_inc(0))"`
PYTHON_LIB=`%{__altpython} \
    -c "from distutils import sysconfig; \
	import sys; \
	sys.stdout.write('-L' + sysconfig.get_config_var('LIBDEST') + \
	'/config -lpython' + sys.version[:3] \
	   + sys.abiflags if hasattr(sys, 'abiflags') else '' \
	   + ' ' + \
	sysconfig.get_config_var('LIBS') + ' ' + \
	sysconfig.get_config_var('SYSLIBS'))"`
PYTHON_MODULE_SUFFIX=`%{__altpython} \
    -c "from distutils import sysconfig; \
	print(sysconfig.get_config_var('SO'))"`

for module in lfc lfcthr lfc2 lfc2thr ; do

gcc %{optflags} -fno-strict-aliasing -fPIC -D_LARGEFILE64_SOURCE -Dlinux \
    -c -pthread -DCTHREAD_LINUX -D_THREAD_SAFE -D_REENTRANT \
    -I../lfc-mysql/%{name}-%{version}/h -DNSTYPE_LFC \
    ${INCLUDE_PYTHON} ../lfc-mysql/%{name}-%{version}/ns/${module}_wrap.c
gcc %{optflags} -fno-strict-aliasing -fPIC -D_LARGEFILE64_SOURCE -Dlinux \
    -shared -o _${module}${PYTHON_MODULE_SUFFIX} ${module}_wrap.o ${PYTHON_LIB} \
    -L../lfc-mysql/%{name}-%{version}/shlib -llfc -llcgdm

done

for module in dpm dpm2 ; do

gcc %{optflags} -fno-strict-aliasing -fPIC -D_LARGEFILE64_SOURCE -Dlinux \
    -c -pthread -DCTHREAD_LINUX -D_THREAD_SAFE -D_REENTRANT \
    -I../dpm-mysql/%{name}-%{version}/h -DNSTYPE_DPNS \
    ${INCLUDE_PYTHON} ../dpm-mysql/%{name}-%{version}/dpm/${module}_wrap.c
gcc %{optflags} -fno-strict-aliasing -fPIC -D_LARGEFILE64_SOURCE -Dlinux \
    -shared -o _${module}${PYTHON_MODULE_SUFFIX} ${module}_wrap.o ${PYTHON_LIB} \
    -L../dpm-mysql/%{name}-%{version}/shlib -ldpm -llcgdm

done

popd
%endif

%install
rm -rf ${RPM_BUILD_ROOT}

%if %systemd
mkdir -p ${RPM_BUILD_ROOT}%{_unitdir}
%endif

pushd lfc-mysql/%{name}-%{version}

make SOAPFLG="`pkg-config --cflags gsoap`" \
     prefix=${RPM_BUILD_ROOT}%{_prefix} install install.man

mkdir -p ${RPM_BUILD_ROOT}%{_datadir}/lfc-mysql

# lfcdaemon unit file / startup script
%if %systemd
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/lfc-mysql/lfcdaemon.conf
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/lfc-mysql/lfcdaemon.init
install -m 644 -p ../../lcgdm-unitfiles/mysql/lfcdaemon.service \
    ${RPM_BUILD_ROOT}%{_datadir}/lfc-mysql
touch ${RPM_BUILD_ROOT}%{_unitdir}/lfcdaemon.service
%else
touch ${RPM_BUILD_ROOT}%{_initrddir}/lfcdaemon
chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/lfcdaemon
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/lfcdaemon
%endif

touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/lfcdaemon
touch ${RPM_BUILD_ROOT}%{_sbindir}/lfcdaemon
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/lfcdaemon
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/lfcdaemon.8
touch ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/NSCONFIG.templ
touch ${RPM_BUILD_ROOT}%{_sbindir}/lfc-shutdown
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/lfc-shutdown
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/lfc-shutdown.8

# lfc-dli unit file / startup script
%if %systemd
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/sysconfig/lfc-dli
rm ${RPM_BUILD_ROOT}/%{_initrddir}/lfc-dli
install -m 644 -p ../../lcgdm-unitfiles/lfc-dli.service \
    ${RPM_BUILD_ROOT}%{_unitdir}
%endif

# Remove static libraries
rm ${RPM_BUILD_ROOT}%{_libdir}/liblfc.a
rm ${RPM_BUILD_ROOT}%{_libdir}/liblcgdm.a

popd

pushd dpm-mysql/%{name}-%{version}

make SOAPFLG="`pkg-config --cflags gsoap`" \
     prefix=${RPM_BUILD_ROOT}%{_prefix} install install.man

mkdir -p ${RPM_BUILD_ROOT}%{_datadir}/dpm-mysql

# dpm unit file / startup script
%if %systemd
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/dpm-mysql/dpm.conf
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/dpm-mysql/dpm.init
install -m 644 -p ../../lcgdm-unitfiles/mysql/dpm.service \
    ${RPM_BUILD_ROOT}%{_datadir}/dpm-mysql
touch ${RPM_BUILD_ROOT}%{_unitdir}/dpm.service
%else
touch ${RPM_BUILD_ROOT}%{_initrddir}/dpm
chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/dpm
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/dpm
%endif

touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/dpm
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpm
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpm
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpm.8
touch ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/DPMCONFIG.templ
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpm-shutdown
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpm-shutdown
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpm-shutdown.8
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpm-buildfsv
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpm-buildfsv
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpm-buildfsv.8

# dpnsdaemon unit file / startup script
%if %systemd
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/dpm-mysql/dpnsdaemon.conf
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/dpm-mysql/dpnsdaemon.init
install -m 644 -p ../../lcgdm-unitfiles/mysql/dpnsdaemon.service \
    ${RPM_BUILD_ROOT}%{_datadir}/dpm-mysql
touch ${RPM_BUILD_ROOT}%{_unitdir}/dpnsdaemon.service
%else
touch ${RPM_BUILD_ROOT}%{_initrddir}/dpnsdaemon
chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/dpnsdaemon
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/dpnsdaemon
%endif
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/dpnsdaemon
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpnsdaemon
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpnsdaemon
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpnsdaemon.8
touch ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/NSCONFIG.templ
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpns-shutdown
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpns-shutdown
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpns-shutdown.8

# argus
%if %{?_with_argus:1}%{!?_with_argus:0}
touch ${RPM_BUILD_ROOT}%{_mandir}/man1/dpns-arguspoll.1
%endif

# dpmcopyd unit file / startup script
%if %systemd
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/dpm-mysql/dpmcopyd.conf
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/dpm-mysql/dpmcopyd.init
install -m 644 -p ../../lcgdm-unitfiles/mysql/dpmcopyd.service \
    ${RPM_BUILD_ROOT}%{_datadir}/dpm-mysql
touch ${RPM_BUILD_ROOT}%{_unitdir}/dpmcopyd.service
%else
touch ${RPM_BUILD_ROOT}%{_initrddir}/dpmcopyd
chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/dpmcopyd
touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/dpmcopyd
%endif

touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/dpmcopyd
touch ${RPM_BUILD_ROOT}%{_sbindir}/dpmcopyd
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/dpmcopyd
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/dpmcopyd.8

for svc in srmv1 srmv2 srmv2.2 ; do
    # unit file / startup script
%if %systemd
    rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/dpm-mysql/${svc}.conf
    rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/dpm-mysql/${svc}.init
    install -m 644 -p ../../lcgdm-unitfiles/mysql/${svc}.service \
	${RPM_BUILD_ROOT}%{_datadir}/dpm-mysql
    touch ${RPM_BUILD_ROOT}%{_unitdir}/${svc}.service
%else
  touch ${RPM_BUILD_ROOT}%{_initrddir}/${svc}
  chmod 755 ${RPM_BUILD_ROOT}%{_initrddir}/${svc}
  touch ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/${svc}
%endif

touch ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/${svc}
touch ${RPM_BUILD_ROOT}%{_sbindir}/${svc}
chmod 755 ${RPM_BUILD_ROOT}%{_sbindir}/${svc}
touch ${RPM_BUILD_ROOT}%{_mandir}/man8/${svc}.8
done

# dpm-rfiod unit file / startup script
%if %systemd
rm ${RPM_BUILD_ROOT}/%{_sysconfdir}/sysconfig/rfiod
rm ${RPM_BUILD_ROOT}/%{_initrddir}/rfiod
install -m 644 -p ../../lcgdm-unitfiles/rfiod.service \
    ${RPM_BUILD_ROOT}%{_unitdir}
%endif

# remove static library
rm ${RPM_BUILD_ROOT}%{_libdir}/libdpm.a
rm ${RPM_BUILD_ROOT}%{_libdir}/liblcgdm.a

popd

# remove the log man page (already exists in the system and not needed anyway)
rm ${RPM_BUILD_ROOT}%{_mandir}/man3/log.3*

%if %{?altpython:1}%{!?altpython:0}
mkdir -p ${RPM_BUILD_ROOT}%{altpython_sitearch}
install -m 644 lfc-mysql/%{name}-%{version}/ns/*.py \
	       dpm-mysql/%{name}-%{version}/dpm/*.py \
	       ${RPM_BUILD_ROOT}%{altpython_sitearch}
install %{altpython}/*.so ${RPM_BUILD_ROOT}%{altpython_sitearch}
%endif

%if %{?fedora}%{!?fedora:0} < 5 && %{?rhel}%{!?rhel:0} < 6
%{__python}    -c 'import compileall; compileall.compile_dir("'"$RPM_BUILD_ROOT"'", 10, "%{python_sitearch}", 1)' > /dev/null
%{__python} -O -c 'import compileall; compileall.compile_dir("'"$RPM_BUILD_ROOT"'", 10, "%{python_sitearch}", 1)' > /dev/null
%if %{?altpython:1}%{!?altpython:0}
%{__altpython}	  -c 'import compileall; compileall.compile_dir("'"$RPM_BUILD_ROOT%{altpython_sitearch}"'", 10, "%{altpython_sitearch}", 1)' > /dev/null
%{__altpython} -O -c 'import compileall; compileall.compile_dir("'"$RPM_BUILD_ROOT%{altpython_sitearch}"'", 10, "%{altpython_sitearch}", 1)' > /dev/null
%endif
%endif

# Add the upgrade scripts 
mkdir ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/upgrades
install -m 755 lfc-mysql/%{name}-%{version}/scripts/upgrades/*.pm ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/upgrades
install -m 755 lfc-mysql/%{name}-%{version}/scripts/upgrades/cns-db* ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/upgrades
install -m 755 lfc-mysql/%{name}-%{version}/scripts/upgrades/dpm-db* ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/upgrades

# Add the star accounting script
mkdir ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/scripts
install -m 755 lfc-mysql/%{name}-%{version}/scripts/StAR-accounting/star-accounting.py ${RPM_BUILD_ROOT}%{_datadir}/lcgdm/scripts


# Add the LFC info provider script
mkdir ${RPM_BUILD_ROOT}%{_libexecdir}
install -m 755 lfc-mysql/%{name}-%{version}/scripts/lcg-info-provider-lfc ${RPM_BUILD_ROOT}%{_libexecdir}

%clean
rm -rf ${RPM_BUILD_ROOT}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post -n lfc-libs -p /sbin/ldconfig

%postun -n lfc-libs -p /sbin/ldconfig

%post -n dpm-libs -p /sbin/ldconfig

%postun -n dpm-libs -p /sbin/ldconfig

%pre -n lfc-server-mysql
getent group lfcmgr > /dev/null || groupadd -r lfcmgr
getent passwd lfcmgr > /dev/null || useradd -r -g lfcmgr \
    -d %{_localstatedir}/lib/lfc -s /bin/bash -c "LFC Manager" lfcmgr
exit 0

%post -n lfc-server-mysql
if [ -e /etc/init.d/lfcdaemon -a ! -h /etc/init.d/lfcdaemon ]; then
	rm /etc/init.d/lfcdaemon
fi
%{_sbindir}/update-alternatives --install %{_sbindir}/lfcdaemon lfcdaemon \
	  %{_libdir}/lfc-mysql/lfcdaemon 20 \
  --slave %{_mandir}/man8/lfcdaemon.8.gz lfcdaemon.8.gz \
	  %{_libdir}/lfc-mysql/lfcdaemon.8.gz \
  --slave %{_datadir}/lcgdm/NSCONFIG.templ NSCONFIG.templ \
	  %{_libdir}/lfc-mysql/NSCONFIG.templ \
%if %systemd
  --slave %{_unitdir}/lfcdaemon.service lfcdaemon.service \
	  %{_datadir}/lfc-mysql/lfcdaemon.service \
%else
  --slave %{_initrddir}/lfcdaemon lfcdaemon.init \
	  %{_sysconfdir}/lfc-mysql/lfcdaemon.init \
  --slave %{_sysconfdir}/sysconfig/lfcdaemon lfcdaemon.conf \
	  %{_sysconfdir}/lfc-mysql/lfcdaemon.conf \
%endif
  --slave %{_sysconfdir}/logrotate.d/lfcdaemon lfcdaemon.logrotate \
	  %{_sysconfdir}/lfc-mysql/lfcdaemon.logrotate \
  --slave %{_sbindir}/lfc-shutdown lfc-shutdown \
	  %{_libdir}/lfc-mysql/lfc-shutdown \
  --slave %{_mandir}/man8/lfc-shutdown.8.gz lfc-shutdown.8.gz \
	  %{_libdir}/lfc-mysql/lfc-shutdown.8.gz

if [ $1 -eq 1 ]; then
%if %systemd
    /bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
    /sbin/chkconfig --add lfcdaemon
%endif
fi

%preun -n lfc-server-mysql
export LANG=C

if [ $1 -eq 0 ]; then
    if ( %{_sbindir}/update-alternatives --display lfcdaemon | \
	grep currently | grep -q lfc-mysql ) ; then
%if %systemd
	/bin/systemctl --no-reload disable lfcdaemon.service \
	    > /dev/null 2>&1 || :
	/bin/systemctl stop lfcdaemon.service > /dev/null 2>&1 || :
%else
	/sbin/service lfcdaemon stop > /dev/null 2>&1 || :
%endif
    fi
    %{_sbindir}/update-alternatives --remove lfcdaemon \
	%{_libdir}/lfc-mysql/lfcdaemon
%if %systemd == 0
    %{_sbindir}/update-alternatives --display lfcdaemon > /dev/null || \
	/sbin/chkconfig --del lfcdaemon > /dev/null 2>&1 || :
%endif
fi

%postun -n lfc-server-mysql
export LANG=C

if [ $1 -ge 1 ]; then
    if ( %{_sbindir}/update-alternatives --display lfcdaemon | \
	grep currently | grep -q lfc-mysql ) ; then
%if %systemd
	/bin/systemctl try-restart lfcdaemon.service > /dev/null 2>&1 || :
%else
	/sbin/service lfcdaemon condrestart > /dev/null 2>&1 || :
%endif
    fi
fi

%post -n lfc-dli
%if %systemd
# Clean up pre-systemd config
if [ -r %{_initrddir}/lfc-dli ] ; then
    /sbin/service lfc-dli stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del lfc-dli > /dev/null 2>&1 || :
fi
%endif

if [ $1 -eq 1 ]; then
%if %systemd
    /bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
    /sbin/chkconfig --add lfc-dli
%endif
fi

%preun -n lfc-dli
if [ $1 -eq 0 ]; then
%if %systemd
    /bin/systemctl --no-reload disable lfc-dli.service > /dev/null 2>&1 || :
    /bin/systemctl stop lfc-dli.service > /dev/null 2>&1 || :
%else
    /sbin/service lfc-dli stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del lfc-dli
%endif
fi

%postun -n lfc-dli
if [ $1 -ge 1 ]; then
%if %systemd
    /bin/systemctl try-restart lfc-dli.service > /dev/null 2>&1 || :
%else
    /sbin/service lfc-dli condrestart > /dev/null 2>&1 || :
%endif
fi

%post -n dpm-server-mysql
%if %systemd
# Clean up pre-systemd config
if [ -r %{_initrddir}/dpm ] ; then
    /sbin/service dpm stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del dpm > /dev/null 2>&1 || :
fi
%endif

%{_sbindir}/update-alternatives --install %{_sbindir}/dpm dpm \
	  %{_libdir}/dpm-mysql/dpm 20 \
  --slave %{_mandir}/man8/dpm.8.gz dpm.8.gz \
	  %{_libdir}/dpm-mysql/dpm.8.gz \
  --slave %{_datadir}/lcgdm/DPMCONFIG.templ DPMCONFIG.templ \
	  %{_libdir}/dpm-mysql/DPMCONFIG.templ \
%if %systemd
  --slave %{_unitdir}/dpm.service dpm.service \
	  %{_datadir}/dpm-mysql/dpm.service \
%else
  --slave %{_initrddir}/dpm dpm.init \
	  %{_sysconfdir}/dpm-mysql/dpm.init \
  --slave %{_sysconfdir}/sysconfig/dpm dpm.conf \
	  %{_sysconfdir}/dpm-mysql/dpm.conf \
%endif
  --slave %{_sysconfdir}/logrotate.d/dpm dpm.logrotate \
	  %{_sysconfdir}/dpm-mysql/dpm.logrotate \
  --slave %{_sbindir}/dpm-shutdown dpm-shutdown \
	  %{_libdir}/dpm-mysql/dpm-shutdown \
  --slave %{_mandir}/man8/dpm-shutdown.8.gz dpm-shutdown.8.gz \
	  %{_libdir}/dpm-mysql/dpm-shutdown.8.gz \
  --slave %{_sbindir}/dpm-buildfsv dpm-buildfsv \
	  %{_libdir}/dpm-mysql/dpm-buildfsv \
  --slave %{_mandir}/man8/dpm-buildfsv.8.gz dpm-buildfsv.8.gz \
	  %{_libdir}/dpm-mysql/dpm-buildfsv.8.gz

if [ $1 -eq 1 ]; then
%if %systemd
    /bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
    /sbin/chkconfig --add dpm
%endif
fi

%preun -n dpm-server-mysql
export LANG=C

if [ $1 -eq 0 ]; then
    if ( %{_sbindir}/update-alternatives --display dpm | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl --no-reload disable dpm.service \
	    > /dev/null 2>&1 || :
	/bin/systemctl stop dpm.service > /dev/null 2>&1 || :
%else
	/sbin/service dpm stop > /dev/null 2>&1 || :
%endif
    fi
    %{_sbindir}/update-alternatives --remove dpm \
	%{_libdir}/dpm-mysql/dpm
%if %systemd == 0
    %{_sbindir}/update-alternatives --display dpm > /dev/null || \
	/sbin/chkconfig --del dpm > /dev/null 2>&1 || :
%endif
fi

%postun -n dpm-server-mysql
export LANG=C

if [ $1 -ge 1 ]; then
    if ( %{_sbindir}/update-alternatives --display dpm | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl try-restart dpm.service > /dev/null 2>&1 || :
%else
	/sbin/service dpm condrestart > /dev/null 2>&1 || :
%endif
    fi
fi

%post -n dpm-name-server-mysql
%if %systemd
# Clean up pre-systemd config
if [ -r %{_initrddir}/dpnsdaemon ] ; then
    /sbin/service dpnsdaemon stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del dpnsdaemon > /dev/null 2>&1 || :
fi
%endif

%{_sbindir}/update-alternatives --install %{_sbindir}/dpnsdaemon dpnsdaemon \
	  %{_libdir}/dpm-mysql/dpnsdaemon 20 \
  --slave %{_mandir}/man8/dpnsdaemon.8.gz dpnsdaemon.8.gz \
	  %{_libdir}/dpm-mysql/dpnsdaemon.8.gz \
  --slave %{_datadir}/lcgdm/NSCONFIG.templ NSCONFIG.templ \
	  %{_libdir}/dpm-mysql/NSCONFIG.templ \
%if %systemd
  --slave %{_unitdir}/dpnsdaemon.service dpnsdaemon.service \
	  %{_datadir}/dpm-mysql/dpnsdaemon.service \
%else
  --slave %{_initrddir}/dpnsdaemon dpnsdaemon.init \
	  %{_sysconfdir}/dpm-mysql/dpnsdaemon.init \
  --slave %{_sysconfdir}/sysconfig/dpnsdaemon dpnsdaemon.conf \
	  %{_sysconfdir}/dpm-mysql/dpnsdaemon.conf \
%endif
  --slave %{_sysconfdir}/logrotate.d/dpnsdaemon dpnsdaemon.logrotate \
	  %{_sysconfdir}/dpm-mysql/dpnsdaemon.logrotate \
  --slave %{_sbindir}/dpns-shutdown dpns-shutdown \
	  %{_libdir}/dpm-mysql/dpns-shutdown \
  --slave %{_mandir}/man8/dpns-shutdown.8.gz dpns-shutdown.8.gz \
	  %{_libdir}/dpm-mysql/dpns-shutdown.8.gz

if [ $1 -eq 1 ]; then
%if %systemd
    /bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
    /sbin/chkconfig --add dpnsdaemon
%endif
fi

%preun -n dpm-name-server-mysql
export LANG=C

if [ $1 -eq 0 ]; then
    if ( %{_sbindir}/update-alternatives --display dpnsdaemon | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl --no-reload disable dpnsdaemon.service \
	    > /dev/null 2>&1 || :
	/bin/systemctl stop dpnsdaemon.service > /dev/null 2>&1 || :
%else
	/sbin/service dpnsdaemon stop > /dev/null 2>&1 || :
%endif
    fi
    %{_sbindir}/update-alternatives --remove dpnsdaemon \
	%{_libdir}/dpm-mysql/dpnsdaemon
%if %systemd == 0
    %{_sbindir}/update-alternatives --display dpnsdaemon > /dev/null || \
	/sbin/chkconfig --del dpnsdaemon > /dev/null 2>&1 || :
%endif
fi

%postun -n dpm-name-server-mysql
export LANG=C

if [ $1 -ge 1 ]; then
    if ( %{_sbindir}/update-alternatives --display dpnsdaemon | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl try-restart dpnsdaemon.service > /dev/null 2>&1 || :
%else
	/sbin/service dpnsdaemon condrestart > /dev/null 2>&1 || :
%endif
    fi
fi

%post -n dpm-copy-server-mysql
%if %systemd
# Clean up pre-systemd config
if [ -r %{_initrddir}/dpmcopyd ] ; then
    /sbin/service dpmcopyd stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del dpmcopyd > /dev/null 2>&1 || :
fi
%endif

%{_sbindir}/update-alternatives --install %{_sbindir}/dpmcopyd dpmcopyd \
	  %{_libdir}/dpm-mysql/dpmcopyd 20 \
  --slave %{_mandir}/man8/dpmcopyd.8.gz dpmcopyd.8.gz \
	  %{_libdir}/dpm-mysql/dpmcopyd.8.gz \
%if %systemd
  --slave %{_unitdir}/dpmcopyd.service dpmcopyd.service \
	  %{_datadir}/dpm-mysql/dpmcopyd.service \
%else
  --slave %{_initrddir}/dpmcopyd dpmcopyd.init \
	  %{_sysconfdir}/dpm-mysql/dpmcopyd.init \
  --slave %{_sysconfdir}/sysconfig/dpmcopyd dpmcopyd.conf \
	  %{_sysconfdir}/dpm-mysql/dpmcopyd.conf \
%endif
  --slave %{_sysconfdir}/logrotate.d/dpmcopyd dpmcopyd.logrotate \
	  %{_sysconfdir}/dpm-mysql/dpmcopyd.logrotate

if [ $1 -eq 1 ]; then
%if %systemd
    /bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
    /sbin/chkconfig --add dpmcopyd
%endif
fi

%preun -n dpm-copy-server-mysql
export LANG=C

if [ $1 -eq 0 ]; then
    if ( %{_sbindir}/update-alternatives --display dpmcopyd | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl --no-reload disable dpmcopyd.service \
	    > /dev/null 2>&1 || :
	/bin/systemctl stop dpmcopyd.service > /dev/null 2>&1 || :
%else
	/sbin/service dpmcopyd stop > /dev/null 2>&1 || :
%endif
    fi
    %{_sbindir}/update-alternatives --remove dpmcopyd \
	%{_libdir}/dpm-mysql/dpmcopyd
%if %systemd == 0
    %{_sbindir}/update-alternatives --display dpmcopyd > /dev/null || \
	/sbin/chkconfig --del dpmcopyd > /dev/null 2>&1 || :
%endif
fi

%postun -n dpm-copy-server-mysql
export LANG=C

if [ $1 -ge 1 ]; then
    if ( %{_sbindir}/update-alternatives --display dpmcopyd | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl try-restart dpmcopyd.service > /dev/null 2>&1 || :
%else
	/sbin/service dpmcopyd condrestart > /dev/null 2>&1 || :
%endif
    fi
fi

%post -n dpm-srm-server-mysql
%if %systemd
# Clean up pre-systemd config
if [ -r %{_initrddir}/srmv1 ] ; then
    /sbin/service srmv1 stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del srmv1 > /dev/null 2>&1 || :
fi
if [ -r %{_initrddir}/srmv2 ] ; then
    /sbin/service srmv2 stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del srmv2 > /dev/null 2>&1 || :
fi
if [ -r %{_initrddir}/srmv2.2 ] ; then
    /sbin/service srmv2.2 stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del srmv2.2 > /dev/null 2>&1 || :
fi
%endif

%{_sbindir}/update-alternatives --install %{_sbindir}/srmv1 srmv1 \
	  %{_libdir}/dpm-mysql/srmv1 20 \
  --slave %{_mandir}/man8/srmv1.8.gz srmv1.8.gz \
	  %{_libdir}/dpm-mysql/srmv1.8.gz \
%if %systemd
  --slave %{_unitdir}/srmv1.service srmv1.service \
	  %{_libdir}/dpm-mysql/srmv1.service \
%else
  --slave %{_initrddir}/srmv1 srmv1.init \
	  %{_sysconfdir}/dpm-mysql/srmv1.init \
  --slave %{_sysconfdir}/sysconfig/srmv1 srmv1.conf \
	  %{_sysconfdir}/dpm-mysql/srmv1.conf \
%endif
  --slave %{_sysconfdir}/logrotate.d/srmv1 srmv1.logrotate \
	  %{_sysconfdir}/dpm-mysql/srmv1.logrotate

%{_sbindir}/update-alternatives --install %{_sbindir}/srmv2 srmv2 \
	  %{_libdir}/dpm-mysql/srmv2 20 \
  --slave %{_mandir}/man8/srmv2.8.gz srmv2.8.gz \
	  %{_libdir}/dpm-mysql/srmv2.8.gz \
%if %systemd
  --slave %{_unitdir}/srmv2.service srmv2.service \
	  %{_datadir}/dpm-mysql/dpm-srmv2.service \
%else
  --slave %{_initrddir}/srmv2 srmv2.init \
	  %{_sysconfdir}/dpm-mysql/srmv2.init \
  --slave %{_sysconfdir}/sysconfig/srmv2 srmv2.conf \
	  %{_sysconfdir}/dpm-mysql/srmv2.conf \
%endif
  --slave %{_sysconfdir}/logrotate.d/srmv2 srmv2.logrotate \
	  %{_sysconfdir}/dpm-mysql/srmv2.logrotate

%{_sbindir}/update-alternatives --install %{_sbindir}/srmv2.2 srmv2.2 \
	  %{_libdir}/dpm-mysql/srmv2.2 20 \
  --slave %{_mandir}/man8/srmv2.2.8.gz srmv2.2.8.gz \
	  %{_libdir}/dpm-mysql/srmv2.2.8.gz \
%if %systemd
  --slave %{_unitdir}/srmv2.2.service srmv2.2.service \
	  %{_datadir}/dpm-mysql/srmv2.2.service \
%else
  --slave %{_initrddir}/srmv2.2 srmv2.2.init \
	  %{_sysconfdir}/dpm-mysql/srmv2.2.init \
  --slave %{_sysconfdir}/sysconfig/srmv2.2 srmv2.2.conf \
	  %{_sysconfdir}/dpm-mysql/srmv2.2.conf \
%endif
  --slave %{_sysconfdir}/logrotate.d/srmv2.2 srmv2.2.logrotate \
	  %{_sysconfdir}/dpm-mysql/srmv2.2.logrotate

if [ $1 -eq 1 ]; then
%if %systemd
    /bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
    /sbin/chkconfig --add srmv1
    /sbin/chkconfig --add srmv2
    /sbin/chkconfig --add srmv2.2
%endif
fi

%preun -n dpm-srm-server-mysql
export LANG=C

if [ $1 -eq 0 ]; then
    if ( %{_sbindir}/update-alternatives --display srmv1 | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl --no-reload disable srmv1.service \
	    > /dev/null 2>&1 || :
	/bin/systemctl stop srmv1.service > /dev/null 2>&1 || :
%else
	/sbin/service srmv1 stop > /dev/null 2>&1 || :
%endif
    fi
    %{_sbindir}/update-alternatives --remove srmv1 \
	%{_libdir}/dpm-mysql/srmv1
%if %systemd == 0
    %{_sbindir}/update-alternatives --display srmv1 > /dev/null || \
	/sbin/chkconfig --del srmv1 > /dev/null 2>&1 || :
%endif

    if ( %{_sbindir}/update-alternatives --display srmv2 | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl --no-reload disable srmv2.service \
	    > /dev/null 2>&1 || :
	/bin/systemctl stop srmv2.service > /dev/null 2>&1 || :
%else
	/sbin/service srmv2 stop > /dev/null 2>&1 || :
%endif
    fi
    %{_sbindir}/update-alternatives --remove srmv2 \
	%{_libdir}/dpm-mysql/srmv2
%if %systemd == 0
    %{_sbindir}/update-alternatives --display srmv2 > /dev/null || \
	/sbin/chkconfig --del srmv2 > /dev/null 2>&1 || :
%endif

    if ( %{_sbindir}/update-alternatives --display srmv2.2 | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl --no-reload disable srmv2.2.service \
	    > /dev/null 2>&1 || :
	/bin/systemctl stop srmv2.2.service > /dev/null 2>&1 || :
%else
	/sbin/service srmv2.2 stop > /dev/null 2>&1 || :
%endif
    fi
    %{_sbindir}/update-alternatives --remove srmv2.2 \
	%{_libdir}/dpm-mysql/srmv2.2
%if %systemd == 0
    %{_sbindir}/update-alternatives --display srmv2.2 > /dev/null || \
	/sbin/chkconfig --del srmv2.2 > /dev/null 2>&1 || :
%endif
fi

%postun -n dpm-srm-server-mysql
export LANG=C

if [ $1 -ge 1 ]; then
    if ( %{_sbindir}/update-alternatives --display srmv1 | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl try-restart srmv1.service > /dev/null 2>&1 || :
%else
	/sbin/service srmv1 condrestart > /dev/null 2>&1 || :
%endif
    fi

    if ( %{_sbindir}/update-alternatives --display srmv2 | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl try-restart srmv2.service > /dev/null 2>&1 || :
%else
	/sbin/service srmv2 condrestart > /dev/null 2>&1 || :
%endif
    fi

    if ( %{_sbindir}/update-alternatives --display srmv2.2 | \
	grep currently | grep -q dpm-mysql ) ; then
%if %systemd
	/bin/systemctl try-restart srmv2.2.service > /dev/null 2>&1 || :
%else
	/sbin/service srmv2.2 condrestart > /dev/null 2>&1 || :
%endif
    fi
fi

%post -n dpm-rfio-server
%if %systemd
# Clean up pre-systemd config
if [ -r %{_initrddir}/rfiod ] ; then
    /sbin/service rfiod stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del rfiod > /dev/null 2>&1 || :
fi
%endif

if [ $1 -eq 1 ]; then
%if %systemd
    /bin/systemctl daemon-reload > /dev/null 2>&1 || :
%else
    /sbin/chkconfig --add rfiod
%endif
fi

%preun -n dpm-rfio-server
if [ $1 -eq 0 ]; then
%if %systemd
    /bin/systemctl --no-reload disable rfiod.service > /dev/null 2>&1 || :
    /bin/systemctl stop rfiod.service > /dev/null 2>&1 || :
%else
    /sbin/service rfiod stop > /dev/null 2>&1 || :
    /sbin/chkconfig --del rfiod
%endif
fi

%postun -n dpm-rfio-server
if [ $1 -ge 1 ]; then
%if %systemd
    /bin/systemctl try-restart rfiod.service > /dev/null 2>&1 || :
%else
    /sbin/service rfiod condrestart > /dev/null 2>&1 || :
%endif
fi

%files libs
%defattr(-,root,root,-)
%{_libdir}/liblcgdm.so.*
%{_libdir}/libCsec_plugin_GSI.so
%{_libdir}/libCsec_plugin_ID.so
%dir %{_datadir}/lcgdm/upgrades
%{_datadir}/lcgdm/upgrades/Common.pm
%doc lfc-mysql/%{name}-%{version}/README lfc-mysql/%{name}-%{version}/LICENSE

%files devel
%defattr(-,root,root,-)
%{_libdir}/liblcgdm.so
%doc %{_mandir}/man3/C*.3*
%doc %{_mandir}/man3/getconfent.3*
%doc %{_mandir}/man3/netclose.3*
%doc %{_mandir}/man3/netread.3*
%doc %{_mandir}/man3/netwrite.3*
%doc %{_mandir}/man3/serrno.3*
%doc %{_mandir}/man4/Castor_limits.4*

%files -n lfc-libs
%defattr(-,root,root,-)
%{_libdir}/liblfc.so.*

%files -n lfc-devel
%defattr(-,root,root,-)
%{_includedir}/lfc
%{_libdir}/liblfc.so
%doc %{_mandir}/man3/lfc_[a-o]*.3*
%doc %{_mandir}/man3/lfc_ping.3*
%doc %{_mandir}/man3/lfc_[q-z]*.3*

%files -n lfc
%defattr(-,root,root,-)
%{_bindir}/lfc-*
%doc %{_mandir}/man1/lfc-*
%{_libexecdir}/lcg-info-provider-lfc

%files -n lfc-perl
%defattr(-,root,root,-)
%{perl_vendorarch}/lfc.so
%{perl_vendorarch}/lfc.pm
%doc %{_mandir}/man3/lfc_perl.3*

%files -n lfc-python
%defattr(-,root,root,-)
%{python_sitearch}/_lfc.so
%{python_sitearch}/lfc.py*
%{python_sitearch}/_lfcthr.so
%{python_sitearch}/lfcthr.py*
%{python_sitearch}/_lfc2.so
%{python_sitearch}/lfc2.py*
%{python_sitearch}/_lfc2thr.so
%{python_sitearch}/lfc2thr.py*
%doc %{_mandir}/man3/lfc_python.3*
%doc %{_mandir}/man3/lfc2_python.3*

%if %{?altpython:1}%{!?altpython:0}
%files -n lfc-%{altpython}
%defattr(-,root,root,-)
%{altpython_sitearch}/_lfc.*so
%{altpython_sitearch}/lfc.py*
%{altpython_sitearch}/_lfcthr.*so
%{altpython_sitearch}/lfcthr.py*
%{altpython_sitearch}/_lfc2.*so
%{altpython_sitearch}/lfc2.py*
%{altpython_sitearch}/_lfc2thr.*so
%{altpython_sitearch}/lfc2thr.py*
%if %{?fedora}%{!?fedora:0} >= 15
%{altpython_sitearch}/__pycache__/lfc*
%endif
%endif

%files -n lfc-server-mysql
%defattr(-,root,root,-)
%dir %{_libdir}/lfc-mysql
%{_libdir}/lfc-mysql/lfcdaemon
%ghost %{_sbindir}/lfcdaemon
%{_libdir}/lfc-mysql/lfc-shutdown
%ghost %{_sbindir}/lfc-shutdown
%{_libdir}/lfc-mysql/NSCONFIG.templ
%ghost %{_datadir}/lcgdm/NSCONFIG.templ
%doc %{_libdir}/lfc-mysql/lfcdaemon.8*
%ghost %{_mandir}/man8/lfcdaemon.8*
%doc %{_libdir}/lfc-mysql/lfc-shutdown.8*
%ghost %{_mandir}/man8/lfc-shutdown.8*
%dir %{_sysconfdir}/lfc-mysql
%if %systemd
%dir %{_datadir}/lfc-mysql
%{_datadir}/lfc-mysql/lfcdaemon.service
%ghost %{_unitdir}/lfcdaemon.service
%else
%{_sysconfdir}/lfc-mysql/lfcdaemon.init
%ghost %{_initrddir}/lfcdaemon
%config(noreplace) %{_sysconfdir}/lfc-mysql/lfcdaemon.conf
%ghost %{_sysconfdir}/sysconfig/lfcdaemon
%endif
%config(noreplace) %{_sysconfdir}/lfc-mysql/lfcdaemon.logrotate
%ghost %{_sysconfdir}/logrotate.d/lfcdaemon
%doc %{_datadir}/lcgdm/create_lfc_tables_mysql.sql
%{_datadir}/lcgdm/upgrades/cns-db-*

%files -n lfc-dli
%defattr(-,root,root,-)
%{_sbindir}/lfc-dli
%doc %{_mandir}/man8/lfc-dli.8*
%if %systemd
%{_unitdir}/lfc-dli.service
%else
%{_initrddir}/lfc-dli
%config(noreplace) %{_sysconfdir}/sysconfig/lfc-dli
%endif
%config(noreplace) %{_sysconfdir}/logrotate.d/lfc-dli

%files -n dpm-libs
%defattr(-,root,root,-)
%{_libdir}/libdpm.so.*

%files -n dpm-devel
%defattr(-,root,root,-)
%{_includedir}/dpm
%{_libdir}/libdpm.so
%doc %{_mandir}/man3/dpm_[a-o]*.3*
%doc %{_mandir}/man3/dpm_ping.3*
%doc %{_mandir}/man3/dpm_put.3*
%doc %{_mandir}/man3/dpm_putx.3*
%doc %{_mandir}/man3/dpm_putdone.3*
%doc %{_mandir}/man3/dpm_[q-z]*.3*
%doc %{_mandir}/man3/dpns_*.3*
%doc %{_mandir}/man3/rfio*.3*

%files -n dpm
%defattr(-,root,root,-)
%{_bindir}/dpm-[a-k]*
%{_bindir}/dpm-[m-z]*
%{_bindir}/dpns-*
%{_bindir}/rf*
%doc %{_mandir}/man1/dpm*
%doc %{_mandir}/man1/dpm-[a-k]*
%doc %{_mandir}/man1/dpm-[m-z]*
%doc %{_mandir}/man1/dpns-*
%doc %{_mandir}/man1/rf*

%files -n dpm-perl
%defattr(-,root,root,-)
%{perl_vendorarch}/dpm.so
%{perl_vendorarch}/dpm.pm

%files -n dpm-python
%defattr(-,root,root,-)
%{_bindir}/dpm-listspaces
%{python_sitearch}/_dpm.so
%{python_sitearch}/dpm.py*
%{python_sitearch}/_dpm2.so
%{python_sitearch}/dpm2.py*
%doc %{_mandir}/man1/dpm-listspaces.1*
%doc %{_mandir}/man3/dpm_python.3*
%doc %{_mandir}/man3/dpm2_python.3*

%if %{?altpython:1}%{!?altpython:0}
%files -n dpm-%{altpython}
%defattr(-,root,root,-)
%{altpython_sitearch}/_dpm.*so
%{altpython_sitearch}/dpm.py*
%{altpython_sitearch}/_dpm2.*so
%{altpython_sitearch}/dpm2.py*
%if %{?fedora}%{!?fedora:0} >= 15
%{altpython_sitearch}/__pycache__/dpm*
%endif
%endif

%files -n dpm-server-mysql
%defattr(-,root,root,-)
%dir %{_libdir}/dpm-mysql
%{_libdir}/dpm-mysql/dpm
%ghost %{_sbindir}/dpm
%{_libdir}/dpm-mysql/dpm-shutdown
%ghost %{_sbindir}/dpm-shutdown
%{_libdir}/dpm-mysql/dpm-buildfsv
%ghost %{_sbindir}/dpm-buildfsv
%doc %{_libdir}/dpm-mysql/dpm.8*
%ghost %{_mandir}/man8/dpm.8*
%doc %{_libdir}/dpm-mysql/dpm-shutdown.8*
%ghost %{_mandir}/man8/dpm-shutdown.8*
%doc %{_libdir}/dpm-mysql/dpm-buildfsv.8*
%ghost %{_mandir}/man8/dpm-buildfsv.8*
%{_libdir}/dpm-mysql/DPMCONFIG.templ
%ghost %{_datadir}/lcgdm/DPMCONFIG.templ
%dir %{_sysconfdir}/dpm-mysql
%if %systemd
%dir %{_datadir}/dpm-mysql
%{_datadir}/dpm-mysql/dpm.service
%ghost %{_unitdir}/dpm.service
%else
%{_sysconfdir}/dpm-mysql/dpm.init
%ghost %{_initrddir}/dpm
%config(noreplace) %{_sysconfdir}/dpm-mysql/dpm.conf
%ghost %{_sysconfdir}/sysconfig/dpm
%endif
%config(noreplace) %{_sysconfdir}/dpm-mysql/dpm.logrotate
%ghost %{_sysconfdir}/logrotate.d/dpm
%{_datadir}/lcgdm/create_dpm_tables_mysql.sql
%{_datadir}/lcgdm/upgrades/dpm-db-*
%{_datadir}/lcgdm/scripts/*

%files -n dpm-name-server-mysql
%defattr(-,root,root,-)
%{_libdir}/dpm-mysql/dpnsdaemon
%ghost %{_sbindir}/dpnsdaemon
%{_libdir}/dpm-mysql/dpns-shutdown
%ghost %{_sbindir}/dpns-shutdown
%doc %{_libdir}/dpm-mysql/dpnsdaemon.8*
%ghost %{_mandir}/man8/dpnsdaemon.8*
%doc %{_libdir}/dpm-mysql/dpns-shutdown.8*
%ghost %{_mandir}/man8/dpns-shutdown.8*
%{_libdir}/dpm-mysql/NSCONFIG.templ
%ghost %{_datadir}/lcgdm/NSCONFIG.templ
%if %systemd
%dir %{_datadir}/dpm-mysql
%{_datadir}/dpm-mysql/dpnsdaemon.service
%ghost %{_unitdir}/dpnsdaemon.service
%else
%{_sysconfdir}/dpm-mysql/dpnsdaemon.init
%ghost %{_initrddir}/dpnsdaemon
%config(noreplace) %{_sysconfdir}/dpm-mysql/dpnsdaemon.conf
%ghost %{_sysconfdir}/sysconfig/dpnsdaemon
%endif
%config(noreplace) %{_sysconfdir}/dpm-mysql/dpnsdaemon.logrotate
%ghost %{_sysconfdir}/logrotate.d/dpnsdaemon
%{_datadir}/lcgdm/create_dpns_tables_mysql.sql
%{_datadir}/lcgdm/upgrades/cns-db-*

%files -n dpm-copy-server-mysql
%defattr(-,root,root,-)
%{_libdir}/dpm-mysql/dpmcopyd
%ghost %{_sbindir}/dpmcopyd
%doc %{_libdir}/dpm-mysql/dpmcopyd.8*
%ghost %{_mandir}/man8/dpmcopyd.8*
%if %systemd
%dir %{_datadir}/dpm-mysql
%{_datadir}/dpm-mysql/dpmcopyd.service
%ghost %{_unitdir}/dpmcopyd.service
%else
%{_sysconfdir}/dpm-mysql/dpmcopyd.init
%ghost %{_initrddir}/dpmcopyd
%config(noreplace) %{_sysconfdir}/dpm-mysql/dpmcopyd.conf
%ghost %{_sysconfdir}/sysconfig/dpmcopyd
%endif
%config(noreplace) %{_sysconfdir}/dpm-mysql/dpmcopyd.logrotate
%ghost %{_sysconfdir}/logrotate.d/dpmcopyd

%files -n dpm-srm-server-mysql
%defattr(-,root,root,-)
%{_libdir}/dpm-mysql/srmv1
%{_libdir}/dpm-mysql/srmv2
%{_libdir}/dpm-mysql/srmv2.2
%ghost %{_sbindir}/srmv1
%ghost %{_sbindir}/srmv2
%ghost %{_sbindir}/srmv2.2
%doc %{_libdir}/dpm-mysql/srmv1.8*
%doc %{_libdir}/dpm-mysql/srmv2.8*
%doc %{_libdir}/dpm-mysql/srmv2.2.8*
%ghost %{_mandir}/man8/srmv1.8*
%ghost %{_mandir}/man8/srmv2.8*
%ghost %{_mandir}/man8/srmv2.2.8*
%if %systemd
%dir %{_datadir}/dpm-mysql
%{_datadir}/dpm-mysql/srmv1.service
%{_datadir}/dpm-mysql/srmv2.service
%{_datadir}/dpm-mysql/srmv2.2.service
%ghost %{_unitdir}/srmv1.service
%ghost %{_unitdir}/srmv2.service
%ghost %{_unitdir}/srmv2.2.service
%else
%{_sysconfdir}/dpm-mysql/srmv1.init
%{_sysconfdir}/dpm-mysql/srmv2.init
%{_sysconfdir}/dpm-mysql/srmv2.2.init
%ghost %{_initrddir}/srmv1
%ghost %{_initrddir}/srmv2
%ghost %{_initrddir}/srmv2.2
%config(noreplace) %{_sysconfdir}/dpm-mysql/srmv1.conf
%config(noreplace) %{_sysconfdir}/dpm-mysql/srmv2.conf
%config(noreplace) %{_sysconfdir}/dpm-mysql/srmv2.2.conf
%ghost %{_sysconfdir}/sysconfig/srmv1
%ghost %{_sysconfdir}/sysconfig/srmv2
%ghost %{_sysconfdir}/sysconfig/srmv2.2
%endif
%config(noreplace) %{_sysconfdir}/dpm-mysql/srmv1.logrotate
%config(noreplace) %{_sysconfdir}/dpm-mysql/srmv2.logrotate
%config(noreplace) %{_sysconfdir}/dpm-mysql/srmv2.2.logrotate
%ghost %{_sysconfdir}/logrotate.d/srmv1
%ghost %{_sysconfdir}/logrotate.d/srmv2
%ghost %{_sysconfdir}/logrotate.d/srmv2.2

%files -n dpm-rfio-server
%defattr(-,root,root,-)
%{_sbindir}/rfiod
%if %systemd
%{_unitdir}/rfiod.service
%else
%{_initrddir}/rfiod
%config(noreplace) %{_sysconfdir}/sysconfig/rfiod
%endif
%config(noreplace) %{_sysconfdir}/logrotate.d/rfiod
%doc %{_mandir}/man8/rfiod.8*

%if %{?_with_argus:1}%{!?_with_argus:0}
%files -n dpm-argus
%defattr(-,root,root,-)
%{_bindir}/dpns-arguspoll
%doc %{_mandir}/man1/dpns-arguspoll.1*
%endif


%changelog
* Mon Dec 10 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.6-1
- Update for new upstream release

* Thu Nov 01 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.5-1
- Update for new upstream release

* Mon Sep 03 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.4-1
- Update for new upstream release

* Tue Jun 05 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3.1-2
- Cleanup sysv scripts from upstream when using systemd

* Mon Jun 04 2012 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.8.3.1-1
- Update for new upstream release
- Dropped postgresql packages (unsupported by upstream)
- Removed configuration bits from the spec file (incomplete)
- Renamed dpm-srm* to srm*, dpm-rfiod to rfiod (using alternatives)

* Tue Feb 28 2012 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.2-3
- Convert to systemd (Fedora 17+)
- Rebuild for new gsoap (Fedora 17+)
- Update obsolete sql scripts

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.2-1
- Update to version 1.8.2

* Thu Nov 10 2011 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.1.2-2
- Implement new package names agreed with upstream

* Fri Sep 02 2011 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.1.2-1
- Update to version 1.8.1.2
- Drop patches lcgdm-withsoname.patch and lcgdm-gsoap.patch (upstream)

* Mon Jun 20 2011 Petr Sabata <contyk@redhat.com> - 1.8.0.1-8
- Perl mass rebuild

* Wed Mar 23 2011 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.0.1-7
- Rebuild for mysql 5.5.10

* Sat Feb 12 2011 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.0.1-6
- Fix duplicate files introduced by the PEP 3149 update

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.0.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Dec 30 2010 David Malcolm <dmalcolm@redhat.com> - 1.8.0.1-4
- update build of python bindings to reflect PEP 3149 in the latest python 3.2,
  which changes the extension of python modules, and the library SONAME

* Mon Dec 27 2010 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.0.1-3
- Add database schema migration to scriptlets
- Fix broken condrestart action in start-up scripts

* Mon Dec 20 2010 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.0.1-2
- Filter private provides from python

* Mon Dec 20 2010 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.8.0.1-1
- Update to version 1.8.0.1
- Drop patch lcgdm-bashisms.patch (fixed upstream)

* Wed Aug 25 2010 Thomas Spura <tomspur@fedoraproject.org> - 1.7.4.7-4
- rebuild with python3.2
  http://lists.fedoraproject.org/pipermail/devel/2010-August/141368.html

* Wed Jul 21 2010 David Malcolm <dmalcolm@redhat.com> - 1.7.4.7-3
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Sun Jun 27 2010 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.4.7-2
- Fix race conditions during make install
- Build python modules for alternative python versions

* Sun Jun 06 2010 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.4.7-1
- Update to version 1.7.4.7
- This version works with gsoap versions > 2.7.15
- Dropped patches lcgdm-typo.patch and lcgdm-man.patch (fixed upstream)

* Tue Jun 01 2010 Marcela Maslanova <mmaslano@redhat.com> - 1.7.4.4-3
- Mass rebuild with perl-5.12.0

* Thu Apr 08 2010 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.4.4-2
- Fix priorities for alternatives
- Add -p flag to install commands

* Mon Mar 29 2010 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.4.4-1
- Update to version 1.7.4.4
- Dropped patches lcgdm-installpermissions.patch, lcgdm-rules.patch,
  lcgdm-initscripts.patch and lcgdm-posinc.patch (fixed upstream)

* Mon Jan 04 2010 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.4.1-1
- Update to version 1.7.4.1
- Dropped patch lcgdm-missing-swig-includes.patch (fixed upstream)

* Thu Dec 10 2009 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.3.1-5
- Merge LFC and DPM to one specfile

* Mon Dec 07 2009 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.3.1-4
- Add missing swig includes

* Tue Nov 24 2009 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.3.1-3
- Don't use /sbin/nologin as shell - doesn't work with su

* Mon Nov 23 2009 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.3.1-2
- Make dlopening work for standalone

* Tue Sep 22 2009 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.3.1-1
- Update to version 1.7.3.1

* Wed Aug 19 2009 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.2.5-2
- Patch refactoring
- Add alternatives support

* Fri Aug 14 2009 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.2.5-1
- Update to version 1.7.2.5
- Dropped patch LFC-nofunctions.patch (fixed upstream)

* Wed Jan 14 2009 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.0.6-1
- Update to version 1.7.0.6
- Dropped patch LFC-glibc28.patch (fixed upstream)
- Dropped patch LFC-perlbug.patch (no longer needed)

* Sun Oct 26 2008 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.7.0.2-1ng
- Update to version 1.7.0.2
- Dropped patch LFC-spelling.patch (fixed upstream)

* Fri May 16 2008 Anders Wäänänen <waananen@nbi.dk> - 1.6.9.1-5ng
- Support Alpha architecture
- Added patch LFC-glibc28.patch for glibc-2.8 support
- Added patch LFC-perlbug.patch for work-around on Fedora 9 x86_64

* Tue Apr 02 2008 Anders Wäänänen <waananen@nbi.dk> - 1.6.9.1-4ng
- Added patch from Mattias Ellert <mattias.ellert@fysast.uu.se>:
    LFC-shliblink.patch - Make clients link dynamically against library

* Tue Mar 18 2008 Anders Wäänänen <waananen@nbi.dk> - 1.6.9.1-3ng
- Added ng to release tag
- Added patches:
    LFC-withsoname.patch - Add sonames libraries (helps package dependencies)
    LFC-nofunctions.patch - Support systems without /etc/init.d/functions
    LFC-spelling.patch - Spelling corrections

* Sat Jan 12 2008 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.6.9.1-2
- Fixing some file permissions in the server package

* Sat Jan 12 2008 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.6.9.1-1
- Update.

* Wed Jul 25 2007 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.6.6.1-1
- Update.

* Thu May 10 2007 Mattias Ellert <mattias.ellert@fysast.uu.se> - 1.6.4.3-1
- Initial build.
