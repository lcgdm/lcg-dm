#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
main()
{
	int i;
	int nb_supported_protocols;
	char **supported_protocols;

	if (dpm_getprotocols (&nb_supported_protocols, &supported_protocols) < 0) {
		sperror ("dpm_getprotocols");
		exit (2);
	}
	printf ("Supported protocols are:");
	for (i = 0; i < nb_supported_protocols; i++) {
		printf (" %s", supported_protocols[i]);
		free (supported_protocols[i]);
	}
	printf ("\n");
	free (supported_protocols);
	exit (0);
}
