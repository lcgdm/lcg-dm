#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "dpm_api.h"
#include "serrno.h"
#define DEFPOLLINT 10

main(argc, argv)
int argc;
char **argv;
{
	static char *f_stat[] = {"Success", "Queued", "Active", "Ready", "Running", "Done", "Failed", "Aborted"};
	struct dpm_putfilestatus *filestatuses;
	int i;
	int nbfiles;
	int nbprotocols;
	int nbreplies;
	static char *protocols[] = {"rfio"};
	int r = 0;
	char r_token[CA_MAXDPMTOKENLEN+1];
	struct dpm_putfilereq *reqfiles;
	int status;

	if (argc < 2) {
		fprintf (stderr, "usage: %s SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 1;

	if ((reqfiles = calloc (nbfiles, sizeof(struct dpm_putfilereq))) == NULL) {
		perror ("calloc");
		exit (1);
	}
	for (i = 0; i < nbfiles; i++)
		reqfiles[i].to_surl = argv[i+1];
	nbprotocols = sizeof(protocols) / sizeof(char *);

	if ((status = dpm_put (nbfiles, reqfiles, nbprotocols, protocols, NULL,
	    0, 0, r_token, &nbreplies, &filestatuses)) < 0) {
		sperror ("dpm_put");
		exit (1);
	}

	printf ("dpm_put returned r_token: %s\n", r_token);

	/* wait for request status "Done" or "Failed" */

	while (status == DPM_QUEUED || status == DPM_ACTIVE) {
		for (i = 0; i < nbreplies; i++) {
			if ((filestatuses+i)->to_surl)
				free ((filestatuses+i)->to_surl);
			if ((filestatuses+i)->turl)
				free ((filestatuses+i)->turl);
			if ((filestatuses+i)->errstring)
				free ((filestatuses+i)->errstring);
		}
		free (filestatuses);
		printf("request state Pending\n");
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if ((status = dpm_getstatus_putreq (r_token, 0, NULL,
		    &nbreplies, &filestatuses)) < 0) {
			sperror ("dpm_getstatus_putreq");
			exit (1);
		}
	}
	printf ("request state %s\n", status == DPM_DONE ? "Done" : "Failed");
	if (status == DPM_FAILED)
		exit (1);
	for (i = 0; i < nbreplies; i++) {
		if ((filestatuses+i)->turl)
			printf ("state[%d] = %s, TURL = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->turl);
		else if (((filestatuses+i)->status & DPM_FAILED) == DPM_FAILED)
			printf ("state[%d] = %s, serrno = %d, errstring = <%s>\n", i,
			    f_stat[(filestatuses+i)->status >> 12],
			    (filestatuses+i)->status & 0xFFF,
			    (filestatuses+i)->errstring ? (filestatuses+i)->errstring : "");
		else
			printf ("state[%d] = %s\n", i,
			    f_stat[(filestatuses+i)->status >> 12]);
		if ((filestatuses+i)->to_surl)
			free ((filestatuses+i)->to_surl);
		if ((filestatuses+i)->turl)
			free ((filestatuses+i)->turl);
		if ((filestatuses+i)->errstring)
			free ((filestatuses+i)->errstring);
	}
	free (filestatuses);
	exit (0);
}
