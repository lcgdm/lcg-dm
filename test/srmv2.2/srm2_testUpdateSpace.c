/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testUpdateSpace.c,v 1.2 2007/01/17 14:18:38 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmUpdateSpaceResponse_ rep;
	//struct ArrayOfTSURLPermissionReturn *repfs;
	struct ns1__srmUpdateSpaceRequest req;
	//struct ns1__TSURLInfo *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__srmUpdateSpaceResponse *repp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;

	if (argc < 7) {
		fprintf (stderr, "usage: %s endPoint s_token [storage_system_info|-] [newSizeOfTotalSpaceDesired|-] [newSizeOfGuaranteedSpaceDesired|-] [newLifeTimeFromCallingTime|-|-1]\n", argv[0]);
		exit (1);
	}

#if 0
	if (parsesurl (argv[3], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#endif

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

	/*
	    if ((req.spaceToken =
		 soap_malloc (&soap, sizeof(struct ns1__TSpaceToken))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	*/
	    req.spaceToken = argv[2];

	    if ((req.storageSystemInfo =
		 soap_malloc (&soap, sizeof(struct ns1__TExtraInfo))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    //req.storageSystemInfo->value = argv[3];
	    /*
	    req.storageSystemInfo->__sizeextraInfoArray = 1;
	    req.storageSystemInfo->extraInfoArray[0]->key = "0";
	    req.storageSystemInfo->extraInfoArray[0]->value = argv[3];
	    */
	    req.storageSystemInfo->__sizeextraInfoArray = 0;
	    req.storageSystemInfo->extraInfoArray = NULL;
	    //printf("Step 1\n");

	    if ( strcmp(argv[4], "-") ) {
	
	if ((req.newSizeOfTotalSpaceDesired =
		soap_malloc (&soap, sizeof(ULONG64))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	
	  *(req.newSizeOfTotalSpaceDesired) = atoll(argv[4]);
	//printf("Step 2\n");
	    }

	if ( strcmp(argv[5], "-") ) {
	
	if ((req.newSizeOfGuaranteedSpaceDesired =
		soap_malloc (&soap, sizeof(ULONG64))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	
	  *(req.newSizeOfGuaranteedSpaceDesired) = atoll(argv[5]);
	//printf("Step 3\n");
	}

	
	if ( strcmp(argv[6], "-") ) {

	if ((req.newLifeTime =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	
	  *(req.newLifeTime) = atoi(argv[6]);
	//printf("Step 4\n");
	}

	/* To send the request ... */

#if 0
	if (soap_call_ns1__srmUpdateSpace (&soap, argv[1], "UpdateSpace",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	srm_endpoint = argv[1];
	SOAP_CALL_NS1(UpdateSpace, re);
#endif

	reqstatp = rep.srmUpdateSpaceResponse->returnStatus;
	//repfs = rep.srmCheckPermissionResponse->arrayOfPermissions;

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	repp = rep.srmUpdateSpaceResponse;
/*  	printf ("srmUpdateSpace provided actual_s_type: %d actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", (*repp->typeOfReservedSpace), repp->sizeOfTotalReservedSpace->value, repp->sizeOfGuaranteedReservedSpace->value, repp->lifetimeOfReservedSpace->value ); */
	printf ("srmUpdateSpace provided actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", *(repp->sizeOfTotalSpace), *(repp->sizeOfGuaranteedSpace), *(repp->lifetimeGranted) );

	soap_end (&soap);
	exit (0);
}
