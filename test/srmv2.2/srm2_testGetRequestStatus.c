/*
 * Copyright (C) 2004-2006 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetRequestStatus.c,v 1.2 2006/12/21 10:11:21 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

	char pdate[21];
	char *pdati = "INFINITE";
	time_t current_time;

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
	//Sint sbrk0;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmGetRequestSummaryResponse_ rep;
	struct ns1__ArrayOfTRequestSummary *repfs;
	struct ns1__srmGetRequestSummaryRequest req;
	struct ns1__TRequestToken *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	/*
	struct ns1__srmStatusOfPutRequestResponse_ srep;
	struct ns1__srmStatusOfPutRequestRequest sreq;
	*/
	char *srm_endpoint;
	char *rtype;

	int j, nbstat, numfiles;

	struct ns1__ArrayOfTRequestSummary *lrepfs;
	size_t srepfs, nzf;
	int nrs;

	(void) setbuf(stdout, NULL);
	(void) setbuf(stderr, NULL);

	if (argc < 3) {
		fprintf (stderr, "usage: %s endPoint reqids\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 2;

#if 0
	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#else
	srm_endpoint = argv[1];
#endif

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

	if ((req.arrayOfRequestTokens =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfString))) == NULL ||
	    (req.arrayOfRequestTokens->stringArray =
		soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	req.arrayOfRequestTokens->__sizestringArray = nbfiles;

	for (i = 0; i < nbfiles; i++) {
	  req.arrayOfRequestTokens->stringArray[i] = argv[i+2];
	}

	/* GG special tests */
	//GOODreq.arrayOfRequestTokens = NULL;

#if 0
#if 0
	if (soap_call_ns1__srmGetRequestSummary (&soap, srm_endpoint, "GetRequestSummary",
#else
	if (soap_call_ns1__srmGetRequestSummary (&soap, argv[1], "GetRequestSummary",
#endif
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	SOAP_CALL_NS1(GetRequestSummary, re);
#endif

	reqstatp = rep.srmGetRequestSummaryResponse->returnStatus;
	repfs = rep.srmGetRequestSummaryResponse->arrayOfRequestSummaries;

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);

	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREPARTIAL_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (&soap);
		exit (1);
		*/
	}
	if (! repfs) {
		printf ("arrayOfRequestSummaries is NULL\n");
		soap_end (&soap);
		exit (0);
	}

						 //Ssbrk0 = sbrk(0);
	printf ("request summaryArray %d\n", repfs->__sizesummaryArray);

#if 0
						 nrs = repfs->__sizesummaryArray;
						 lrepfs = malloc(sizeof(struct ns1__ArrayOfTRequestSummary));
						 lrepfs->__sizesummaryArray = nrs;
						 lrepfs->summaryArray = calloc(nrs, sizeof(struct ns1__TRequestSummary *));
						 nzf = sizeof(struct ns1__TRequestSummary);
						 srepfs = calloc(nrs, nzf);
						 printf (" nrs nzf lrepfs srepfs: %d %d %d %d \n", nrs, nzf, lrepfs, srepfs);
						 for (i = 0; i < repfs->__sizesummaryArray; i++) {
						   memmove(srepfs+nzf*i, repfs->summaryArray[i], nzf);
						   lrepfs->summaryArray[i] = srepfs+nzf*i;
						   printf("Type of      req: %s\n", soap_ns1__TRequestType2s (&soap, (*repfs->summaryArray[i]->requestType)));
						   printf("Request    token: %s\n", repfs->summaryArray[i]->requestToken);
						   printf("Address    token: %d\n", repfs->summaryArray[i]->requestToken);
						   printf("Type of     lreq: %s\n", soap_ns1__TRequestType2s (&soap, (*lrepfs->summaryArray[i]->requestType)));
						   printf("Request   ltoken: %s\n", lrepfs->summaryArray[i]->requestToken);
						   printf("Address   ltoken: %d\n", lrepfs->summaryArray[i]->requestToken);
						 }
#endif

#if 1
	for (i = 0; i < repfs->__sizesummaryArray; i++) {
		printf("======= Begin Request ========\n");
		//printf("Finished   files: %d\n", repfs->summaryArray[i]->numOfFinishedRequests);
		//printf("Processing files: %d\n", repfs->summaryArray[i]->numOfProgressingRequests);
		//BADprintf("Number of  files: %d\n", repfs->summaryArray[i]->totalNumFilesInRequest);
		//printf("Number of  files: %d\n", (*repfs->summaryArray[i]->totalNumFilesInRequest));

		if ( repfs->summaryArray[i]->requestType )
		  printf("Type of      req: %s\n", soap_ns1__TRequestType2s (&soap, (*repfs->summaryArray[i]->requestType)));
		printf("Request    token: %s\n", repfs->summaryArray[i]->requestToken);


		printf ("state[%d]: %d %s", i, repfs->summaryArray[i]->status->statusCode,
			soap_ns1__TStatusCode2s (&soap,repfs->summaryArray[i]->status->statusCode));
		if ( (repfs->summaryArray[i]->totalNumFilesInRequest) )
		  printf (", totalNumFilesInRequest: %d", *(repfs->summaryArray[i]->totalNumFilesInRequest));
		if ( (repfs->summaryArray[i]->numOfCompletedFiles) )
		  printf (", numOfCompletedFiles: %d", *(repfs->summaryArray[i]->numOfCompletedFiles));
		if ( (repfs->summaryArray[i]->numOfWaitingFiles) )
		  printf (", numOfWaitingFiles: %d", *(repfs->summaryArray[i]->numOfWaitingFiles));
		if ( (repfs->summaryArray[i]->numOfFailedFiles) )
		  printf (", numOfFailedFiles: %d", *(repfs->summaryArray[i]->numOfFailedFiles));
		printf("\n");

		if ( repfs->summaryArray[i]->requestType ) {
		if ( ! strcmp(soap_ns1__TRequestType2s (&soap, (*repfs->summaryArray[i]->requestType)), "PREPARE_TO_PUT") ) {
		  printPutRequest(&soap, srm_endpoint, repfs->summaryArray[i]->requestToken);
		} else if ( ! strcmp(soap_ns1__TRequestType2s (&soap, (*repfs->summaryArray[i]->requestType)), "PREPARE_TO_GET") ) {
		  printGetRequest(&soap, srm_endpoint, repfs->summaryArray[i]->requestToken);
		} else if ( ! strcmp(soap_ns1__TRequestType2s (&soap, (*repfs->summaryArray[i]->requestType)), "BRING_ONLINE") ) {
		  printBrgRequest(&soap, srm_endpoint, repfs->summaryArray[i]->requestToken);
		} else if ( ! strcmp(soap_ns1__TRequestType2s (&soap, (*repfs->summaryArray[i]->requestType)), "COPY") ) {
		  printCpyRequest(&soap, srm_endpoint, repfs->summaryArray[i]->requestToken);
		} else {
		  // Unknown or Unimplemented type ...
		}
		}



		//Sprintf("MemLeak: %d\n", sbrk(0)-sbrk0);
	}
#else
	for (i = 0; i < lrepfs->__sizesummaryArray; i++) {
		printf("======= Begin Request ========\n");
		//printf("Finished   files: %d\n", repfs->summaryArray[i]->numOfFinishedRequests);
		//printf("Processing files: %d\n", repfs->summaryArray[i]->numOfProgressingRequests);
		//BADprintf("Number of  files: %d\n", repfs->summaryArray[i]->totalNumFilesInRequest);
		//printf("Number of  files: %d\n", (*repfs->summaryArray[i]->totalNumFilesInRequest));

		if ( lrepfs->summaryArray[i]->requestType )
		  printf("Type of      req: %s\n", soap_ns1__TRequestType2s (&soap, (*lrepfs->summaryArray[i]->requestType)));
		printf("Request    token: %s\n", lrepfs->summaryArray[i]->requestToken);


		printf ("state[%d]: %d %s", i, lrepfs->summaryArray[i]->status->statusCode,
			soap_ns1__TStatusCode2s (&soap,lrepfs->summaryArray[i]->status->statusCode));
		if ( (lrepfs->summaryArray[i]->totalNumFilesInRequest) )
		  printf (", totalNumFilesInRequest: %d", *(lrepfs->summaryArray[i]->totalNumFilesInRequest));
		if ( (lrepfs->summaryArray[i]->numOfCompletedFiles) )
		  printf (", numOfCompletedFiles: %d", *(lrepfs->summaryArray[i]->numOfCompletedFiles));
		if ( (lrepfs->summaryArray[i]->numOfWaitingFiles) )
		  printf (", numOfWaitingFiles: %d", *(lrepfs->summaryArray[i]->numOfWaitingFiles));
		if ( (lrepfs->summaryArray[i]->numOfFailedFiles) )
		  printf (", numOfFailedFiles: %d", *(lrepfs->summaryArray[i]->numOfFailedFiles));
		printf("\n");

		if ( ! strcmp(soap_ns1__TRequestType2s (&soap, (*lrepfs->summaryArray[i]->requestType)), "PREPARE_TO_PUT") ) {
		  printPutRequest(&soap, srm_endpoint, lrepfs->summaryArray[i]->requestToken);
		} else if ( ! strcmp(soap_ns1__TRequestType2s (&soap, (*lrepfs->summaryArray[i]->requestType)), "PREPARE_TO_GET") ) {
		  printGetRequest(&soap, srm_endpoint, lrepfs->summaryArray[i]->requestToken);
		} else if ( ! strcmp(soap_ns1__TRequestType2s (&soap, (*lrepfs->summaryArray[i]->requestType)), "BRING_ONLINE") ) {
		  printBrgRequest(&soap, srm_endpoint, lrepfs->summaryArray[i]->requestToken);
		} else if ( ! strcmp(soap_ns1__TRequestType2s (&soap, (*lrepfs->summaryArray[i]->requestType)), "COPY") ) {
		  printCpyRequest(&soap, srm_endpoint, lrepfs->summaryArray[i]->requestToken);
		} else {
		  // Unknown or Unimplemented type ...
		}



		printf("MemLeak: %d\n", sbrk(0)-sbrk0);
	}
#endif
						 //Pprintf("MemLeak: %d\n", sbrk(0)-sbrk0);
	soap_end (&soap);
	exit (0);
}

int
printPutRequest (struct soap* soap, char* srm_endpoint, char* r_token) {

      struct ns1__srmStatusOfPutRequestResponse_ srep;
      struct ns1__srmStatusOfPutRequestRequest sreq;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__ArrayOfTPutRequestFileStatus *repfs;
	int i;

	memset (&sreq, 0, sizeof(sreq));
	sreq.requestToken = r_token;
	//Pprintf("Step 8 %s %s\n", r_token, srm_endpoint);

		if (soap_call_ns1__srmStatusOfPutRequest (soap, srm_endpoint,
		    "StatusOfPutRequest", &sreq, &srep)) {
			soap_print_fault (soap, stderr);
			soap_end (soap);
			exit (1);
		}
		//(void) time (&current_time);
		current_time = time (0);

	//Pprintf("Step 9 %s\n", r_token);
		reqstatp = srep.srmStatusOfPutRequestResponse->returnStatus;
		repfs = srep.srmStatusOfPutRequestResponse->arrayOfFileStatuses;

	printf ("request putstatus %s\n", soap_ns1__TStatusCode2s (soap, reqstatp->statusCode));
	printf ("request putstate %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREREQUEST_USCOREINPROGRESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (soap);
		exit (1);
		*/
		//return;
	}

	repfs = srep.srmStatusOfPutRequestResponse->arrayOfFileStatuses;
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		/*
		soap_end (soap);
		exit (1);
		*/
		return;
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
	  if (repfs->statusArray[i]->transferURL) {
		  strcpy(pdate, soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingPinLifetime)));
		  if ( ! strcmp(pdate, "1969-12-31T23:59:59Z") )
		    strcpy(pdate, pdati);
		  else
		    strcpy(pdate, soap_dateTime2s (soap, current_time + (time_t) (*repfs->statusArray[i]->remainingPinLifetime)));
			printf ("PutR: state[%d] = %d, %s, SURL = %s, TURL = %s, fsz = %llu, pinDate = %s", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode),
				(repfs->statusArray[i])->SURL,
				(repfs->statusArray[i])->transferURL,
				(*repfs->statusArray[i]->fileSize),
				pdate
				//soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingPinLifetime))
				);
			if ( repfs->statusArray[i]->remainingFileLifetime ) {
			  strcpy(pdate, soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingFileLifetime)));
			  if ( ! strcmp(pdate, "1969-12-31T23:59:59Z") )
			    strcpy(pdate, pdati);
			  else
			    strcpy(pdate, soap_dateTime2s (soap, current_time + (time_t) (*repfs->statusArray[i]->remainingFileLifetime)));
			  printf (" lifeDate = %s\n", pdate);
			} else 
			  printf ("\n");
	  }
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("PutR: state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
			printf ("PutR: state[%d] = %d, %s\n", i,
				(repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode));
	}

	//BADsoap_dealloc(soap, NULL);
	//soap_free(soap);
	//??free (repfs);
	//??free (srep.srmStatusOfPutRequestResponse->arrayOfFileStatuses);
	      return;
	    }

int
printGetRequest (struct soap* soap, char* srm_endpoint, char* r_token) {

      struct ns1__srmStatusOfGetRequestResponse_ srep;
      struct ns1__srmStatusOfGetRequestRequest sreq;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__ArrayOfTGetRequestFileStatus *repfs;
	int i;

	memset (&sreq, 0, sizeof(sreq));
	sreq.requestToken = r_token;
	//Pprintf("Step 8 %s %s\n", r_token, srm_endpoint);

		if (soap_call_ns1__srmStatusOfGetRequest (soap, srm_endpoint,
		    "StatusOfGetRequest", &sreq, &srep)) {
			soap_print_fault (soap, stderr);
			soap_end (soap);
			exit (1);
		}

	//Pprintf("Step 9 %s\n", r_token);
		reqstatp = srep.srmStatusOfGetRequestResponse->returnStatus;
		//printf("request state 2 %d\n", reqstatp->statusCode);
		repfs = srep.srmStatusOfGetRequestResponse->arrayOfFileStatuses;

	printf ("request getstatus %s\n", soap_ns1__TStatusCode2s (soap, reqstatp->statusCode));
	printf ("request getstate %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREREQUEST_USCOREINPROGRESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (soap);
		exit (1);
		*/
		//return;
	}

	repfs = srep.srmStatusOfGetRequestResponse->arrayOfFileStatuses;
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		/*
		soap_end (soap);
		exit (1);
		*/
		return;
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
	  if (repfs->statusArray[i]->transferURL) {
		  strcpy(pdate, soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingPinTime)));
		  if ( ! strcmp(pdate, "1969-12-31T23:59:59Z") )
		    strcpy(pdate, pdati);
			printf ("GetR: state[%d] = %d, %s, SURL = %s, TURL = %s, fsz = %llu, pinDate = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode),
				(repfs->statusArray[i])->sourceSURL,
				(repfs->statusArray[i])->transferURL,
				(*repfs->statusArray[i]->fileSize),
				pdate
				//soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingPinTime))
				);
	  }
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("GetR: state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
			printf ("GetR: state[%d] = %d, %s\n", i,
				(repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode));
	}

	//soap_free(soap);
	      return;
	    }

int
printBrgRequest (struct soap* soap, char* srm_endpoint, char* r_token) {

      struct ns1__srmStatusOfBringOnlineRequestResponse_ srep;
      struct ns1__srmStatusOfBringOnlineRequestRequest sreq;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__ArrayOfTBringOnlineRequestFileStatus *repfs;
	int i;

	memset (&sreq, 0, sizeof(sreq));
	sreq.requestToken = r_token;
	//Pprintf("Step 8 %s %s\n", r_token, srm_endpoint);

		if (soap_call_ns1__srmStatusOfBringOnlineRequest (soap, srm_endpoint,
		    "StatusOfBringOnlineRequest", &sreq, &srep)) {
			soap_print_fault (soap, stderr);
			soap_end (soap);
			exit (1);
		}

	//Pprintf("Step 9 %s\n", r_token);
		reqstatp = srep.srmStatusOfBringOnlineRequestResponse->returnStatus;
		//printf("request state 2 %d\n", reqstatp->statusCode);
		repfs = srep.srmStatusOfBringOnlineRequestResponse->arrayOfFileStatuses;

	printf ("request brgstatus %s\n", soap_ns1__TStatusCode2s (soap, reqstatp->statusCode));
	printf ("request brgstate %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREREQUEST_USCOREINPROGRESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (soap);
		exit (1);
		*/
		//return;
	}

	repfs = srep.srmStatusOfBringOnlineRequestResponse->arrayOfFileStatuses;
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		/*
		soap_end (soap);
		exit (1);
		*/
		return;
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
	  if (repfs->statusArray[i]->sourceSURL) {
		  strcpy(pdate, soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingPinTime)));
		  if ( ! strcmp(pdate, "1969-12-31T23:59:59Z") )
		    strcpy(pdate, pdati);
			printf ("BrgR: state[%d] = %d, %s, SURL = %s, fsz = %llu, pinDate = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode),
				(repfs->statusArray[i])->sourceSURL,
				//(repfs->statusArray[i])->transferURL,
				(*repfs->statusArray[i]->fileSize),
				pdate
				//soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingPinTime))
				);
	  }
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("BrgR: state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
			printf ("BrgR: state[%d] = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode);
	}

	//soap_free(soap);
	      return;
	    }

int
printCpyRequest (struct soap* soap, char* srm_endpoint, char* r_token) {

      struct ns1__srmStatusOfCopyRequestResponse_ srep;
      struct ns1__srmStatusOfCopyRequestRequest sreq;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__ArrayOfTCopyRequestFileStatus *repfs;
	int i;

	memset (&sreq, 0, sizeof(sreq));
	sreq.requestToken = r_token;
	//Pprintf("Step 8 %s %s\n", r_token, srm_endpoint);

		if (soap_call_ns1__srmStatusOfCopyRequest (soap, srm_endpoint,
		    "StatusOfCopyRequest", &sreq, &srep)) {
			soap_print_fault (soap, stderr);
			soap_end (soap);
			exit (1);
		}

	//Pprintf("Step 9 %s\n", r_token);
		reqstatp = srep.srmStatusOfCopyRequestResponse->returnStatus;
		//printf("request state 2 %d\n", reqstatp->statusCode);
		repfs = srep.srmStatusOfCopyRequestResponse->arrayOfFileStatuses;

	printf ("request cpystatus %s\n", soap_ns1__TStatusCode2s (soap, reqstatp->statusCode));
	printf ("request cpystate %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREREQUEST_USCOREINPROGRESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (soap);
		exit (1);
		*/
		//return;
	}

	repfs = srep.srmStatusOfCopyRequestResponse->arrayOfFileStatuses;
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		/*
		soap_end (soap);
		exit (1);
		*/
		return;
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
	  if (repfs->statusArray[i]->targetSURL) {
		  strcpy(pdate, soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingFileLifetime)));
		  if ( ! strcmp(pdate, "1969-12-31T23:59:59Z") )
		    strcpy(pdate, pdati);
			printf ("CpyR: state[%d] = %d, %s, sSURL = %s, tSURL = %s, fsz = %llu, pinDate = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (soap, (repfs->statusArray[i])->status->statusCode),
				(repfs->statusArray[i])->sourceSURL,
				(repfs->statusArray[i])->targetSURL,
				(*repfs->statusArray[i]->fileSize),
				pdate
				//soap_dateTime2s (soap, (time_t) (*repfs->statusArray[i]->remainingFileLifetime))
				);
	  }
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("CpyR: state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
			printf ("CpyR: state[%d] = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode);
	}

	//soap_free(soap);
	      return;
	    }
