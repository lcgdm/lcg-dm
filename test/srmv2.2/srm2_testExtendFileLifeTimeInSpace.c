/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testExtendFileLifeTimeInSpace.c,v 1.3 2007/02/15 06:41:07 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmExtendFileLifeTimeInSpaceResponse_ rep;
	struct ns1__ArrayOfTSURLLifetimeReturnStatus *repfs;
	struct ns1__srmExtendFileLifeTimeInSpaceRequest req;
	struct ns1__TSURL *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfGetRequestResponse_ srep;
	struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;

	if (argc < 4) {
		fprintf (stderr, "usage: %s spaceToken [newLifetime|-] SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 3;

	if (parsesurl (argv[3], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

#if 0
	if ((req.requestToken =
		soap_malloc (&soap, sizeof(struct ns1__TRequestToken))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.requestToken->value = argv[1];
#else
	if ((req.arrayOfSURLs =
	    soap_malloc (&soap, sizeof(struct ns1__ArrayOfAnyURI))) == NULL || 
	    (req.arrayOfSURLs->urlArray =
		soap_malloc (&soap, nbfiles * sizeof(char *))) == NULL) {
		perror ("mallocA");
		soap_end (&soap);
		exit (1);
	}

#if 1
	if ( strncmp(argv[1], "-", 1) ) {
	  req.spaceToken = argv[1];
	  //printf("Step 0\n");
	} else
	  req.spaceToken = NULL;

	if ((req.newLifeTime =
		soap_malloc (&soap, sizeof(int))) == NULL) {
		perror ("mallocL");
		soap_end (&soap);
		exit (1);
	}	
	if ( strcmp(argv[2], "-") ) {
	  *(req.newLifeTime) = atoi(argv[2]);
	  //printf("Step 1 %d\n", nbfiles);
	} else
	  req.newLifeTime = NULL;
#endif
#endif

	req.arrayOfSURLs->__sizeurlArray = nbfiles;
	for (i = 0; i < nbfiles; i++) 
	  req.arrayOfSURLs->urlArray[i] = argv[i+3];

	if (soap_call_ns1__srmExtendFileLifeTimeInSpace (&soap, srm_endpoint, "ExtendFileLifeTimeInSpace",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmExtendFileLifeTimeInSpaceResponse->returnStatus;
	repfs = rep.srmExtendFileLifeTimeInSpaceResponse->arrayOfFileStatuses;

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		/*
		soap_end (&soap);
		exit (1);
		*/
	}

	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
		if ((repfs->statusArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else {
#if 0
			printf ("state[%d] = %d, %s, SURL = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
				(repfs->statusArray[i])->surl);
#else
			printf ("state[%d] = %d, %s", 
				i,
				(repfs->statusArray[i])->status->statusCode,
				soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode));
			if ( (repfs->statusArray[i])->pinLifetime )
			  /* FIXME */
			  //printf (", pinLifetime: %s", soap_dateTime2s (&soap, (time_t) *(repfs->statusArray[i])->pinLifetime));
			  printf (", pinLifetime: %d", *(repfs->statusArray[i])->pinLifetime);
			if ( (repfs->statusArray[i])->fileLifetime )
			  printf (", fileLifetime: %d", *(repfs->statusArray[i])->fileLifetime);
			printf (" SURL = %s\n", 
				(repfs->statusArray[i])->surl);
#endif
		}
	}

	soap_end (&soap);
	exit (0);
}

