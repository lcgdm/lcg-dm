/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testCopy.c,v 1.2 2007/07/19 08:06:24 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmCopyResponse_ rep;
	struct ns1__ArrayOfTCopyRequestFileStatus *repfs;
	struct ns1__srmCopyRequest req;
	struct ns1__TCopyFileRequest *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfCopyRequestResponse_ srep;
	struct ns1__srmStatusOfCopyRequestRequest sreq;
	char *srm_endpoint;

	//userRequestDescription
	//char *u_token;

	(void) setbuf(stdout, NULL);
	(void) setbuf(stderr, NULL);

	if (argc < 3) {
		fprintf (stderr, "usage: %s u_token string-of-fromSURLs string-of-toSURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = (argc - 2)/2;
	if ( (nbfiles*2 + 2) < argc ) {
		fprintf (stderr, "usage: %s u_token string-of-fromSURLs string-of-toSURLs\n", argv[0]);
		exit (1);
	}

	if (parsesurl (argv[2], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	//1807flags |= CGSI_OPT_DELEG_FLAG;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.arrayOfFileRequests =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTCopyFileRequest))) == NULL ||
	    (req.arrayOfFileRequests->requestArray =
		soap_malloc (&soap, nbfiles * sizeof(struct ns1__TCopyFileRequest *))) == NULL /*||
	    (req.arrayOfTransferProtocols =
	    soap_malloc (&soap, sizeof(struct ArrayOf_USCORExsd_USCOREstring))) == NULL*/) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.arrayOfFileRequests->requestArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TCopyFileRequest))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.arrayOfFileRequests->__sizerequestArray = nbfiles;
	//req.arrayOfTransferProtocols->__ptr = protocols;
	//req.arrayOfTransferProtocols->__size = nbproto;
	if ( ! strcmp(argv[1], "-") )
	  req.userRequestDescription = argv[1];

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.arrayOfFileRequests->requestArray[i];
		/* good */
		//memset (reqfilep, 0, sizeof(*reqfilep));
		/* good as well */
		memset (reqfilep, 0, sizeof(struct ns1__TCopyFileRequest));
		if ((reqfilep->targetSURL =
		    soap_malloc (&soap, sizeof(char *))) == NULL /* ||
		    (reqfilep->targetSURLInfo->SURL =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL */ ) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		if ((reqfilep->sourceSURL =
		    soap_malloc (&soap, sizeof(char *))) == NULL /* ||
		    (reqfilep->sourceSURLInfo->SURL =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL */ ) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		reqfilep->sourceSURL = argv[i+2];
		reqfilep->targetSURL = argv[i+2+nbfiles];

		/* good
		reqfilep->lifetime = NULL;
		reqfilep->fileStorageType = NULL;
		reqfilep->spaceToken = NULL;
		reqfilep->overwriteMode = NULL;
		reqfilep->dirOption = NULL;
		*/

	}

	if (soap_call_ns1__srmCopy (&soap, srm_endpoint, "Copy",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmCopyResponse->returnStatus;
	//FSrepfs = rep.srmCopyResponse->arrayOfFileStatuses;
	if (rep.srmCopyResponse->requestToken) {
		r_token = rep.srmCopyResponse->requestToken;
		printf ("soap_call_ns1__srmCopy returned r_token %s\n",
		    r_token);
	}

	memset (&sreq, 0, sizeof(sreq));
	sreq.requestToken = rep.srmCopyResponse->requestToken;

	/* wait for file "ready" */

	while (reqstatp->statusCode == SRM_USCOREREQUEST_USCOREQUEUED ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCOREINPROGRESS ||
		reqstatp->statusCode == SRM_USCOREREQUEST_USCORESUSPENDED) {
	printf ("request status0 %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
		printf("request state0 %d\n", reqstatp->statusCode);
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if (soap_call_ns1__srmStatusOfCopyRequest (&soap, srm_endpoint,
		    "StatusOfCopyRequest", &sreq, &srep)) {
			soap_print_fault (&soap, stderr);
			soap_end (&soap);
			exit (1);
		}
		reqstatp = srep.srmStatusOfCopyRequestResponse->returnStatus;
		//FSrepfs = srep.srmStatusOfCopyRequestResponse->arrayOfFileStatuses;

		/* TEMPORARY, because not fully implemented ... */
		if ( r>3 ) {
		  printf ("request state 00\n");
			soap_end (&soap);
			exit (1);
		}
	}

	printf ("request status %s\n", soap_ns1__TStatusCode2s (&soap, reqstatp->statusCode));
	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREPARTIAL_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
	  if (reqstatp->explanation) {
	    printf ("explanation: %s\n", reqstatp->explanation);
	  }
	  if ( ! r ) {
	    soap_end (&soap);
	    exit (1);
	  }
	}

#define FS 1
#ifdef FS
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (0);
	}

	printf ("request statusArray %d\n", repfs->__sizestatusArray);
	for (i = 0; i < repfs->__sizestatusArray; i++) {
		if (repfs->statusArray[i]->targetSURL)
			printf ("state[%d] = %d, %s, target SURL = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    soap_ns1__TStatusCode2s (&soap, (repfs->statusArray[i])->status->statusCode),
			    (repfs->statusArray[i])->targetSURL);
		else if ((repfs->statusArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->statusArray[i])->status->statusCode,
			    (repfs->statusArray[i])->status->explanation);
		else
			printf ("state[%d] = %d\n", i,
			    (repfs->statusArray[i])->status->statusCode);
	}
#endif

	soap_end (&soap);
	exit (0);
}
