import os,lfc, errno
from testClass import _test, _ntest 

class test_ok(_test):
    def info(self):
        return "lfc_rmdir OK"
    def prepare(self):
        self.name = "/grid/dteam/python_rmdir_test"
        ret = lfc.lfc_mkdir(self.name, 0755)
    def test(self):
        ret = lfc.lfc_rmdir(self.name)
        return ("",ret)
    def ret(self):
        return ""
    def compare(self,testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal != testRetVal):
            retval = False
        return retval

class test_ENOENT(_ntest):
    def info(self):
        return "lfc_rmdir: named directory doesn't exist (ENOENT)"
    def test(self):
        name = "/for_sure_not_existing_directory"
        ret = lfc.lfc_rmdir(name)
        return ("",lfc.cvar.serrno, ret)
    def ret(self):
        return ("",errno.ENOENT)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval

class test_EACCES(_ntest):
    def info(self):
        return "lfc_rmdir (EACCES)"
    def prepare(self):
        self.name = "/grid/dteam/python_test/rmdir_test"
        lfc.lfc_mkdir(os.path.dirname(self.name),0755)
        lfc.lfc_mkdir(self.name,0755)
        lfc.lfc_chmod(os.path.dirname(self.name),0400)
    def test(self):
        ret = lfc.lfc_rmdir(self.name)
        return ("",lfc.cvar.serrno, ret)
    def ret(self):
        return ("",errno.EACCES)
    def clean(self):
        lfc.lfc_chmod(self.name,0755)
        lfc.lfc_rmdir(self.name)
        lfc.lfc_rmdir(os.path.dirname(self.name))
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval

class test_EEXIST(_ntest):
    def info(self):
        return "lfc_rmdir (EEXIST)"
    def prepare(self):
        self.name = "/grid/dteam/python_test/rmdir_test"
        lfc.lfc_mkdir(os.path.dirname(self.name),0755)
        self.guid = self.get_guid()
        lfc.lfc_creatg(self.name,self.guid,0664)
    def test(self):
        ret = lfc.lfc_rmdir(os.path.dirname(self.name))
        return ("",lfc.cvar.serrno, ret)
    def ret(self):
        return ("",errno.EEXIST)
    def clean(self):
        lfc.lfc_unlink(self.name)
        lfc.lfc_rmdir(os.path.dirname(self.name))
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval

class test_ENOTDIR(_ntest):
    def info(self):
        return "lfc_rmdir (ENOTDIR)"
    def prepare(self):
        self.name = "/grid/dteam/python_test/rmdir_test"
        lfc.lfc_mkdir(os.path.dirname(self.name),0755)
        self.guid = self.get_guid()
        lfc.lfc_creatg(self.name,self.guid,0664)
    def test(self):
        ret = lfc.lfc_rmdir(self.name)
        return ("",lfc.cvar.serrno, ret)
    def ret(self):
        return ("",errno.ENOTDIR)
    def clean(self):
        lfc.lfc_unlink(self.name)
        lfc.lfc_rmdir(os.path.dirname(self.name))
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval

class test_EINVAL(_ntest):
    def info(self):
        return "lfc_rmdir (EINVAL)"
    def prepare(self):
        self.name = "/grid/dteam/python_rmdir_test"
        lfc.lfc_mkdir(self.name,0755)
        lfc.lfc_chdir(self.name)
    def test(self):
        ret = lfc.lfc_rmdir(self.name)
        return ("",lfc.cvar.serrno, ret)
    def ret(self):
        return ("",errno.ENOTDIR)
    def clean(self):
        lfc.lfc_chdir("/")
        lfc.lfc_rmdir(self.name)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval

class test_ENAMETOOLONG(_ntest):
    def info(self):
        return "The length of path exceeds CA_MAXPATHLEN (ENAMETOOLONG): "
    def test(self):
        path = ""
        for i in xrange(0,1023+1):
            path=path + "a"
        ret=lfc.lfc_rmdir(path)
        return ("", lfc.cvar.serrno, ret)
    def ret(self):
        return ("", errno.ENAMETOOLONG)

    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval

class lfc_mkdir_test:
    def __init__(self):
        self.name = "lfc_rmdir_test"
        self.tests=[test_ok, test_EACCES, test_ENOENT, test_EEXIST, test_ENOTDIR, test_EINVAL, test_ENAMETOOLONG]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'

lfc_mkdir_test().run()
