import os,lfc
from testClass import _test 

class test_create_dir_ok(_test):
    def info(self):
        return "Directory creation"
    def test(self):
        self.name = "/grid/dteam/python_mkdir_test"
        ret = lfc.lfc_mkdir(self.name, 0755)
        return ("",ret)
    def ret(self):
        return ("",0)
    def compare(self,testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal != testRetVal):
            retval = False
        return retval
    def clean(self):
        lfc.lfc_rmdir(self.name)



class test_create_dir_pd(_test):
    def __init__(self):
        self.retVal=-1
    def info(self):
        return "Directory creation (permission denied)"
    def test(self):
        self.name = "/python_mkdir_test"
        ret = lfc.lfc_mkdir(self.name, 0755)
        return ("",ret)
    def ret(self):
        return ("",-1)
    def clean(self):
        lfc.lfc_rmdir(self.name)
    def compare(self,testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal != testRetVal):
            retval = False
        return retval

class lfc_mkdir_test:
    def __init__(self):
        self.name = "lfc_mkdir_test"
        self.tests=[test_create_dir_ok, test_create_dir_pd]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'

lfc_mkdir_test().run()
