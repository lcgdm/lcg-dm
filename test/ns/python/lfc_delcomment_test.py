import os, lfc, sys, commands, errno
from testClass import _test, _ntest

global testHome 

class test_delcomment(_test):
    def info(self):
        return "Delete comment on existing file"
    def prepare(self):
        guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_delcomment_test"
        ret = lfc.lfc_creatg(self.name,guid,0664)
        lfc.lfc_setcomment(self.name,"Primary comment")
    def clean(self):
        lfc.lfc_unlink(self.name)
        pass
    def test(self):
        comment="             "
        ret=lfc.lfc_delcomment(self.name)
        lfc.lfc_getcomment(self.name, comment)
        return (comment.strip(),ret)
    def ret(self):
        return ""
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
        else:
            retval = False
        return retval

class test_delcomment_pd(_ntest):
    def info(self):
        return "Delete comment on existing dir without privilegies (EPERM)"
    def test(self):
        self.name = "/grid"
        comment="             "
        ret=lfc.lfc_delcomment(self.name)
        lfc.lfc_getcomment(self.name, comment)
        return (comment.strip(), lfc.cvar.serrno, ret)
    def ret(self):
        return ("", errno.EPERM)
    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            terval = retval & (err == reterr)
        else:
            retval = False
        return retval


class test_nonexisting_name(_ntest):
    def info(self):
	return "Delete comment on nonexisting file name (ENOENT): "
    def prepare(self):
        self.name="/fhgsdjfgsagfhs"
    def test(self):
        comment="                              "
        ret=lfc.lfc_delcomment(self.name)
        lfc.lfc_getcomment(self.name, comment)
        return (comment.strip(), lfc.cvar.serrno, ret)
    def ret(self):
        return ("", errno.ENOENT) 

    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval

class test_ENAMETOOLONG(_ntest):
    def info(self):
        return "The length of path exceeds CA_MAXPATHLEN (ENAMETOOLONG): "
    def test(self):
        path = ""
        for i in xrange(0,1023+1):
            path=path + "a"
        ret=lfc.lfc_delcomment(path)
        return ("", lfc.cvar.serrno, ret)
    def ret(self):
        return ("", errno.ENAMETOOLONG)

    def compare(self, testVal, retVal):
        ((ret, reterr), retRetVal) = retVal
        (test, err, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
            retvat = retval & (err == reterr)
        else:
            retval = False
        return retval


class lfc_setcomment_test:
    def __init__(self):
	self.name = "lfc_delcomment_test"
        self.tests=[test_delcomment, test_delcomment_pd, test_nonexisting_name, test_ENAMETOOLONG]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_setcomment_test().run()
