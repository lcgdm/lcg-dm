import os, lfc, sys, commands
from testClass import _test

global testHome 

class test_setcomment_ok(_test):
    def info(self):
        return "Set file comment existing file name"
    def prepare(self):
        guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_setcomment_test"
        ret = lfc.lfc_creatg(self.name,guid,0664)
    def clean(self):
        lfc.lfc_unlink(self.name)
        pass
    def test(self):
        self.comment="Test comment"
        comment="             "
        ret=lfc.lfc_setcomment(self.name,self.comment)
        lfc.lfc_getcomment(self.name, comment)
        return (comment.strip(),ret)
    def ret(self):
        return self.comment + '\x00'
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
        else:
            retval = False
        return retval

class test_update_comment(_test):
    def info(self):
        return "Update comment on existing file name"
    def prepare(self):
        guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_setcomment_test"
        ret = lfc.lfc_creatg(self.name,guid,0664)
        lfc.lfc_setcomment(self.name,"Primary comment")
    def clean(self):
        lfc.lfc_unlink(self.name)
        pass
    def test(self):
        self.comment="Test comment"
        comment="             "
        ret=lfc.lfc_setcomment(self.name,self.comment)
        lfc.lfc_getcomment(self.name, comment)
        return (comment.strip(),ret)
    def ret(self):
        return self.comment + '\x00'
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & (ret == test)
        else:
            retval = False
        return retval

class test_nonexisting_name(_test):
    def __init__(self):
        self.retVal = -1
    def info(self):
	return "Set comment on nonexisting file name: "
    def prepare(self):
        self.name="/fhgsdjfgsagfhs"
    def test(self):
        self.comment="Test comment"
        comment="                              "
        ret=lfc.lfc_setcomment(self.name,self.comment)
        lfc.lfc_getcomment(self.name, comment)
        return (comment.strip(),ret)
    def ret(self):
        return ""

    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = (ret == test)
        else:
            retval = False
        return retval


class lfc_setcomment_test:
    def __init__(self):
	self.name = "lfc_setcomment_test"
        self.tests=[test_setcomment_ok, test_update_comment, test_nonexisting_name]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_setcomment_test().run()
