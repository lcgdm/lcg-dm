import os, lfc, sys, commands
from testClass import _test

global testHome 

class test_root_dir(_test):
    def info(self):
        return "Test root dir: "
    def test(self):
        statg=lfc.lfc_filestatg()
        statgPtr=lfc.lfc_filestatgPtr(statg)
        ret=lfc.lfc_statg("/","", statgPtr)
        return (statg,ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.fileid=2L
        retval.filemode=16877
        retval.nlink=1
        retval.uid=0
        retval.gid=0
        retval.filesize=0L
        retval.atime=1184059742
        retval.mtime=1171381061
        retval.ctime=1171381061
        retval.fileclass=0
        retval.status='-'
        retval.guid=""
        retval.csumtype=""
        retval.csumvalue=""
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
        else:
            retval = False
        return retval


class test_existing_guid(_test):
    def info(self):
	return "Test existing guid: "
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_filestatg_guid_test"
        ret = lfc.lfc_creatg(self.name,self.guid,0664)
    def clean(self):
        lfc.lfc_unlink(self.name)
    def test(self):
        statg=lfc.lfc_filestatg()
        ret=lfc.lfc_statg("",self.guid, statg)
        return (statg,ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.fileid=24553L
        retval.filemode=33204
        retval.nlink=1
        retval.uid=137
        retval.gid=101
        retval.filesize=0L
        retval.atime=1184059742
        retval.mtime=1171381061
        retval.ctime=1171381061
        retval.fileclass=0
        retval.status='-'
        retval.guid=self.guid
        retval.csumtype=" "
        retval.csumvalue=" "
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
	    retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
            retval = retval & ( test.status == ret.status )
            retval = retval & ( test.guid == ret.guid )
#	    retval = retval & ( test.csumtype == ret.csumtype )
#	    retval = retval & ( test.csumvalue == ret.csumvalue )
        else:
            retval = False
        return retval


class test_nonexisting_guid(_test):
    def __init__(self):
        self.retVal = -1
    def info(self):
	return "Test nonexisting guid: "
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
        else:
            retval = False
        return retval


class test_nonexistent_file(_test):
    def __init__(self):
        self.retVal = -1
    def info(self):
        return "Check for nonexistent file: "
    def test(self):
        stat=lfc.lfc_filestat()
        statPtr=lfc.lfc_filestatPtr(stat)
        ret=lfc.lfc_stat("/nonexisting",statPtr)
        return stat,ret
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.fileid=0L
        retval.filemode=0
        retval.nlink=-1
        retval.uid=0
        retval.gid=0
        retval.filesize=0L
        retval.atime=0
        retval.mtime=0
        retval.ctime=0
        retval.fileclass=0
        retval.status=' '
        retval.guid=""
        retval.csumtype=""
        retval.csumvalue=""
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
        else:
            retval = False
        return retval



class lfc_statg_test:
    def __init__(self):
	self.name = "lfc_statg_test"
        self.tests=[test_root_dir,test_existing_guid,test_nonexistent_file]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_statg_test().run()
