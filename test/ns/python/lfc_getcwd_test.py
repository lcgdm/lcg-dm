import os, lfc, sys, commands, errno
from testClass import _test, _ntest

global testHome 

class test_ok(_test):
    def info(self):
	return "lfc_getcwd OK "
    def prepare(self):
        self.path = "/grid/dteam"
        lfc.lfc_chdir(self.path)
    def test(self):
        path = "                     "
        ret = lfc.lfc_getcwd(path,len(path))
        return ((ret.strip(),path.strip()),0)
    def ret(self):
        return (self.path,self.path+"\0")
    def compare(self, testVal, retVal):
        ((ret1,ret2), retRetVal) = retVal
        ((test1, test2), testRetVal) = testVal
        if ((ret1 == test1) & (test2 == ret2)):
            retval = True
        else:
            retval = False
        return retval

class test_ENOENT(_ntest):
    def info(self):
        return "lfc_getcwd on non-existing directory"
    def prepare(self):
        self.name = "/grid/dteam/python_getcwd_test"
        ret = lfc.lfc_mkdir(self.name,0755)
        lfc.lfc_chdir(self.name)
        lfc.lfc_rmdir(self.name)
    def test(self):
        self.path = "                                   "
        ret = lfc.lfc_getcwd(self.path,len(self.path))
        return ((ret,self.path,lfc.cvar.serrno),0)
    def ret(self):
        return (None,self.path,errno.ENOENT)
    def compare(self, testVal, retVal):
        ((ret1,ret2,reterr), retRetVal) = retVal
        ((test1, test2, testerr), testRetVal) = testVal
	print ret1, test1
	print test2, ret2
	print reterr, testerr
        if ((ret1 == test1) & (test2 == ret2) & (reterr == testerr)):
            retval = True
        else:
            retval = False
        return retval

class test_ERANGE(_ntest):
    def info(self):
        return "lfc_getcwd insifficient buffer space (ERANGE)"
    def prepare(self):
        self.name = "/grid/dteam/python_getcwd_test"
        ret = lfc.lfc_mkdir(self.name,0755)
        lfc.lfc_chdir(self.name)
        lfc.lfc_rmdir(self.name)
    def test(self):
        self.path = " "
        ret = lfc.lfc_getcwd(self.path,len(self.path))
        return ((ret,self.path,lfc.cvar.serrno),0)
    def ret(self):
        return (None,self.path,errno.ERANGE)
    def compare(self, testVal, retVal):
        ((ret1,ret2,reterr), retRetVal) = retVal
        ((test1, test2, testerr), testRetVal) = testVal
        if ((ret1 == test1) & (test2 == ret2) & (reterr == testerr)):
            retval = True
        else:
            retval = False
        return retval

class lfc_getcwd_test:
    def __init__(self):
	self.name = "lfc_getcwd_test"
        self.tests=[test_ok, test_ERANGE]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_getcwd_test().run()
