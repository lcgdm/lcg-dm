import os, lfc, sys, commands
from testClass import _test, _ntest

global testHome 

class test_ok(_test):
    def info(self):
	return "lfc_rename OK "
    def clean(self):
        lfc.lfc_unlink(self.new_name)
    def prepare(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_filerename_test"
        ret = lfc.lfc_creatg(self.name,self.guid,0664)
        self.stat=lfc.lfc_filestatg()
        ret=lfc.lfc_statg(self.name, "", self.stat)

    def test(self):
        self.new_name="/grid/dteam/python_filerenamed_test"
	ret = lfc.lfc_rename(self.name,self.new_name)
        stat=lfc.lfc_filestatg()
        ret=lfc.lfc_statg(self.new_name, "", stat)
        return (stat,ret)
    def ret(self):
        return self.stat
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
	    retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
            retval = retval & ( test.status == ret.status )
            retval = retval & ( test.guid == ret.guid )
        else:
            retval = False
        return retval

#	rename inexisting file
#	rename existing file but without privileges

class lfc_rename_test:
    def __init__(self):
	self.name = "lfc_rename_test"
        self.tests=[test_ok]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_rename_test().run()
