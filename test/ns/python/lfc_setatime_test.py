import os, lfc, sys, commands
from testClass import _test, _ntest

global testHome 

class test_setatime_ok(_test):
    def info(self):
        return "Set file size on existing file name"
    def prepare(self):
        guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/grid/dteam/python_setatime_test"
        ret = lfc.lfc_creatg(self.name,guid,0664)
        self.start=lfc.lfc_filestat()
        lfc.lfc_stat(self.name, self.start)
        
    def clean(self):
        lfc.lfc_unlink(self.name)
    def test(self):
        ret=lfc.lfc_setatime(self.name,None)
        stat=lfc.lfc_filestat()
        lfc.lfc_stat(self.name, stat)
        return (stat,ret)
    def ret(self):
        return self.start
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.atime != ret.atime )
        else:
            retval = False
        return retval


class lfc_setatime_test:
    def __init__(self):
	self.name = "lfc_setatime_test"
        self.tests=[test_setatime_ok]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_setatime_test().run()
