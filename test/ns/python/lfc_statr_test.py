import os, lfc, sys, commands
from testClass import _test

global testHome 

class test_existing_replica(_test):
    def info(self):
	return "Test existing replica: "
    def prepare(self):
        self.sfn="sfn://test-se.cern.ch/grid/dteam/hary/lfc_statr_test"
        self.guid=commands.getoutput('uuidgen').split('/n')[0]
        self.name="/grid/dteam/lfc_statr_test"
        lfc.lfc_creatg(self.name, self.guid, 0664)
        lfc.lfc_addreplica(self.guid, None, os.getenv("LFC_HOST"), self.sfn, '-', 'D', "", "")
    def clean(self):
        lfc.lfc_delreplica(self.guid, None, self.sfn)
        lfc.lfc_unlink(self.name)
    def test(self):
        statg=lfc.lfc_filestatg()
        ret=lfc.lfc_statr(self.sfn, statg)
        return (statg,ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.fileid=25159L
        retval.filemode=33204
        retval.nlink=1
        retval.uid=137
        retval.gid=101
        retval.filesize=0L
        retval.atime=1184059742
        retval.mtime=1171381061
        retval.ctime=1171381061
        retval.fileclass=0
        retval.status='-'
        retval.guid=self.guid
        retval.csumtype="w"
        retval.csumvalue=" "
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
	    retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
            retval = retval & ( test.status == ret.status )
            retval = retval & ( test.guid == ret.guid )
	    retval = retval & ( test.csumtype == ret.csumtype )
#	    retval = retval & ( test.csumvalue == ret.csumvalue )
        else:
            retval = False
        return retval


class test_nonexisting_replica(_test):
    def __init__(self):
        self.retVal = -1
    def info(self):
	return "Test nonexisting replica: "
    def test(self):
        statg=lfc.lfc_filestatg()
        ret=lfc.lfc_statr("sfn://test-se.cern.ch/grid/dteam/hary/lfc_statr_test1", statg)
        return (statg,ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.fileid=0L
        retval.filemode=0
        retval.nlink=-1
        retval.uid=0
        retval.gid=0
        retval.filesize=0L
        retval.atime=0
        retval.mtime=0
        retval.ctime=0
        retval.fileclass=0
        retval.status=' '
        retval.guid=""
        retval.csumtype=""
        retval.csumvalue=""
        return retval

    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
        else:
            retval = False
        return retval


class lfc_statr_test:
    def __init__(self):
	self.name = "lfc_statr_test"
        self.tests=[test_existing_replica, test_nonexisting_replica]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
lfc_statr_test().run()
