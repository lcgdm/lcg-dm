import os, lfc, sys
from testClass import _test, _ntest

global testHome 
global name

class test_ok(_test):
    def info(self):
	return "lfc_setacl OK "
    def clean(self):
        lfc.lfc_unlink(name)
        pass
    def prepare(self):
        self.guid = self.get_guid()
        lfc.lfc_creatg(name,self.guid,0664)
        nentries0, self.acls_list = lfc.lfc_getacl(name, lfc.CA_MAXACLENTRIES)

    def test(self):
        #nentries0, self.acls_list = lfc.lfc_getacl(name, lfc.CA_MAXACLENTRIES)
        self.acls_list[0].a_perm=7
	self.acls_list[1].a_perm=5
	self.acls_list[2].a_perm=3
        self.guid = self.get_guid()
        ret = lfc.lfc_setacl(name,self.acls_list)
        nentries, acls_list = lfc.lfc_getacl(name, lfc.CA_MAXACLENTRIES)
        return (acls_list,ret)

    def ret(self):
        return self.acls_list

    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retRetVal == testRetVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test[0].a_perm == ret[0].a_perm )
            retval = retval & ( test[1].a_perm == ret[1].a_perm )
            retval = retval & ( test[2].a_perm == ret[2].a_perm )
        else:
            retval = False
        return retval

class test_EPERM(_ntest):
    def info(self):
        return "lfc_setacl permission denied (EPERM)"
    def clean(self):
        lfc.lfc_unlink(self.name)
    def prepare(self):
        self.guid = self.get_guid()
        self.name = "/grid/dteam"
        nentries0, self.acls_list = lfc.lfc_getacl(name, lfc.CA_MAXACLENTRIES)
        print nentries0, self.acls_list

    def test(self):
        self.acls_list[0].a_perm=4
        self.acls_list[1].a_perm=4
        self.acls_list[2].a_perm=4
        ret = lfc.lfc_setacl(name,self.acls_list)
        nentries, acls_list = lfc.lfc_getacl(name, lfc.CA_MAXACLENTRIES)
        return (acls_list,ret)
    def ret(self):
        self.acls_list[0].a_perm=7
        self.acls_list[1].a_perm=7
        self.acls_list[2].a_perm=5
        return self.acls_list
 
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test[0].a_perm == ret[0].a_perm )
            retval = retval & ( test[1].a_perm == ret[1].a_perm )
            retval = retval & ( test[2].a_perm == ret[2].a_perm )
        else:
            retval = False
        return retval


class test_pd(_ntest):
    def info(self):
	return "lfc_creatg in permission denied"
    def test(self):
        self.guid = commands.getoutput('uuidgen').split('/n')[0]
        self.name = "/python_filecreatg_test_pd"
        ret = lfc.lfc_creatg(self.name,self.guid,0664)

        statg=lfc.lfc_filestatg()
        ret=lfc.lfc_statg("",self.guid, statg)
        return (statg,ret)
    def ret(self):
        retval=lfc.lfc_filestatg()
        retval.nlink=0
        retval.filesize=0L
        retval.atime=1184059742
        retval.mtime=1171381061
        retval.ctime=1171381061
        retval.fileclass=0
        retval.status=' '
        return retval
    def compare(self, testVal, retVal):
        (ret, retRetVal) = retVal
        (test, testRetVal) = testVal
        retval = True
        if (retRetVal == testRetVal):
            retval = retval & ( test.nlink == ret.nlink )
            retval = retval & ( test.filesize == ret.filesize )
            retval = retval & ( test.fileclass == ret.fileclass )
            #retval = retval & ( test.status == ret.status )
        else:
            retval = False
        return retval

class lfc_setacl_test:
    def __init__(self):
	self.name = "lfc_setacl_test"
        self.tests=[test_ok]

    def run(self):
        retVal = True
        for testclass in self.tests:
            testInstance = testclass()
            testInstance.prepare()
            ret1 = testInstance.compare(testInstance.test(), (testInstance.ret(), testInstance.getRetVal()))
            testInstance.clean()
            retVal = retVal & ret1
            if ret1:
                print "%-60s[OK]" % testInstance.info()
            else:
                print "%-60s[FAILED]" % testInstance.info()
        return retVal


os.environ['LFC_HOME'] = 'lxb1941.cern.ch:/grid/dteam'
os.environ['LFC_HOST'] = 'lxb1941.cern.ch'
testHome = "python_lfc_test"
name = "/grid/dteam/python_setacl_test"
lfc_setacl_test().run()
