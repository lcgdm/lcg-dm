/*
 * Copyright (C) 1999-2005 by CERN/IT/PDP/DM
 * All rights reserved
 */
 
	/* 
	 * Create a given number of replicas, per thread, for an LFN
	 * and get the time to list all the replicas for
	 * each thread. Doesn't delete created LFN.
	 * Command syntax is:
	 *	create_files [-d dir] [-r number_of_replicas] 
	 *                   [-t number_of_threads]	 
	 */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <uuid/uuid.h>
#include <sys/times.h>
#include <sys/time.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "Cns.h"
#include "Cns_api.h"
#include "Cthread_api.h"
#include "serrno.h"
#define NREPS 10
extern	char	*getenv();
extern	char	*optarg;
char Cnsdir[CA_MAXPATHLEN+1];
int nb_replicas = NREPS;

main(argc, argv)
int argc;
char **argv;
{
	int c;
	char Cnshost[CA_MAXHOSTNAMELEN+1];
	char filename[CA_MAXPATHLEN+1];
	char guid[CA_MAXGUIDLEN+1];
	char sfn_path[CA_MAXSFNLEN+1];
	const char* server_name = "castorgrid.cern.ch";
	char dir[CA_MAXPATHLEN+1];
	void *doit(void *);
	char *dp;
	char *endp;
	int errflg = 0;
	int i, j;
	int nb_threads = 1;
	char *p;
	char pid4print[11];
	struct Cns_filestat statbuf;
	struct Cns_filestatg statg;
	int *tid;
	struct tm *tm = NULL;
	struct timeval utime;
	long start_time_us, end_time_us;
	uuid_t uuid;
	time_t current_time;
#if defined(_WIN32)
	WSADATA wsadata;
#endif
	/* get command-line options */
	while ((c = getopt (argc, argv, "d:r:t:")) != EOF) {
		switch (c) {
		case 'd':
			strcpy(dir, optarg);
			break;
		case 'r':
			nb_replicas = strtol (optarg, &dp, 10);
			if (*dp != '\0') {
				fprintf (stderr, "invalid value for option -r\n", nb_replicas);
				errflg++;
			}
			if (nb_replicas > 999999) {
			  fprintf(stderr, "Maximum number of replicas is 999999\n");
			  errflg++;
			}
			break;
		case 't':
			nb_threads = strtol (optarg, &dp, 10);
			if (*dp != '\0' || nb_threads <= 0) {
				fprintf (stderr, "invalid value for option -t\n");
				errflg++;
			}
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, "WSAStartup unsuccessful\n");
		exit (SYERR);
	}
#endif

	/* set up directory name according to command line options. */
	sprintf (pid4print, "%d", getpid());
	if (dir) {
		if (*dir != '/') {
			if ((p = getenv ("CASTOR_HOME")) == NULL ||
			    strlen (p) + strlen (dir) + strlen (pid4print) + 20 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				sprintf (Cnsdir, "%s/%s", p, dir);
		} else {
			if (strlen (dir) + strlen (pid4print) + 19 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				strcpy (Cnsdir, dir);
		}
	} else {
		gethostname (Cnshost, sizeof(Cnshost));
		if ((p = getenv ("CASTOR_HOME")) == NULL ||
		    strlen (p) + strlen (Cnshost) + strlen (pid4print) + 37 > CA_MAXPATHLEN) {
			fprintf (stderr, "cannot set dir name\n");
			errflg++;
		} else {
			(void) time (&current_time);
			tm = localtime (&current_time);
			sprintf (Cnsdir, "%s/Cnstest/%s/%d%02d%02d", p, Cnshost,
			    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday);
		}
	}
	if (errflg) {
		fprintf (stderr, "usage: %s %s\n", argv[0],
		    "[-d dir] [-r number_of_replicas] [-t number_of_threads]");
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR);
	}

	/* create the directory if not there already */

	if (Cns_stat (Cnsdir, &statbuf) < 0) {
		if (serrno == ENOENT) {
			endp = strrchr (Cnsdir, '/');
			p = endp;
			while (p > Cnsdir) {
				*p = '\0';
				c = Cns_access (Cnsdir, F_OK);
				if (c == 0) break;
				p = strrchr (Cnsdir, '/');
			}
			while (p <= endp) {
				*p = '/';
				c = Cns_mkdir (Cnsdir, 0777);
				if (c < 0 && serrno != EEXIST) {
					fprintf (stderr, "cannot create %s: %s\n",
					    Cnsdir, sstrerror(serrno));
					errflg++;
					break;
				}
				p += strlen (p);
			}
		} else {
			fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
			errflg++;
		}
	}

	if (tm == NULL) {
		(void) time (&current_time);
		tm = localtime (&current_time);
	}


	/* generate an LFN with guid for each thread*/
	for (j = 0; j < nb_threads; j++) {
	  sprintf (filename, "%s/thread-%d",Cnsdir, j);

	  uuid_generate(uuid);
	  uuid_unparse(uuid, guid);
	  
	  if (Cns_creatg (filename, guid, 0666) < 0) {
	    fprintf (stderr, "%s: %s\n", filename, sstrerror(serrno));
	  }
	  
	  /* generate replicas */
	  for (i = 0; i < nb_replicas; i++) {
	    sprintf(sfn_path, "sfn://%s/dteam/caitriana/thread%d/replica%d", server_name, j, i);
	    if (Cns_addreplica(guid, NULL, server_name, sfn_path, '-', '\0', NULL, NULL) < 0) {
	      fprintf(stderr, "Cannot addreplica %s : %s : %s\n", filename, sfn_path,
		      sstrerror(serrno));
	      break;
	    }
	    
	  }
	}
	
	/* start threads going */
	gettimeofday( &utime, NULL);
	start_time_us = utime.tv_sec*1000000+utime.tv_usec;
	if (! errflg) {
		if ((tid = calloc (nb_threads, sizeof(int))) == NULL) {
			fprintf (stderr, "malloc error\n");
			errflg++;
		} else {
			for (i = 0; i < nb_threads; i++) {
			  if ((tid[i] = Cthread_create (&doit, &i)) < 0) {
			      fprintf (stderr, " error creating thread %d\n", i);
			      errflg++;
			    }
			}
			for (i = 0; i < nb_threads; i++) {
				(void)Cthread_join (tid[i], NULL);
			}
		}
	}
	gettimeofday( &utime, NULL);
	end_time_us = utime.tv_sec*1000000+utime.tv_usec;

	/* remove all the replicas */
	for (j = 0; j < nb_threads; j++) {
	  for (i = 0; i < nb_replicas; i++) {
	    sprintf(sfn_path, "sfn://%s/dteam/caitriana/thread%d/replica%d", server_name, j, i);
	    sprintf(filename, "%s/thread-%d", Cnsdir, j);
	    Cns_statg(filename, NULL, &statg);
	    if (Cns_delreplica(statg.guid, NULL, sfn_path) < 0) {
	      fprintf(stderr, "Cannot delreplica: %s : %s : %s\n", filename, sfn_path,
		      sstrerror(serrno));
	      break;
	    }
	  }
	}
#if defined(_WIN32)
	WSACleanup();
#endif
	if (errflg)
		exit (USERR);
	printf ("TOTAL: %d\n", end_time_us - start_time_us);
	
	exit (0);
}

void *
doit(arg)
void *arg;
{
  char filename[CA_MAXPATHLEN+1];
  struct Cns_filestatg statg;
  Cns_list list;
  struct Cns_filereplica* rp;
  int flags;
  struct timeval utime;
  long start_time_us, end_time_us;

  /* get correct LFN for this thread */
  sprintf(filename, "%s/thread-%d", Cnsdir, *(int *)arg);

  gettimeofday( &utime, NULL);
  start_time_us = utime.tv_sec*1000000+utime.tv_usec;

  /* list replicas for this LFN */
  flags = CNS_LIST_BEGIN;
  while ((rp = Cns_listreplica(filename, NULL, flags, &list)) != NULL) {
      if(flags == CNS_LIST_BEGIN) {
        flags = CNS_LIST_CONTINUE;
      }
  }
  (void) Cns_listreplica(filename, statg.guid, CNS_LIST_END, &list);
  
  gettimeofday( &utime, NULL);
  end_time_us = utime.tv_sec*1000000+utime.tv_usec;

  printf ("%d \t", end_time_us-start_time_us);

  return (NULL);
}
