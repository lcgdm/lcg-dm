/*
 * Copyright (C) 1999-2005 by CERN/IT/PDP/DM
 * All rights reserved
 */

	/* Inserts a given number of files then deletes them,
	 * measuring the deletion rate, using a given number of
	 * threads. 
	 */

	/* If dir is specified on the command line but does not start with
	 * a slash, it is prefixed by $CASTOR_HOME..
	 * Cnshost is set to the value of the environment variable CNS_HOST.
	 * If not set, the value is taken from shift.conf.
	 * If not set there either, use localhost.
	 * If nb_threads is not specified, the program is single threaded.
	 * Command syntax is:
	 *	delete_files [-d dir] [-f number of files] [-t nb_threads] 
	 *   [-r use relative paths] [-n nesting level of directories] [-x use
	 *	transactions] [-c number of deletes per transaction]
	 * N.B. Total number of files set must be < 1,000,000 
	 */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/types.h>
#include <uuid/uuid.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "Cns_api.h"
#include "Cns.h"
#include "serrno.h"
#include "Cthread_api.h"
#define NFILES 10
extern	char	*getenv();
extern	char	*optarg;

char Cnsdir[CA_MAXPATHLEN+1];
char toplevel[CA_MAXPATHLEN+1];
int nb_threads = 1;
int relative = 0;
int transactions = 0;
int nb_files = NFILES;
int nb_deletes = 0;
long commit_interval = 1;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char Cnshost[CA_MAXHOSTNAMELEN+1];
	char thread_dir[CA_MAXPATHLEN+1];
	char guid[CA_MAXGUIDLEN+1];
	char append[CA_MAXPATHLEN+1];
	uuid_t uuid;
	time_t current_time;
	char dir[CA_MAXPATHLEN+1];
	void *doit(void *);
	char *dp;
	char *endp;
	int errflg = 0;
	int dir_flag = 0;
	int depth = 0;
	int i,j;
	char *p;
	char pid4print[11];
	struct Cns_filestat statbuf;
	struct Cns_filestat dirstat;
	int *tid;
	struct tm *tm = NULL;
	struct timeval utime;
	long start_time_us, end_time_us;
	char filename[CA_MAXPATHLEN+1];
	int num_to_delete = 0;
	clock_t start_time_ms, end_time_ms;
#if defined(_WIN32)
	WSADATA wsadata;
#endif
	/* get command-line options */
	while ((c = getopt (argc, argv, "d:f:n:c:t:rx")) != EOF) {
		switch (c) {
		case 'd':
			strcpy(dir, optarg);
			break;
		case 'f':
			nb_files = strtol (optarg, &dp, 10);
			if (*dp != '\0') {
			  fprintf (stderr, "invalid value for option -f\n");
			  errflg++;
			}
			break;
		case 'n':
		        depth = strtol (optarg, &dp, 10);
			if (depth < 0) {
			        fprintf (stderr, "invalid value for option -n\n");
				errflg++;
			}
			break;
		case 't':
  		        nb_threads = strtol(optarg, &dp, 10);
			if (nb_threads < 0) {
			  fprintf (stderr, "invalid value for option -n\n");
			  errflg++;
			}
			break;
		case 'r':
		        relative = 1;
			break;
		case 'x':
		        transactions = 1;
			break;
		case 'c':
		        commit_interval = strtol (optarg, &dp, 10);
			if (*dp != '\0' || commit_interval <= 0) {
				fprintf (stderr, "invalid value for option -c\n");
				errflg++;
			}
			break;
		case '?':
			errflg++;
			break;
		default:
		        break;
		}
	}

#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, "WSAStartup unsuccessful\n");
		exit (SYERR);
	}
#endif

	/* set up directory path */
	sprintf (pid4print, "%d", getpid());
	if (dir) {
		if (*dir != '/') {
			if ((p = getenv ("CASTOR_HOME")) == NULL ||
			    strlen (p) + strlen (dir) + strlen (pid4print) + 20 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				sprintf (Cnsdir, "%s/%s", p, dir);
		} else {
			if (strlen (dir) + strlen (pid4print) + 19 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				strcpy (Cnsdir, dir);
		}
	} else {
		gethostname (Cnshost, sizeof(Cnshost));
		if ((p = getenv ("CASTOR_HOME")) == NULL ||
		    strlen (p) + strlen (Cnshost) + strlen (pid4print) + 37 > CA_MAXPATHLEN) {
			fprintf (stderr, "cannot set dir name\n");
			errflg++;
		} else {
			(void) time (&current_time);
			tm = localtime (&current_time);
			sprintf (Cnsdir, "%s/Cnstest/%s/%d%02d%02d", p, Cnshost,
			    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday);
		}
	}

	if (errflg) {
		fprintf (stderr, "usage: %s %s %s\n", argv[0],
		    "[-d dir] [-f num_files]");

#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR);
	}
	
	sprintf(toplevel, "%s",Cnsdir);

	/* if extra directories were requested, add them to the
	   pathname */
	if (depth) {
	  for ( i = 1; i <= depth; i++) {
	    sprintf(append,"/dir%d",i);
	    strcat(Cnsdir,append);
	  }
	}
	
	/* if the top level directory doesn't already exist, flag that
	   you are going to create it */
	if (Cns_stat (toplevel, &statbuf) < 0) {
	  dir_flag = 1;
	}

	/* create the directory if not there already */
	if (Cns_stat (Cnsdir, &statbuf) < 0) {
		if (serrno == ENOENT) {
			endp = strrchr (Cnsdir, '/');
			p = endp;
			while (p > Cnsdir) {
				*p = '\0';
				c = Cns_access (Cnsdir, F_OK);
				if (c == 0) break;
				p = strrchr (Cnsdir, '/');
			}
			while (p <= endp) {
				*p = '/';
				c = Cns_mkdir (Cnsdir, 0777);
				if (c < 0 && serrno != EEXIST) {
					fprintf (stderr, "cannot create %s: %s\n",
					    Cnsdir, sstrerror(serrno));
					errflg++;
					break;
				}
				p += strlen (p);
			}
		} else {
			fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
			errflg++;
		}
	}

	/* if multi-threaded, create an extra directory for each
           thread */
        if (nb_threads > 1) {
          for (i=0; i< nb_threads; i++) {
            sprintf(thread_dir, "%s", Cnsdir);
            sprintf(append, "/thread-%d", i);
            strcat(thread_dir, append);
            if (Cns_stat (thread_dir, &statbuf) <0) {               
	      if (Cns_mkdir (thread_dir, 0777) < 0) {
		fprintf(stderr, "cannot create %s: %s\n", thread_dir, sstrerror(serrno));
		errflg++;
		break;
              }
            }
          }
        }

	/* create the files */
	for (j=0; j < nb_threads; j++) {
          strcpy(dir, Cnsdir);
          strcat(dir, "/");
          if (nb_threads > 1)
            sprintf(thread_dir, "thread-%d/",j);
          else sprintf(thread_dir, "");           
	  strcat(dir,thread_dir);
          if (relative) {
            if ( Cns_chdir(dir) < 0) {
              fprintf (stderr, "Cannot chdir %s: %s\n", Cnsdir, sstrerror(serrno));
            }
          }
	  for (i = 0; i < nb_files; i++) {
	    sprintf (filename, "%s/file%d", dir, i);
	    uuid_generate(uuid);
	    uuid_unparse(uuid, guid);
	    if (Cns_creatg (filename, guid, 0666) < 0) {
	      fprintf (stderr, "%s: %s\n", filename, sstrerror(serrno));
	      break;
	    }
	  }
	}

	/* start all the threads going */
        gettimeofday( &utime, NULL);
        start_time_us = utime.tv_sec*1000000+utime.tv_usec;
        if (! errflg) {
	  if ((tid = calloc (nb_threads, sizeof(int))) == NULL) {
	    fprintf (stderr, "malloc error\n");
	    errflg++;
	  } else {
	    for (i = 0; i < nb_threads; i++) {
	      if ((tid[i] = Cthread_create (&doit, &i)) < 0) {
		fprintf (stderr, " error creating thread %d\n", i);
		errflg++;
	      }
	    }
	    for (i = 0; i < nb_threads; i++) {
	      (void)Cthread_join (tid[i], NULL);
	    }
	  }
        }
	gettimeofday( &utime, NULL);
        end_time_us = utime.tv_sec*1000000+utime.tv_usec;

	sprintf(dir, "%s/../", toplevel);
	if (Cns_chdir(dir) < 0) {
	  fprintf (stderr, "Cannot chdir %s: %s\n", dir, sstrerror(serrno));
	}

	/* clear the undeleted files from the thread directories */
	if (nb_threads > 1) {
	  for (j=0; j < nb_threads; j++) {
	    sprintf(thread_dir, "%s", Cnsdir);
            sprintf(append, "/thread-%d", j);
            strcat(thread_dir, append);
	    if (Cns_stat (thread_dir, &dirstat) < 0) {
	      fprintf(stderr, "%s: %s\n", thread_dir, sstrerror(serrno));
	    }
	    num_to_delete = dirstat.nlink;
	    if (num_to_delete > 0) {
	      for (i=nb_files-1; i > nb_files-1 - num_to_delete; i--) {
		sprintf (filename, "%s/file%d", thread_dir, i);
		if (Cns_unlink(filename) < 0) {
		  fprintf(stderr, "%s: %s\n", filename, sstrerror(serrno));
		}
	      }
	    }
	    if (Cns_rmdir (thread_dir) < 0 ) {
	      fprintf (stderr, "%s: %s\n", thread_dir, sstrerror(serrno));
	    }
	  }
	}
	if (depth > 0) {
	  /* recursively delete the extra directories */
	  if (Cns_rmdir (Cnsdir) < 0) {
	    fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
	  }
	  for (i=depth-1; i>=1; i--) {
	    sprintf(dir,"%s",toplevel);
	    for (j=1; j<=i; j++) {
	      sprintf(append, "/dir%d", j);
	      strcat(dir, append);
	    }
	    if (Cns_rmdir (dir) < 0) {
	      fprintf (stderr, "%s: %s\n", dir, sstrerror(serrno));
	    }
	  }
	}
	if (dir_flag != 0) {
	  /* if you created the top level directory, remove it */
	  if (Cns_rmdir (toplevel) < 0) {
	    fprintf (stderr, "%s: %s\n", toplevel, sstrerror(serrno));
	  }
	}
	if (errflg)
	  exit (USERR);
	printf ("%8.4f\n", 1000000.*nb_deletes/(end_time_us - start_time_us));
	/*	printf(" %lu \t %lu \t %d\n", start_time_us, end_time_us, nb_deletes); */
	exit (0);
}

void *
doit(arg)
void *arg;
{
  char dir[CA_MAXPATHLEN+1];
  char thread_dir[CA_MAXPATHLEN+1];
  char filename[CA_MAXPATHLEN+1];
  int i, j = 0;

  /* get correct directory for this thread */
  strcpy(dir, Cnsdir);
  strcat(dir, "/");
  
  if (nb_threads > 1 ){
    sprintf(thread_dir, "thread-%d/",*(int *)arg);
    strcat(dir,thread_dir);
  }

  /* delete the files from this thread's directory */
  if (relative) {
    /* cd to the right directory */
    if (Cns_chdir(dir) < 0) {
      fprintf (stderr, "Cannot chdir %s: %s\n", Cnsdir, sstrerror(serrno));
    }
    i = 0;
    while (nb_deletes < nb_files) {
      if(transactions) Cns_starttrans (NULL, "delete_files_rate");
      for (j=0; j < commit_interval && j < nb_files; j++) {
	sprintf (filename, "file%d", i);
	if (Cns_unlink (filename) < 0) {
	  fprintf (stderr, "%s: %s\n", filename, sstrerror(serrno));
	  if (transactions) Cns_aborttrans();
	}
	nb_deletes++;
	i++;
      }
      if (transactions) Cns_endtrans();
    }
    /* cd back to the base directory */
    //   sprintf(dir,"%s/../",toplevel);
    /*    sprintf(dir, "%s/", Cnsdir);
    if (Cns_chdir(dir) < 0) {
      fprintf (stderr, "Cannot chdir %s: %s\n", dir, sstrerror(serrno));
      }*/
  }
  else {   /* no chdir */
    i = 0;
    while (nb_deletes < nb_files) {
      if(transactions) Cns_starttrans (NULL, "delete_files_rate");
      for (j=0; j < commit_interval && j < nb_files; j++) {
	sprintf (filename, "%s/file%d", dir, i);
	if (Cns_unlink (filename) < 0) {
	  fprintf (stderr, "%s: %s\n", filename, sstrerror(serrno));
	  if (transactions) Cns_aborttrans();
	}
	nb_deletes++;
	i++;
      }
      if (transactions) Cns_endtrans();
    }
  }
  sprintf(dir, "%s/%s", Cnsdir, thread_dir);
  /*  if (Cns_rmdir (dir) < 0) {
    fprintf (stderr, "%s: %s\n", dir, sstrerror(serrno));
    }*/
}
