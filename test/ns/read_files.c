/*
 * Copyright (C) 1999-2004 by CERN/IT/PDP/DM
 * All rights reserved
 */

	/* 
	 * Create a given number of files in a directory, for each
	 * thread, and get the time to read through the directory. 
	 *
	 * Usage:
	 *      read_files -d [dir] [-f number_of_files] [-t number_of_threads] 
	 */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <uuid/uuid.h>
#include <sys/times.h>
#include <sys/time.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "Cns.h"
#include "Cns_api.h"
#include "Cthread_api.h"
#include "serrno.h"
#define NFILES 10
extern	char	*getenv();
extern	char	*optarg;

char Cnsdir[CA_MAXPATHLEN+1];
int nb_files = NFILES;
int nb_threads = 1;
int relative = 0;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char Cnshost[CA_MAXHOSTNAMELEN+1];
    	char append[CA_MAXPATHLEN+1];
	char thread_dir[CA_MAXPATHLEN+1];
	char filename[CA_MAXPATHLEN+1];
	char fnbuf[CA_MAXPATHLEN+1];
	char guid[CA_MAXGUIDLEN+1];
	char dir[CA_MAXPATHLEN+1];
	void *doit(void *);
	char *dp;
	char *endp;
	int errflg = 0;
	int i,j;
	char *p;
	char pid4print[11];
	struct Cns_filestat statbuf;
	int *tid;
	struct tm *tm = NULL;
	struct timeval utime;
	long start_time_us, end_time_us;
	int depth = 0;
	time_t current_time;
	uuid_t uuid;
#if defined(_WIN32)
	WSADATA wsadata;
#endif
	/* get command-line options */
	while ((c = getopt (argc, argv, "d:f:t:")) != EOF) {
		switch (c) {
		case 'd':
			strcpy(dir, optarg);
			break;
		case 'f':
			nb_files = strtol (optarg, &dp, 10);
			if (*dp != '\0') {
				fprintf (stderr, "invalid value for option -f\n", nb_files);
				errflg++;
			}
			if (nb_files > 999999) {
			  fprintf(stderr, "Maximum number of files in directory is 999999\n");
			  errflg++;
			}
			break;
		case 't':
			nb_threads = strtol (optarg, &dp, 10);
			if (*dp != '\0' || nb_threads <= 0) {
				fprintf (stderr, "invalid value for option -t\n");
				errflg++;
			}
			break;
		case 'r':
		        relative = 1;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, "WSAStartup unsuccessful\n");
		exit (SYERR);
	}
#endif
	
	/* set up directory name according to command line options */
	sprintf (pid4print, "%d", getpid());
	if (dir) {
		if (*dir != '/') {
			if ((p = getenv ("CASTOR_HOME")) == NULL ||
			    strlen (p) + strlen (dir) + strlen (pid4print) + 20 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				sprintf (Cnsdir, "%s/%s", p, dir);
		} else {
			if (strlen (dir) + strlen (pid4print) + 19 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				strcpy (Cnsdir, dir);
		}
	} else {
		gethostname (Cnshost, sizeof(Cnshost));
		if ((p = getenv ("CASTOR_HOME")) == NULL ||
		    strlen (p) + strlen (Cnshost) + strlen (pid4print) + 37 > CA_MAXPATHLEN) {
			fprintf (stderr, "cannot set dir name\n");
			errflg++;
		} else {
			(void) time (&current_time);
			tm = localtime (&current_time);
			sprintf (Cnsdir, "%s/Cnstest/%s/%d%02d%02d", p, Cnshost,
			    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday);
		}
	}
	if (errflg) {
		fprintf (stderr, "usage: %s %s\n", argv[0],
		    "[-d dir] [-f number_of_files] [-t number_of_threads]");
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR);
	}

	/* create the directory if not there already */

	if (Cns_stat (Cnsdir, &statbuf) < 0) {
		if (serrno == ENOENT) {
			endp = strrchr (Cnsdir, '/');
			p = endp;
			while (p > Cnsdir) {
				*p = '\0';
				c = Cns_access (Cnsdir, F_OK);
				if (c == 0) break;
				p = strrchr (Cnsdir, '/');
			}
			while (p <= endp) {
				*p = '/';
				c = Cns_mkdir (Cnsdir, 0777);
				if (c < 0 && serrno != EEXIST) {
					fprintf (stderr, "cannot create %s: %s\n",
					    Cnsdir, sstrerror(serrno));
					errflg++;
					break;
				}
				p += strlen (p);
			}
		} else {
			fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
			errflg++;
		}
	}

	/* if multi-threaded, create an extra directory for each
	   thread */
	if (nb_threads > 1) {
	  for (i=0; i< nb_threads; i++) {
	    sprintf(thread_dir, "%s", Cnsdir);
	    sprintf(append, "/thread-%d", i);
	    strcat(thread_dir, append);
	    if (Cns_stat (thread_dir, &statbuf) <0) {
	      if (Cns_mkdir (thread_dir, 0777) < 0) {
		fprintf(stderr, "cannot create %s: %s\n",								thread_dir, sstrerror(serrno));
		errflg++;
		break;		
	      }			
	    }		
	  }
 	}

	/* set up filename base */
	if (tm == NULL) {
		(void) time (&current_time);
		tm = localtime (&current_time);
	}

	sprintf (filename, "%02d%02d%02d_%d_",
		   tm->tm_hour, tm->tm_min, tm->tm_sec, getpid());

	/* create all the files in the correct directories */
	for (j=0; j < nb_threads; j++) {  
	  strcpy(dir, Cnsdir);
	  strcat(dir, "/");
	  if (nb_threads > 1) 
	    sprintf(thread_dir, "thread-%d/",j);
	  else sprintf(thread_dir, ""); 
	  strcat(dir,thread_dir);
	  if (relative) {
	    if ( Cns_chdir(dir) < 0) {
	      fprintf (stderr, "Cannot chdir %s: %s\n", Cnsdir, sstrerror(serrno));
	    }
	    strcpy (fnbuf, filename);
	  }
	  else {
	    strcpy(fnbuf, dir);
	    strcat(fnbuf, filename);
	  }
	  p = fnbuf + strlen (fnbuf);

	  for (i = 0; i < nb_files; i++) {
#if defined(_WIN32)
	    sprintf (p, "%d", rand());
#else
	    sprintf (p, "%d", lrand48());
#endif
	    uuid_generate(uuid);
	    uuid_unparse(uuid, guid);

	    if (Cns_creatg (fnbuf, guid, 0777) < 0) {
	      fprintf (stderr, "Error in creatg: %s: %s\n", fnbuf, sstrerror(serrno));
	      break;
	    }
	  }
	}

	/* start all the threads going */
	gettimeofday( &utime, NULL);
	start_time_us = utime.tv_sec*1000000+utime.tv_usec;
	if (! errflg) {
		if ((tid = calloc (nb_threads, sizeof(int))) == NULL) {
			fprintf (stderr, "malloc error\n");
			errflg++;
		} else {
			for (i = 0; i < nb_threads; i++) {
			  if ((tid[i] = Cthread_create (&doit, &i)) < 0) {
			      fprintf (stderr, " error creating thread %d\n", i);
			      errflg++;
			    }
			}
			for (i = 0; i < nb_threads; i++) {
				(void)Cthread_join (tid[i], NULL);
			}
		}
	}
	gettimeofday( &utime, NULL);
	end_time_us = utime.tv_sec*1000000+utime.tv_usec;

#if defined(_WIN32)
	WSACleanup();
#endif
	if (errflg)
		exit (USERR);
	printf ("%d\n", end_time_us - start_time_us);
	
	exit (0);
}

void *
doit(arg)
void *arg;
{
  Cns_DIR *dirp;
  struct Cns_direnstatg *entry;
  char this_entry[CA_MAXPATHLEN+1];
  char thread_dir[CA_MAXPATHLEN+1];
  char dir[CA_MAXPATHLEN+1];
  char guid[CA_MAXGUIDLEN+1];
  struct Cns_filestatg stat;

  /* get correct directory for this thread */
  strcpy(dir, Cnsdir);
  strcat(dir, "/");

  if (nb_threads > 1 ){
	  sprintf(thread_dir, "thread-%d/",*(int *)arg);
	  strcat(dir,thread_dir);
  }

  if (relative) {
    if ( Cns_chdir(dir) < 0) {
      fprintf (stderr, "Cannot chdir %s: %s\n", Cnsdir, sstrerror(serrno));
    }
  }

  /* opendir */
  dirp = Cns_opendir(dir);

  /* readdir for all files */
  while ((entry = Cns_readdirg(dirp)) != NULL) {
    sprintf(this_entry, "%s/%s", dir, entry->d_name);
    sprintf(guid, "%s", entry->guid);
  }

  /* closedir */
  Cns_closedir(dirp);

  return (NULL);
}
