#!/usr/bin/perl

#
# Performance test : creates files for different number of files and threads.
# A fixed number of files (currently set to 5000) are inserted and the
# insert rate calculated. This is repeated 5 times for each reading.
#

use strict;
use warnings;
use Getopt::Long;

use FindBin;

# First do it using absolute pathnames
my $outfile = "results/insert/insert-rate-rel-notrans.dat";
get_results($outfile, "yes", 0, "no", 0);

# then with relative pathnames
$outfile = "results/insert/insert-rate-rel-trans.dat";
#get_results($outfile, "yes", 0, "yes", 100);

# the subroutine for running the command with the given parameters.

sub get_results {

  my($file, $relative, $depth, $transactions, $commit_size) = @_;
  
  my $optargs = "";
  if ($relative eq "yes") {
    $optargs = $optargs."-r ";
  }
  if ($depth > 0) {
    $optargs = $optargs."-n $depth ";
  }
  if ($transactions eq "yes") {
    $optargs = $optargs."-x ";
  }
  if ($commit_size > 0) {
    $optargs = $optargs."-c $commit_size ";
  }
  
  # run the "create_files_rate" command with the different number of files and threads
  # and get the times back in a file
  
  my @threads = (1);
  my ($num_threads, $result);
  
  open(OUTFILE, '>', "$file") or die "Can't open $file: $!\n";
  
  print OUTFILE "num_threads, total_files, rate (inserts/sec)\n";
  
  my($sec, $min, $hour, $day, $mon, $year, @rest) = localtime(time);
  $year += 1900;
  my $timestamp = "$year-$mon-$day-$hour:$min:$sec";
  my $i = 0;
  my $total_files = 1000;
  my @data; 
  foreach $num_threads (@threads) {
    $i = 0;
    while ($i < 5) {
      my $filename =  "results/insert/$num_threads-thread-insert-rate-$i.dat";
      `./create_files_rate -d /grid/caitriana/test/$timestamp-$num_threads -f $total_files -t $num_threads $optargs > $filename`;
      open(INFILE, "$filename") or die "Can't open $filename: $!\n";
      my $start_time = 0;
      my $end_time = 0;
      my $num_files = 0;
      while (<INFILE>) {
        chomp;
	@data = split /\s+/, $_;
	$start_time = $data[1];
	$end_time = $data[2];
	$num_files = $data[3];
      }
      close INFILE;
      my $rate = 1000000*$num_files/($end_time-$start_time);
      print OUTFILE "$num_threads \t $start_time \t $end_time \t $num_files \t $rate\n";
      `lfc-rm -rf /grid/caitriana/test/$timestamp-$num_threads`;
      $i+=1;
    }
    # delete the files that have just been produced, before testing with a different number of threads
    `lfc-rm -rf /grid/caitriana/test/`;
  }
  close OUTFILE;
}
