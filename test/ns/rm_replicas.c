/*
 * Copyright (C) 1999-2004 by CERN/IT/PDP/DM
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: rm_replicas.c,v $ $Revision: 1.3 $ $Date: 2005/04/08 13:12:50 $ CERN IT-PDP/DM Jean-Philippe Baud";
#endif /* not lint */

/** remove the specified replicas... **/

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <uuid/uuid.h>
#include <sys/times.h>
#include <sys/types.h>
#include <unistd.h>
#include "Cns.h"
#include "Cns_api.h"
#include "serrno.h"
#define NFILES 10
extern	char	*getenv();
extern	char	*optarg;

main(argc, argv)
int argc;
char **argv;
{
	int c;
	char Cnshost[CA_MAXHOSTNAMELEN+1];
	char filename[CA_MAXPATHLEN+1];
	char Cnsdir[CA_MAXPATHLEN+1];
	char sfn_path[CA_MAXSFNLEN+1];
	char dir[CA_MAXPATHLEN+1];
	char *fn = NULL;
	char *dp;
	char *endp;
	int errflg = 0;
	int i;
	char *p;
	char pid4print[11];
	struct Cns_filestat statbuf;
	struct Cns_filestatg statg;
	Cns_list list;
	struct Cns_filereplica* rp;
	int flags;
	
	while ((c = getopt (argc, argv, "d:f:")) != EOF) {
	  switch (c) {
	  case 'd':
	    strcpy(dir, optarg);
	    break;
	  case 'f':
	    fn = optarg;
	    break;
	  case '?':
	    errflg++;
	    break;
	  default:
	    break;
	  }
	}

	sprintf(filename, "%s", fn);

	if (dir) {
	  if (*dir != '/') {
	    if ((p = getenv ("CASTOR_HOME")) == NULL ||
		strlen (p) + strlen (dir) + strlen (pid4print) + 20 > CA_MAXPATHLEN) {
	      fprintf (stderr, "invalid value for option -d\n");
	      errflg++;
	    } else
	      sprintf (Cnsdir, "%s/%s/", p, dir);
	  } else {
	    if (strlen (dir) + strlen (pid4print) + 19 > CA_MAXPATHLEN) {
	      fprintf (stderr, "invalid value for option -d\n");
	      errflg++;
	    } else
	      strcpy (Cnsdir, dir);
	  }
	} else {
	  gethostname (Cnshost, sizeof(Cnshost));
	  if ((p = getenv ("CASTOR_HOME")) == NULL ||
	      strlen (p) + strlen (Cnshost) + strlen (pid4print) + 37 > CA_MAXPATHLEN) {
	    fprintf (stderr, "cannot set dir name\n");
	    errflg++;
	  } else {
	    fprintf (stderr, "Please set a directory name\n");
	    errflg++;
	  }
	}
	
	strcat(Cnsdir, "/");
	strcat(Cnsdir, filename);
	printf("Filepath %s\n", Cnsdir);

	flags = CNS_LIST_BEGIN;
	while ((rp = Cns_listreplica(Cnsdir, NULL, flags, &list)) != NULL) {
	  if(flags == CNS_LIST_BEGIN) {
	    flags = CNS_LIST_CONTINUE;
	  }
	  Cns_statr(rp -> sfn, &statg);
	  Cns_delreplica(statg.guid, NULL, rp -> sfn); 
	}
	(void) Cns_listreplica(Cnsdir, statg.guid, CNS_LIST_END, &list);
	
	exit (0);
}
