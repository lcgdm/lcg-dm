/*
 * Copyright (C) 1999-2005 by CERN/IT/PDP/DM
 * All rights reserved
 */

	/* create a given number of files, then get the time to query for
	 * one guid. 
	 */

	/* If dir is specified on the command line but does not start with
	 * a slash, it is prefixed by $CASTOR_HOME..
	 * Cnshost is set to the value of the environment variable CNS_HOST.
	 * If not set, the value is taken from shift.conf.
	 * If not set there either, use localhost.
	 * Command syntax is:
	 *	query_files [-d dir] [-f num_files] [-t num_threads]
	 */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <uuid/uuid.h>
#include <sys/times.h>
#include <sys/time.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "Cns.h"
#include "Cns_api.h"
#include "Cthread_api.h"
#include "serrno.h"
#define NFILES 10
#define MAX_FILES 3
extern	char	*getenv();
extern	char	*optarg;
char Cnsdir[CA_MAXPATHLEN+1];
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char Cnshost[CA_MAXHOSTNAMELEN+1];
	char dir[CA_MAXPATHLEN+1];
	void *doit(void *);
	char *dp;
	char *endp;
	int errflg = 0;
	int i,j,k;
	int dir_flag = 0;
	char *p;
	char pid4print[11];
	struct Cns_filestat statbuf;
	struct Cns_filestatg statg;
	int *tid;
	struct tm *tm = NULL;
	char filename[CA_MAXPATHLEN+1];
	char append[CA_MAXPATHLEN+1];
	char fnbuf[CA_MAXPATHLEN+1];
	char subdir[CA_MAXPATHLEN+1];
	int nb_files = NFILES;
	int nb_threads = 1;
	int current_numfiles = 0;
	char guid[CA_MAXGUIDLEN+1];
	uuid_t uuid;
	time_t current_time;
#if defined(_WIN32)
	WSADATA wsadata;
#endif
	/* get command-line options */
	while ((c = getopt (argc, argv, "d:f:t:")) != EOF) {
		switch (c) {
		case 'd':
			strcpy(dir, optarg);
			break;
		case 'f':
			nb_files = strtol (optarg, &dp, 10);
			if (*dp != '\0') {
			  fprintf (stderr, "invalid value for option -f\n");
			  errflg++;
			}
			break;
		case 't':
		  nb_threads = strtol (optarg, &dp, 10);
		  if (*dp != '\0' || nb_threads <= 0) {
		    fprintf (stderr, "invalid value for option -t\n");
		    errflg++;
		  }
		  break;
		}
	}

#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, "WSAStartup unsuccessful\n");
		exit (SYERR);
	}
#endif

	/* set up directory name according to command line options */
	sprintf (pid4print, "%d", getpid());
	if (dir) {
		if (*dir != '/') {
			if ((p = getenv ("CASTOR_HOME")) == NULL ||
			    strlen (p) + strlen (dir) + strlen (pid4print) + 20 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				sprintf (Cnsdir, "%s/%s", p, dir);
		} else {
			if (strlen (dir) + strlen (pid4print) + 19 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				strcpy (Cnsdir, dir);
		}
	} else {
		gethostname (Cnshost, sizeof(Cnshost));
		if ((p = getenv ("CASTOR_HOME")) == NULL ||
		    strlen (p) + strlen (Cnshost) + strlen (pid4print) + 37 > CA_MAXPATHLEN) {
			fprintf (stderr, "cannot set dir name\n");
			errflg++;
		} else {
			(void) time (&current_time);
			tm = localtime (&current_time);
			sprintf (Cnsdir, "%s/Cnstest/%s/%d%02d%02d", p, Cnshost,
			    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday);
		}
	}

	if (errflg) {
		fprintf (stderr, "usage: %s %s %s\n", argv[0],
		    "[-d dir] [-f num_files] [-t num_threads]");

#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR);
	}
	
	/* create the directory if not there already */
	if (Cns_stat (Cnsdir, &statbuf) < 0) {
		if (serrno == ENOENT) {
			endp = strrchr (Cnsdir, '/');
			p = endp;
			while (p > Cnsdir) {
				*p = '\0';
				c = Cns_access (Cnsdir, F_OK);
				if (c == 0) break;
				p = strrchr (Cnsdir, '/');
			}
			while (p <= endp) {
				*p = '/';
				c = Cns_mkdir (Cnsdir, 0777);
				if (c < 0 && serrno != EEXIST) {
					fprintf (stderr, "cannot create %s: %s\n",
					    Cnsdir, sstrerror(serrno));
					errflg++;
					break;
				}
				dir_flag = 1;
				p += strlen (p);
			}
		} else {
			fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
			errflg++;
		}
	}

	/* extra directories needed if number of files is greater */
	/* than the maximum allowed per directory in the LFC schema */
	if (nb_files > MAX_FILES) {
	  for (j=0; j < nb_files; j+=MAX_FILES) {
	    sprintf(subdir, "%s", Cnsdir);
	    sprintf(append, "/%d", j);
	    strcat(subdir, append);
	    if (Cns_mkdir (subdir, 0777) < 0) {
	      fprintf(stderr, "cannot create %s: %s\n",
		      subdir, sstrerror(serrno));
	      break;
	    }
	  }
	}

	(void) time (&current_time);
	tm = localtime (&current_time);

	strcpy(dir, Cnsdir);
	strcat(dir, "/");
	if (nb_files > MAX_FILES) 
	  strcat(dir, "0/");
	
	sprintf (filename, "%02d%02d%02d_%d_",
		 tm->tm_hour, tm->tm_min, tm->tm_sec, getpid());
	
	strcpy(fnbuf, dir);
	strcat(fnbuf, filename);
	p = fnbuf + strlen (fnbuf);

	k = 0;
	/* create the files */
	for (i = 0; i < nb_files; i++) {
	  if (k == MAX_FILES) {
	    strcpy(dir, Cnsdir);
	    strcat(dir, "/");
	    
	    sprintf(subdir, "%d/", current_numfiles);
	    strcat(dir, subdir);
	    
	    strcpy(fnbuf, dir);
	    strcat(fnbuf, filename);
	    
	    /* pointer to the filename */
	    p = fnbuf + strlen (fnbuf);
	    k = 0;
	  }
	  uuid_generate(uuid);
	  uuid_unparse(uuid, guid);
	  sprintf (p, "%d", lrand48());
	  if (Cns_creatg (fnbuf, guid, 0666) < 0) {
	    fprintf (stderr, "%s: %s\n", fnbuf, sstrerror(serrno));
	    break;
	  }
	  current_numfiles++;
	  k++;
	}

	/* get all the threads going */
	if (! errflg) {
	  if ((tid = calloc (nb_threads, sizeof(int))) == NULL) {
	    fprintf (stderr, "malloc error\n");
	    errflg++;
	  } else {
	    for (i = 0; i < nb_threads; i++) {
	      if ((tid[i] = Cthread_create (&doit, &i)) < 0) {
		fprintf (stderr, " error creating thread %d\n", i);
		errflg++;
	      }
	    }
	    for (i = 0; i < nb_threads; i++) {
	      (void)Cthread_join (tid[i], NULL);
	    }
	  }
	}

	printf("\n");

#if defined(_WIN32)
	WSACleanup();
#endif
	if (errflg)
		exit (USERR);
	exit (0);
}


void *
doit(arg)
void *arg;
{
  char filename[CA_MAXPATHLEN+1];
  char guid[CA_MAXGUIDLEN+1];
  struct Cns_filestatg statg;
  char sfn_path[CA_MAXSFNLEN+1];
  const char* server_name = "foo.example.com";
  struct timeval utime;
  long start_time_us, end_time_us;
  uuid_t uuid;
  int flags;
  Cns_list list;
  struct Cns_filereplica* rp;
  
  /* put in the LFN we are going to query for */
  uuid_generate(uuid);
  uuid_unparse(uuid, guid);
  sprintf(filename, "%s/thread-%d-lfn", Cnsdir, *(int *)arg);
  if (Cns_creatg(filename, guid, 0666) <0) {
    fprintf (stderr, "%s: %s\n", guid, sstrerror(serrno));
  }
  
  /* query for the LFN */
  gettimeofday( &utime, NULL);
  start_time_us = utime.tv_sec*1000000+utime.tv_usec;

  if (Cns_statg(filename, guid, &statg) < 0) {
    fprintf (stderr, "%s: %s\n", guid, sstrerror(serrno));
  }
  
  gettimeofday( &utime, NULL);
  end_time_us = utime.tv_sec*1000000+utime.tv_usec;
  
  printf ("%d\t", end_time_us - start_time_us);

  /* remove the LFN */
  if (Cns_unlink (filename) < 0) {
    fprintf (stderr, "%s: %s\n", filename, sstrerror(serrno));
  }
  
  return (NULL);
}
