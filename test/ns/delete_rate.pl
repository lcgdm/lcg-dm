#!/usr/bin/perl

#
# Performance test : deletes files for different number of files and threads.
# A fixed number of files (currently set to 5000) are inserted and the
# delete rate calculated. This is repeated 5 times for each reading.
#

use strict;
use warnings;
use Getopt::Long;

use FindBin;

# First do it using absolute pathnames, no transactions
#my $outfile = "results/delete/delete-rate-abs.dat";
#get_results($outfile, "no", 0, "no", 0);

# relative pathnames, without transactions
my $outfile = "results/delete/delete-rate-rel-notrans.dat";
get_results($outfile, "yes", 0, "no", 0);

# relative pathnames, without transactions
#$outfile = "results/delete/delete-rate-rel-trans.dat"
#get_results($outfile, "yes", 0, "no", 0);

# relative pathnames, with transactions
$outfile = "results/delete/delete-rate-rel-trans.dat";
get_results($outfile, "yes", 0, "yes", 100);

# the subroutine for running the command with the given parameters.

sub get_results {

  my($file, $relative, $depth, $transactions, $commit_size) = @_;
  
  my $optargs = "";
  if ($relative eq "yes") {
    $optargs = $optargs."-r ";
  }
  if ($depth > 0) {
    $optargs = $optargs."-n $depth ";
  }
  if ($transactions eq "yes") {
    $optargs = $optargs."-x ";
  }
  if ($commit_size > 0) {
    $optargs = $optargs."-c $commit_size ";
  }
  
  # run the "create_files_rate" command with the different number of files and threads
  # and get the times back in a file
  
  my @threads = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 50);
  my ($num_threads, $result);
  
  open(OUTFILE, '>', "$file") or die "Can't open $file: $!\n";
  
  print OUTFILE "num_threads, total_files, rate (deletes/sec)\n";
  
  my($sec, $min, $hour, $day, $mon, $year, @rest) = localtime(time);
  $year += 1900;
  my $timestamp = "$year-$mon-$day-$hour:$min:$sec";
  my $i = 0;
  my $total_files = 5000;  #must be less than 1,000,000
  
  foreach $num_threads (@threads) {
    $i = 0;
    while ($i < 5) {
      $result = `./delete_files_rate -d /grid/dteam/caitriana/test2/delete/$timestamp-$num_threads -f $total_files -t $num_threads $optargs`;
      print OUTFILE "$num_threads \t $total_files \t $result \n";
      `nsrm -rf /grid/dteam/caitriana/test2/delete/$timestamp-$num_threads`;
      $i+=1;
    }
    # delete the files that have just been produced, before testing with a different number of threads
    `nsrm -rf /grid/dteam/caitriana/test2/delete/`;
  }
  close OUTFILE; 
}
