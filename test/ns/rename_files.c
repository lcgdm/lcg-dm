/*
 * Copyright (C) 1999-2001 by CERN/IT/PDP/DM
 * All rights reserved
 */
 
#ifndef lint
static char sccsid[] = "@(#)$RCSfile: rename_files.c,v $ $Revision: 1.1.1.1 $ $Date: 2001/02/09 15:03:33 $ CERN IT-PDP/DM Jean-Philippe Baud";
#endif /* not lint */

	/* ask the name server running on Cnshost to create files and
	 * directories in a given directory dir, then try to rename them
	 * and finally remove them.
	 */

	/* If dir is specified on the command line but does not start with
	 * a slash, it is prefixed by $CASTOR_HOME.
	 * If dir is not specified, the default is:
	 *	$CASTOR_HOME/Cnstest/client_hostname/ccyymmdd
	 * Cnshost is set to the value of the environment variable CNS_HOST.
	 * If not set, the value is taken from shift.conf.
	 * If not set there either, use localhost.
	 * Command syntax is:
	 *	rename_files [-d dir]
	 */
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "Cns_api.h"
#include "serrno.h"
#define NFILES 10
extern	char	*getenv();
extern	char	*optarg;
main(argc, argv)
int argc;
char **argv;
{
	int c;
	char Cnsdir[CA_MAXPATHLEN+1];
	char Cnshost[CA_MAXHOSTNAMELEN+1];
	time_t current_time;
	char *dir = NULL;
	char *dp;
	time_t end_time;
	char *endp;
	int errflg = 0;
	char filename[CA_MAXPATHLEN+1];
	int i;
	int nb_files = NFILES;
	int nb_threads = 1;
	char *p;
	char pid4print[11];
	time_t start_time;
	struct Cns_filestat statbuf;
	struct tm *tm;
#if defined(_WIN32)
	WSADATA wsadata;
#endif

	while ((c = getopt (argc, argv, "d:")) != EOF) {
		switch (c) {
		case 'd':
			dir = optarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
#if defined(_WIN32)
	if (WSAStartup (MAKEWORD (2, 0), &wsadata)) {
		fprintf (stderr, "WSAStartup unsuccessful\n");
		exit (SYERR);
	}
#endif
	sprintf (pid4print, "%d", getpid());
	if (dir) {
		if (*dir != '/') {
			if ((p = getenv ("CASTOR_HOME")) == NULL ||
			    strlen (p) + strlen (dir) + strlen (pid4print) + 20 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				sprintf (Cnsdir, "%s/%s", p, dir);
		} else {
			if (strlen (dir) + strlen (pid4print) + 19 > CA_MAXPATHLEN) {
				fprintf (stderr, "invalid value for option -d\n");
				errflg++;
			} else
				strcpy (Cnsdir, dir);
		}
	} else {
		gethostname (Cnshost, sizeof(Cnshost));
		if ((p = getenv ("CASTOR_HOME")) == NULL ||
		    strlen (p) + strlen (Cnshost) + strlen (pid4print) + 37 > CA_MAXPATHLEN) {
			fprintf (stderr, "cannot set dir name\n");
			errflg++;
		} else {
			(void) time (&current_time);
			tm = localtime (&current_time);
			sprintf (Cnsdir, "%s/Cnstest/%s/%d%02d%02d", p, Cnshost,
			    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday);
		}
	}
	if (errflg) {
		fprintf (stderr, "usage: %s %s\n", argv[0], "[-d dir]");
#if defined(_WIN32)
		WSACleanup();
#endif
		exit (USERR);
	}

	sprintf (filename, "%s/%02d%02d%02d_%d_",
	    Cnsdir, tm->tm_hour, tm->tm_min, tm->tm_sec, getpid());
	p = filename + strlen (filename);

	/* create the directory if not there already */

	if (Cns_stat (Cnsdir, &statbuf) < 0) {
		if (serrno == ENOENT) {
			endp = strrchr (Cnsdir, '/');
			p = endp;
			while (p > Cnsdir) {
				*p = '\0';
				c = Cns_access (Cnsdir, F_OK);
				if (c == 0) break;
				p = strrchr (Cnsdir, '/');
			}
			while (p <= endp) {
				*p = '/';
				c = Cns_mkdir (Cnsdir, 0777);
				if (c < 0 && serrno != EEXIST) {
					fprintf (stderr, "cannot create %s: %s\n",
					    dir, sstrerror(serrno));
					errflg++;
					break;
				}
				p += strlen (p);
			}
		} else {
			fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
			errflg++;
		}
	}

	while (! errflg) {
		printf ("Changing directory to %s\n", Cnsdir);
		if (Cns_chdir (Cnsdir) < 0) {
			fprintf (stderr, "%s: %s\n", Cnsdir, sstrerror(serrno));
			errflg++;
			break;
		}
		printf ("Creating file1\n");
		if (Cns_creat ("file1", 0666) < 0) {
			fprintf (stderr, "file1: %s\n", sstrerror(serrno));
			errflg++;
			break;
		}
		printf ("Creating file2\n");
		if (Cns_creat ("file2", 0666) < 0) {
			fprintf (stderr, "file2: %s\n", sstrerror(serrno));
			errflg++;
			break;
		}
		printf ("Creating file4\n");
		if (Cns_creat ("file4", 0666) < 0) {
			fprintf (stderr, "file4: %s\n", sstrerror(serrno));
			errflg++;
			break;
		}

		printf ("Renaming file1 to non-existing file file3\n");
		if (Cns_rename ("file1", "file3") < 0) {
			errflg++;
			break;
		}
		printf ("Renaming file2 to existing file file4\n");
		if (Cns_rename ("file2", "file4") < 0) {
			errflg++;
			break;
		}

		printf ("Creating dir1\n");
		if (Cns_mkdir ("dir1", 0777) < 0) {
			fprintf (stderr, "cannot create 'dir1': %s\n", sstrerror(serrno));
			errflg++;
			break;
		}
		printf ("Creating dir2\n");
		if (Cns_mkdir ("dir2", 0777) < 0) {
			fprintf (stderr, "cannot create 'dir2': %s\n", sstrerror(serrno));
			errflg++;
			break;
		}
		printf ("Creating dir4\n");
		if (Cns_mkdir ("dir4", 0777) < 0) {
			fprintf (stderr, "cannot create 'dir4': %s\n", sstrerror(serrno));
			errflg++;
			break;
		}

		printf ("Renaming dir1 to non-existing directory dir3\n");
		if (Cns_rename ("dir1", "dir3") < 0) {
			errflg++;
			break;
		}
		printf ("Renaming dir2 to existing empty directory dir4\n");
		if (Cns_rename ("dir2", "dir4") < 0) {
			errflg++;
			break;
		}

		printf ("Removing file3\n");
		if (Cns_unlink ("file3") < 0) {
			fprintf (stderr, "file3: %s\n", sstrerror(serrno));
			errflg++;
			break;
		}
		printf ("Removing file4\n");
		if (Cns_unlink ("file4") < 0) {
			fprintf (stderr, "file4: %s\n", sstrerror(serrno));
			errflg++;
			break;
		}
		printf ("Removing dir3\n");
		if (Cns_rmdir ("dir3") < 0) {
			fprintf (stderr, "dir3: %s\n", sstrerror(serrno));
			errflg++;
			break;
		}
		printf ("Removing dir4\n");
		if (Cns_rmdir ("dir4") < 0) {
			fprintf (stderr, "dir4: %s\n", sstrerror(serrno));
			errflg++;
			break;
		}
		break;
	}
#if defined(_WIN32)
	WSACleanup();
#endif
	if (errflg)
		exit (USERR);
	exit (0);
}
