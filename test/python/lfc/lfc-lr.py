#!/usr/bin/python

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the files given as parameters, this script displays the GUID, the
#  comment and the list of replicas.
#
#  It uses the lfc_statg, lfc_getcomment and lfc_listreplica methods.
#
#=============================================================================

import sys
import os
import lfc


#=============================================================================
#  Function list_replicas
#=============================================================================
def list_replicas(*names):
  
  #---------------------------------------------------------------------------
  #  Loop on the file names given as parameters
  #---------------------------------------------------------------------------
  for name in names:
    
    if name[0] != '/':
      if 'LFC_HOME' in os.environ:
        name = os.environ['LFC_HOME'] + '/' + name
      else:
        sys.exit('Relative folder path requires LFC_HOME to be set and exported')
    
    
    #-------------------------------------------------------------------------
    # stat an existing entry in the LFC and print the GUID
    #-------------------------------------------------------------------------
    statg = lfc.lfc_filestatg()
    res   = lfc.lfc_statg(name, '', statg)
    
    if res != 0:
      err_num = lfc.cvar.serrno
      err_string = lfc.sstrerror(err_num)
      sys.exit('Error ' + str(err_num) + ' while looking for ' + name + ': ' + err_string)
    
    if statg.filemode & 040000:
      print ('%06o' % statg.filemode), name, 'is a folder'
    
    guid = statg.guid
    if guid:
      print ('%06o' % statg.filemode), name, 'has guid:' + guid
    else:
      print ('%06o' % statg.filemode), name, 'has NO guid'
    
    
    #-------------------------------------------------------------------------
    # retrieve the comment on a file
    #-------------------------------------------------------------------------
    buffer = ' ' * lfc.CA_MAXCOMMENTLEN
    
    res = lfc.lfc_getcomment(name, buffer)
    
    if res != 0:
       err_num = lfc.cvar.serrno
       if err_num != 2:
          err_string = lfc.sstrerror(err_num)
          print 'Error ' + str(err_num) + ' while reading the comment for ' + name + ': ' + err_string
    else:
      print "Comment: '" + buffer.rstrip(' ') + "'"
    
    
    #-------------------------------------------------------------------------
    # list the replicas of a given entry, starting from the GUID
    #-------------------------------------------------------------------------
    listp = lfc.lfc_list()
    flag  = lfc.CNS_LIST_BEGIN
    num_replicas = 0
    
    while 1:
      res = lfc.lfc_listreplica('', guid, flag, listp)
      if res == None:
        break
      else:
        flag = lfc.CNS_LIST_CONTINUE
        print '   ==>', res.sfn
        num_replicas += 1
    
    lfc.lfc_listreplica('', guid, lfc.CNS_LIST_END, listp)
    print 'Found ' + str(num_replicas) + ' replica(s)\n'


#=============================================================================
#  Main program
#=============================================================================
if __name__ == '__main__':
  if len(sys.argv) <= 1:
    sys.exit('Usage: ' + sys.argv[0] + ' path ...')
  
  list_replicas(*sys.argv[1:])
