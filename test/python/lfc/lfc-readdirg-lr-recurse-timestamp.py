#!/usr/bin/python

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the folder given as parameter, this script recursively lists the files
#  and their replicas WITHOUT minimizing the number of simultaneous sessions.
#
#  It uses the lfc_opendirg, lfc_readdirg and lfc_listreplica methods.
#
#  For large file trees, it crashes.  In order to facilitate crash analysis :
#  For each line, it displays the current timestamp and flushes the output.
#
#=============================================================================

import sys
import os
import time
import lfc


#=============================================================================
#  Function readdirg_lr_recurse_timestamp
#=============================================================================
def readdirg_lr_recurse_timestamp(*args):
  
  if len(args) < 1:
    folder = ''
    prefix = ''
  else:
    folder = args[0]
    prefix = folder + '/'
  
  if (folder == '') or (folder[0] != '/'):
    if 'LFC_HOME' in os.environ:
      folder = os.environ['LFC_HOME'] + '/' + prefix
    else:
      sys.exit('Relative folder path requires LFC_HOME to be set and exported')
  
  time_format = '%Y/%m/%d %H:%M:%S %Z'
  
  #---------------------------------------------------------------------------
  # Open the folder
  #---------------------------------------------------------------------------
  dir = lfc.lfc_opendirg(folder, '')
  if dir == None:
    err_num    = lfc.cvar.serrno
    err_string = lfc.sstrerror(err_num)
    sys.exit('Error ' + str(err_num) + ' on folder ' + folder + ': ' + err_string)
  
  listp = lfc.lfc_list()
  
  #---------------------------------------------------------------------------
  # Loop on the entries of the folder
  #---------------------------------------------------------------------------
  while 1:
    entry = lfc.lfc_readdirg(dir)
    if entry == None:
      break
    name = prefix + entry.d_name
    sys.stdout.write(time.strftime(time_format) + '  ' + name + "\n")
    sys.stdout.flush()
    
    #-------------------------------------------------------------------------
    # If the entry is a regular file, list its replicas using its GUID
    #-------------------------------------------------------------------------
    if not (entry.filemode & 060000):     # Exclude folders and symbolic links
      flag  = lfc.CNS_LIST_BEGIN
      
      while 1:
        res = lfc.lfc_listreplica('', entry.guid, flag, listp)
        if res == None:
          break
        else:
          flag = lfc.CNS_LIST_CONTINUE
          sys.stdout.write(time.strftime(time_format) + '     ==> ' + res.sfn + "\n")
          sys.stdout.flush()
      
      lfc.lfc_listreplica('', entry.guid, lfc.CNS_LIST_END, listp)
    
    #-------------------------------------------------------------------------
    # If the entry is a folder, recurse
    #-------------------------------------------------------------------------
    if entry.filemode & 040000:
      readdirg_lr_recurse_timestamp(name)
  
  lfc.lfc_closedir(dir)


#=============================================================================
#  Main program
#=============================================================================
if __name__ == '__main__':
  readdirg_lr_recurse_timestamp(*sys.argv[1:])
