#!/usr/bin/python

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the folder given as parameter, this script lists the files, their
#  mode, their size and their replicas with optimized performance.
#
#  It uses the lfc_opendirg, lfc_readdirg and lfc_getreplicas methods.
#
#=============================================================================

import sys
import os
import lfc


#=============================================================================
#  Function readdirg_grs
#=============================================================================
def readdirg_grs(*args):
  
  if len(args) < 1:
    folder = ''
  else:
    folder = args[0]
  
  if (folder == '') or (folder[0] != '/'):
    if 'LFC_HOME' in os.environ:
      folder = os.environ['LFC_HOME'] + '/' + folder
    else:
      sys.exit('Relative folder path requires LFC_HOME to be set and exported')
  
  #---------------------------------------------------------------------------
  # Open the folder
  #---------------------------------------------------------------------------
  dir = lfc.lfc_opendirg(folder, '')
  if dir == None:
    err_num    = lfc.cvar.serrno
    err_string = lfc.sstrerror(err_num)
    sys.exit('Error ' + str(err_num) + ' on folder ' + folder + ': ' + err_string)
  
  files = []
  guids = []
  listp = lfc.lfc_list()
  
  #---------------------------------------------------------------------------
  # Loop on the entries of the folder to build :
  # - the list of all files with Name, Filemode and GUID,
  # - the list of GUID for all regular files.
  #---------------------------------------------------------------------------
  while 1:
    entry = lfc.lfc_readdirg(dir)
    if entry == None:
      break
    if   entry.filemode & 040000:
      marker = '/'
    elif entry.filemode & 020000:     # not entry.guid:
      marker = '@'
    else:
      marker = ''
    files.append({'name': entry.d_name + marker,
                  'mode': entry.filemode,
                  'guid': entry.guid})
    if not (entry.filemode & 060000):     # Exclude folders and symbolic links
      guids.append(entry.guid)
  
  lfc.lfc_closedir(dir)
  
  #---------------------------------------------------------------------------
  # Get all replicas at once and build dictionaries per GUID
  #---------------------------------------------------------------------------
  guidSizes    = {}
  guidReplicas = {}
  (res, rep_entries) = lfc.lfc_getreplicas(guids, '')
  if res != 0:
    print 'lfc_getreplicas :  Error ' + str(res) + "\n"
  else:
    for entry in rep_entries:
      if entry.errcode == 0:
        if entry.guid in guidReplicas:
          guidReplicas[entry.guid].append(entry.sfn) 
        else:
          guidSizes[entry.guid]    = entry.filesize
          guidReplicas[entry.guid] = [entry.sfn]
  
  #---------------------------------------------------------------------------
  # Loop on the list of files to print their Filemode, Name and Replicas
  #---------------------------------------------------------------------------
  for myfile in files:
    print ('%06o' % myfile['mode']) + '  ' + myfile['name']
    guid = myfile['guid']
    if guid in guidReplicas:
      nb_replicas = 0
      for replica in guidReplicas[guid]:
        if replica != '':
          nb_replicas += 1
          print '   ==>', replica
      print 'Found ' + str(nb_replicas) + ' replica(s)     Size=' +  \
            str(guidSizes[guid])
    print


#=============================================================================
#  Main program
#=============================================================================
if __name__ == '__main__':
  readdirg_grs(*sys.argv[1:])
