#!/bin/bash

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  This script displays the performance of the commands given in parameter.
#  Commands can begin with bash, perl or python scripts, java classes,
#  or executables.
#
#  Parameters :
#  1:  Number of runs
#  2:  Number of decimals to display for elapsed time
#  3 and following :  Commands to test
#
#=============================================================================

#-----------------------------------------------------------------------------
#  Verify that there are at least 3 parameters
#-----------------------------------------------------------------------------
if [[ $# -lt 3 ]]
then
  echo "Usage:  $0  Runs  Decimals  Command  ..."
  exit 1
fi

#-----------------------------------------------------------------------------
#  First 2 parameters
#-----------------------------------------------------------------------------
NB_RUNS="$1"
DECIMALS="$2"

shift 2

#-----------------------------------------------------------------------------
#  Select the adequate PYTHON interpreter
#-----------------------------------------------------------------------------
if type python32 > /dev/null 2>&1
then
  PYTHON=python32
else
  PYTHON=python
fi

#-----------------------------------------------------------------------------
#  Following parameters are commands.
#  For each one, define the adequate interpreter.
#  Calculate the length of the longest command.
#-----------------------------------------------------------------------------
NUM=0
LENGTH_MAX=0
for COMMAND in "$@"
do
  PROGRAM=${COMMAND%% *}
  SUFFIX=${PROGRAM##*.}
  case "$SUFFIX" in
    (class) INTERPRETER[$NUM]='java    '  ;;
    (pl)    INTERPRETER[$NUM]='perl -w '  ;;
    (py)    INTERPRETER[$NUM]="$PYTHON  " ;;
    (sh)    INTERPRETER[$NUM]='bash    '  ;;
    (*)     INTERPRETER[$NUM]=''          ;;
  esac
  
  LENGTH=${#COMMAND}
  if [[ $LENGTH_MAX -lt $LENGTH ]]
  then
    LENGTH_MAX=$LENGTH
  fi
  
  ELAPSED_SUM[$NUM]=0
  NUM=$(( NUM + 1 ))
done

#-----------------------------------------------------------------------------
#  Function nano_to_float (nano, variable_name)
#
#  This function divides the (integer) first argument by 10**9 and sets the
#  result in the variable whose name is given as second argument
#-----------------------------------------------------------------------------
function nano_to_float ()
{
  local GIGA=1000000000
  local NANO=$1
  local INTEGER=$(( NANO / GIGA ))
  if [[ $NANO -ge $GIGA ]]
  then
    NANO=$(( NANO - (INTEGER * $GIGA ) ))
  fi
  export $2=$INTEGER.$(printf '%09d' $NANO)
}

#-----------------------------------------------------------------------------
#  Function remove_dot_class (path, variable_name)
#
#  This function removes the first '.class' from the first token of the first
#  argument and sets the result in the variable whose name is given as second
#  argument
#-----------------------------------------------------------------------------
function remove_dot_class ()
{
  local PROGRAM=${1%% *}
  local SUFFIX=${PROGRAM##*.}
  if [[ "$SUFFIX" = "class" ]]
  then
    export $2="${1/.class/}"
  fi
}

#-----------------------------------------------------------------------------
#  Loop on runs
#    Loop on commands 
#-----------------------------------------------------------------------------

for (( RUN=0; RUN<$NB_RUNS; RUN++ ))
do
  NUM=0
  echo
  for COMMAND in "$@"
  do
    remove_dot_class  "$COMMAND"  COMMAND
    printf "%-8s%-${LENGTH_MAX}s   "  "${INTERPRETER[$NUM]}"  "$COMMAND"
    TIME_NANO=$(date '+%s%N')
    NB_LINES=$(sh -c "${INTERPRETER[$NUM]}$COMMAND"  |  wc -l)
    ELAPSED_NANO=$(( $(date '+%s%N') - TIME_NANO ))
    ELAPSED_SUM[$NUM]=$(( ELAPSED_SUM[$NUM] + ELAPSED_NANO ))
    nano_to_float  $ELAPSED_NANO  ELAPSED_FLOAT
    printf "%5.${DECIMALS}f s  %5s lines\n"  $ELAPSED_FLOAT  $NB_LINES
    NUM=$(( NUM + 1 ))
  done
done


#-----------------------------------------------------------------------------
#  Loop on commands 
#-----------------------------------------------------------------------------
echo
echo 'Mean elapsed times'
echo '------------------'

NUM=0
for COMMAND in "$@"
do
  remove_dot_class  "$COMMAND"  COMMAND
  nano_to_float  $(( ELAPSED_SUM[$NUM] / NB_RUNS ))  ELAPSED_FLOAT
  printf "%-8s%-${LENGTH_MAX}s   %5.${DECIMALS}f s\n"  \
         "${INTERPRETER[$NUM]}"  "$COMMAND"  $ELAPSED_FLOAT
  NUM=$(( NUM + 1 ))
done
echo
