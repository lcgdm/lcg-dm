#!/bin/bash

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  This script tests the performances of the LFC python API on the LFC hosts.
#
#  It handles correcly paths containing blanks.
#
#  It uses the following scripts :
#     lfc-create-symlinks.py
#     lfc-readdirg.py
#     lfc-rm-symlinks-of-dir.py
#
#  Optional first  parameter :  Number of powers of 4 to test
#  Optional second parameter :  File containing the list of LFC hosts
#                               If it is not given, then the file is specified
#                               in the LFC_HOSTS_FILE environment variable
#
#=============================================================================
TIME0=$(date '+%s')

export LFC_CONNTIMEOUT=3
export LFC_CONRETRY=2
export LFC_CONRETRYINT=3

STEP=4
REGULAR_FILE=_regular_file_for_symbolic_links.txt


#-----------------------------------------------------------------------------
#  If the file containing the list of the LFC hosts is not given as first
#  parameter, it should be in the LFC_HOSTS_FILE environment variable.
#-----------------------------------------------------------------------------
NB_STEPS="${1:-8}"
if [[ "$2" == "" ]]
then
  if [[ "$LFC_HOSTS_FILE" == "" ]]
  then
    echo "Usage:  $0  [nb_steps  [file_containing_list_of_LFC_hosts]]"
    echo '        By default:  nb_steps=8  file=$LFC_HOSTS_FILE'
    exit 1
  else
    echo "LFC_HOSTS_FILE=$LFC_HOSTS_FILE"
  fi
else
  LFC_HOSTS_FILE="$2"
fi

#-----------------------------------------------------------------------------
#  gLite version
#-----------------------------------------------------------------------------
export GLITE_VERSION="$(glite-version)"
if [[ "$GLITE_VERSION" == "" ]]
then
  echo "HOSTNAME=$HOSTNAME"  >> /dev/stderr
  exit 1
fi

#-----------------------------------------------------------------------------
#  Extract the user's VO from his proxy
#-----------------------------------------------------------------------------
export LCG_GFAL_VO="$(voms-proxy-info -vo)"
if [[ "$LCG_GFAL_VO" == "" ]]
then
  echo "HOSTNAME=$HOSTNAME"  >> /dev/stderr
  exit 1
fi

#-----------------------------------------------------------------------------
#  Extract the user CN from his proxy
#-----------------------------------------------------------------------------
SUBMITTER="$(voms-proxy-info -subject)"
SUBMITTER="${SUBMITTER//CN=limited proxy/}"
SUBMITTER="${SUBMITTER//CN=proxy/}"
SUBMITTER="${SUBMITTER#*CN=}"
SUBMITTER="${SUBMITTER%%/*}"
SUBMITTER="${SUBMITTER// /_}"
USER_HOME="/grid/$LCG_GFAL_VO/$SUBMITTER"

export LFC_HOME="$USER_HOME/$HOSTNAME"

#-----------------------------------------------------------------------------
#  Calculate the Storage Element for the VO
#-----------------------------------------------------------------------------
VAR_DEFAULT_SE=VO_$(perl -we "print uc('$LCG_GFAL_VO')")_DEFAULT_SE
SE_HOST="${!VAR_DEFAULT_SE:-srm.cern.ch}"

#-----------------------------------------------------------------------------
#  Calculate the Maximum WallClock Time in seconds
#-----------------------------------------------------------------------------
unset MaxWCTime
if [[ (-r .BrokerInfo) || "$GLITE_WMS_LOG_DESTINATION" ]]
then
  #---------------------------------------------------------------------------
  #  If the '.BrokerInfo' file exists, it should contain the full name of the
  #  CE queue, and 'ldapsearch' will get the true Maximum WallClock Time.
  #---------------------------------------------------------------------------
  if [[ -r .BrokerInfo ]]
  then
    GlueCEUniqueID="$(glite-brokerinfo getCE)"
    if [[ "$GlueCEUniqueID" ]]
    then
      MaxWCTime=$(ldapsearch  -x  -LLL  -H "ldap://$LCG_GFAL_INFOSYS"  \
                -b "o=grid"  "GlueCEUniqueID=$GlueCEUniqueID"          \
                GlueCEPolicyMaxWallClockTime                        |  \
                grep     '^ *GlueCEPolicyMaxWallClockTime *:'       |  \
                sed -e 's/^ *GlueCEPolicyMaxWallClockTime *: *//')
    fi
    if [[ "$MaxWCTime" == "" ]]
    then
      echo "HOSTNAME=$HOSTNAME"  >> /dev/stderr
    fi
  fi
  #---------------------------------------------------------------------------
  #  If the '.BrokerInfo' file does NOT exist, then I do NOT have the full
  #  name of the CE queue.
  #  So I guess the CE hostname and I take the max of all its queues.
  #---------------------------------------------------------------------------
  if [[ "$MaxWCTime" == "" ]]
  then
    MaxWCTime=0
    for WCTime in $(lcg-info  --vo "$LCG_GFAL_VO"  --list-ce  \
                  --query="CE=$GLITE_WMS_LOG_DESTINATION:*"   \
                  --attrs="MaxWCTime"                      |  \
                  grep '^ *- *MaxWCTime'                   |  \
                  sed -e 's/^ *- *MaxWCTime *//')
    do
      if [[ $MaxWCTime -lt $WCTime ]]
      then
        MaxWCTime=$WCTime
      fi
    done
  fi
  export MaxWCTime=$(( 60 * MaxWCTime ))
fi

#-----------------------------------------------------------------------------
#  If started with fewer than 3 parameters, start itself with each LFC_HOST
#  as third parameter
#-----------------------------------------------------------------------------
if [[ $# -lt 3 ]]
then
  TIMESTAMP="$(date -u +%Y%m%d-%H%M%S)"
  SCRIPT="$(basename "$0")"
  LOGNAME="${LFC_HOSTS_FILE/.*/}"
  LOGFILE="$LOGNAME.$TIMESTAMP"
  
  echo "TIMESTAMP=${TIMESTAMP}-UTC"          >  "$LOGFILE.log"
  echo "SCRIPT=$SCRIPT"                      >> "$LOGFILE.log"
  echo "HOSTNAME=$HOSTNAME"                  >> "$LOGFILE.log"
  echo "REGULAR_FILE=$REGULAR_FILE"          >> "$LOGFILE.log"
  echo "GLITE_VERSION=$GLITE_VERSION"        >> "$LOGFILE.log"
  echo "LCG_GFAL_INFOSYS=$LCG_GFAL_INFOSYS"  >> "$LOGFILE.log"
  echo "LCG_GFAL_VO=$LCG_GFAL_VO"            >> "$LOGFILE.log"
  echo "SE_HOST=$SE_HOST"                    >> "$LOGFILE.log"
  echo "GlueCEUniqueID=$GlueCEUniqueID"      >> "$LOGFILE.log"
  echo "GLITE_WMS_LOG_DESTINATION=$GLITE_WMS_LOG_DESTINATION"  \
                                             >> "$LOGFILE.log"
  echo "MaxWCTime=$MaxWCTime"                >> "$LOGFILE.log"
  echo "USER_HOME=$USER_HOME"                >> "$LOGFILE.log"
  echo "LFC_HOME=$LFC_HOME"                  >> "$LOGFILE.log"
  echo                                       >> "$LOGFILE.log"
  
  echo voms-proxy-info -subject              >> "$LOGFILE.log"
  voms-proxy-info -subject                   >> "$LOGFILE.log"
  echo                                       >> "$LOGFILE.log"
  
  #---------------------------------------------------------------------------
  #  The list of LFC hosts is in the file given as second parameter.
  #  If the default LFC host for the VO is defined and is not already in this
  #  list, then add it to this list.
  #---------------------------------------------------------------------------
  LFC_HOST_VO="$(lcg-infosites --vo "$LCG_GFAL_VO" lfc)"
  LFC_HOST_VO_FOUND=0
  
  while read LFC_HOST
  do
    if [[ ("$LFC_HOST" != "") && ("${LFC_HOST:0:1}" != "#") ]]
    then
      if [[ "$LFC_HOST" == "$LFC_HOST_VO" ]]
      then
        LFC_HOST_VO_FOUND=1
      fi
      LFC_HOSTS[${#LFC_HOSTS[@]}]="$LFC_HOST"
    fi
  done  < "$LFC_HOSTS_FILE"  ||  exit 1
  
  if   [[ "$LFC_HOST_VO" == "" ]]
  then
    echo "HOSTNAME=$HOSTNAME"  >> /dev/stderr
  elif [[ "$LFC_HOST_VO_FOUND" == 0 ]]
  then
    LFC_HOSTS[${#LFC_HOSTS[@]}]="$LFC_HOST_VO"
  fi
  
  #---------------------------------------------------------------------------
  #  Loop on the list of LFC hosts and start itself with each LFC_HOST as
  #  third parameter
  #---------------------------------------------------------------------------
  for LFC_HOST in "${LFC_HOSTS[@]}"
  do
    echo "Started symlinks performance on LFC_HOST=$LFC_HOST"
    bash "$0" "$NB_STEPS" "$LFC_HOSTS_FILE" "$LFC_HOST"  \
        > "$LOGFILE.$LFC_HOST.log"  2>&1  &
  done

  #---------------------------------------------------------------------------
  #  Wait for all spawned processes to terminate
  #---------------------------------------------------------------------------
  wait

  #---------------------------------------------------------------------------
  #  Concatenate the log files and parse the result
  #---------------------------------------------------------------------------
  for LFC_HOST in "${LFC_HOSTS[@]}"
  do
    cat "$LOGFILE.$LFC_HOST.log"                         >> "$LOGFILE.log"
    rm  "$LOGFILE.$LFC_HOST.log"
  done
  echo "Terminated ALL :  $(date -u +%Y%m%d-%H%M%S-%Z)"  >> "$LOGFILE.log"
  
  perl -w lfc-perf-symlinks-parselog.pl "$LOGFILE.log"    > "$LOGFILE.tsv"  \
                                                   2> "$LOGFILE.error.tsv"
  
  ln -f "$LOGFILE.tsv" "$LOGFILE.csv"
  SCRIPTNAME=${SCRIPT/.*/}
  ln -f "$LOGFILE.log"       "$SCRIPTNAME.log"
  ln -f "$LOGFILE.tsv"       "$SCRIPTNAME.tsv"
  ln -f "$LOGFILE.error.tsv" "$SCRIPTNAME.error.tsv"
  
  #---------------------------------------------------------------------------
  #  Exit
  #---------------------------------------------------------------------------
  echo
  echo "$0 terminated"
  echo "Log files in '$LOGFILE.log'"
  echo "             '$LOGFILE.tsv'"
  echo "             '$LOGFILE.error.tsv'"
  echo
  exit
fi


#-----------------------------------------------------------------------------
#  Here the script is started with LFC_HOST as third parameter
#-----------------------------------------------------------------------------
TIMEFORMAT=$'real\t%3R s'

export LFC_HOST="$3"

echo '-----------------------------------------------------------------------'
echo
echo LFC_HOST="$LFC_HOST"
echo

#-----------------------------------------------------------------------------
#  The '_lfc.so' file is compiled for 32 bits architecture, so select the
#  adequate python executable
#-----------------------------------------------------------------------------
if type python32 > /dev/null 2>&1
then
  PYTHON=python32
else
  PYTHON=python
fi

#-----------------------------------------------------------------------------
#  Preparation :  If $USER_HOME does not exist, create it
#-----------------------------------------------------------------------------
echo lfc-ls -d "$USER_HOME"
lfc-ls -d "$USER_HOME"  > /dev/null  2>&1
if [[ $? != 0 ]]
then
  echo lfc-mkdir "$USER_HOME"
  time lfc-mkdir "$USER_HOME"
  if [[ $? != 0 ]]
  then
    echo 
    echo User can NOT use LFC on this host, SKIPPED
    echo
    exit 1
  fi
fi

echo lfc-ls -d "$LFC_HOME"
lfc-ls -d "$LFC_HOME"  > /dev/null  2>&1

#-----------------------------------------------------------------------------
#  If $LFC_HOME does not exist, create it
#-----------------------------------------------------------------------------
if [[ $? != 0 ]]
then
  echo lfc-mkdir "$LFC_HOME"
  time lfc-mkdir "$LFC_HOME"
  if [[ $? != 0 ]]
  then
    echo 
    echo User can NOT use LFC on this host, SKIPPED
    exit 1
  fi
#-----------------------------------------------------------------------------
#  If $LFC_HOME exists, delete the folders and files to be created
#-----------------------------------------------------------------------------
else
  #---------------------------------------------------------------------------
  #  If they exist, delete the symbolic links and the folders
  #---------------------------------------------------------------------------
  FOLDER=_folder_0_files
  echo lfc-ls -d "$FOLDER"
  lfc-ls -d "$FOLDER"  > /dev/null  2>&1
  if [[ $? == 0 ]]
  then
    echo lfc-rm -r "$FOLDER"
    lfc-rm -r "$FOLDER"
  fi
  
  NUMBER=1
  for (( I=0; I<NB_STEPS; I++ ))
  do
    FOLDER=_folder_${NUMBER}_files
    echo lfc-ls -d "$FOLDER"
    lfc-ls -d "$FOLDER"  > /dev/null  2>&1
    if [[ $? == 0 ]]
    then
      echo "$PYTHON" lfc-rm-symlinks-of-dir.py "$FOLDER"
      "$PYTHON" lfc-rm-symlinks-of-dir.py "$FOLDER"
      echo lfc-rm -r "$FOLDER"
      lfc-rm -r "$FOLDER"
    fi
    NUMBER=$((NUMBER*STEP))
  done
  #---------------------------------------------------------------------------
  #  If it exists, delete the regular file
  #---------------------------------------------------------------------------
  echo lfc-ls -d "$REGULAR_FILE"
  lfc-ls -d "$REGULAR_FILE"  > /dev/null  2>&1
  if [[ $? == 0 ]]
  then
    echo lcg-del -a "lfn:$REGULAR_FILE"
    lcg-del -a "lfn:$REGULAR_FILE"
  fi
fi

#-----------------------------------------------------------------------------
#  Issue 10 pings to get the RTT (round trip time)
#-----------------------------------------------------------------------------
echo
ping -c 10 "$LFC_HOST"

#-----------------------------------------------------------------------------
#  Create a regular file
#-----------------------------------------------------------------------------
echo
echo lcg-cr "file:$PWD/$0" -l "lfn:$REGULAR_FILE" -d "$SE_HOST"
time lcg-cr "file:$PWD/$0" -l "lfn:$REGULAR_FILE" -d "$SE_HOST"

#-----------------------------------------------------------------------------
#  For each folder to be created :
#  -  Verify that there is enough time left
#  -  Create this folder,
#  -  Create symbolic links in this folders,
#  -  Delete the symbolic links
#  -  Delete the folder
#-----------------------------------------------------------------------------
FOLDER=_folder_0_files
echo
echo lfc-mkdir "$FOLDER"
time lfc-mkdir "$FOLDER"
echo
echo "$PYTHON" lfc-readdirg.py "$FOLDER" '|' wc -l
echo -n "$LFC_HOME/$FOLDER :"
time "$PYTHON" lfc-readdirg.py "$FOLDER"  |  wc -l
echo
echo lfc-rm -r "$FOLDER"
time lfc-rm -r "$FOLDER"

NUMBER=1
for (( I=0; I<NB_STEPS; I++ ))
do
  FOLDER=_folder_${NUMBER}_files
  echo
  
  if [[ "$MaxWCTime" && ( $MaxWCTime -gt 0 ) &&  \
        ( $(( 4 * ($(date '+%s')-TIME0) )) -gt $MaxWCTime ) ]]
  then
    echo "Not enough time left for $NUMBER files"
    break
  fi
  
  echo lfc-mkdir "$FOLDER"
  time lfc-mkdir "$FOLDER"
  echo
  echo "$PYTHON" lfc-create-symlinks.py "$REGULAR_FILE" "$FOLDER" "$NUMBER"
  time "$PYTHON" lfc-create-symlinks.py "$REGULAR_FILE" "$FOLDER" "$NUMBER"
  echo
  echo "$PYTHON" lfc-readdirg.py "$FOLDER" '|' wc -l
  echo -n "$LFC_HOME/$FOLDER :"
  time "$PYTHON" lfc-readdirg.py "$FOLDER"  |  wc -l
  echo
  echo "$PYTHON" lfc-rm-symlinks-of-dir.py "$FOLDER"
  time "$PYTHON" lfc-rm-symlinks-of-dir.py "$FOLDER"
  echo
  echo lfc-rm -r "$FOLDER"
  time lfc-rm -r "$FOLDER"
  NUMBER=$((NUMBER*STEP))
done

#-----------------------------------------------------------------------------
#  Delete the regular file
#-----------------------------------------------------------------------------
echo
echo lcg-del -a "lfn:$REGULAR_FILE"
time lcg-del -a "lfn:$REGULAR_FILE"

#-----------------------------------------------------------------------------
#  Delete $LFC_HOME
#-----------------------------------------------------------------------------
echo
echo lfc-rm -r "$LFC_HOME"
time lfc-rm -r "$LFC_HOME"

echo
echo "Terminated $LFC_HOST :  $(date -u +%Y%m%d-%H%M%S-%Z)"
echo
