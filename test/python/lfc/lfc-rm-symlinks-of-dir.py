#!/usr/bin/python

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  This script deletes all symbolic links of a folder.
#
#  It uses the lfc_readdirg and lfc_unlink methods.
#
#=============================================================================

import sys
import os
import lfc

if len(sys.argv) <= 1:
  sys.exit('Usage: ' + sys.argv[0] + ' folder_path')

#-----------------------------------------------------------------------------
#  First parameter is the path of the folder to delete
#  If it is a relative path, prepend $LFC_HOME
#-----------------------------------------------------------------------------
folder = sys.argv[1]
if folder[0] != '/':
  if 'LFC_HOME' in os.environ:
    folder = os.environ['LFC_HOME'] + '/' + folder
  else:
    sys.exit('Relative folder path requires LFC_HOME to be set and exported')

#-----------------------------------------------------------------------------
#  Open the folder to get the list of its files
#-----------------------------------------------------------------------------
dir = lfc.lfc_opendirg(folder, '')
if dir == None:
  err_num = lfc.cvar.serrno
  err_string = lfc.sstrerror(err_num)
  sys.exit('Error ' + str(err_num) + ' on folder ' + folder + ': ' + err_string)

#-----------------------------------------------------------------------------
#  Loop on the files contained by the folder.
#  If they are neither subfolders nor regular files, we suppose they are
#  symbolic links, and we put their name in the 'symlinks' list.
#-----------------------------------------------------------------------------
b_OK  = True
symlinks = []

while 1:
  entry = lfc.lfc_readdirg(dir)
  if entry == None:
    break
  if (entry.filemode & 040000):
    print '  ', entry.d_name, 'is a folder'
    b_OK = False
  elif entry.guid:
    print '  ', entry.d_name, 'is a regular file'
    b_OK = False
  if b_OK:
    symlinks.append(entry.d_name)

#-----------------------------------------------------------------------------
#  Close the folder.
#  If at least one file found is not a symbolic link, stop.
#-----------------------------------------------------------------------------
lfc.lfc_closedir(dir)

if not b_OK:
  sys.exit(1)

print folder, ': ', len(symlinks), 'symbolic links found'

if len(symlinks) > 0:
  #---------------------------------------------------------------------------
  #  Here all files inside the folder are symbolic links.
  #  Open an LFC session.
  #---------------------------------------------------------------------------
  res = lfc.lfc_startsess('', '')
  if res != 0:
    err_num = lfc.cvar.serrno
    err_string = lfc.sstrerror(err_num)
    sys.exit('Error ' + str(err_num) + ' starting LFC session: ' + err_string)
  
  #---------------------------------------------------------------------------
  #  Loop on the symbolic links contained by the folder to delete them
  #---------------------------------------------------------------------------
  number = 0
  
  for name in symlinks:
    name = folder + '/' + name
    res  = lfc.lfc_unlink(name)
    if res == 0:
      number += 1
    else:
      err_num = lfc.cvar.serrno
      err_string = lfc.sstrerror(err_num)
      print "Error " + str(err_num) + " while deleting " + name + ": " + err_string
  
  print folder, ': ', number, 'symbolic links deleted'
  
  #---------------------------------------------------------------------------
  #  Close the LFC session.
  #---------------------------------------------------------------------------
  lfc.lfc_endsess()
