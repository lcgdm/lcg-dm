#!/bin/bash

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the LFC hosts listed in the file given as first parameter or else
#  specified in the LFC_HOSTS_FILE environment variable, this script purges
#  the files left by performance tests on symbolic links.
#
#  It handles correcly paths containing blanks.
#
#  It uses the lfc-rmdir-of-symlinks.py script.
#
#=============================================================================
export LFC_CONNTIMEOUT=3
export LFC_CONRETRY=2
export LFC_CONRETRYINT=3

REGULAR_FILE=_regular_file_for_symbolic_links.txt


#-----------------------------------------------------------------------------
#  If the file containing the list of the LFC hosts is not given as first
#  parameter, it should be in the LFC_HOSTS_FILE environment variable.
#-----------------------------------------------------------------------------
if [[ "$1" == "" ]]
then
  if [[ "$LFC_HOSTS_FILE" == "" ]]
  then
    echo "Usage:  $0  [file_containing_list_of_LFC_hosts]"
    echo '        By default, $LFC_HOSTS_FILE'
    exit 1
  else
    echo "LFC_HOSTS_FILE=$LFC_HOSTS_FILE"
  fi
else
  LFC_HOSTS_FILE="$1"
fi

#-----------------------------------------------------------------------------
#  Select the adequate PYTHON interpreter
#-----------------------------------------------------------------------------
if type python32 > /dev/null 2>&1
then
  PYTHON=python32
else
  PYTHON=python
fi

#-----------------------------------------------------------------------------
#  Extract the user's VO from his proxy
#-----------------------------------------------------------------------------
export LCG_GFAL_VO="$(voms-proxy-info -vo)"
if [[ "$LCG_GFAL_VO" == "" ]]
then
  echo "HOSTNAME=$HOSTNAME"  >> /dev/stderr
  exit 1
fi

#-----------------------------------------------------------------------------
#  Extract the user CN from his proxy
#-----------------------------------------------------------------------------
SUBMITTER="$(voms-proxy-info -subject)"
SUBMITTER="${SUBMITTER//CN=limited proxy/}"
SUBMITTER="${SUBMITTER//CN=proxy/}"
SUBMITTER="${SUBMITTER#*CN=}"
SUBMITTER="${SUBMITTER%%/*}"
SUBMITTER="${SUBMITTER// /_}"

export LFC_HOME="/grid/$LCG_GFAL_VO/$SUBMITTER"


#-----------------------------------------------------------------------------
#  Loop on the LFC hosts listed in the file
#-----------------------------------------------------------------------------
while read LFC_HOST
do
  if [[ ("$LFC_HOST" != "") && ("${LFC_HOST:0:1}" != "#") ]]
  then
    export LFC_HOST
    echo
    echo "LFC_HOST=$LFC_HOST"
    
    #-------------------------------------------------------------------------
    #  Folders at the first level should be names of worker nodes
    #-------------------------------------------------------------------------
    lfc-ls  |  while read WN_HOST
    do
      echo
      echo "    WN_HOST=$WN_HOST"
      
      #-----------------------------------------------------------------------
      #  For each folder at the second level, delete the symbolic links it
      #  contains, then the folder.
      #-----------------------------------------------------------------------
      lfc-ls "$WN_HOST"  |  while read FOLDER
      do
        if [[ "$FOLDER" != "$REGULAR_FILE" ]]
        then
          echo "        FOLDER=$FOLDER"
          "$PYTHON" lfc-rmdir-of-symlinks.py "$WN_HOST/$FOLDER"
        fi
      done
      
      #-----------------------------------------------------------------------
      #  Delete the regular file pointed by the symbolic links.
      #  If 'lcg-del' fails, try 'lcg-uf'.
      #-----------------------------------------------------------------------
      lfc-ls "$WN_HOST/$REGULAR_FILE"  > /dev/null  2>&1
      if [[ $? == 0 ]]
      then
        echo "    "lcg-del -a "lfn:$WN_HOST/$REGULAR_FILE"
        lcg-del -a "lfn:$WN_HOST/$REGULAR_FILE"
        if [[ $? != 0 ]]
        then
          echo "    "lcg-uf $(lcg-lg "lfn:$WN_HOST/$REGULAR_FILE") \
                            $(lcg-lr "lfn:$WN_HOST/$REGULAR_FILE")
          lcg-uf $(lcg-lg "lfn:$WN_HOST/$REGULAR_FILE") \
                 $(lcg-lr "lfn:$WN_HOST/$REGULAR_FILE")
        fi
      fi
      
      #-----------------------------------------------------------------------
      #  Delete the worker node subfolder
      #-----------------------------------------------------------------------
      if [[ $(lfc-ls "$WN_HOST" | wc -l) -eq 0 ]]
      then
        echo "    "lfc-rm -r "$WN_HOST"
        lfc-rm -r "$WN_HOST"
      fi
    done
    
    echo
  fi
done  < "$LFC_HOSTS_FILE"  ||  exit 1
