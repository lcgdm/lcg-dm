#!/usr/bin/python

#=============================================================================
#
#  Copyright 2006  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  For the folder given as parameter, this script lists the files and their
#  replicas.
#
#  It uses the lfc_opendirg and lfc_readdirxr methods.
#
#=============================================================================

import sys
import os
import lfc


#=============================================================================
#  Function readdirxr
#=============================================================================
def readdirxr(*args):
  
  if len(args) < 1:
    folder = ''
  else:
    folder = args[0]
  
  if (folder == '') or (folder[0] != '/'):
    if 'LFC_HOME' in os.environ:
      folder = os.environ['LFC_HOME'] + '/' + folder
    else:
      sys.exit('Relative folder path requires LFC_HOME to be set and exported')
  
  #---------------------------------------------------------------------------
  # Open the folder
  #---------------------------------------------------------------------------
  dir = lfc.lfc_opendirg(folder, '')
  if dir == None:
    err_num = lfc.cvar.serrno
    err_string = lfc.sstrerror(err_num)
    sys.exit('Error ' + str(err_num) + ' on folder ' + folder + ': ' + err_string)
  
  #---------------------------------------------------------------------------
  # Loop on the entries of the folder
  #---------------------------------------------------------------------------
  while 1:
    read_pt = lfc.lfc_readdirxr(dir, '')
    if read_pt == None:
      break
    entry, replicas = read_pt
    if   entry.filemode & 040000:
      comment = '   (Folder)'
    elif entry.nbreplicas == 0:
      comment = '   (No replica)'
    else:
      comment = ''
    print ('%06o' % entry.filemode) + '  ' + entry.d_name + comment
    #-------------------------------------------------------------------------
    # If the entry is a regular file, list its replicas
    #-------------------------------------------------------------------------
    if comment == '':
      for replica in replicas:
         print '    ==>', replica.sfn
  
  lfc.lfc_closedir(dir)


#=============================================================================
#  Main program
#=============================================================================
if __name__ == '__main__':
  readdirxr(*sys.argv[1:])
