#!/bin/bash

export LFC_HOST=lxb1540.cern.ch
export LFC_HOME=/grid/atlas/dq2/ddmf1

echo
echo "LFC_HOST=$LFC_HOST"
echo "LFC_HOME=$LFC_HOME"

#  3 runs.  Elapsed time with 0 decimal.
bash  compare-perf.sh   3   0            \
      lfc-readdirxr-recurse.py           \
      lfc-readdirg-recurse-gr.py         \
      lfc-readdirg-recurse-readdirxr.py  \
      lfc-readdirg-recurse-grs.py
