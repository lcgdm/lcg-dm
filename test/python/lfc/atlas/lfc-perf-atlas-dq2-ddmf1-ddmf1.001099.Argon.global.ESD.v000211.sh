#!/bin/bash

export LFC_HOST=lxb1540.cern.ch
export LFC_HOME=/grid/atlas/dq2/ddmf1/ddmf1.001099.Argon.global.ESD.v000211

echo
echo "LFC_HOST=$LFC_HOST"
echo "LFC_HOME=$LFC_HOME"

#  5 runs.  Elapsed time with 1 decimal.
bash  compare-perf.sh   5   1            \
      lfc-readdirxr.py                   \
      lfc-readdirg-lr.py                 \
      lfc-readdirg-recurse-gr.py         \
      lfc-readdirg-recurse-readdirxr.py  \
      lfc-readdirg-recurse-grs.py
