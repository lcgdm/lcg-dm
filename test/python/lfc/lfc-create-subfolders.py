#!/usr/bin/python

#=============================================================================
#
#  Copyright 2007  Etienne URBAH for the EGEE project
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details at
#  http://www.gnu.org/licenses/gpl.html
#
#  This script creates subfolders in the folder given as first parameter.
#  The number of subfolders is the second parameter.
#
#  It uses the lfc_mkdir method.
#
#=============================================================================

import sys
import os
import lfc

if len(sys.argv) <= 2:
  sys.exit('Usage: ' + sys.argv[0] + '  folder_path  number_of_subfolders')

(folder, nb_subfolders) = (sys.argv[1], int(sys.argv[2]))
if nb_subfolders < 1:
  sys.exit(folder + ' :  0 subfolders created')

if folder[0] != '/':
  if 'LFC_HOME' in os.environ:
    folder = os.environ['LFC_HOME'] + '/' + folder
  else:
    sys.exit('Relative folder path requires LFC_HOME to be set and exported')

targetname = folder + '/subfolder'
nb_char    = len(str(nb_subfolders-1))

if lfc.lfc_startsess('', '') != 0:
  err_num = lfc.cvar.serrno
  err_string = lfc.sstrerror(err_num)
  sys.exit('Error ' + str(err_num) + ' starting LFC session: ' + err_string)

for number in range(nb_subfolders):
  name = targetname + str(number).zfill(nb_char)
  res  = lfc.lfc_mkdir(name, 0775)
  if res != 0:
    err_num = lfc.cvar.serrno
    err_string = lfc.sstrerror(err_num)
    print 'Error ' + str(err_num) + ' while creating ' + name + ': ' + err_string
    number -= 1
    break

lfc.lfc_endsess()
print folder, ': ', (number+1), 'subfolders created'
