/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testReserveSpace.c,v 1.3 2005/11/09 16:16:00 grodid Exp $

/*  #include "u64subr.h" */
#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *s_token;
	struct ns1__srmReserveSpaceResponse_ rep;
	//struct ArrayOfTSURLPermissionReturn *repfs;
	struct ns1__srmReserveSpaceRequest req;
	//struct ns1__TSURLInfo *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	struct ns1__srmReserveSpaceResponse *repp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;
	char tmpbut[25];
	char tmpbug[25];

	if (argc < 8) {
		fprintf (stderr, "usage: %s endPoint user_space_token_description storage_system_info sizeOfTotalSpaceDesired sizeOfGuaranteedSpaceDesired lifetimeOfSpaceToReserve typeOfSpace\n", argv[0]);
		exit (1);
	}
	//nbfiles = argc - 3;

#if 0
	if (parsesurl (argv[3], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}
#endif

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));

#if 0
	    if ((req.spaceToken =
		 soap_malloc (&soap, sizeof(struct ns1__TSpaceToken))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    req.spaceToken->value = argv[2];
#endif

	    if ((req.storageSystemInfo =
		 soap_malloc (&soap, sizeof(struct ns1__TStorageSystemInfo))) == NULL) {
	      perror ("malloc");
	      soap_end (&soap);
	      exit (1);
	    }
	    req.storageSystemInfo->value = argv[3];

	if ((req.sizeOfTotalSpaceDesired =
		soap_malloc (&soap, sizeof(struct ns1__TSizeInBytes))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.sizeOfTotalSpaceDesired->value = atoll(argv[4]);

	if ((req.sizeOfGuaranteedSpaceDesired =
		soap_malloc (&soap, sizeof(struct ns1__TSizeInBytes))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.sizeOfGuaranteedSpaceDesired->value = atoll(argv[5]);

	if ((req.lifetimeOfSpaceToReserve =
		soap_malloc (&soap, sizeof(struct ns1__TLifeTimeInSeconds))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.lifetimeOfSpaceToReserve->value = atoi(argv[6]);

	req.userSpaceTokenDescription = argv[2];
	req.typeOfSpace = atoi(argv[7]);

	/* To send the request ... */

#if 0
	if (soap_call_ns1__srmReserveSpace (&soap, argv[1], "SrmReserveSpace",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
#else
	srm_endpoint = argv[1];
	SOAP_CALL_NS1(ReserveSpace);
#endif

	reqstatp = rep.srmReserveSpaceResponse->returnStatus; 
	//repfs = rep.srmCheckPermissionResponse->arrayOfPermissions;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	repp = rep.srmReserveSpaceResponse;
	if (repp->referenceHandleOfReservedSpace) {
		s_token = repp->referenceHandleOfReservedSpace->value;
		printf ("soap_call_ns1__srmReserveSpace returned s_token: %s\n",
		    s_token);
	}
	printf ("srmReserveSpace provided actual_s_type: %d actual_t_space: %llu actual_g_space: %llu actual_lifetime: %d\n", (*repp->typeOfReservedSpace), repp->sizeOfTotalReservedSpace->value, repp->sizeOfGuaranteedReservedSpace->value, repp->lifetimeOfReservedSpace->value );

	soap_end (&soap);
	exit (0);
}
