/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testLs.c,v 1.8 2007/04/22 11:30:48 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i, j;
	int isurl = 1; // pointer to the 1st actual SURL in the arg list
	int nbfiles;
	int nbproto = 0;
	static enum xsd__boolean trueoption = true_;  //  JPB
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
	int r = 0;
	char *r_token;
	struct ns1__srmLsResponse_ rep;
	struct ns1__ArrayOfTMetaDataPathDetail *repfs;
	struct ns1__srmLsRequest req;
	struct ns1__TSURLInfo *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfGetRequestResponse_ srep;
	struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;
	int booll = 0;
	int boolR = 0;
        int nolv = 0;

	if (argc < 2) {
		fprintf (stderr, "usage: %s [-R|-l|-2] SURLs\n", argv[0]);
		exit (1);
	}

	if ( strchr(argv[1], '-' ) == argv[1] ) {
	  isurl = 2;
	  if ( strchr(argv[1], 'l' ) )
	    booll = 1 ;
          if ( strchr(argv[1], 'R' ) )
            boolR = 1 ;
	  if ( strchr(argv[1], '2' ) )
            nolv = 1 ;
	}

	nbfiles = argc - isurl;

	if (parsesurl (argv[isurl], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.paths =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTSURLInfo))) == NULL ||
	    (req.paths->surlInfoArray =
	        soap_malloc (&soap, nbfiles * sizeof(struct ns1__TSURLInfo *))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.paths->surlInfoArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TSURLInfo))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.paths->__sizesurlInfoArray = nbfiles;
	if ( booll ) 
	  req.fullDetailedList  = &trueoption;  //  GG
	if ( boolR ) 
	  req.allLevelRecursive = &trueoption;  //  GG new

        req.numOfLevels = &nolv;

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.paths->surlInfoArray[i];
		if ((reqfilep->SURLOrStFN =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
		reqfilep->SURLOrStFN->value = argv[i+isurl];
		reqfilep->storageSystemInfo = NULL;
	}

	if (soap_call_ns1__srmLs (&soap, srm_endpoint, "Ls",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmLsResponse->returnStatus;
	repfs = rep.srmLsResponse->details;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	if (! repfs) {
		printf ("arrayOfTMetaDataPathDetail is NULL\n");
		soap_end (&soap);
		exit (1);
	}

	printf ("request state %d %d\n", reqstatp->statusCode, repfs->__sizepathDetailArray);  //  GG
	for (i = 0; i < repfs->__sizepathDetailArray; i++) {

	  printABranch(repfs, i);

	}
	soap_end (&soap); 
	exit (0);

}

printABranch (struct ns1__ArrayOfTMetaDataPathDetail *repfs, int i) {

  struct ns1__ArrayOfTMetaDataPathDetail *repfsl;
  int j, il, nsub;

  printf("Stat: %d\n", repfs->pathDetailArray[i]->status->statusCode);  //  GG

  if ( repfs->pathDetailArray[i]->subPaths )
    printf("Path: %s Subpath: %d\n", repfs->pathDetailArray[i]->path, repfs->pathDetailArray[i]->subPaths->__sizepathDetailArray);
  else
    printf("Path: %s\n", repfs->pathDetailArray[i]->path);

  if ( repfs->pathDetailArray[i]->status->statusCode ) {
    printf("----=============\n");
    return; // continue;
  }
  printf("Size: %d\n", repfs->pathDetailArray[i]->size->value);
  printf("Type: %d\n", (*repfs->pathDetailArray[i]->type));
  if ( repfs->pathDetailArray[i]->fileStorageType )
    printf("FStT: %d\n", (*repfs->pathDetailArray[i]->fileStorageType));
  if ( repfs->pathDetailArray[i]->owner )
    printf("OwnR: %s\n", repfs->pathDetailArray[i]->owner->value);
  if ( repfs->pathDetailArray[i]->checkSumType )
    printf("ChST: %s\n", repfs->pathDetailArray[i]->checkSumType->value);
  if ( repfs->pathDetailArray[i]->createdAtTime )
    printf("Crea: %s\n", repfs->pathDetailArray[i]->createdAtTime->value);
  if ( repfs->pathDetailArray[i]->lastModificationTime )
    printf("Last: %s\n", repfs->pathDetailArray[i]->lastModificationTime->value);
  if ( repfs->pathDetailArray[i]->ownerPermission )
    printf("OwnP: %d\n", repfs->pathDetailArray[i]->ownerPermission->mode);
  if ( repfs->pathDetailArray[i]->otherPermission )
    printf("OthP: %d\n", repfs->pathDetailArray[i]->otherPermission->mode);
  // Warning: there should be a loop here if __size>1
  if ( repfs->pathDetailArray[i]->userPermissions ) {
    printf("UsrP: %d",
	   repfs->pathDetailArray[i]->userPermissions->__sizeuserPermissionArray);
    for (j = 0; j < repfs->pathDetailArray[i]->userPermissions->__sizeuserPermissionArray; j++ ) {
      printf(" %s:%d",
	     repfs->pathDetailArray[i]->userPermissions->userPermissionArray[j]->userID->value,
	     repfs->pathDetailArray[i]->userPermissions->userPermissionArray[j]->mode);
    }
    printf("\n");
  }

  if ( repfs->pathDetailArray[i]->groupPermissions ) {
    printf("GrpP: %d",
	   repfs->pathDetailArray[i]->groupPermissions->__sizegroupPermissionArray);
    for (j = 0; j < repfs->pathDetailArray[i]->groupPermissions->__sizegroupPermissionArray; j++ ) {
      printf(" %s:%d",
	     repfs->pathDetailArray[i]->groupPermissions->groupPermissionArray[j]->groupID->value,
	     repfs->pathDetailArray[i]->groupPermissions->groupPermissionArray[j]->mode);
    }
    printf("\n");
  }

  if ( repfs->pathDetailArray[i]->lifetimeAssigned )
    printf("LifA: %d\n", repfs->pathDetailArray[i]->lifetimeAssigned);
  if ( repfs->pathDetailArray[i]->lifetimeLeft )
    printf("LifL: %d\n", repfs->pathDetailArray[i]->lifetimeLeft);

  printf("=================\n");

  //Loop on subpaths ...
  if ( ! repfs->pathDetailArray[i]->subPaths )
    return;
  repfsl = repfs->pathDetailArray[i]->subPaths;
  nsub = repfsl->__sizepathDetailArray;
  if ( ! nsub ) 
    return;
  for (il = 0; il < nsub; il++) {
    printf("================= Branch: %d\n", il);
    printABranch(repfsl, il);
  }

  return;

}

