/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testReleaseFiles.c,v 1.6 2006/04/28 07:16:25 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10
 
#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmReleaseFilesResponse_ rep;
	struct ns1__ArrayOfTSURLReturnStatus *repfs;
	struct ns1__srmReleaseFilesRequest req;
	struct ns1__TSURL *reqfilep;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfGetRequestResponse_ srep;
	struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;
	int keepFlag = 2;  //  Special Test
	static enum xsd__boolean trueoption  = true_;
	static enum xsd__boolean falseoption = false_;
	
	if (argc < 3) {
		fprintf (stderr, "usage: %s reqid SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 2;

	if (parsesurl (argv[2], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.requestToken =
		soap_malloc (&soap, sizeof(struct ns1__TRequestToken))) == NULL ||
	    (req.siteURLs =
		soap_malloc (&soap, sizeof(struct ns1__ArrayOfTSURL))) == NULL ||
	    (req.siteURLs->surlArray =
		soap_malloc (&soap, nbfiles * sizeof(struct ns1__TSURL*))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < nbfiles; i++) {
		if ((req.siteURLs->surlArray[i] =
		    soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
			perror ("malloc");
			soap_end (&soap);
			exit (1);
		}
	}
	req.requestToken->value = argv[1];
	req.siteURLs->__sizesurlArray = nbfiles;

	switch (keepFlag) {
	case 0:
	  break;
	case 1:
	  req.keepSpace = &falseoption;
	  break;
        case 2:
          req.keepSpace = &trueoption;
          break;
	default:
	  break;
	}

	for (i = 0; i < nbfiles; i++) {
		reqfilep = req.siteURLs->surlArray[i];
		reqfilep->value = argv[i+2];

		/* GG special tests */
		/*
		if ( i == 0 )
		  //GOODreq.siteURLs->surlArray[i] = NULL;
		  //CRASHreqfilep->value = NULL;
		  */
		
	}

	/* GG special tests */
	//GOODreq.siteURLs = NULL;

	if (soap_call_ns1__srmReleaseFiles (&soap, srm_endpoint, "ReleaseFiles",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = rep.srmReleaseFilesResponse->returnStatus;
	repfs = rep.srmReleaseFilesResponse->arrayOfFileStatuses;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}
	if (! repfs) {
		printf ("arrayOfFileStatuses is NULL\n");
		soap_end (&soap);
		exit (1);
	}

	for (i = 0; i < repfs->__sizesurlReturnStatusArray; i++) { 
		if ((repfs->surlReturnStatusArray[i])->status->explanation)
			printf ("state[%d] = %d, explanation = %s\n", i,
			    (repfs->surlReturnStatusArray[i])->status->statusCode,
			    (repfs->surlReturnStatusArray[i])->status->explanation);
		else
			printf ("state[%d] = %d, SURL = %s\n", i,
			    (repfs->surlReturnStatusArray[i])->status->statusCode,
				(repfs->surlReturnStatusArray[i])->surl->value);
	}
	soap_end (&soap);
	exit (0);
}

