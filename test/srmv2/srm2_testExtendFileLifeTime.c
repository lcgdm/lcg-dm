/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testExtendFileLifeTime.c,v 1.5 2006/01/27 09:37:00 grodid Exp $

#include "srmv2H.h"
#include "srmSoapBinding.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif

#include "parsesurl.ic"
#include "soapcallns1.ic"

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
#if 0
	int nbproto = 0;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
#endif
	int r = 0;
	char *r_token;
	struct ns1__srmExtendFileLifeTimeResponse_ rep;
	struct ns1__srmExtendFileLifeTimeRequest req;
	struct ns1__TReturnStatus *reqstatp;
	char *sfn;
	struct soap soap;
	struct ns1__srmStatusOfGetRequestResponse_ srep;
	struct ns1__srmStatusOfGetRequestRequest sreq;
	char *srm_endpoint;

	if (argc < 4) {
		fprintf (stderr, "usage: %s reqid SURL seconds\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 3;

	if (parsesurl (argv[2], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	//while (*protocols[nbproto]) nbproto++;

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	memset (&req, 0, sizeof(req));
	if ((req.requestToken =
		soap_malloc (&soap, sizeof(struct ns1__TRequestToken))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}

	req.requestToken->value = argv[1];

	if ((req.siteURL =
		soap_malloc (&soap, sizeof(struct ns1__TSURL))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.siteURL->value = argv[2];

	if ((req.newLifeTime =
		soap_malloc (&soap, sizeof(struct ns1__TLifeTimeInSeconds))) == NULL) {
		perror ("malloc");
		soap_end (&soap);
		exit (1);
	}
	req.newLifeTime->value = atol(argv[3]);
	
	/* GG special tests */
	//CRASHreq.requestToken->value = NULL;
	//GOODreq.requestToken = NULL;
	//CRASHreq.siteURL->value = NULL;
	//GOODreq.siteURL = NULL;
	//GOODreq.newLifeTime->value = 0;
	//GOODreq.newLifeTime = NULL;

	if (soap_call_ns1__srmExtendFileLifeTime (&soap, srm_endpoint, "ExtendFileLifeTime",
	    &req, &rep)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}

	reqstatp = rep.srmExtendFileLifeTimeResponse->returnStatus;

	printf ("request state %d\n", reqstatp->statusCode);
	if (reqstatp->statusCode != SRM_USCORESUCCESS &&
	    reqstatp->statusCode != SRM_USCOREDONE) {
		if (reqstatp->explanation)
			printf ("explanation: %s\n", reqstatp->explanation);
		soap_end (&soap);
		exit (1);
	}

	if (rep.srmExtendFileLifeTimeResponse->newTimeExtended) 
		printf("time extended to: %d\n", rep.srmExtendFileLifeTimeResponse->newTimeExtended->value);

	soap_end (&soap);
	exit (0);
}

