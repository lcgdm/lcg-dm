#include <stdio.h>
#include "srmv1H.h"
#include "ISRM.nsmap"

#define SRM_EP_PATH "/srm/managerv1"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif
parsesurl (const char *surl, char **endpoint, char **sfn)
{
	int len;
	int lenp;
	char *p;
	static char srm_ep[256];

	if (strncmp (surl, "srm://", 6)) {
		errno = EINVAL;
		return (-1);
	}
	if (p = strstr (surl + 6, "?SFN=")) {
		*sfn = p + 5;
	} else if (p = strchr (surl + 6, '/')) {
		*sfn = p;
	} else {
		errno = EINVAL;
		return (-1);
	}
#ifdef GFAL_SECURE
	strcpy (srm_ep, "https://");
	lenp = 8;
#else
	strcpy (srm_ep, "http://");
	lenp = 7;
#endif
	len = p - surl - 6;
	if (lenp + len >= sizeof(srm_ep)) {
		errno = EINVAL;
		return (-1);
	}
	strncpy (srm_ep + lenp, surl + 6, len);
	*(srm_ep + lenp + len) = '\0';
	if (strchr (srm_ep + lenp, '/') == NULL) {
		if (strlen (SRM_EP_PATH) + lenp + len >= sizeof(srm_ep)) {
			errno = EINVAL;
			return (-1);
		}
		strcat (srm_ep, SRM_EP_PATH);
	}
	*endpoint = srm_ep;
	return (0);
}

main (int argc, char **argv)
{
	int flags;
	struct ns5__advisoryDeleteResponse out;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;
	struct ArrayOfstring surlarray;

	if (argc != 2) {
		fprintf (stderr, "usage: %s SURL\n", argv[0]);
		exit (1);
	}

	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	/* issue "advisoryDelete" request */

	surlarray.__ptr = &argv[1];
	surlarray.__size = 1;

	if (soap_call_ns5__advisoryDelete (&soap, srm_endpoint,
	    "advisoryDelete", &surlarray, &out)) {
		soap_print_fault (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	soap_end (&soap);
	exit (0);
}
