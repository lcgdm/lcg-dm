#include "srmv1H.h"
#include "ISRM.nsmap"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/srm/managerv1"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif
parsesurl (const char *surl, char **endpoint, char **sfn)
{
	int len;
	int lenp;
	char *p;
	static char srm_ep[256];

	if (strncmp (surl, "srm://", 6)) {
		errno = EINVAL;
		return (-1);
	}
	if (p = strstr (surl + 6, "?SFN=")) {
		*sfn = p + 5;
	} else if (p = strchr (surl + 6, '/')) {
		*sfn = p;
	} else {
		errno = EINVAL;
		return (-1);
	}
#ifdef GFAL_SECURE
	strcpy (srm_ep, "https://");
	lenp = 8;
#else
	strcpy (srm_ep, "http://");
	lenp = 7;
#endif
	len = p - surl - 6;
	if (lenp + len >= sizeof(srm_ep)) {
		errno = EINVAL;
		return (-1);
	}
	strncpy (srm_ep + lenp, surl + 6, len);
	*(srm_ep + lenp + len) = '\0';
	if (strchr (srm_ep + lenp, '/') == NULL) {
		if (strlen (SRM_EP_PATH) + lenp + len >= sizeof(srm_ep)) {
			errno = EINVAL;
			return (-1);
		}
		strcat (srm_ep, SRM_EP_PATH);
	}
	*endpoint = srm_ep;
	return (0);
}

main(argc, argv)
int argc;
char **argv;
{
	int flags;
	int i;
	int nbfiles;
	int nbproto = 0;
	struct ns5__getEstGetTimeResponse out;
	struct ns5__getRequestStatusResponse outq;
	struct ArrayOfstring protoarray;
	static char *protocols[] = {
#if GFAL_ENABLE_RFIO
		"rfio",
#endif
#if GFAL_ENABLE_DCAP
		"gsidcap",
#endif
		""
	};
	int r = 0;
	int reqid;
	struct ns1__RequestStatus *reqstatp;
	char *sfn;
	struct soap soap;
	char *srm_endpoint;
	struct ArrayOfstring surlarray;

	if (argc < 2) {
		fprintf (stderr, "usage: %s SURLs\n", argv[0]);
		exit (1);
	}
	nbfiles = argc - 1;

	if (parsesurl (argv[1], &srm_endpoint, &sfn) < 0) {
		perror ("parsesurl");
		exit (1);
	}

	while (*protocols[nbproto]) nbproto++;

	soap_init(&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	if ((surlarray.__ptr = malloc (nbfiles * sizeof(char *))) == NULL) {
		perror ("malloc");
		exit (1);
	}
	for (i = 0; i < nbfiles; i++)
		surlarray.__ptr[i] = argv[i+1];
	surlarray.__size = nbfiles;
	protoarray.__ptr = protocols;
	protoarray.__size = nbproto;

	if (soap_call_ns5__getEstGetTime (&soap, srm_endpoint, "getEstGetTime",
	    &surlarray, &protoarray, &out)) {
		soap_print_fault (&soap, stderr);
		soap_print_fault_location (&soap, stderr);
		soap_end (&soap);
		exit (1);
	}
	reqstatp = out._Result;
	if (reqstatp->fileStatuses == NULL) {
		errno = EPROTONOSUPPORT;
		perror ("getEstGetTime");
		soap_end (&soap);
		exit (1);
	}
	reqid = reqstatp->requestId;

	printf ("soap_call_ns5__getEstGetTime returned reqid: %d\n", reqid);

	/* wait for file "ready" */

	while (strcmp (reqstatp->state, "pending") == 0 ||
	    strcmp (reqstatp->state, "Pending") == 0) {
		printf("request state %s\n", reqstatp->state);
		sleep ((r++ == 0) ? 1 : (reqstatp->retryDeltaTime > 0) ?
		    reqstatp->retryDeltaTime : DEFPOLLINT);
		if (soap_call_ns5__getRequestStatus (&soap, srm_endpoint,
		    "getRequestStatus", reqid, &outq)) {
			soap_print_fault (&soap, stderr);
			soap_end (&soap);
			exit (1);
		}
		reqstatp = outq._Result;
	}
	printf ("request state %s\n", reqstatp->state);
	if (strcmp (reqstatp->state, "failed") == 0 ||
	    strcmp (reqstatp->state, "Failed") == 0) {
		if (reqstatp->errorMessage)
			printf ("errorMessage: %s\n", reqstatp->errorMessage);
		soap_end (&soap);
		exit (1);
	}
	for (i = 0; i < reqstatp->fileStatuses->__size; i++) {
		if (strcmp ((reqstatp->fileStatuses->__ptr[i])->state, "ready") &&
		    strcmp ((reqstatp->fileStatuses->__ptr[i])->state, "Ready"))
			printf ("state[%d] = %s\n", i, (reqstatp->fileStatuses->__ptr[i])->state);
		else
			printf ("state[%d] = %s, TURL = %s\n", i,
			    (reqstatp->fileStatuses->__ptr[i])->state,
			    (reqstatp->fileStatuses->__ptr[i])->TURL);
	}
	soap_end (&soap);
	exit (0);
}
