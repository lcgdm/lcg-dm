#include <stdio.h>
#include "srmv1H.h"
#include "ISRM.nsmap"
#ifdef GFAL_SECURE
#include "cgsi_plugin.h"
#endif
main(argc, argv)
int argc;
char **argv;
{
	int flags;
	struct ns5__pingResponse out;
	struct soap soap;

	if (argc != 2) {
		fprintf (stderr, "usage: %s srm_endpoint\n", argv[0]);
		exit (1);
	}

	soap_init (&soap);
#ifdef GFAL_SECURE
	flags = CGSI_OPT_DISABLE_NAME_CHECK;
	soap_register_plugin_arg (&soap, client_cgsi_plugin, &flags);
#endif

	if (soap_call_ns5__ping (&soap, argv[1], "ping", &out)) {
		soap_print_fault (&soap, stderr);
		exit (1);
	}
	if (out._Result)
		printf ("SRM server is alive\n");
	else
		printf ("SRM server is not responding\n");
	exit (0);
}
