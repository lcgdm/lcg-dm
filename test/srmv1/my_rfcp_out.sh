#!/bin/bash
#
# This script is a part of SRM (V1) test suite. This is a wrapper script around
# rfcp, used to copy files to the remote machine (to TURL got from SRM
# server).
#

# $Id: my_rfcp_out.sh,v 1.8 2005/12/06 21:26:40 grodid Exp $

echo 'TURL:' $2
TURL_RFCP=`echo $2 | perl -pe 's;(.*?)://(.*)$;$2;g'`

TURL_RFCP=`echo $TURL_RFCP | perl -pe 's;//;:/;'`

if [ "$NEW_RFIO_SYNTAX" ]; then
    TURL_RFCP=$2;  ###  For the NEW RFIOD 30/11/05
fi

echo 'TURL_rfcp:  ' $TURL_RFCP

which rfcp

rfcp $1 $TURL_RFCP

exit $?

