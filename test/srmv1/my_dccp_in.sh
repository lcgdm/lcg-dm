#!/bin/bash
#
# This script is a part of SRM test suite. This is a wrapper script around
# rfcp, used to copy files from the remote machine (from TURL got from SRM
# server) and store it locally.
#

# $Id: my_dccp_in.sh,v 1.5 2005/11/29 11:04:19 jamesc Exp $

echo 'TURL:' $1
TURL_RFCP=`echo $1 | sed 's;rfio://;;g'`
TURL_RFCP=`echo $1 | perl -pe 's;rfio://([^/]*?):?//?(.*)$;$1:/$2;g'`

TURL_RFCP=$1

echo 'TURL_DCCP:  ' $TURL_RFCP

/opt/d-cache/dcap/bin/dccp $TURL_RFCP $2
exit $?

