/*
 * Copyright (C) 2003-2008 by CERN/IT/ADC/CA
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: Csec_errmsg.c,v $ $Revision: 1.4 $ $Date: 2008/09/29 08:02:48 $ CERN IT/ADC/CA Benjamin Couturier";
#endif /* not lint */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <stdlib.h>
#include <ctype.h>

#include "Csec.h"
#include "serrno.h"

int DLL_DECL
Csec_clear_errmsg() {
  struct Csec_api_thread_info *thip;
  struct Csec_error_node_s *p;

  if (Csec_apiinit (&thip))
    return (-1);

  p = thip->err_last;
  while(p) {
    struct Csec_error_node_s *r = p->prev;
    free(p->err);
    free(p);
    p = r;
  }
  thip->err_first = NULL;
  thip->err_last = NULL;

  errno = 0;
  serrno = 0;

  return(0);
}

/* Csec_errmsg - send error message to error buffer in per-thread structure */
/* should preserve errno, serrno */
int DLL_DECL
Csec_errmsg(char *func, char *msg, ...) {
  
  va_list args;
  int save_errno, save_serrno;
  struct Csec_api_thread_info *thip;
  int pos1,alloced;
  struct Csec_error_node_s *p;
  
  save_errno = errno;
  save_serrno = serrno;

  if (Csec_apiinit (&thip))
    return (-1);
  va_start (args, msg);

  alloced = ERRBUFSIZE+1;

  p = (struct Csec_error_node_s *)malloc(sizeof(struct Csec_error_node_s));
  p->prev = thip->err_last;
  p->err = (char *)malloc(alloced);
  p->err[0] = '\0';

  thip->err_last = p;

  if (func) {
    snprintf (p->err, alloced, "%s: ", func);
  }

  pos1 = strlen (p->err);
  vsnprintf (&p->err[pos1], alloced - pos1, msg, args);
  p->err[alloced-1] = '\0';

  /* remove trailing newlines or spaces */
  pos1 = strlen (p->err);
  while(pos1>0 && isspace(p->err[pos1-1])) --pos1;
  p->err[pos1] = '\0';

  /* remove newlines or spaces at start */
  pos1 = 0;
  while(p->err[pos1] && isspace(p->err[pos1])) ++pos1;
  if (pos1>0) 
    memmove(p->err, &p->err[pos1], strlen(p->err)-pos1+1);

  /* replace a newline and any directly following newlines or spaces with ", " */
  pos1 = 0;
  while(pos1<strlen(p->err)) {
    int pos2;

    if (p->err[pos1] != '\n') {
      ++pos1; continue;
    }
    pos2 = pos1;
    while(p->err[pos2] && isspace(p->err[pos2])) ++pos2;
    if (pos2-pos1 == 1) {
      if (strlen(p->err)+1 >= alloced) {
        alloced *= 2;
        p->err = realloc(p->err, alloced);
      }
    }
    memmove(&p->err[pos1+2], &p->err[pos2], strlen(p->err)-pos2+1);
    p->err[pos1] = ',';
    p->err[pos1+1] = ' ';
    pos1 += 2;
  }

  Csec_trace("ERROR", "%s\n", p->err);

  errno = save_errno;
  serrno = save_serrno;
  return (0);
}

/* Csec_errErrorMessage - send error message to user defined client buffer or to stderr */
char *Csec_getErrorMessage() {
  return Csec_getErrorMessageSummary(0);
}

/* Csec_getErrorMessageSummary() returns concatenation of all the errors recorded, but
 * ignoring individual errors in the list which would make the overall summary
 * (including terminating null) longer than maxlen. The limit is disabled for maxlen=0
 */
char *Csec_getErrorMessageSummary(size_t maxlen) {
  struct Csec_api_thread_info *thip;
  struct Csec_error_node_s *p;
  int save_errno, save_serrno;

  save_errno = errno;
  save_serrno = serrno;
  
  if (Csec_apiinit (&thip))
    return NULL;

  if (!thip->err_summary) {
    thip->err_summary = (char *)malloc(ERRBUFSIZE+1);
    thip->err_summary_size = ERRBUFSIZE+1;
  }
  thip->err_summary[0] = '\0';

  p = thip->err_last;
  while(p) {
    char *r;
    size_t s = strlen(thip->err_summary);
    if (s) s+=2;
    s += strlen(p->err) + 1;

    if (maxlen>0 && s>maxlen) {
      p = p->prev;
      continue;
    }

    if (s > thip->err_summary_size) {
      r = (char *)realloc(thip->err_summary,s);
      if (r) {
        thip->err_summary = r;
        thip->err_summary_size = s;
      } else {
        break;
      }
    }

    if (thip->err_summary[0])
      strcat(thip->err_summary, "; ");

    strcat(thip->err_summary,p->err);
    p = p->prev;
  }

  if (!thip->err_summary[0]) {
    size_t l = maxlen ? (maxlen-1) : (thip->err_summary_size-1);
    char const *s = NULL;
    if (save_serrno != 0) {
      s = sstrerror(save_serrno);
    } else if (save_errno != 0) {
      s = sstrerror(save_errno);
    }
    if (s) {
      snprintf(thip->err_summary, l, "%s", s);
    } else {
      snprintf(thip->err_summary, l, "No error description available");
    }
    thip->err_summary[l] = '\0';
  }

  errno = save_errno;
  serrno = save_serrno;

  return thip->err_summary;
}
