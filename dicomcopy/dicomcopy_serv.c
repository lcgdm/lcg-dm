/*
 * Copyright (C) 2007-2011 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dicomcopy_serv.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

#include <dlfcn.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#endif
#include "Cinit.h"
#include "Clogit.h"
#include "Cnetdb.h"
#include "dicomcopy_server.h"
#include "dpm.h"
#include "dpm_api.h"
#include "dpm_backend.h"
#include "dpm_server.h"
#include "dpm_util.h"
#include "dpns_api.h"
#include "marshall.h"
#include "net.h"
#include "patchlevel.h"
#include "serrno.h"
#define DEFPOLLINT 5

/*
 * Maximum number of sockets on which the server can listen
 */
#define MAX_LISTEN_SOCKS        16

int (*dpm_dicomcopyfile)(char *, char *, int *, char *);
char func[16];
char localdomain[CA_MAXHOSTNAMELEN+1];
char localhost[CA_MAXHOSTNAMELEN+1];
int maxfds;
int nbchildren = 0;
int reqctr = 0;
extern gid_t *Cdecode_groups (char *, int *);
extern int Cdomainname (char *, int);
extern int isTrustedHost (int, char *, char *, char *, char *);
int listen_ipv4only=0, listen_ipv6only=0;
static int na_key = -1;
extern char *optarg;
extern int optind;
void check_child_exit();
void wait4child();
#ifdef CSEC 
#include <Csec_api.h>
Csec_context_t sec_ctx;
char *Csec_mech;
char *Csec_auth_id;
#endif

dicomcopy_main(main_args)
struct main_args *main_args;
{
	struct addrinfo *ai;
	struct addrinfo *aitop;
	int bol;
	int c;
	time_t curtime;
	struct dpm_dbfd dbfd;
	DBLISTPTR dblistptr;
	void *dlhandle;
	void doit (int);
	struct dpm_backend_req dpm_req;
	char *dpmconfigfile = NULL;
	struct sockaddr_storage from;
	int fromlen;
	int gaierrno;
	char *getconfent();
	struct addrinfo hints;
	int i;
	time_t lastcheckconfig = 0;
	time_t lastdbpoll = 0;
	int listen_socks[MAX_LISTEN_SOCKS];
	char logfile[CA_MAXPATHLEN+1];
	int maxchildren = DPM_MAXWORKERS;
	int nfds;
	int num_listen_socks;
	int on = 1;	/* for REUSEADDR and IPV6_V6ONLY */
	char *p;
	int pid;
	void procreq();
	fd_set readfd, readmask;
	int rqfd;
	int s;
	char strport[NI_MAXSERV];
	struct timeval timeval;

	strcpy (func, "dicomcopyd");
	strcpy (logfile, LOGFILE);
	Cinitlog ("dicomcopyd", LOGFILE);

	/* process command line options if any */

	while ((c = getopt (main_args->argc, main_args->argv, "46c:l:")) != EOF) {
		switch (c) {
		case '4':
			listen_ipv4only++;
			break;
		case '6':
			listen_ipv6only++;
			break;
		case 'c':
			dpmconfigfile = optarg;
			break;
		case 'l':
			if (Cinitlog ("dicomcopyd", optarg) < 0) {
				dpmlogit (func, "Invalid logfile: %s\n", optarg);
				exit (USERR);
			}
			strcpy (logfile, optarg);
			break;
		}
	}

	if (listen_ipv4only && listen_ipv6only) {
		dpmlogit (func, "Can not choose to listen for only IPv4 and "
		    "also only for IPv6\n");
		return (USERR);
	}

	dpmlogit (func, "started (DICOMCOPYD %s-%d)\n", BASEVERSION, PATCHLEVEL);
	gethostname (localhost, CA_MAXHOSTNAMELEN+1);
	if (Cdomainname (localdomain, sizeof(localdomain)) < 0) {
		dpmlogit (func, "Unable to get local domain name\n");
		return (SYERR);
	}
	if (strchr (localhost, '.') == NULL) {
		strcat (localhost, ".");
		strcat (localhost, localdomain);
	}

	/* get DB login info from the Disk Pool Manager server config file */

	if (dpm_get_dbconf (dpmconfigfile) < 0)
		return (CONFERR);

	/* load the plugin */

	if ((dlhandle = dlopen ("libdpm_dicom.so", RTLD_NOW)) == NULL) {
		dpmlogit (func, "could not load plugin libdpm_dicom.so: %s\n",
		    dlerror());
		return (CONFERR);
	}
	dpm_dicomcopyfile = (int (*) (char *, char *, int *, char *)) dlsym (dlhandle, "dpm_dicomcopyfile");

	/* connect to the database */

	memset (&dbfd, 0, sizeof(dbfd));
	if (dpm_opendb (&dbfd) < 0)
		return (SYERR);

	FD_ZERO (&readmask);
	FD_ZERO (&readfd);
#if ! defined(_WIN32)
	signal (SIGPIPE, SIG_IGN);
	signal (SIGXFSZ, SIG_IGN);
#endif

	/* open request socket */

	serrno = 0;
	memset (&hints, 0, sizeof(struct addrinfo));
	if (listen_ipv4only)
		hints.ai_family = PF_INET;
	else if (listen_ipv6only)
		hints.ai_family = PF_INET6;
	else
		hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	if ((p = getenv ("DICOMCOPYD_PORT")) || (p = getconfent ("DICOMCOPYD", "PORT", 0))) {
		strncpy (strport, p, sizeof(strport));
		strport[sizeof(strport)-1] = '\0';
	} else {
		snprintf (strport, sizeof(strport), "%u", DICOMCOPYD_PORT);
	}

	if (gaierrno=Cgetaddrinfo (NULL, strport, &hints, &aitop)) {
		dpmlogit (func, DP002, "Cgetaddrinfo",
		    (gaierrno != EAI_SYSTEM) ? Cgai_strerror(gaierrno) : neterror());
		return (CONFERR);
	}

	num_listen_socks = 0;
	for (ai = aitop; ai; ai = ai->ai_next) {
		int fo = 0;
		if (ai->ai_family != PF_INET && ai->ai_family != PF_INET6)
			continue;
		if (num_listen_socks >= MAX_LISTEN_SOCKS) {
			dpmlogit (func, "Too many listen sockets\n");
			freeaddrinfo (aitop);
			return (CONFERR);
		}
		if ((s = socket (ai->ai_family, ai->ai_socktype, ai->ai_protocol))<0)
			continue;
		if (setsockopt (s, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on))) {
			dpmlogit (func, DP002, "setsockopt (SO_REUSEADDR)", neterror());
			close (s);
			continue;
		}
		if (ai->ai_family == PF_INET6) {
#ifdef IPV6_V6ONLY
			if (setsockopt (s, IPPROTO_IPV6, IPV6_V6ONLY,
			    (char *)&on, sizeof(on))) {
				fo = 1;
				dpmlogit (func, DP002, "setsockopt (IPV6_V6ONLY)", neterror());
			}
#else
			fo = 1;
#endif
		}
		if (bind (s, ai->ai_addr, ai->ai_addrlen) < 0) {
			dpmlogit (func, DP002, "bind", neterror());
			close (s);
			continue;
		}
		if (fo) {
#ifdef IPV6_V6ONLY
			dpmlogit (func, "Was not able to set the IPV6_V6ONLY "
			    "socket option on the IPv6 listen socket\n");
#else
			dpmlogit (func, "Was compiled on a system that does not "
			    "support the IPV6_V6ONLY socket option\n");
#endif
			if (listen_ipv6only) {
				dpmlogit (func, "Not proceeding as the IPv6 only flag was specified\n");
				return (CONFERR);
			}
			dpmlogit (func, "Incoming IPv4 will be accepted and handled as "
			    "IPv4-mapped IPv6 addresses\n");
		}
		listen_socks[num_listen_socks] = s;
		++num_listen_socks;
		listen (s, 5) ;
	}
	freeaddrinfo (aitop);

	if (num_listen_socks == 0) {
		dpmlogit (func, "Could not listen on any sockets\n");
		return (CONFERR);
	}

	nfds = -1;
	for (i=0; i<num_listen_socks; ++i) {
		FD_SET (listen_socks[i], &readmask);
		if (listen_socks[i]>nfds)
			nfds = listen_socks[i];
	}
	++nfds;

#ifdef CSEC
	/* initialise the security context for the first time */
	Csec_server_initContext (&sec_ctx, CSEC_SERVICE_TYPE_HOST, NULL);
#endif

	/* main loop */

	while (1) {
		/* check if any child has ended */

		check_child_exit();

		/* check if config parameters have changed */

		if ((curtime = time (0)) > lastcheckconfig + CHKCONFIGINT) {
			lastcheckconfig = curtime;
			if ((p = getconfent ("DICOMCOPYD", "MAX_WORKERS", 0)))
				maxchildren = atoi (p);
		}
		for (i=0; i < num_listen_socks; ++i) {
			s = listen_socks[i];
			if (FD_ISSET (s, &readfd)) {
				FD_CLR (s, &readfd);
				fromlen = sizeof(from);
				rqfd = accept (s, (struct sockaddr *) &from, &fromlen);
				doit (rqfd);
			}
		}
		while (reqctr > 0) {
			if (nbchildren + 1 > maxchildren) break;
			pid = fork ();
			switch (pid) {
			case -1:
				dpmlogit (func, DP002, "fork", strerror (errno));
				break;
			case 0:
				for (i=0; i < num_listen_socks; ++i)
					close (listen_socks[i]);
				Cinitlog ("dicomcopyd", logfile);
				procreq ();
				break;
			default:
				nbchildren++;
				dpmlogit (func, "decrementing reqctr\n");
				reqctr--;
			}
		}

		/* regularly poll the DB for work */

		if (reqctr == 0 && nbchildren < maxchildren &&
		    (curtime = time (0)) > lastdbpoll + DPM_DBPINGI) {
			lastdbpoll = curtime;
			bol = 1;
			while ((c = dpm_list_pending_dicomreq (&dbfd, bol,
			    &dpm_req, 0, &dblistptr)) == 0) {
				bol = 0;
				dpmlogit (func, "incrementing reqctr (DB poll)\n");
				reqctr++;
			}
			(void) dpm_list_pending_dicomreq (&dbfd, bol, &dpm_req,
			    1, &dblistptr);	/* free res */
		}

		memcpy (&readfd, &readmask, sizeof(readmask));
		timeval.tv_sec = CHECKI;
		timeval.tv_usec = 0;
		if (select (nfds, &readfd, (fd_set *)0, (fd_set *)0, &timeval) < 0) {
			FD_ZERO (&readfd);
		}
	}
	(void) dpm_closedb (&dbfd);
	dlclose (dlhandle);
	exit (0);
}

main(argc, argv)
int argc;
char **argv;
{
#if ! defined(_WIN32)
	struct main_args main_args;

	if ((maxfds = Cinitdaemon ("dicomcopyd", wait4child)) < 0)
		exit (SYERR);
	main_args.argc = argc;
	main_args.argv = argv;
	exit (dicomcopy_main (&main_args));
#else
	if (Cinitservice ("dicomcopyd", &dicomcopy_main))
		exit (SYERR);
#endif
}

void
doit(s)
int s;
{
	int c;
	const char *clienthost = NULL;
	const char *clientip;
	const char *clientname;
	int magic;
	char req_data[REQBUFSZ-3*LONGSIZE];
	int req_type;

	(void) Cgetnetaddress (s, NULL, 0, &na_key, &clientip, &clientname, 0, 0);
	if (clientip == NULL)
		clientip = "unknown";
	if (clientname == NULL)
		clientname = "unknown";

#ifdef CSEC
	Csec_server_reinitContext (&sec_ctx, CSEC_SERVICE_TYPE_HOST, NULL);
	if (Csec_server_establishContext (&sec_ctx, s) < 0) {
		dpmlogit (func, "[%s] (%s): Could not establish an authenticated connection: %s !\n",
		    clientip, clientname, Csec_getErrorMessageSummary (LOGBUFSZ-140));
		netclose (s);
		return;
	}
	Csec_server_getClientId (&sec_ctx, &Csec_mech, &Csec_auth_id);
	if (strcmp (Csec_mech, "ID") != 0 &&
	     Csec_isIdAService (Csec_mech, Csec_auth_id) < 0 ||
	     ! isTrustedHost (s, localhost, localdomain, "DPM", "TRUST")) {
		dpmlogit (func, "[%s] (%s): Host is not trusted, identity provided was (%s,\"%s\")\n",
		    clientip, clientname, Csec_mech, Csec_auth_id);
		sendrep (s, DPM_RC, EACCES);
		return;
	}
#endif
	if ((c = getreq (s, &magic, &req_type, req_data, &clienthost)) == 0) {
		if (req_type == DPM_INCREQCTR) {
			dpmlogit (func, "inc_reqctr request from %s\n", clienthost);
			reqctr++;
		}
	} else {
		dpmlogit (func, "[%s] (%s): Failure getting the request: %s\n",
		    clientip, clientname, sstrerror(c));
	}
	sendrep (s, DPM_RC, c);
}

int
getreq(s, magic, req_type, req_data, clienthost)
int s;
int *magic;
int *req_type;
char *req_data;
const char **clienthost;
{
	struct hostent *hp;
	int l;
	unsigned int msglen;
	int n;
	char *rbp;
	char req_hdr[3*LONGSIZE];

	serrno = 0;
	l = netread_timeout (s, req_hdr, sizeof(req_hdr), DPM_TIMEOUT);
	if (l == sizeof(req_hdr)) {
		rbp = req_hdr;
		unmarshall_LONG (rbp, n);
		*magic = n;
		unmarshall_LONG (rbp, n);
		*req_type = n;
		unmarshall_LONG (rbp, msglen);
		if (msglen > REQBUFSZ) {
			dpmlogit (func, DP046, REQBUFSZ);
			return (-1);
		}
		l = msglen - sizeof(req_hdr);
		n = netread_timeout (s, req_data, l, DPM_TIMEOUT);
		if (n > 0 && n == l) {
			if (*clienthost == NULL) {
				if ((*clienthost =
				    Cgetnetaddress (s, NULL, 0, &na_key, NULL, NULL, 0, 0)) == NULL) {
					dpmlogit (func, "Could not find the address of the client\n");
					return (SEINTERNAL);
				}
			}
			return (0);
		}
		l = n;
	}
	if (l > 0)
		dpmlogit (func, DP004, l);
	else if (l < 0) {
		dpmlogit (func, DP002, "netread", neterror());
		if (serrno == SETIMEDOUT)
			return (SETIMEDOUT);
	}
	return (SEINTERNAL);
}

char *
dpm_getturl4replica(surl, lifetime, s_token, ret_policy, f_type, r_token, status)
char *surl;
time_t lifetime;
char *s_token;
char ret_policy;
char f_type;
char *r_token;
int *status;
{
	int nbreplies;
	static char *protocols[] = {"rfio"};
	struct dpm_putfilereq putfilereq;
	struct dpm_putfilestatus *putfilestatus;
	int r = 0;
	char *turl;

	memset (&putfilereq, 0, sizeof(putfilereq));
	putfilereq.to_surl = surl;
	putfilereq.lifetime = lifetime;
	putfilereq.f_type = f_type;
	strcpy (putfilereq.s_token, s_token);
	putfilereq.ret_policy = (ret_policy == '_') ? 0 : ret_policy;
	if ((*status = dpm_put (1, &putfilereq, 1, protocols, NULL, 4,
	    0, r_token, &nbreplies, &putfilestatus)) < 0)
		return (NULL);

	/* wait for request status "Done" or "Failed" */

	while (*status == DPM_QUEUED || *status == DPM_ACTIVE) {
		if (putfilestatus->to_surl)
			free (putfilestatus->to_surl);
		if (putfilestatus->turl)
			free (putfilestatus->turl);
		if (putfilestatus->errstring)
			free (putfilestatus->errstring);
		free (putfilestatus);
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if ((*status = dpm_getstatus_putreq (r_token, 0, NULL,
		    &nbreplies, &putfilestatus)) < 0)
			return (NULL);
	}
	if (putfilestatus->turl)
		turl = putfilestatus->turl;
	if (putfilestatus->to_surl)
		free (putfilestatus->to_surl);
	if (putfilestatus->errstring)
		free (putfilestatus->errstring);
	free (putfilestatus);
	return (turl);
}

void
procreq()
{
	int alloced_gids = 0;
	int bol;
	int c;
	struct dpm_dbfd dbfd;
	DBLISTPTR dblistptr;
	struct dpm_backend_req dpm_req;
	struct dpm_dicom_filereq dfr_entry;
	struct dpm_dicom_filereq dfr_entry1;
	char errbuf[256];
	int errcode;
	struct dpm_filestatus *filestatuses;
	char **fqan = NULL;
	gid_t *gids = NULL;
	int i;
	int nb_file_err = 0;
	int nbgids;
	int nbreplies;
	char *p;
	char *pfn;
	char r_token[CA_MAXDPMTOKENLEN+1];
	dpm_dbrec_addr rec_addr;
	dpm_dbrec_addr rec_addrf;
	char server[CA_MAXHOSTNAMELEN+1];
	int status;
	char *surl;
	char *turl;
	char *voname = NULL;
	int waiting_req;

	/* Connect to the database */

	dpm_seterrbuf (errbuf, sizeof(errbuf));
	memset (&dbfd, 0, sizeof(dbfd));
	if (dpm_opendb (&dbfd) < 0)
		exit (SYERR);

	/* Get next request and mark it "active" */

	(void) dpm_start_tr (0, &dbfd);
	if (dpm_get_next_dicomreq (&dbfd, DPM_QUEUED, &dpm_req, 1, &rec_addr) < 0) {
		dpm_abort_tr (&dbfd);
		(void) dpm_closedb (&dbfd);
		exit (serrno == ENOENT ? 0 : SYERR);
	}
	dpm_req.status = DPM_ACTIVE;
	if (dpm_update_dicomreq_entry (&dbfd, &rec_addr, &dpm_req) < 0) {
		dpm_abort_tr (&dbfd);
		(void) dpm_closedb (&dbfd);
		exit (serrno == ENOENT ? 0 : SYERR);
	}
	dpm_end_tr (&dbfd);

	nbgids = 1;
	gids = &dpm_req.r_gid;
#ifdef CSEC
	if (dpm_req.r_uid) {
		dpm_client_setAuthorizationId (dpm_req.r_uid, dpm_req.r_gid,
		    "GSI", dpm_req.client_dn);
#ifdef VIRTUAL_ID
#ifdef USE_VOMS
		if ((gids = Cdecode_groups (dpm_req.groups, &nbgids)) == NULL)
			goto free_voms_info;
		alloced_gids = 1;
		if ((fqan = calloc (nbgids, sizeof(char *))) == NULL)
			goto free_voms_info;
		if (Cns_getgrpbygids (nbgids, gids, fqan) < 0)
			goto free_voms_info;
		if ((voname = strdup (fqan[0])) == NULL)
			goto free_voms_info;
		if ((p = strchr (voname, '/')))
			*p = '\0';

		/* pass the VOMS auth data to the DPM */
		dpm_client_setVOMS_data (voname, fqan, nbgids);
#endif
#endif
	}
#endif
	dpmlogit (func, "processing request %s from %s\n", dpm_req.r_token,
	    dpm_req.client_dn);
fileloop:
	waiting_req = 0;
	for (i = 0; i < dpm_req.nbreqfiles; i++) {

		/* Get File Request */

		(void) dpm_start_tr (0, &dbfd);
		if (dpm_get_dfr_by_fullid (&dbfd, dpm_req.r_token, i,
		    &dfr_entry, 1, &rec_addrf) < 0) {
			dpm_abort_tr (&dbfd);
			continue;
		}
		if (dfr_entry.status != DPM_QUEUED) {	/* No need to process */
			dpm_abort_tr (&dbfd);
			continue;
		}

		/* Check if there is already an active recall for this file */

		bol = 1;
		while ((c = dpm_list_dfr_by_surl (&dbfd, bol, dfr_entry.surl,
		    &dfr_entry1, 0, NULL, 0, &dblistptr)) == 0) {
			bol = 0;
			if (dfr_entry1.status == DPM_ACTIVE) break;
		}
		(void) dpm_list_dfr_by_surl (&dbfd, bol, dfr_entry.surl,
		    &dfr_entry1, 0, NULL, 1, &dblistptr);
		if (c != 1) {
			waiting_req++;
			dpm_abort_tr (&dbfd);
			continue;
		}

		/* Mark the file request as active */

		dfr_entry.status = DPM_ACTIVE;
		(void) dpm_update_dfr_entry (&dbfd, &rec_addrf, &dfr_entry);
		(void) dpm_end_tr (&dbfd);

		/* Get turl for replica */

		errbuf[0] = '\0';
		pfn = NULL;
		server[0] = '\0';
		surl = dfr_entry.surl;
		dpmlogit (func, "getting turl for %s\n", surl);
		if ((turl = dpm_getturl4replica (surl, dpm_req.lifetime,
		    dpm_req.s_token, dpm_req.ret_policy, dpm_req.f_type,
		    r_token, &status)) == NULL)
			goto upd_dfr;

		/* Copy file using dicom protocol */

		dpmlogit (func, "copying %s to %s\n", dfr_entry.dicom_fn, turl);
		errcode = 0;
		status = dpm_dicomcopyfile (dfr_entry.dicom_fn, turl,
		    &errcode, errbuf);
		dpmlogit (func, "dpm_dicomcopyfile returned %d, errcode = %d\n",
		    status, errcode);
		if (status < 0)
			serrno = errcode;
upd_dfr:
		filestatuses = NULL;
		if (status < 0) {
			status = DPM_FAILED | serrno;
			(void) dpm_abortfiles (r_token, 1, &surl,
			    &nbreplies, &filestatuses);
		} else {
			status = dpm_putdone (r_token, 1, &surl,
			    &nbreplies, &filestatuses);
			if (status < 0)
				status = DPM_FAILED | serrno;
		}
		dpm_free_filest (nbreplies, filestatuses);
		if (turl) {
			p = strchr (turl + 7, '/');
			*p = '\0';
			strcpy (server, turl + 7);
			*p = ':';
			pfn = turl + 7;
		}
		if (status == 0)
			status = DPM_READY;
		(void) dpm_start_tr (0, &dbfd);
		if (dpm_get_dfr_by_fullid (&dbfd, dpm_req.r_token, i,
		    &dfr_entry, 1, &rec_addrf) < 0) {
			dpm_abort_tr (&dbfd);
			continue;
		}
		if (dfr_entry.status == DPM_ACTIVE) {
			dfr_entry.status = status;
			(void) dpm_update_dfr_entry (&dbfd, &rec_addrf, &dfr_entry);
		}
		(void) dpm_end_tr (&dbfd);
		if (status != DPM_READY) {
			if (strstr (errbuf, "Failed for all SURLs"))
				errbuf[0] = '\0';
			dpmlogit (func, "%s: %s\n", surl, *errbuf ? errbuf :
			    status2str (status, errbuf));
			nb_file_err++;
		}

		dpmlogit (func, "calling dpm_updatefilestatus\n");
		dpm_updatefilestatus ('G', dpm_req.r_token, surl,
		    server, pfn, dfr_entry.status, errbuf);
		free (turl);
	}
	if (waiting_req) {
		sleep (10);
		goto fileloop;
	}

	/* Set final dicomcopy request status */

	(void) dpm_start_tr (0, &dbfd);
	if (dpm_get_dicomreq_by_token (&dbfd, dpm_req.r_token, &dpm_req,
	    1, &rec_addr) == 0) {
		if (nb_file_err == 0)
			dpm_req.status = DPM_SUCCESS;
		else if (nb_file_err != dpm_req.nbreqfiles)
			dpm_req.status = DPM_DONE;
		else {
			if (dpm_req.nbreqfiles == 1)
				dpm_req.status = dfr_entry.status;
			else
				dpm_req.status = DPM_FAILED | EINVAL;
		}
		(void) dpm_update_dicomreq_entry (&dbfd, &rec_addr, &dpm_req);
		dpm_end_tr (&dbfd);
	}
free_voms_info:
	free (voname);
	if (fqan) {
		for (i = 0; i < nbgids; i++)
			free (fqan[i]);
		free (fqan);
	}
	if (alloced_gids)
		free (gids);
	(void) dpm_closedb (&dbfd);
	exit (0);
}

#if ! defined(_WIN32)
void
wait4child()
{
}

void
check_child_exit()
{
	pid_t pid;
	int status;

	while ((pid = waitpid (-1, &status, WNOHANG)) > 0) {
		dpmlogit (func, "process %d exiting with status %x\n",
		    pid, status & 0xFFFF);
		nbchildren--;
	}
}
#endif
