/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_dicomcopyfile.c,v $ $Revision: 1.1 $ $Date: 2008/01/14 10:06:10 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include "rfio_api.h"

dpm_dicomcopyfile (char *dicom_fn, char *turl, int *errcode, char *errbuf)
{
	char *buffer;
	int bufsize = 131072;
	int inpbytes;
	int inpfd;
	int outbytes;
	int outfd;

	if ((buffer = malloc (bufsize)) == NULL) {
		*errcode = ENOMEM;
		return (-1);
	}
	if ((inpfd = rfio_open64 (dicom_fn, O_RDONLY, 0644)) < 0) {
		*errcode = rfio_serrno();
		return (-1);
	}
	if ((outfd = rfio_open64 (turl, O_WRONLY|O_CREAT, 0644)) < 0) {
		*errcode = rfio_serrno();
		rfio_close (inpfd);
		return (-1);
	}
	while (1) {
		if ((inpbytes = rfio_read (inpfd, buffer, bufsize)) < 0) {
			*errcode = rfio_serrno();
			break;
		}
		if (inpbytes == 0) break;
		if ((outbytes = rfio_write (outfd, buffer, inpbytes)) < 0) {
			*errcode = rfio_serrno();
			break;
		}
		if (outbytes < inpbytes) {
			*errcode = ENOSPC;
			outbytes = -1;
			break;
		}
	}
	rfio_close (inpfd);
	rfio_close (outfd);
	if (inpbytes < 0 || outbytes < 0)
		return (-1);
	return (0);
}
