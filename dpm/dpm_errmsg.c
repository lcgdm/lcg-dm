/*
 * Copyright (C) 2004-2008 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_errmsg.c,v $ $Revision: 1.4 $ $Date: 2008/03/25 14:36:26 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include "Csnprintf.h"
#include "dpm.h"
#include "dpm_api.h"

/*	dpm_seterrbuf - set receiving buffer for error messages */

int DLL_DECL
dpm_seterrbuf(char *buffer, int buflen)
{
	struct dpm_api_thread_info *thip;

	if (dpm_apiinit (&thip))
		return (-1);
	thip->errbufp = buffer;
	thip->errbuflen = buflen;
	return (0);
}

/* dpm_errmsg - send error message to user defined client buffer or to stderr */

int DLL_DECL
dpm_errmsg(char *func, char *msg, ...)
{
	va_list args;
	char prtbuf[PRTBUFSZ];
	int save_errno;
	struct dpm_api_thread_info *thip;

	save_errno = errno;
	if (dpm_apiinit (&thip))
		return (-1);
	va_start (args, msg);
	if (func) {
		Csnprintf (prtbuf, PRTBUFSZ, "%s: ", func);
		prtbuf[PRTBUFSZ-1] = '\0';
	} else {
		*prtbuf = '\0';
	}
	if ((strlen(prtbuf) + 1) < PRTBUFSZ) {
		Cvsnprintf (prtbuf + strlen(prtbuf), PRTBUFSZ - strlen(prtbuf), msg, args);
		prtbuf[PRTBUFSZ-1] = '\0';
	}
	va_end (args);
	if (thip->errbufp) {
		if (strlen (prtbuf) < thip->errbuflen) {
			strcpy (thip->errbufp, prtbuf);
		} else if (thip->errbuflen > 2) {
			strncpy (thip->errbufp, prtbuf, thip->errbuflen - 2);
			thip->errbufp[thip->errbuflen-2] = '\n';
			thip->errbufp[thip->errbuflen-1] = '\0';
		}
	} else {
		fprintf (stderr, "%s", prtbuf);
	}
	errno = save_errno;
	return (0);
}
