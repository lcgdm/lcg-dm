.\" @(#)$RCSfile: dpm_delreplica.man,v $ $Revision: 1.1 $ $Date: 2006/04/08 14:51:17 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2006 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH DPM_DELREPLICA 3 "$Date: 2006/04/08 14:51:17 $" LCG "DPM Library Functions"
.SH NAME
dpm_delreplica \- delete a given replica
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_delreplica (char *" pfn )
.SH DESCRIPTION
.B dpm_delreplica
deletes a given replica.
It removes the file from disk, updates the pool free space and removes the
replica entry from the DPNS.
.TP
.I pfn
is the Physical File Name for the replica.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file or replica does not exist.
.TP
.B EACCES
Search permission is denied on a component of the parent directory or
the effective user ID does not match the owner of the file or
write permission on the file entry itself is denied.
.TP
.B EFAULT
.I pfn
is a NULL pointer.
.TP
.B ENAMETOOLONG
The length of
.I pfn
exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR dpns_addreplica(3) ,
.BR dpns_listreplica(3)
