/*
 * Copyright (C) 2007-2008 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm-getspacetokens.c,v $ $Revision: 1.2 $ $Date: 2008/01/29 06:29:13 $ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*      dpm-getspacetokens - get list of space tokens */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "Cgetopt.h"
#include "dpm_api.h"
#include "serrno.h"
int help_flag;

main(argc, argv)
int argc;
char **argv;
{
	int c;
	int errflg = 0;
	int i;
	static struct Coptions longopts[] = {
		{"help", NO_ARGUMENT, &help_flag, 1},
		{"token_desc", REQUIRED_ARGUMENT, 0, OPT_U_DESC},
		{0, 0, 0, 0}
	};
	int nbreplies = 0;
	char **s_tokens = NULL;
	char *u_token = NULL;

	Copterr = 1;
	Coptind = 1;
	while ((c = Cgetopt_long (argc, argv, "", longopts, NULL)) != EOF) {
		switch (c) {
		case OPT_U_DESC:
			u_token = Coptarg;
			break;
		case '?':
			errflg++;
			break;
		default:
			break;
		}
	}
	if (Coptind < argc) {
		errflg++;
	}
	if (errflg || help_flag) {
		fprintf (stderr, "usage: %s %s", argv[0],
		    "\t[--token_desc user_space_token_description]\n");
		exit (errflg ? USERR : 0);
	}

	if (dpm_getspacetoken (u_token, &nbreplies, &s_tokens) < 0) {
		fprintf (stderr, "dpm-getspacetokens: %s\n", sstrerror(serrno));
		for (i = 0; i < nbreplies; i++)
			free (s_tokens[i]);
		free (s_tokens);
		exit (serrno == EINVAL ? USERR : SYERR);
	}
	for (i = 0; i < nbreplies; i++) {
		printf ("%s\n", s_tokens[i]);
		free (s_tokens[i]);
	}
	free (s_tokens);
	exit (0);
}
