.\" @(#)$RCSfile: dpm_getprotocols.man,v $ $Revision: 1.1.1.1 $ $Date: 2004/12/15 06:53:28 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_GETPROTOCOLS 3 "$Date: 2004/12/15 06:53:28 $" LCG "DPM Library Functions"
.SH NAME
dpm_getprotocols \- get the list of supported protocols
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getprotocols (int *" nb_supported_protocols ,
.BI "char ***" supported_protocols )
.SH DESCRIPTION
.B dpm_getprotocols
gets the list of supported protocols.
.TP
.I nb_supported_protocols
will be set to the number of protocols in the array of supported protocols.
.TP
.I supported_protocols
will be set to the address of an array of pointers to the supported protocols.
This array of pointers is allocated by the API and the client application is
responsible for freeing the array when not needed anymore.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I nb_supported_protocols
or
.I supported_protocols
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for storing the array of supported protocols.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SECOMERR
Communication error.
