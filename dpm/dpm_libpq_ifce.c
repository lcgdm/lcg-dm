/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_libpq_ifce.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm.h"
#include "dpm_server.h"
#include "serrno.h"
#include "u64subr.h"
gid_t *Cdecode_groups (char *, int *);
char *Cencode_groups (int, gid_t *, char *, size_t);

static char db_name[33];
static char db_pwd[33];
static char db_srvr[33];
static char db_user[33];

int
dpm_get_dbconf(dpmconfig)
char *dpmconfig;
{
	FILE *cf;
	char cfbuf[80];
	char func[16];
	char dpmconfigfile[CA_MAXPATHLEN+1];
	char *p;
	char *p_n, *p_p, *p_s, *p_u;
	int rc = 0;

	strcpy (func, "dpm_get_dbconf");

	/* get DB login info from the Disk Pool Manager server config file */

	if (! dpmconfig) {
		if (strncmp (DPMCONFIG, "%SystemRoot%\\", 13) == 0 &&
		    (p = getenv ("SystemRoot")))
			sprintf (dpmconfigfile, "%s%s", p, strchr (DPMCONFIG, '\\'));
		else
			strcpy (dpmconfigfile, DPMCONFIG);
	} else if (strlen (dpmconfig) > CA_MAXPATHLEN) {
		serrno = ENAMETOOLONG;
		return (-1);
	} else
		strcpy (dpmconfigfile, dpmconfig);

	if ((cf = fopen (dpmconfigfile, "r")) == NULL) {
		dpmlogit (func, DP023, dpmconfigfile);
		serrno = SENOCONFIG;
		return (-1);
	}
	if (fgets (cfbuf, sizeof(cfbuf), cf) &&
	    strlen (cfbuf) >= 5 && (p_u = strtok (cfbuf, "/\n")) &&
	    (p_p = strtok (NULL, "@\n")) && (p_s = strtok (NULL, "/\n"))) {
		p_n = strtok (NULL, "\n");
		if (strlen (p_u) >= sizeof(db_user) ||
		    strlen (p_p) >= sizeof(db_pwd) ||
		    strlen (p_s) >= sizeof(db_srvr) ||
		    (p_n && strlen (p_n) >= sizeof(db_name))) {
			dpmlogit (func, DP009, dpmconfigfile, "incorrect");
			serrno = EINVAL;
			rc = -1;
		} else {
			strcpy (db_user, p_u);
			strcpy (db_pwd, p_p);
			strcpy (db_srvr, p_s);
			if (p_n)
				strcpy (db_name, p_n);
			else
				strcpy (db_name, "dpm_db");
		}
	} else {
		dpmlogit (func, DP009, dpmconfigfile, "incorrect");
		serrno = EINVAL;
		rc = -1;
	}
	(void) fclose (cf);
	return (rc);
}

dpm_init_dbpkg()
{
	return (0);
}

void
dpm_libpq_error(func, sql_method, res, dbfd)
char *func;
char *sql_method;
PGresult *res;
struct dpm_dbfd *dbfd;
{
	ConnStatusType status = PQstatus (dbfd->Pconn);

	dpmlogit (func, "%s error: %s\n", sql_method, PQresultErrorMessage (res));
	PQclear (res);
	if (status == CONNECTION_BAD) {
		dpmlogit (func, "Trying to reconnect\n");
		dpm_closedb (dbfd);
		if (dpm_opendb (dbfd) < 0)
			dpmlogit (func, "Reconnect failed\n");
	}
	serrno = SEINTERNAL;
}

dpm_abort_tr(dbfd)
struct dpm_dbfd *dbfd;
{
	PGresult *res;

	res = PQexec (dbfd->Pconn, "ROLLBACK");
	PQclear (res);	/* to avoid memory leaks */
	dbfd->tr_started = 0;
	return (0);
}

dpm_closedb(dbfd)
struct dpm_dbfd *dbfd;
{
	PQfinish (dbfd->Pconn);
	return (0);
}

dpm_decode_cpr_entry(res, row, lock, rec_addr, cpr_entry)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_copy_filereq *cpr_entry;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	strcpy (cpr_entry->r_token, PQgetvalue (res, row, i++));
	cpr_entry->f_ordinal = atoi (PQgetvalue (res, row, i++));
	strcpy (cpr_entry->from_surl, PQgetvalue (res, row, i++));
	strcpy (cpr_entry->to_surl, PQgetvalue (res, row, i++));
	cpr_entry->f_lifetime = atoi (PQgetvalue (res, row, i++));
	cpr_entry->f_type = *(PQgetvalue (res, row, i++));
	strcpy (cpr_entry->s_token, PQgetvalue (res, row, i++));
	cpr_entry->ret_policy = *(PQgetvalue (res, row, i++));
	cpr_entry->ac_latency = *(PQgetvalue (res, row, i++));
	cpr_entry->flags = atoi (PQgetvalue (res, row, i++));
	cpr_entry->actual_size = strtou64 (PQgetvalue (res, row, i++));
	cpr_entry->status = atoi (PQgetvalue (res, row, i++));
	strcpy (cpr_entry->errstring, PQgetvalue (res, row, i));
}

dpm_decode_fs_entry(res, row, lock, rec_addr, fs_entry)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_fs *fs_entry;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	strcpy (fs_entry->poolname, PQgetvalue (res, row, i++));
	strcpy (fs_entry->server, PQgetvalue (res, row, i++));
	strcpy (fs_entry->fs, PQgetvalue (res, row, i++));
	fs_entry->status = atoi (PQgetvalue (res, row, i++));
	fs_entry->weight = atoi (PQgetvalue (res, row, i));
}

dpm_decode_gfr_entry(res, row, lock, rec_addr, gfr_entry)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_get_filereq *gfr_entry;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	strcpy (gfr_entry->r_token, PQgetvalue (res, row, i++));
	gfr_entry->f_ordinal = atoi (PQgetvalue (res, row, i++));
	gfr_entry->r_uid = atoi (PQgetvalue (res, row, i++));
	strcpy (gfr_entry->from_surl, PQgetvalue (res, row, i++));
	strcpy (gfr_entry->protocol, PQgetvalue (res, row, i++));
	gfr_entry->lifetime = atoi (PQgetvalue (res, row, i++));
	gfr_entry->f_type = *(PQgetvalue (res, row, i++));
	strcpy (gfr_entry->s_token, PQgetvalue (res, row, i++));
	gfr_entry->ret_policy = *(PQgetvalue (res, row, i++));
	gfr_entry->flags = atoi (PQgetvalue (res, row, i++));
	strcpy (gfr_entry->server, PQgetvalue (res, row, i++));
	strcpy (gfr_entry->pfn, PQgetvalue (res, row, i++));
	gfr_entry->actual_size = strtou64 (PQgetvalue (res, row, i++));
	gfr_entry->status = atoi (PQgetvalue (res, row, i++));
	strcpy (gfr_entry->errstring, PQgetvalue (res, row, i));
}

dpm_decode_pfr_entry(res, row, lock, rec_addr, pfr_entry)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_put_filereq *pfr_entry;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	strcpy (pfr_entry->r_token, PQgetvalue (res, row, i++));
	pfr_entry->f_ordinal = atoi (PQgetvalue (res, row, i++));
	strcpy (pfr_entry->to_surl, PQgetvalue (res, row, i++));
	strcpy (pfr_entry->protocol, PQgetvalue (res, row, i++));
	pfr_entry->lifetime = atoi (PQgetvalue (res, row, i++));
	pfr_entry->f_lifetime = atoi (PQgetvalue (res, row, i++));
	pfr_entry->f_type = *(PQgetvalue (res, row, i++));
	strcpy (pfr_entry->s_token, PQgetvalue (res, row, i++));
	pfr_entry->ret_policy = *(PQgetvalue (res, row, i++));
	pfr_entry->ac_latency = *(PQgetvalue (res, row, i++));
	pfr_entry->requested_size = strtou64 (PQgetvalue (res, row, i++));
	strcpy (pfr_entry->server, PQgetvalue (res, row, i++));
	strcpy (pfr_entry->pfn, PQgetvalue (res, row, i++));
	pfr_entry->actual_size = strtou64 (PQgetvalue (res, row, i++));
	pfr_entry->status = atoi (PQgetvalue (res, row, i++));
	strcpy (pfr_entry->errstring, PQgetvalue (res, row, i));
}

dpm_decode_pool_entry(res, row, lock, rec_addr, pool_entry)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_pool *pool_entry;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	strcpy (pool_entry->poolname, PQgetvalue (res, row, i++));
	pool_entry->defsize = strtou64 (PQgetvalue (res, row, i++));
	pool_entry->gc_start_thresh = atoi (PQgetvalue (res, row, i++));
	pool_entry->gc_stop_thresh = atoi (PQgetvalue (res, row, i++));
	pool_entry->def_lifetime = atoi (PQgetvalue (res, row, i++));
	pool_entry->defpintime = atoi (PQgetvalue (res, row, i++));
	pool_entry->max_lifetime = atoi (PQgetvalue (res, row, i++));
	pool_entry->maxpintime = atoi (PQgetvalue (res, row, i++));
	strcpy (pool_entry->fss_policy, PQgetvalue (res, row, i++));
	strcpy (pool_entry->gc_policy, PQgetvalue (res, row, i++));
	strcpy (pool_entry->mig_policy, PQgetvalue (res, row, i++));
	strcpy (pool_entry->rs_policy, PQgetvalue (res, row, i++));
	pool_entry->gids = Cdecode_groups (PQgetvalue (res, row, i++), &pool_entry->nbgids);
	pool_entry->ret_policy = *(PQgetvalue (res, row, i++));
	pool_entry->s_type = *(PQgetvalue (res, row, i));
}

dpm_decode_spcmd_entry(res, row, lock, rec_addr, dpm_spcmd)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_space_reserv *dpm_spcmd;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	strcpy (dpm_spcmd->s_token, PQgetvalue (res, row, i++));
	strcpy (dpm_spcmd->client_dn, PQgetvalue (res, row, i++));
	dpm_spcmd->s_uid = atoi (PQgetvalue (res, row, i++));
	dpm_spcmd->s_gid = atoi (PQgetvalue (res, row, i++));
	dpm_spcmd->ret_policy = *(PQgetvalue (res, row, i++));
	dpm_spcmd->ac_latency = *(PQgetvalue (res, row, i++));
	dpm_spcmd->s_type = *(PQgetvalue (res, row, i++));
	strcpy (dpm_spcmd->u_token, PQgetvalue (res, row, i++));
	dpm_spcmd->t_space = strtou64 (PQgetvalue (res, row, i++));
	dpm_spcmd->g_space = strtou64 (PQgetvalue (res, row, i++));
	dpm_spcmd->u_space = strtoi64 (PQgetvalue (res, row, i++));
	strcpy (dpm_spcmd->poolname, PQgetvalue (res, row, i++));
	dpm_spcmd->assign_time = atoi (PQgetvalue (res, row, i++));
	dpm_spcmd->expire_time = atoi (PQgetvalue (res, row, i++));
	strcpy (dpm_spcmd->groups, PQgetvalue (res, row, i));
}

dpm_decode_xferreq_entry(res, row, lock, rec_addr, dpm_req)
PGresult *res;
int row;
int lock;
dpm_dbrec_addr *rec_addr;
struct dpm_req *dpm_req;
{
	int i = 0;

	if (lock)
		strcpy (*rec_addr, PQgetvalue (res, row, i++));
	dpm_req->r_ordinal = atoi (PQgetvalue (res, row, i++));
	strcpy (dpm_req->r_token, PQgetvalue (res, row, i++));
	dpm_req->r_uid = atoi (PQgetvalue (res, row, i++));
	dpm_req->r_gid = atoi (PQgetvalue (res, row, i++));
	strcpy (dpm_req->client_dn, PQgetvalue (res, row, i++));
	strcpy (dpm_req->clienthost, PQgetvalue (res, row, i++));
	dpm_req->r_type = *(PQgetvalue (res, row, i++));
	strcpy (dpm_req->u_token, PQgetvalue (res, row, i++));
	dpm_req->flags = atoi (PQgetvalue (res, row, i++));
	dpm_req->retrytime = atoi (PQgetvalue (res, row, i++));
	dpm_req->nbreqfiles = atoi (PQgetvalue (res, row, i++));
	dpm_req->ctime = atoi (PQgetvalue (res, row, i++));
	dpm_req->stime = atoi (PQgetvalue (res, row, i++));
	dpm_req->etime = atoi (PQgetvalue (res, row, i++));
	dpm_req->status = atoi (PQgetvalue (res, row, i++));
	strcpy (dpm_req->errstring, PQgetvalue (res, row, i++));
	strcpy (dpm_req->groups, PQgetvalue (res, row, i));
}

dpm_delete_cprs_by_token(dbfd, r_token)
struct dpm_dbfd *dbfd;
char *r_token;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_copy_filereq WHERE r_token = '%s'";
	char func[25];
	PGresult *res;
	char sql_stmt[90];

	strcpy (func, "dpm_delete_cprs_by_token");
	sprintf (sql_stmt, delete_stmt, r_token);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_delete_fs_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_fs WHERE ROWID = %s";
	char func[20];
	PGresult *res;
	char sql_stmt[70];

	strcpy (func, "dpm_delete_fs_entry");
	sprintf (sql_stmt, delete_stmt, *rec_addr);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_delete_gfrs_by_token(dbfd, r_token)
struct dpm_dbfd *dbfd;
char *r_token;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_get_filereq WHERE r_token = '%s'";
	char func[25];
	PGresult *res;
	char sql_stmt[90];

	strcpy (func, "dpm_delete_gfrs_by_token");
	sprintf (sql_stmt, delete_stmt, r_token);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_delete_pending_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_pending_req WHERE ROWID = %s";
	char func[25];
	PGresult *res;
	char sql_stmt[70];

	strcpy (func, "dpm_delete_pending_entry");
	sprintf (sql_stmt, delete_stmt, *rec_addr);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_delete_pfrs_by_token(dbfd, r_token)
struct dpm_dbfd *dbfd;
char *r_token;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_put_filereq WHERE r_token = '%s'";
	char func[25];
	PGresult *res;
	char sql_stmt[90];

	strcpy (func, "dpm_delete_pfrs_by_token");
	sprintf (sql_stmt, delete_stmt, r_token);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_delete_pool_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_pool WHERE ROWID = %s";
	char func[22];
	PGresult *res;
	char sql_stmt[70];

	strcpy (func, "dpm_delete_pool_entry");
	sprintf (sql_stmt, delete_stmt, *rec_addr);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_delete_req_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_req WHERE ROWID = %s";
	char func[21];
	PGresult *res;
	char sql_stmt[70];

	strcpy (func, "dpm_delete_req_entry");
	sprintf (sql_stmt, delete_stmt, *rec_addr);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_delete_spcmd_entry(dbfd, rec_addr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
{
	static char delete_stmt[] =
		"DELETE FROM dpm_space_reserv WHERE ROWID = %s";
	char func[23];
	PGresult *res;
	char sql_stmt[70];

	strcpy (func, "dpm_delete_spcmd_entry");
	sprintf (sql_stmt, delete_stmt, *rec_addr);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "DELETE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_end_tr(dbfd)
struct dpm_dbfd *dbfd;
{
	PGresult *res;

	res = PQexec (dbfd->Pconn, "COMMIT");
	PQclear (res);	/* to avoid memory leaks */
	dbfd->tr_started = 0;
	return (0);
}

dpm_get_cpr_by_fullid(dbfd, r_token, f_ordinal, cpr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
int f_ordinal;
struct dpm_copy_filereq *cpr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[22];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_cpr_by_fullid");
	sprintf (sql_stmt, lock ? query4upd : query, r_token, f_ordinal);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_cpr_entry (res, 0, lock, rec_addr, cpr_entry);
	PQclear (res);
	return (0);
}

dpm_get_cpr_by_surl(dbfd, r_token, surl, cpr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *surl;
struct dpm_copy_filereq *cpr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[20];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND (from_surl = '%s' OR to_surl = '%s')";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND (from_surl = '%s' OR to_surl = '%s') \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[4700];

	strcpy (func, "dpm_get_cpr_by_surl");
	PQescapeStringConn (dbfd->Pconn, escaped_name, surl,
	    strlen (surl), NULL);
	sprintf (sql_stmt, lock ? query4upd : query, r_token, escaped_name, escaped_name);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_cpr_entry (res, 0, lock, rec_addr, cpr_entry);
	PQclear (res);
	return (0);
}

dpm_get_cpr_by_surls(dbfd, r_token, from_surl, to_surl, cpr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *from_surl;
char *to_surl;
struct dpm_copy_filereq *cpr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_name2[CA_MAXSFNLEN*2+1];
	char func[21];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND from_surl = '%s' AND to_surl = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' AND from_surl = '%s' AND to_surl = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[4699];

	strcpy (func, "dpm_get_cpr_by_surls");
	PQescapeStringConn (dbfd->Pconn, escaped_name, from_surl,
	    strlen (from_surl), NULL);
	PQescapeStringConn (dbfd->Pconn, escaped_name2, to_surl,
	    strlen (to_surl), NULL);
	sprintf (sql_stmt, lock ? query4upd : query, r_token, escaped_name, escaped_name2);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_cpr_entry (res, 0, lock, rec_addr, cpr_entry);
	PQclear (res);
	return (0);
}

dpm_get_fs_entry(dbfd, server, fs, fs_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *server;
char *fs;
struct dpm_fs *fs_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[17];
	static char query[] =
		"SELECT \
		 POOLNAME, SERVER, FS, STATUS, WEIGHT \
		FROM dpm_fs \
		WHERE server = '%s' AND fs = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 POOLNAME, SERVER, FS, STATUS, WEIGHT \
		FROM dpm_fs \
		WHERE server = '%s' AND fs = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_fs_entry");
	sprintf (sql_stmt, lock ? query4upd : query, server, fs);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_fs_entry (res, 0, lock, rec_addr, fs_entry);
	PQclear (res);
	return (0);
}

dpm_get_gfr_by_fullid(dbfd, r_token, f_ordinal, gfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
int f_ordinal;
struct dpm_get_filereq *gfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[22];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_gfr_by_fullid");
	sprintf (sql_stmt, lock ? query4upd : query, r_token, f_ordinal);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_gfr_entry (res, 0, lock, rec_addr, gfr_entry);
	PQclear (res);
	return (0);
}

dpm_get_gfr_by_pfn(dbfd, bol, pfn, gfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol; 
char *pfn;
struct dpm_get_filereq *gfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE pfn = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE pfn = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[2423];

	strcpy (func, "dpm_get_gfr_by_pfn");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		PQescapeStringConn (dbfd->Pconn, escaped_name, pfn,
		    strlen (pfn), NULL);
		sprintf (sql_stmt, lock ? query4upd : query, escaped_name);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_gfr_entry (dblistptr->res, dblistptr->row, lock, rec_addr, gfr_entry);
	dblistptr->row++;
	return (0);
}

dpm_get_gfr_by_surl(dbfd, r_token, from_surl, gfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *from_surl;
struct dpm_get_filereq *gfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[20];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' AND from_surl = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' AND from_surl = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[2482];

	strcpy (func, "dpm_get_gfr_by_surl");
	PQescapeStringConn (dbfd->Pconn, escaped_name, from_surl,
	    strlen (from_surl), NULL);
	sprintf (sql_stmt, lock ? query4upd : query, r_token, escaped_name);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_gfr_entry (res, 0, lock, rec_addr, gfr_entry);
	PQclear (res);
	return (0);
}

dpm_get_gfr_by_surl_uid(dbfd, bol, from_surl, uid, gfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol; 
char *from_surl;
uid_t uid;
struct dpm_get_filereq *gfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[24];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE from_surl = '%s' AND R_UID = %d AND STATUS < %d";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE from_surl = '%s' AND R_UID = %d AND STATUS < %d \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[2475];

	strcpy (func, "dpm_get_gfr_by_surl_uid");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		PQescapeStringConn (dbfd->Pconn, escaped_name, from_surl,
		    strlen (from_surl), NULL);
		sprintf (sql_stmt, lock ? query4upd : query, escaped_name, uid, DPM_FAILED);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_gfr_entry (dblistptr->res, dblistptr->row, lock, rec_addr, gfr_entry);
	dblistptr->row++;
	return (0);
}

dpm_get_max_get_lifetime(dbfd, pfn, lifetime)
struct dpm_dbfd *dbfd;
char *pfn;
time_t *lifetime;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[25];
	static char query[] =
		"SELECT MAX(lifetime) \
		FROM dpm_get_filereq \
		WHERE pfn = '%s'";
	PGresult *res;
	char sql_stmt[2267];

	strcpy (func, "dpm_get_max_get_lifetime");
	PQescapeStringConn (dbfd->Pconn, escaped_name, pfn,
	    strlen (pfn), NULL);
	sprintf (sql_stmt, query, escaped_name);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	if (PQgetisnull (res, 0, 0)) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	*lifetime = atoi (PQgetvalue (res, 0, 0));
	PQclear (res);
	return (0);
}

dpm_get_next_req(dbfd, status, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
int status;
struct dpm_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[17];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE status = %d \
		ORDER BY ctime, r_ordinal \
		LIMIT 1 \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_next_req");
	sprintf (sql_stmt, query4upd, status);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_xferreq_entry (res, 0, lock, rec_addr, dpm_req);
	PQclear (res);
	return (0);
}

dpm_get_pending_req_by_token(dbfd, r_token, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
struct dpm_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[29];
	static char query[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE r_token = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE r_token = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_pending_req_by_token");
	sprintf (sql_stmt, lock ? query4upd : query, r_token);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_xferreq_entry (res, 0, lock, rec_addr, dpm_req);
	PQclear (res);
	return (0);
}

dpm_get_pending_reqs_by_u_desc(dbfd, bol, u_token, uid, dpm_req, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *u_token;
uid_t uid;
struct dpm_req *dpm_req; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[31];
	static char queryo[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE r_uid = %d";
	static char queryto[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		WHERE u_token = '%s' AND r_uid = %d";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_pending_reqs_by_u_desc");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		if (*u_token)
			sprintf (sql_stmt, queryto, u_token, uid);
		else
			sprintf (sql_stmt, queryo, uid);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_xferreq_entry (dblistptr->res, dblistptr->row, 0, NULL, dpm_req);
	dblistptr->row++;
	return (0);
}

dpm_get_pfr_by_fullid(dbfd, r_token, f_ordinal, pfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
int f_ordinal;
struct dpm_put_filereq *pfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[22];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' AND f_ordinal = %d \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_pfr_by_fullid");
	sprintf (sql_stmt, lock ? query4upd : query, r_token, f_ordinal);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_pfr_entry (res, 0, lock, rec_addr, pfr_entry);
	PQclear (res);
	return (0);
}

dpm_get_pfr_by_pfn(dbfd, pfn, pfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *pfn;
struct dpm_put_filereq *pfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE pfn = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE pfn = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[2450];

	strcpy (func, "dpm_get_pfr_by_pfn");
	PQescapeStringConn (dbfd->Pconn, escaped_name, pfn,
	    strlen (pfn), NULL);
	sprintf (sql_stmt, lock ? query4upd : query, escaped_name);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_pfr_entry (res, 0, lock, rec_addr, pfr_entry);
	PQclear (res);
	return (0);
}

dpm_get_pfr_by_surl(dbfd, r_token, to_surl, pfr_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
char *to_surl;
struct dpm_put_filereq *pfr_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char func[20];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' AND to_surl = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' AND to_surl = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[2507];

	strcpy (func, "dpm_get_pfr_by_surl");
	PQescapeStringConn (dbfd->Pconn, escaped_name, to_surl,
	    strlen (to_surl), NULL);
	sprintf (sql_stmt, lock ? query4upd : query, r_token, escaped_name);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_pfr_entry (res, 0, lock, rec_addr, pfr_entry);
	PQclear (res);
	return (0);
}

dpm_get_pool_entry(dbfd, poolname, pool_entry, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *poolname;
struct dpm_pool *pool_entry;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[19];
	static char query[] =
		"SELECT \
		 POOLNAME, DEFSIZE, GC_START_THRESH, \
		 GC_STOP_THRESH, DEF_LIFETIME, DEFPINTIME, \
		 MAX_LIFETIME, MAXPINTIME, \
		 FSS_POLICY, GC_POLICY, MIG_POLICY, RS_POLICY, \
		 GROUPS, RET_POLICY, S_TYPE \
		FROM dpm_pool \
		WHERE poolname = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 POOLNAME, DEFSIZE, GC_START_THRESH, \
		 GC_STOP_THRESH, DEF_LIFETIME, DEFPINTIME, \
		 MAX_LIFETIME, MAXPINTIME, \
		 FSS_POLICY, GC_POLICY, MIG_POLICY, RS_POLICY, \
		 GROUPS, RET_POLICY, S_TYPE \
		FROM dpm_pool \
		WHERE poolname = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_pool_entry");
	sprintf (sql_stmt, lock ? query4upd : query, poolname);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_pool_entry (res, 0, lock, rec_addr, pool_entry);
	PQclear (res);
	return (0);
}

dpm_get_req_by_token(dbfd, r_token, dpm_req, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *r_token;
struct dpm_req *dpm_req;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[21];
	static char query[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE r_token = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE r_token = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_req_by_token");
	sprintf (sql_stmt, lock ? query4upd : query, r_token);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_xferreq_entry (res, 0, lock, rec_addr, dpm_req);
	PQclear (res);
	return (0);
}

dpm_get_reqs4del(dbfd, bol, time_lower, time_upper, max_recs, dpm_req, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
time_t time_lower;
time_t time_upper;
int max_recs;
struct dpm_req *dpm_req;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[17];
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE ctime = %d \
		AND (status = %d OR status >= %d)";
	static char query4updlim1[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE ctime <= %d \
		AND (status = %d OR status >= %d) \
		LIMIT %d";
	static char query4updlim2[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE ctime >= %d AND ctime <= %d \
		AND (status = %d OR status >= %d) \
		LIMIT %d";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_reqs4del");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		if (max_recs) {
			if (time_lower != 0)
				sprintf (sql_stmt, query4updlim2, time_lower, time_upper, DPM_SUCCESS, DPM_DONE, max_recs);
			else
				sprintf (sql_stmt, query4updlim1, time_upper, DPM_SUCCESS, DPM_DONE, max_recs);
		} else {
			if (time_lower != time_upper) {
				serrno = EINVAL;
				return (-1);
			}
			sprintf (sql_stmt, query4upd, time_lower, DPM_SUCCESS, DPM_DONE);
		}
		res = PQexec (dbfd->Pconn, sql_stmt);
		 if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_xferreq_entry (dblistptr->row, 1, rec_addr, dpm_req);
	dblistptr->row++;
	return (0);
}

dpm_get_reqs_by_u_desc(dbfd, bol, u_token, uid, dpm_req, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *u_token;
uid_t uid;
struct dpm_req *dpm_req; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[23];
	static char queryo[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE r_uid = %d";
	static char queryto[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_req \
		WHERE u_token = '%s' AND r_uid = %d";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_reqs_by_u_desc");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		if (*u_token)
			sprintf (sql_stmt, queryto, u_token, uid);
		else
			sprintf (sql_stmt, queryo, uid);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_xferreq_entry (dblistptr->res, dblistptr->row, 0, NULL, dpm_req);
	dblistptr->row++;
	return (0);
}

dpm_get_spcmd_by_token(dbfd, s_token, dpm_spcmd, lock, rec_addr)
struct dpm_dbfd *dbfd;
char *s_token;
struct dpm_space_reserv *dpm_spcmd;
int lock;
dpm_dbrec_addr *rec_addr;
{
	char func[23];
	static char query[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE s_token = '%s'";
	static char query4upd[] =
		"SELECT ROWID, \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE s_token = '%s' \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_spcmd_by_token");
	sprintf (sql_stmt, lock ? query4upd : query, s_token);
	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	if (PQntuples (res) == 0) {
		PQclear (res);
		serrno = ENOENT;
		return (-1);
	}
	dpm_decode_spcmd_entry (res, 0, lock, rec_addr, dpm_spcmd);
	PQclear (res);
	return (0);
}

dpm_get_spcmd_by_u_desc(dbfd, bol, u_token, uid, dpm_spcmd, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *u_token;
uid_t uid;
struct dpm_space_reserv *dpm_spcmd; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[24];
	static char querya[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv";
	static char queryo[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE (s_uid = 0 OR s_uid = %d)";
	static char queryto[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE u_token = '%s' AND (s_uid = 0 OR s_uid = %d)";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_get_spcmd_by_u_desc");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		if (*u_token)
			sprintf (sql_stmt, queryto, u_token, uid);
		else if (uid)
			sprintf (sql_stmt, queryo, uid);
		else
			strcpy (sql_stmt, querya);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_spcmd_entry (dblistptr->res, dblistptr->row, 0, NULL, dpm_spcmd);
	dblistptr->row++;
	return (0);
}

dpm_insert_cpr_entry(dbfd, cpr_entry)
struct dpm_dbfd *dbfd;
struct dpm_copy_filereq *cpr_entry;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_name2[CA_MAXSFNLEN*2+1];
	char filesize_str[21];
	char func[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_copy_filereq \
		(R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING) \
		VALUES \
		('%s', %d, '%s', '%s', %d, '%c', '%s', '%c', '%c', %d, %s, %d, '%s')";
	PGresult *res;
	char sql_stmt[5029];

	strcpy (func, "dpm_insert_cpr_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, cpr_entry->from_surl,
	    strlen (cpr_entry->from_surl), NULL);
	PQescapeStringConn (dbfd->Pconn, escaped_name2, cpr_entry->to_surl,
	    strlen (cpr_entry->to_surl), NULL);
	sprintf (sql_stmt, insert_stmt,
	    cpr_entry->r_token, cpr_entry->f_ordinal, escaped_name,
	    escaped_name2, cpr_entry->f_lifetime, cpr_entry->f_type,
	    cpr_entry->s_token, cpr_entry->ret_policy, cpr_entry->ac_latency,
	    cpr_entry->flags,
	    u64tostr (cpr_entry->actual_size, filesize_str, -1),
	    cpr_entry->status, cpr_entry->errstring);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_insert_fs_entry(dbfd, fs_entry)
struct dpm_dbfd *dbfd;
struct dpm_fs *fs_entry;
{
	char func[20];
	static char insert_stmt[] =
		"INSERT INTO dpm_fs \
		(POOLNAME, SERVER, FS, STATUS, WEIGHT) \
		VALUES \
		('%s', '%s', '%s', %d, %d)";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_insert_fs_entry");
	sprintf (sql_stmt, insert_stmt,
	    fs_entry->poolname, fs_entry->server, fs_entry->fs,
	    fs_entry->status, fs_entry->weight);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_insert_gfr_entry(dbfd, gfr_entry)
struct dpm_dbfd *dbfd;
struct dpm_get_filereq *gfr_entry;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_name2[CA_MAXSFNLEN*2+1];
	char filesize_str[21];
	char func[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_get_filereq \
		(R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING) \
		VALUES \
		('%s', %d, %d, '%s', '%s', %d, '%c', '%s', '%c', %d, '%s', '%s', %s, %d, '%s')";
	PGresult *res;
	char sql_stmt[5120];

	strcpy (func, "dpm_insert_gfr_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, gfr_entry->from_surl,
	    strlen (gfr_entry->from_surl), NULL);
	PQescapeStringConn (dbfd->Pconn, escaped_name2, gfr_entry->pfn,
	    strlen (gfr_entry->pfn), NULL);
	sprintf (sql_stmt, insert_stmt,
	    gfr_entry->r_token, gfr_entry->f_ordinal, gfr_entry->r_uid,
	    escaped_name, gfr_entry->protocol,
	    gfr_entry->lifetime, gfr_entry->f_type, gfr_entry->s_token,
	    gfr_entry->ret_policy, gfr_entry->flags, gfr_entry->server, escaped_name2,
	    u64tostr (gfr_entry->actual_size, filesize_str, -1), gfr_entry->status,
	    gfr_entry->errstring);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_insert_pending_entry(dbfd, dpm_req)
struct dpm_dbfd *dbfd;
struct dpm_req *dpm_req;
{
	char escaped_name[255*2+1];
	char func[25];
	static char insert_stmt[] =
		"INSERT INTO dpm_pending_req \
		(R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS) \
		VALUES \
		(%d, '%s', %d, %d, '%s', '%s', '%c', '%s', %d, %d, %d, %d, %d, %d, %d, '%s', '%s')";
	PGresult *res;
	char sql_stmt[1733];

	strcpy (func, "dpm_insert_pending_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, dpm_req->client_dn,
	    strlen (dpm_req->client_dn), NULL);
	sprintf (sql_stmt, insert_stmt,
	    dpm_req->r_ordinal, dpm_req->r_token, dpm_req->r_uid,
	    dpm_req->r_gid, escaped_name, dpm_req->clienthost,
	    dpm_req->r_type, dpm_req->u_token,
	    dpm_req->flags, dpm_req->retrytime, dpm_req->nbreqfiles,
	    dpm_req->ctime, dpm_req->stime, dpm_req->etime,
	    dpm_req->status, dpm_req->errstring, dpm_req->groups);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_insert_pfr_entry(dbfd, pfr_entry)
struct dpm_dbfd *dbfd;
struct dpm_put_filereq *pfr_entry;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char escaped_name2[CA_MAXSFNLEN*2+1];
	char filesize_str[21];
	char func[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_put_filereq \
		(R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING) \
		VALUES \
		('%s', %d, '%s', '%s', %d, %d, '%c', '%s', '%c', '%c', %s, '%s', '%s', %s, %d, '%s')";
	char reqsize_str[21];
	PGresult *res;
	char sql_stmt[5162];

	strcpy (func, "dpm_insert_pfr_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, pfr_entry->to_surl,
	    strlen (pfr_entry->to_surl), NULL);
	PQescapeStringConn (dbfd->Pconn, escaped_name2, pfr_entry->pfn,
	    strlen (pfr_entry->pfn), NULL);
	sprintf (sql_stmt, insert_stmt,
	    pfr_entry->r_token, pfr_entry->f_ordinal, escaped_name,
	    pfr_entry->protocol, pfr_entry->lifetime, pfr_entry->f_lifetime,
	    pfr_entry->f_type, pfr_entry->s_token,
	    pfr_entry->ret_policy, pfr_entry->ac_latency,
	    u64tostr (pfr_entry->requested_size, reqsize_str, -1),
	    pfr_entry->server, escaped_name2,
	    u64tostr (pfr_entry->actual_size, filesize_str, -1), pfr_entry->status,
	    pfr_entry->errstring);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_insert_pool_entry(dbfd, pool_entry)
struct dpm_dbfd *dbfd;
struct dpm_pool *pool_entry;
{
	char defsize_str[21];
	char func[22];
	char groups[256];
	static char insert_stmt[] =
		"INSERT INTO dpm_pool \
		(POOLNAME, DEFSIZE, GC_START_THRESH, \
		 GC_STOP_THRESH, DEF_LIFETIME, DEFPINTIME, \
		 MAX_LIFETIME, MAXPINTIME, \
		 FSS_POLICY, GC_POLICY, MIG_POLICY, RS_POLICY, \
		 GROUPS, RET_POLICY, S_TYPE) \
		VALUES \
		('%s', %s, %d, %d, %d, %d, %d, %d, '%s', '%s', '%s', '%s', '%s', '%c', '%c')";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_insert_pool_entry");
	sprintf (sql_stmt, insert_stmt,
	    pool_entry->poolname, u64tostr (pool_entry->defsize, defsize_str, -1),
	    pool_entry->gc_start_thresh, pool_entry->gc_stop_thresh,
	    pool_entry->def_lifetime, pool_entry->defpintime,
	    pool_entry->max_lifetime, pool_entry->maxpintime,
	    pool_entry->fss_policy, pool_entry->gc_policy,
	    pool_entry->mig_policy, pool_entry->rs_policy,
	    Cencode_groups (pool_entry->nbgids, pool_entry->gids, groups, sizeof(groups)),
	    pool_entry->ret_policy, pool_entry->s_type);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_insert_spcmd_entry(dbfd, dpm_spcmd)
struct dpm_dbfd *dbfd;
struct dpm_space_reserv *dpm_spcmd;
{
	char escaped_name[255*2+1];
	char func[23];
	char g_space_str[21];
	static char insert_stmt[] =
		"INSERT INTO dpm_space_reserv \
		(S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS) \
		VALUES \
		('%s', '%s', %d, %d, '%c', '%c', '%c', '%s', %s, %s, %s, '%s', %d, %d, '%s')";
	PGresult *res;
	char sql_stmt[1416];
	char t_space_str[21];
	char u_space_str[21];

	strcpy (func, "dpm_insert_spcmd_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, dpm_spcmd->client_dn,
	    strlen (dpm_spcmd->client_dn), NULL);
	sprintf (sql_stmt, insert_stmt,
	    dpm_spcmd->s_token, escaped_name,
	    dpm_spcmd->s_uid, dpm_spcmd->s_gid, dpm_spcmd->ret_policy,
	    dpm_spcmd->ac_latency, dpm_spcmd->s_type, dpm_spcmd->u_token,
	    u64tostr (dpm_spcmd->t_space, t_space_str, -1),
	    u64tostr (dpm_spcmd->g_space, g_space_str, -1),
	    i64tostr (dpm_spcmd->u_space, u_space_str, -1),
	    dpm_spcmd->poolname, dpm_spcmd->assign_time, dpm_spcmd->expire_time,
	    dpm_spcmd->groups);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_insert_xferreq_entry(dbfd, dpm_req)
struct dpm_dbfd *dbfd;
struct dpm_req *dpm_req;
{
	char escaped_name[255*2+1];
	char func[25];
	static char insert_stmt[] =
		"INSERT INTO dpm_req \
		(R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS) \
		VALUES \
		(%d, '%s', %d, %d, '%s', '%s', '%c', '%s', %d, %d, %d, %d, %d, %d, %d, '%s', '%s')";
	PGresult *res;
	char sql_stmt[1725];

	strcpy (func, "dpm_insert_xferreq_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, dpm_req->client_dn,
	    strlen (dpm_req->client_dn), NULL);
	sprintf (sql_stmt, insert_stmt,
	    dpm_req->r_ordinal, dpm_req->r_token, dpm_req->r_uid,
	    dpm_req->r_gid, escaped_name, dpm_req->clienthost,
	    dpm_req->r_type, dpm_req->u_token,
	    dpm_req->flags, dpm_req->retrytime, dpm_req->nbreqfiles,
	    dpm_req->ctime, dpm_req->stime, dpm_req->etime,
	    dpm_req->status, dpm_req->errstring, dpm_req->groups);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		if (strstr (PQresultErrorMessage (res), "duplicate")) {
			serrno = EEXIST;
			PQclear (res);
		} else
			dpm_libpq_error (func, "INSERT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_list_cpr_entry(dbfd, bol, r_token, cpr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *r_token;
struct dpm_copy_filereq *cpr_entry; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, FROM_SURL, \
		 TO_SURL, F_LIFETIME, F_TYPE, \
		 S_TOKEN, RET_POLICY, AC_LATENCY, FLAGS, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_copy_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_cpr_entry");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		sprintf (sql_stmt, lock ? query4upd : query, r_token);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_cpr_entry (dblistptr->res, dblistptr->row, lock, rec_addr, cpr_entry);
	dblistptr->row++;
	return (0);
}

dpm_list_expired_puts(dbfd, bol, pfr_entry, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_put_filereq *pfr_entry; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[22];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE (status = %d OR status = %d) AND lifetime <= %d";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_expired_puts");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		sprintf (sql_stmt, query, DPM_READY, DPM_RUNNING, time (0));
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_pfr_entry (dblistptr->res, dblistptr->row, 0, NULL, pfr_entry);
	dblistptr->row++;
	return (0);
}

dpm_list_expired_spaces(dbfd, bol, dpm_spcmd, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_space_reserv *dpm_spcmd; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[24];
	static char query[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv \
		WHERE expire_time <= %d";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_expired_spaces");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		sprintf (sql_stmt, query, time (0));
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_spcmd_entry (dblistptr->res, dblistptr->row, 0, NULL, dpm_spcmd);
	dblistptr->row++;
	return (0);
}

dpm_list_rr_puts(dbfd, bol, pfr_entry, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_put_filereq *pfr_entry; 
int endlist;
DBLISTPTR *dblistptr;
{
	char func[17];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE status = %d OR status = %d";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_rr_puts");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		sprintf (sql_stmt, query, DPM_READY, DPM_RUNNING);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_pfr_entry (dblistptr->res, dblistptr->row, 0, NULL, pfr_entry);
	dblistptr->row++;
	return (0);
}

dpm_list_fs_entry(dbfd, bol, poolname, fs_entry, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *poolname;
struct dpm_fs *fs_entry;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[18];
	static char query[] =
		"SELECT \
		 POOLNAME, SERVER, FS, STATUS, WEIGHT \
		FROM dpm_fs \
		WHERE poolname = '%s' \
		ORDER BY server, fs";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_fs_entry");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		sprintf (sql_stmt, query, poolname);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_fs_entry (dblistptr->res, dblistptr->row, 0, NULL, fs_entry);
	dblistptr->row++;
	return (0);
}

dpm_list_gfr_entry(dbfd, bol, r_token, gfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *r_token;
struct dpm_get_filereq *gfr_entry; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, R_UID, FROM_SURL, \
		 PROTOCOL, LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, FLAGS, SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_get_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_gfr_entry");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		sprintf (sql_stmt, lock ? query4upd : query, r_token);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_gfr_entry (dblistptr->res, dblistptr->row, lock, rec_addr, gfr_entry);
	dblistptr->row++;
	return (0);
}

dpm_list_pending_req(dbfd, bol, dpm_req, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_req *dpm_req; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[21];
	static char query[] =
		"SELECT \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE, U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_ORDINAL, R_TOKEN, R_UID, \
		 R_GID, CLIENT_DN, CLIENTHOST, \
		 R_TYPE,  U_TOKEN, \
		 FLAGS, RETRYTIME, NBREQFILES, \
		 CTIME, STIME, ETIME, \
		 STATUS, ERRSTRING, GROUPS \
		FROM dpm_pending_req \
		FOR UPDATE";
	PGresult *res;

	strcpy (func, "dpm_list_pending_req");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		res = PQexec (dbfd->Pconn, lock ? query4upd : query);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_xferreq_entry (dblistptr->res, dblistptr->row, lock, rec_addr, dpm_req);
	dblistptr->row++;
	return (0);
}

dpm_list_pfr_entry(dbfd, bol, r_token, pfr_entry, lock, rec_addr, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
char *r_token;
struct dpm_put_filereq *pfr_entry; 
int lock;
dpm_dbrec_addr *rec_addr;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[19];
	static char query[] =
		"SELECT \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal";
	static char query4upd[] =
		"SELECT ROWID, \
		 R_TOKEN, F_ORDINAL, TO_SURL, PROTOCOL, \
		 LIFETIME, F_LIFETIME, F_TYPE, S_TOKEN, \
		 RET_POLICY, AC_LATENCY, REQUESTED_SIZE, \
		 SERVER, PFN, \
		 ACTUAL_SIZE, STATUS, ERRSTRING \
		FROM dpm_put_filereq \
		WHERE r_token = '%s' \
		ORDER BY f_ordinal \
		FOR UPDATE";
	PGresult *res;
	char sql_stmt[1024];

	strcpy (func, "dpm_list_pfr_entry");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		sprintf (sql_stmt, lock ? query4upd : query, r_token);
		res = PQexec (dbfd->Pconn, sql_stmt);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_pfr_entry (dblistptr->res, dblistptr->row, lock, rec_addr, pfr_entry);
	dblistptr->row++;
	return (0);
}

dpm_list_pool_entry(dbfd, bol, pool_entry, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_pool *pool_entry;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[20];
	static char query[] =
		"SELECT \
		 POOLNAME, DEFSIZE, GC_START_THRESH, \
		 GC_STOP_THRESH, DEF_LIFETIME, DEFPINTIME, \
		 MAX_LIFETIME, MAXPINTIME, \
		 FSS_POLICY, GC_POLICY, MIG_POLICY, RS_POLICY, \
		 GROUPS, RET_POLICY, S_TYPE \
		FROM dpm_pool \
		ORDER BY poolname";
	PGresult *res;

	strcpy (func, "dpm_list_pool_entry");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		res = PQexec (dbfd->Pconn, query);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_pool_entry (dblistptr->res, dblistptr->row, 0, NULL, pool_entry);
	dblistptr->row++;
	return (0);
}

dpm_list_spcmd_entry(dbfd, bol, dpm_spcmd, endlist, dblistptr)
struct dpm_dbfd *dbfd;
int bol;
struct dpm_space_reserv *dpm_spcmd;
int endlist;
DBLISTPTR *dblistptr;
{
	char func[21];
	static char query[] =
		"SELECT \
		 S_TOKEN, CLIENT_DN, S_UID, S_GID, RET_POLICY, \
		 AC_LATENCY, S_TYPE, U_TOKEN, T_SPACE, G_SPACE, \
		 U_SPACE, POOLNAME, ASSIGN_TIME, EXPIRE_TIME, GROUPS \
		FROM dpm_space_reserv";
	PGresult *res;

	strcpy (func, "dpm_list_spcmd_entry");
	if (endlist) {
		PQclear (dblistptr->res);
		return (1);
	}
	if (bol) {
		res = PQexec (dbfd->Pconn, query);
		if (PQresultStatus (res) != PGRES_TUPLES_OK) {
			dpm_libpq_error (func, "SELECT", res, dbfd);
			return (-1);
		}
		dblistptr->res = res;
		dblistptr->row = 0;
	}
	if (dblistptr->row >= PQntuples (dblistptr->res))
		return (1);
	dpm_decode_spcmd_entry (dblistptr->res, dblistptr->row, 0, NULL, dpm_spcmd);
	dblistptr->row++;
	return (0);
}

dpm_opendb(dbfd)
struct dpm_dbfd *dbfd; 
{               
	char connectstr[256];
	char func[16];
	int ntries;
		
	strcpy (func, "dpm_opendb");
	sprintf (connectstr, "dbname=%s user=%s password=%s", db_name, db_user, db_pwd);
	ntries = 0;
	while (1) {
		dbfd->Pconn = PQconnectdb (connectstr);
		if (PQstatus (dbfd->Pconn) == CONNECTION_OK) return (0);
		if (ntries++ >= MAXRETRY) break;
		sleep (RETRYI);
	}
	dpmlogit (func, "CONNECT error: %s\n", PQerrorMessage (dbfd->Pconn));
	serrno = SEINTERNAL;
	return (-1);
}

dpm_pingdb(dbfd)
struct dpm_dbfd *dbfd;
{
	char func[16];
	static char query_stmt[] = "SELECT 1";
	PGresult *res;

	strcpy (func, "dpm_pingdb");
	res = PQexec (dbfd->Pconn, query_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_start_tr(s, dbfd)
int s;
struct dpm_dbfd *dbfd;
{
	PGresult *res;

	res = PQexec (dbfd->Pconn, "BEGIN");
	PQclear (res);	/* to avoid memory leaks */
	dbfd->tr_started = 1;
	return (0);
}

dpm_unique_id(dbfd, unique_id)
struct dpm_dbfd *dbfd;
u_signed64 *unique_id;
{
	char func[16];
	static char query_stmt[] = "SELECT NEXTVAL('dpm_unique_id')";
	PGresult *res;

	strcpy (func, "dpm_unique_id");

	res = PQexec (dbfd->Pconn, query_stmt);
	if (PQresultStatus (res) != PGRES_TUPLES_OK) {
		dpm_libpq_error (func, "SELECT", res, dbfd);
		return (-1);
	}
	*unique_id = strtou64 (PQgetvalue (res, 0, 0));
	PQclear (res);
	return (0);
}

dpm_update_cpr_entry (dbfd, rec_addr, cpr_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_copy_filereq *cpr_entry;
{
	char escaped_msg[255*2+1];
	char filesize_str[21];
	char func[21];
	PGresult *res;
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_copy_filereq SET \
		F_LIFETIME = %d, F_TYPE = '%c', S_TOKEN = '%s', \
		ACTUAL_SIZE = %s, STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_cpr_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_msg, cpr_entry->errstring,
	    strlen (cpr_entry->errstring), NULL);
	sprintf (sql_stmt, update_stmt,
	    cpr_entry->f_lifetime, cpr_entry->f_type, cpr_entry->s_token,
	    u64tostr (cpr_entry->actual_size, filesize_str, -1),
	    cpr_entry->status, escaped_msg, *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_update_fs_entry (dbfd, rec_addr, fs_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_fs *fs_entry;
{
	char func[20];
	PGresult *res;
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_fs SET \
		STATUS = %d, WEIGHT = %d \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_fs_entry");
	sprintf (sql_stmt, update_stmt,
	    fs_entry->status, fs_entry->weight, *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_update_gfr_entry (dbfd, rec_addr, gfr_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_get_filereq *gfr_entry;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char filesize_str[21];
	char func[21];
	PGresult *res;
	char sql_stmt[2776];
	static char update_stmt[] =
		"UPDATE dpm_get_filereq SET \
		LIFETIME = %d, F_TYPE = '%c', S_TOKEN = '%s', \
		SERVER = '%s', PFN = '%s', ACTUAL_SIZE = %s, \
		STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_gfr_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, gfr_entry->pfn,
	    strlen (gfr_entry->pfn), NULL);
	sprintf (sql_stmt, update_stmt,
	    gfr_entry->lifetime, gfr_entry->f_type, gfr_entry->s_token,
	    gfr_entry->server, escaped_name,
	    u64tostr (gfr_entry->actual_size, filesize_str, -1),
	    gfr_entry->status, gfr_entry->errstring, *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_update_pending_entry (dbfd, rec_addr, dpm_req)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_req *dpm_req;
{
	char func[25];
	PGresult *res;
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_pending_req SET \
		NBREQFILES = %d, STIME = %d, ETIME = %d, \
		STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_pending_entry");
	sprintf (sql_stmt, update_stmt,
	    dpm_req->nbreqfiles, dpm_req->stime, dpm_req->etime,
	    dpm_req->status, dpm_req->errstring,
	    *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_update_pfr_entry (dbfd, rec_addr, pfr_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_put_filereq *pfr_entry;
{
	char escaped_name[CA_MAXSFNLEN*2+1];
	char filesize_str[21];
	char func[21];
	char reqsize_str[21];
	PGresult *res;
	char sql_stmt[2840];
	static char update_stmt[] =
		"UPDATE dpm_put_filereq SET \
		LIFETIME = %d, F_LIFETIME = %d, F_TYPE = '%c', S_TOKEN = '%s', \
		REQUESTED_SIZE = %s, SERVER = '%s', PFN = '%s', \
		ACTUAL_SIZE = %s, STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_pfr_entry");
	PQescapeStringConn (dbfd->Pconn, escaped_name, pfr_entry->pfn,
	    strlen (pfr_entry->pfn), NULL);
	sprintf (sql_stmt, update_stmt, pfr_entry->lifetime,
	    pfr_entry->f_lifetime, pfr_entry->f_type, pfr_entry->s_token,
	    u64tostr (pfr_entry->requested_size, reqsize_str, -1),
	    pfr_entry->server, escaped_name,
	    u64tostr (pfr_entry->actual_size, filesize_str, -1),
	    pfr_entry->status, pfr_entry->errstring, *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_update_pool_entry (dbfd, rec_addr, pool_entry)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_pool *pool_entry;
{
	char defsize_str[21];
	char func[22];
	char groups[256];
	PGresult *res;
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_pool SET \
		DEFSIZE = %s, GC_START_THRESH = %d, GC_STOP_THRESH = %d, \
		DEF_LIFETIME = %d, DEFPINTIME = %d, MAX_LIFETIME = %d, \
		MAXPINTIME = %d, \
		FSS_POLICY = '%s', GC_POLICY = '%s', MIG_POLICY = '%s', \
		RS_POLICY = '%s', GROUPS = '%s', RET_POLICY = '%c', S_TYPE = '%c' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_pool_entry");
	sprintf (sql_stmt, update_stmt,
	    u64tostr (pool_entry->defsize, defsize_str, -1),
	    pool_entry->gc_start_thresh, pool_entry->gc_stop_thresh,
	    pool_entry->def_lifetime, pool_entry->defpintime,
	    pool_entry->max_lifetime, pool_entry->maxpintime,
	    pool_entry->fss_policy, pool_entry->gc_policy,
	    pool_entry->mig_policy, pool_entry->rs_policy,
	    Cencode_groups (pool_entry->nbgids, pool_entry->gids, groups, sizeof(groups)),
	    pool_entry->ret_policy, pool_entry->s_type, *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}

dpm_update_spcmd_entry (dbfd, rec_addr, dpm_spcmd)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_space_reserv *dpm_spcmd;
{
	char func[23];
	char g_space_str[21];
	PGresult *res;
	char sql_stmt[1024];
	char t_space_str[21];
	char u_space_str[21];
	static char update_stmt[] =
		"UPDATE dpm_space_reserv SET \
		S_GID = %d, T_SPACE = %s, G_SPACE = %s, U_SPACE = %s, \
		POOLNAME = '%s', ASSIGN_TIME = %d, EXPIRE_TIME = %d, GROUPS = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_spcmd_entry");
	sprintf (sql_stmt, update_stmt,
	    dpm_spcmd->s_gid,
	    u64tostr (dpm_spcmd->t_space, t_space_str, -1),
	    u64tostr (dpm_spcmd->g_space, g_space_str, -1),
	    i64tostr (dpm_spcmd->u_space, u_space_str, -1),
	    dpm_spcmd->poolname,
	    dpm_spcmd->assign_time, dpm_spcmd->expire_time,
	    dpm_spcmd->groups, *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}



dpm_update_uspcincr_spcmd_entry (dbfd, rec_addr, dpm_spcmd, uspaceincr)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_space_reserv *dpm_spcmd;
long long int uspaceincr;
{
	char func[23];
	char g_space_str[21];
	PGresult *res;
	char sql_stmt[1024];
	char t_space_str[21];
	char u_space_str[21];
	static char update_stmt[] =
		"UPDATE dpm_space_reserv SET \
		S_GID = %d, T_SPACE = %s, G_SPACE = %s, U_SPACE = U_SPACE + (%s), \
		POOLNAME = '%s', ASSIGN_TIME = %d, EXPIRE_TIME = %d, GROUPS = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_spcmd_entry");
	sprintf (sql_stmt, update_stmt,
	    dpm_spcmd->s_gid,
	    u64tostr (dpm_spcmd->t_space, t_space_str, -1),
	    u64tostr (dpm_spcmd->g_space, g_space_str, -1),
	    i64tostr (uspaceincr, u_space_str, -1),
	    dpm_spcmd->poolname,
	    dpm_spcmd->assign_time, dpm_spcmd->expire_time,
	    dpm_spcmd->groups, *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}


dpm_update_xferreq_entry (dbfd, rec_addr, dpm_req)
struct dpm_dbfd *dbfd;
dpm_dbrec_addr *rec_addr;
struct dpm_req *dpm_req;
{
	char func[25];
	PGresult *res;
	char sql_stmt[1024];
	static char update_stmt[] =
		"UPDATE dpm_req SET \
		NBREQFILES = %d, STIME = %d, ETIME = %d, \
		STATUS = %d, ERRSTRING = '%s' \
		WHERE ROWID = %s";

	strcpy (func, "dpm_update_xferreq_entry");
	sprintf (sql_stmt, update_stmt,
	    dpm_req->nbreqfiles, dpm_req->stime, dpm_req->etime,
	    dpm_req->status, dpm_req->errstring,
	    *rec_addr);

	res = PQexec (dbfd->Pconn, sql_stmt);
	if (PQresultStatus (res) != PGRES_COMMAND_OK) {
		dpm_libpq_error (func, "UPDATE", res, dbfd);
		return (-1);
	}
	PQclear (res);
	return (0);
}
