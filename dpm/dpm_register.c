/*
 * Copyright (C) 2007-2010 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_register.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_register - register external files in DPNS */

#include <sys/types.h>
#include <stdlib.h>
#include "dpns_api.h"
#include "serrno.h"

int DLL_DECL
dpm_register(char *filename, mode_t filemode, uid_t owneruid, gid_t ownergid, u_signed64 filesize, char *server, char *pfn, const char status)
{
	struct Cns_fileid Cnsfileid;

	if (Cns_creatx (filename, filemode, &Cnsfileid) < 0) {
		Cns_errmsg (NULL, "cannot create %s\n", filename);
		return (-1);
	}
	if (Cns_chown (filename, owneruid, ownergid) < 0) {
		Cns_errmsg (NULL, "cannot change ownership\n");
		return (-1);
	}
	if (Cns_setfsize (NULL, &Cnsfileid, filesize) < 0) {
		Cns_errmsg (NULL, "cannot set filesize\n");
		return (-1);
	}
	if (Cns_addreplicax (NULL, &Cnsfileid, server, pfn, status, 'P', NULL,
	    NULL, 'P', NULL) < 0) {
		Cns_errmsg (NULL, "cannot add replica %s\n", pfn);
		return (-1);
	}
	return (0);
}
