.\" @(#)$RCSfile: dpm_getpoolfs.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2011 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_GETPOOLFS 3 "$Date$" LCG "DPM Library Functions"
.SH NAME
dpm_getpoolfs \- get list of filesystems for a given pool
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getpoolfs (char *" poolname ,
.BI "int *" nbfs ,
.BI "struct dpm_fs **" dpm_fs )
.SH DESCRIPTION
.B dpm_getpoolfs
gets list of filesystems for a given pool.
.LP
The input argument is:
.TP
.I poolname
specifies the disk pool name previously defined using
.BR dpm_addpool .
.LP
The output arguments are:
.TP
.I nbfs
will be set to the number of replies in the array of filesystems.
.TP
.I dpm_fs
will be set to the address of an array of dpm_fs structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_fs {
	char		poolname[CA_MAXPOOLNAMELEN+1];
	char		server[CA_MAXHOSTNAMELEN+1];
	char		fs[80];
	u_signed64	capacity;
	u_signed64	free;
	int		status;
	int		weight;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.IR poolname ,
.I nbfs
or
.I dpm_fs
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for storing the reply.
.TP
.B EINVAL
The pool is unknown or the length of
.I poolname
exceeds
.BR CA_MAXPOOLNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SECOMERR
Communication error.
