.\" @(#)$RCSfile: dpm-reservespace.man,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2006-2011 by CERN/IT/GD/ITR
.\" All rights reserved
.\"
.TH DPM-RESERVESPACE 1 "$Date$" LCG "DPM Commands"
.SH NAME
dpm-reservespace \- reserve space
.SH SYNOPSIS
.B dpm-reservespace
.BI --gspace " size_guaranteed_space"
[
.BI --ac_latency " access_latency"
] [
.BI --gid " group_ids"
] [
.BI --group " group_names"
] [
.B --help
] [
.BI --lifetime " space_lifetime"
] [
.BI --poolname " pool_name"
] [
.BI --ret_policy " retention_policy"
] [
.BI --s_type " space_type"
] [
.B --si
] [
.BI --token_desc " u_token"
]
.SH DESCRIPTION
.B dpm-reservespace
reserves space.
.SH OPTIONS
.TP
.I size_guaranteed_space
desired size of guaranteed space in bytes.
The number may also have a suffix k, M, G, T or P to indicate kB, MB, GB, TB or
PB respectively.
.TP
.I access_latency
specifies the access latency requested. It can be
.BR O " (for ONLINE)"
or
.BR N " (for NEARLINE)."
.TP
.I group_ids
if not zero, the space is restricted to this comma separated list of group gids.
If the caller does not belong to the group, it must have ADMIN privileges.
By default, the space is reserved for the user if the requester has a proxy
without VOMS extension or for the group if the requester has a proxy with
VOMS extension.
.TP 
.I group_names
if set, the space is restricted to this comma separated list of group names.
If the caller does not belong to the group, it must have ADMIN privileges.
.TP
.I space_lifetime
specifies the desired space lifetime relative to the current time.
It can be "Inf" (for infinite) or expressed in years (suffix 'y'), months
(suffix 'm'), days (suffix 'd'), hours (suffix 'h') or seconds (no suffix).
.TP
.I pool_name
if set, the space is reserved in that disk pool.
.TP
.I retention_policy
specifies the retention policy requested. It can be
.BR R " (for Replica),"
.BR O " (for Output)"
or
.BR C " (for Custodial)."
.TP
.I s_type
specifies the type of space requested. It can be
.BR V " (for Volatile),"
.BR D " (for Durable),"
.BR P " (for Permanent)"
or
.BR - " (for any)."
.TP
.I u_token
specifies the user provided description associated with the request.
.TP
.B --si
use powers of 1000 not 1024 for sizes.
.SH EXAMPLE
.nf
.ft CW
dpm-reservespace --gspace 10G --lifetime 1d --token_desc myspace
.sp
fe869590-b771-4002-b11a-8e7430d72911
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm_reservespace(3)
