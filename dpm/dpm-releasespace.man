.\" @(#)$RCSfile: dpm-releasespace.man,v $ $Revision: 1.1 $ $Date: 2007/02/12 14:10:06 $ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN/IT/GD/ITR
.\" All rights released
.\"
.TH DPM-RELEASESPACE 1 "$Date: 2007/02/12 14:10:06 $" LCG "DPM Commands"
.SH NAME
dpm-releasespace \- release space
.SH SYNOPSIS
.B dpm-releasespace
.BI --space_token " s_token"
[
.B --force
] [
.B --help
]
.sp
.B dpm-releasespace
.BI --token_desc " u_token"
[
.B --force
] [
.B --help
]
.SH DESCRIPTION
.B dpm-releasespace
releases space.
.SH OPTIONS
.TP
.I s_token
specifies the token returned by a previous reservespace request.
.TP
.I u_token
specifies the user provided description associated with the reservespace request.
.TP
.B force
force file release. If
.B force
is not specified, the space will not be released if it has
files that are still pinned in the space.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm_releasespace(3)
