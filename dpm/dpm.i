/*********************************************
    SWIG input file for DPM
 (including typemaps for non-trivial functions
*********************************************/

%module dpm
%include "typemaps.i"
%{
#include <dirent.h>
#include "dpns_api.h"
#include "dpm_api.h"
#include "serrno.h"
%}


/* Typemaps are only valid for python */
#ifdef SWIGPYTHON


/***************************************
    Global typemaps definition
***************************************/


%typemap(in) u_signed64 {
  if (PyInt_Check ($input))
      $1 = PyInt_AsUnsignedLongLongMask ($input);
  else if (PyLong_Check ($input))
      $1 = PyLong_AsUnsignedLongLong ($input);
  else {
      PyErr_SetString (PyExc_TypeError, "int or long expected");
      return NULL;
  }
}


%typemap(out) char[ANY] {
      $result = PyString_FromString ($1);
}


/***************************************
    Generic Typemaps definition
***************************************/


%typemap(in) (char *) {
    if ($input == Py_None) {
        $1 = NULL;
    } else {
        $1 = PyString_AsString ($input);
    }
}

/* Nothing to free, need to overload default typemap as of swig 1.3.28 */
%typemap(freearg) (char *) %{%}


%typemap(in) (gid_t *gids) {
    int size;

    if ($input == Py_None || (size = PyList_Size($input)) < 0)
        $1 = NULL;
    else {
        int i;

        $1 = (gid_t *) calloc (size + 1, sizeof (gid_t));

        for (i = 0; i < size; ++i) {
            $1[i] = PyInt_AsLong (PyList_GetItem($input, i));
        }
    }
}


%typemap(out) gid_t * {

  int size = 0;

  /* Error, return None */
  if ($1 == 0) {
      $result = Py_None;
      Py_INCREF(Py_None);
  }
  /* No error */
  else {
    /* the size of the C table is the previous field (int type) of the same structure */
    size = (arg1)->nbgids;
    $result = PyList_New(size);

    int i;
    for (i = 0; i < size; i++)
      PyList_SetItem($result, i, PyInt_FromLong ($1[i]));
  }
}


%typemap(out) int {
  $result = PyInt_FromLong($1);
}


/***************************************
    Typemaps definition for getlinks
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct dpns_linkinfo **OUTPUT){
   int tmp_int;
   struct dpns_linkinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct dpns_linkinfo **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct dpns_linkinfo * aux = (struct dpns_linkinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpns_linkinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of links */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct dpns_linkinfo **OUTPUT) {(int *nbentries, struct dpns_linkinfo **linkinfos)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int dpns_getlinks (const char *path, const char *guid, int *nbentries, struct dpns_linkinfo **linkinfos);

%clear (int *nbentries, struct dpns_linkinfo **linkinfos);


/***************************************
    Typemaps definition for getreplica
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct dpns_filereplica **OUTPUT){
   int tmp_int;
   struct dpns_filereplica *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(argout) (int *LENGTH, struct dpns_filereplica **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None);
      PyTuple_SetItem(tuple, 0, Py_None); //new
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct dpns_filereplica * aux = (struct dpns_filereplica *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpns_filereplica *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }

    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of replicas */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct dpns_filereplica **OUTPUT) {(int *return_value, struct dpns_filereplica **rep_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpns_getreplica(const char *path, const char *guid, const char *se,
                   int *return_value, struct dpns_filereplica **rep_entries);

%clear (int *return_value, struct dpns_filereplica **rep_entries);


/***************************************
    Typemaps definition for getreplicax
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct dpns_filereplicax **OUTPUT){
   int tmp_int;
   struct dpns_filereplicax *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(argout) (int *LENGTH, struct dpns_filereplicax **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None);
      PyTuple_SetItem(tuple, 0, Py_None); //new
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct dpns_filereplicax * aux = (struct dpns_filereplicax *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpns_filereplicax *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of replicas */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct dpns_filereplicax **OUTPUT) {(int *return_value, struct dpns_filereplicax **rep_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpns_getreplicax(const char *path, const char *guid, const char *se,
                   int *return_value, struct dpns_filereplicax **rep_entries);

%clear (int *return_value, struct dpns_filereplicax **rep_entries);




/****************************
   Typemap for readdirxr
****************************/

%typemap(out) struct dpns_direnrep *OUTPUT{

  /* Don't let python garbage-collect these structures, because they are freed with dpns_closedir */
  int PYTHON_OWNED=0;

  /* Error, return None */
  if($1 == 0){
    Py_INCREF(Py_None);
    $result = Py_None;
  }
  /* No error */
  else{
    $result = PyTuple_New(2);
    /* Python object for the dpns_direnrep set by the C call */
    PyObject * res = SWIG_NewPointerObj($1, $descriptor(struct dpns_direnrep *), PYTHON_OWNED);
    /* First element of the returned tuple is this direnrep object */
    PyTuple_SetItem($result, 0, res);

    if($1->nbreplicas > 0){
       PyObject *tuple = PyTuple_New($1->nbreplicas);
       int i;
       for(i=0; i< $1->nbreplicas; i++){
          /* Python object for each of the dpns_rep_info structs set by the C call */
          PyObject * aux_rep = SWIG_NewPointerObj(i+($1->rep), $descriptor(struct dpns_rep_info *), PYTHON_OWNED);
          PyTuple_SetItem(tuple, i, aux_rep);
       }
       /* Second element of the returned tuple is a tuple of these dpns_rep_info objects */
       PyTuple_SetItem($result, 1, tuple);
    }
    /* If there are no replicas, set the second element to None */
    else{
       Py_INCREF(Py_None);
       PyTuple_SetItem($result, 1, Py_None);
    }
  }
}


/* C/C++ declarations and apply statement */

%apply (struct dpns_direnrep *OUTPUT) {(struct dpns_direnrep *)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
struct dpns_direnrep *dpns_readdirxr (dpns_DIR *dirp, char *se);

%clear (struct dpns_direnrep *);






/*************************************
    Typemaps definition for setacl
*************************************/

%typemap(in) (int LENGTH, struct dpns_acl *TUPLE){

  /* Check the input list */
  if(!PyList_Check($input)){
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else{
    /* Length of the python list (how many dpns_acl structs we will need) */
    $1 = PyList_Size($input);

    /* Reserve space to store all the list members in an array (after converting them) */
    $2 = (struct dpns_acl *) malloc($1 * sizeof(struct dpns_acl)) ;

    int i;
    for(i=0; i<$1; i++){
      PyObject * aux_object = PyList_GetItem($input, i);

      /* Temp pointer */
      struct dpns_acl * aux_p;

      /* This moves aux_p to point to a newly allocated dpns_acl struct representing the item in the python list */
      SWIG_ConvertPtr(aux_object, (void**) &aux_p,
                      $descriptor(struct dpns_acl *), SWIG_POINTER_EXCEPTION);

      /* Copy the value to the array */
      $2[i] = *(aux_p);
    }

  }/* end of: No error */

}/* end of: typemap(in) */



/* Free the temporary dpns_acl array passed to the C function after the dpns_setacl function has returned */
%typemap(freearg) (int LENGTH, struct dpns_acl *TUPLE) {
  if($2){
     free($2);
   }
}


/* C/C++ declarations and apply statement */

%apply (int LENGTH, struct dpns_acl *TUPLE) {(int nentries, struct dpns_acl *acl)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpns_setacl(const char *path, int nentries, struct dpns_acl *acl);

%clear (int nentries, struct dpns_acl *acl);




/***************************************
    Typemaps definition for getacl
***************************************/

%typemap(in) (int LENGTH, struct dpns_acl *OUTPUT){
   int len = PyInt_AsLong($input);
   $1 = len;
   $2 = (struct dpns_acl *) malloc(len * sizeof(struct dpns_acl));
}


%typemap(argout) (int LENGTH, struct dpns_acl *OUTPUT) {

  PyObject *tuple;
  int len = PyInt_AsLong($result);

  /* Error */
  if(len < 0){
      tuple = PyList_New(1);
      Py_INCREF(Py_None);
      PyList_SetItem(tuple, 0, Py_None);
  }

  /* No error */
  else{

    /* No structs returned, only number of ACL entries */
    if($1 == 0){
      tuple = PyList_New(1);
      Py_INCREF(Py_None);
      PyList_SetItem(tuple, 0, Py_None);
    }

    /* Structs returned in array */
    else{
       tuple = PyList_New(len);
       int i;

       for(i=0; i<len; i++){
         /* Let python garbage-collect only the 1st one. That will already delete it all!
            Because the original var was dpns_al* and not dpns_acl** (only 1 pointer responsible) */
         int PYTHON_OWNED = i>0 ? 0 : 1;

          /* Python object for each of the dpns_acl structs set by the C call */
         PyObject * aux_obj = SWIG_NewPointerObj(&$2[i], $descriptor(struct dpns_acl *), PYTHON_OWNED);

         PyList_SetItem(tuple, i, aux_obj);

       }/* end of for */

    }/* end of: Structs returned in array */

  }/* end of: No error */

/* Return the results (homogeneous for every case) */
  PyObject * old_result = $result;
  $result = PyList_New(2);
  /* First element of the returned tuple is the C call return value */
  PyList_SetItem($result, 0, old_result);
  /* Second element of the returned tuple is a tuple of the python dpns_acl objects */
  PyList_SetItem($result, 1, tuple);


}/* end of: typemap(argout) */



/* C/C++ declarations and apply statement */

%apply (int LENGTH, struct dpns_acl *OUTPUT) {(int nentries, struct dpns_acl *acl)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpns_getacl(const char *path, int nentries, struct dpns_acl *acl);

%clear (int nentries, struct dpns_acl *acl);



/***************************************
    Typemaps definition for getusrmap
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct dpns_userinfo **OUTPUT){
   int tmp_int;
   struct dpns_userinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct dpns_userinfo **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;	

      struct dpns_userinfo * aux = (struct dpns_userinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpns_userinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of user entries */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct dpns_userinfo **OUTPUT) {(int *nbentries, struct dpns_userinfo **usr_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int dpns_getusrmap (int *nbentries, struct dpns_userinfo **usr_entries);

%clear (int *nbentries, struct dpns_userinfo **usr_entries);



/***************************************
    Typemaps definition for getgrpmap
***************************************/


%typemap(in, numinputs=0) (int *LENGTH, struct dpns_groupinfo **OUTPUT){
   int tmp_int;
   struct dpns_groupinfo *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(out) int{
  $result = PyInt_FromLong($1);
}


%typemap(argout) (int *LENGTH, struct dpns_groupinfo **OUTPUT) {

  PyObject *tuple;

  /* Error, return None */
  if(PyInt_AsLong($result) != 0){
      tuple = PyTuple_New(1);
      Py_INCREF(Py_None); 
      PyTuple_SetItem(tuple, 0, Py_None); //new      
      PyObject * old_result = $result;
      $result = PyTuple_New(2);
      PyTuple_SetItem($result, 0, old_result);
      PyTuple_SetItem($result, 1, tuple);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;	

      struct dpns_groupinfo * aux = (struct dpns_groupinfo *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpns_groupinfo *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }
    PyObject * old_result = $result;

    /* Return a tuple containing the returned code and the tuple of group entries */
    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);
    PyTuple_SetItem($result, 1, tuple);

  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct dpns_groupinfo **OUTPUT) {(int *nbentries, struct dpns_groupinfo **grp_entries)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
int dpns_getgrpmap (int *nbentries, struct dpns_groupinfo **grp_entries);

%clear (int *nbentries, struct dpns_groupinfo **grp_entries);




/********************************************
    Typemaps definition for dpm_getpoolfs
********************************************/

%typemap(in, numinputs=0) (int *LENGTH, struct dpm_fs **OUTPUT){
   int tmp_int;
   struct dpm_fs *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(argout) (int *LENGTH, struct dpm_fs **OUTPUT) {

  PyObject *tuple;
  PyObject *old_result = $result;

  $result = PyTuple_New(2);
  PyTuple_SetItem($result, 0, old_result);

  /* Error, return None */
  if(PyInt_AsLong(old_result) != 0){
      Py_INCREF(Py_None);
      PyTuple_SetItem($result, 1, Py_None);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct dpm_fs * aux = (struct dpm_fs *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpm_fs *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }

    PyTuple_SetItem($result, 1, tuple);
  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct dpm_fs **OUTPUT) {(int *nbfs, struct dpm_fs **dpm_fs)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_getpoolfs(char *poolname, int *nbfs, struct dpm_fs **dpm_fs);

%clear (int *nbfs, struct dpm_fs **dpm_fs);




/********************************************
    Typemaps definition for dpm_getpools
********************************************/

%typemap(in, numinputs=0) (int *LENGTH, struct dpm_pool **OUTPUT){
   int tmp_int;
   struct dpm_pool *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(argout) (int *LENGTH, struct dpm_pool **OUTPUT) {

  PyObject *tuple;
  PyObject *old_result = $result;

  $result = PyTuple_New(2);
  PyTuple_SetItem($result, 0, old_result);

  /* Error, return None */
  if(PyInt_AsLong(old_result) != 0){
      Py_INCREF(Py_None);
      PyTuple_SetItem($result, 1, Py_None);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct dpm_pool * aux = (struct dpm_pool *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpm_pool *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }

    PyTuple_SetItem($result, 1, tuple);
  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, struct dpm_pool **OUTPUT) {(int *nbpools, struct dpm_pool **dpm_pools)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_getpools(int *nbpools, struct dpm_pool **dpm_pools);

%clear (int *nbpools, struct dpm_pool **dpm_pools);




/********************************************
    Typemaps definition for dpm_getprotocols
********************************************/

%typemap(in, numinputs=0) (int *LENGTH, char ***OUTPUT){
   int tmp_int;
   char **tmp_struct = 0;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(argout) (int *LENGTH, char ***OUTPUT) {

  PyObject *tuple;
  PyObject *old_result = $result;

  $result = PyTuple_New(2);
  PyTuple_SetItem($result, 0, old_result);

  /* Error, return None */
  if(PyInt_AsLong(old_result) != 0){
      Py_INCREF(Py_None);
      PyTuple_SetItem($result, 1, Py_None);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for (i=0; i<*$1; i++) {
      PyTuple_SetItem(tuple, i, PyString_FromString ((*$2)[i]));
      free((*$2)[i]);
    }
    free(*$2);

    PyTuple_SetItem($result, 1, tuple);
  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, char ***OUTPUT) {(int *nb_supported_protocols, char ***supported_protocols)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_getprotocols (int *nb_supported_protocols, char ***supported_protocols);

%clear (int *nb_supported_protocols, char ***supported_protocols);




/********************************************
    Typemaps definition for dpm_getspacemd
********************************************/

%typemap(in) (int LENGTH, char **TUPLE){

  /* Check the input list */
  if(!PyList_Check($input)){
    PyErr_SetString(PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else {
    /* Length of the python list (how many space tokens) */
    $1 = PyList_Size($input);

    /* Reserve space to store all the space tokens in an array (after converting them) */
    $2 = (char **) calloc ($1, sizeof(char *)) ;

    int i;
    for (i=0; i<$1; ++i)
      $2[i] = PyString_AsString (PyList_GetItem($input, i));

  }/* end of: No error */

}/* end of: typemap(in) */



/* Free the temporary space tokens array passed to the C function after the dpm_getspacemd function has returned */
%typemap(freearg) (int LENGTH, char **TUPLE) {
  if($2){
     free($2);
   }
}


%typemap(in, numinputs=0) (int *LENGTH, struct dpm_space_metadata **OUTPUT){
   int tmp_int;
   struct dpm_space_metadata *tmp_struct;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(argout) (int *LENGTH, struct dpm_space_metadata **OUTPUT) {

  PyObject *tuple;
  PyObject *old_result = $result;

  $result = PyTuple_New(2);
  PyTuple_SetItem($result, 0, old_result);

  /* Error, return None */
  if(PyInt_AsLong(old_result) != 0){
      Py_INCREF(Py_None);
      PyTuple_SetItem($result, 1, Py_None);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for(i=0; i<*$1; i++){
      /* Let python garbage-collect only the 1st one. That will already delete it all! */
      int PYTHON_OWNED = i>0 ? 0 : 1;

      struct dpm_space_metadata * aux = (struct dpm_space_metadata *) (*$2)+i;

      /* Create a python object based on the C struct set by the C function */
      PyObject * aux_obj = SWIG_NewPointerObj(aux, $descriptor(struct dpm_space_metadata *), PYTHON_OWNED);

      /* Add the python object to the tuple to return */
      PyTuple_SetItem(tuple, i, aux_obj);

    }

    PyTuple_SetItem($result, 1, tuple);
  }
}


/* C/C++ declarations and apply statement */

%apply (int LENGTH, char **TUPLE) {(int nbtokens, char **s_tokens)};
%apply (int *LENGTH, struct dpm_space_metadata **OUTPUT) {(int *nbreplies, struct dpm_space_metadata **spacemd)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_getspacemd (int nbtokens, char **s_tokens, int *nbreplies, struct dpm_space_metadata **spacemd);

%clear (int nbtokens, char **s_tokens);
%clear (int *nbreplies, struct dpm_space_metadata **spacemd);





/***********************************************
    Typemaps definition for dpm_getspacetoken
***********************************************/

%typemap(in, numinputs=0) (int *LENGTH, char ***OUTPUT){
   int tmp_int;
   char **tmp_struct = 0;
   $1 = &tmp_int;
   $2 = &tmp_struct;
}


%typemap(argout) (int *LENGTH, char ***OUTPUT) {

  PyObject *tuple;
  PyObject *old_result = $result;

  $result = PyTuple_New(2);
  PyTuple_SetItem($result, 0, old_result);

  /* Error, return None */
  if(PyInt_AsLong(old_result) != 0){
      Py_INCREF(Py_None);
      PyTuple_SetItem($result, 1, Py_None);
  }
  /* No error */
  else{
    tuple = PyTuple_New(*$1);

    int i;
    for (i=0; i<*$1; i++) {
      PyTuple_SetItem(tuple, i, PyString_FromString ((*$2)[i]));
      free((*$2)[i]);
    }
    free(*$2);

    PyTuple_SetItem($result, 1, tuple);
  }
}


/* C/C++ declarations and apply statement */

%apply (int *LENGTH, char ***OUTPUT) {(int *nbreplies, char ***s_tokens)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_getspacetoken (const char *u_token, int *nbreplies, char ***s_tokens);

%clear (int *replies, char ***s_tokens);





/***********************************************
    Typemaps definition for dpm_addfs
***********************************************/

/* C/C++ declarations */

extern int dpm_addfs(char *poolname, char *server, char *fs, int status, int weight);






/***********************************************
    Typemaps definition for dpm_addpool
***********************************************/

%typemap(in) (struct dpm_pool *POOL){

  /* Temp pointer */
  struct dpm_pool * aux_p;

  /* This moves aux_p to point to a newly allocated dpns_acl struct representing the item in the python list */
  SWIG_ConvertPtr($input, (void**) &aux_p,
          $descriptor(struct dpm_pool *), SWIG_POINTER_EXCEPTION);

  /* Copy the value to the array */
  $1 = aux_p;

}/* end of: typemap(in) */


/* C/C++ declarations and apply statement */

%apply (struct dpm_pool *POOL) {(struct dpm_pool *dpm_pool)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_addpool(struct dpm_pool *dpm_pool);

%clear (struct dpm_pool *dpm_pool);





/***********************************************
    Typemaps definition for dpm_delreplica
***********************************************/

/* C/C++ declarations */

extern int dpm_delreplica(char *pfn);





/***********************************************
    Typemaps definition for dpm_modifyfs
***********************************************/

/* C/C++ declarations */

extern int dpm_modifyfs(char *server, char *fs, int status, int weight);






/***********************************************
    Typemaps definition for dpm_modifypool
***********************************************/

%typemap(in) (struct dpm_pool *POOL){

  /* Temp pointer */
  struct dpm_pool * aux_p;

  /* This moves aux_p to point to a newly allocated dpns_acl struct representing the item in the python list */
  SWIG_ConvertPtr($input, (void**) &aux_p,
          $descriptor(struct dpm_pool *), SWIG_POINTER_EXCEPTION);

  /* Copy the value to the array */
  $1 = aux_p;

}/* end of: typemap(in) */


/* C/C++ declarations and apply statement */

%apply (struct dpm_pool *POOL) {(struct dpm_pool *dpm_pool)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_modifypool(struct dpm_pool *dpm_pool);

%clear (struct dpm_pool *dpm_pool);





/***********************************************
    Typemaps definition for dpm_ping
***********************************************/


%typemap(in, numinputs=0) (char *INFO) {
   char tmp[256];
   $1 = tmp;
}


%typemap(argout) (char *INFO) {
    PyObject * old_result = $result;

    $result = PyTuple_New(2);
    PyTuple_SetItem($result, 0, old_result);

    /* Error, return None */
    if(PyInt_AsLong(old_result) != 0){
        Py_INCREF(Py_None);
        PyTuple_SetItem($result, 1, Py_None);
    }
    /* No error */
    else{
        PyTuple_SetItem($result, 1, PyString_FromString ($1));
    }
}

/* C/C++ declarations */

%apply (char *INFO) {(char *info)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_ping(char *host, char *info);

%clear (char *info);





/***********************************************
    Typemaps definition for dpm_releasespace
***********************************************/

/* C/C++ declarations */

extern int dpm_releasespace(char *s_token, int force);





/***********************************************
    Typemaps definition for dpm_reservespace
***********************************************/

%typemap(in) u_signed64 {
    $1 = PyLong_AsLongLong ($input);
}


%typemap(in) time_t {
    $1 = PyLong_AsLong ($input);
}

%typemap(in) (int LENGTH, gid_t *INPUT){

  /* Empty list */
  if ($input == Py_None) {
      $1 = 0;
      $2 = NULL;
  }

  /* Check the input list */
  else if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }

  /* No error */
  else {
    int i;

    /* Length of the python list */
    $1 = PyList_Size($input);

    $2 = (gid_t *) calloc ($1, sizeof (gid_t));

    for (i = 0; i < $1; ++i) {
        $2[i] = PyInt_AsLong (PyList_GetItem($input, i));
    }
  }/* end of: No error */

}/* end of: typemap(in) */


/* Free the temporary array passed to the C function */
%typemap(freearg) (int LENGTH, gid_t *INPUT) {
  if ($2) {
     free ($2);
  }
}


%typemap(in, numinputs=0) (char *OUTPUT) {
   char tmp[2];
   $1 = tmp;
}


%typemap(argout) (char *OUTPUT) {
    PyObject*   old_result;
    PyObject*   toadd;

    if (!PyTuple_Check($result)) {
        old_result = $result;
        $result = PyTuple_New(1);
        PyTuple_SetItem($result, 0, old_result);
    }

    toadd = PyTuple_New(1);
    if (PyInt_AsLong (PyTuple_GetItem ($result, 0)) != 0) {
        Py_INCREF(Py_None);
        PyTuple_SetItem(toadd, 0, Py_None);
    } else {
        $1[1] = 0;
        PyTuple_SetItem(toadd, 0, PyString_FromString ($1));
    }

    old_result = $result;
    $result = PySequence_Concat(old_result, toadd);
    Py_DECREF(old_result);
    Py_DECREF(toadd);
}


%typemap(in, numinputs=0) (char *TOKEN) {
   char tmp[CA_MAXDPMTOKENLEN+1];
   $1 = tmp;
}


%typemap(argout) (char *TOKEN) {
    PyObject*   old_result;
    PyObject*   toadd;

    if (!PyTuple_Check($result)) {
        old_result = $result;
        $result = PyTuple_New(1);
        PyTuple_SetItem($result, 0, old_result);
    }

    toadd = PyTuple_New(1);
    if (PyInt_AsLong (PyTuple_GetItem ($result, 0)) != 0) {
        Py_INCREF(Py_None);
        PyTuple_SetItem(toadd, 0, Py_None);
    } else {
        PyTuple_SetItem(toadd, 0, PyString_FromString ($1));
    }

    old_result = $result;
    $result = PySequence_Concat(old_result, toadd);
    Py_DECREF(old_result);
    Py_DECREF(toadd);
}


%typemap(in, numinputs=0) (u_signed64 *OUTPUT) {
   u_signed64 tmp;
   $1 = &tmp;
}


%typemap(argout) (u_signed64 *OUTPUT) {
    PyObject*   old_result;
    PyObject*   toadd;

    if (!PyTuple_Check($result)) {
        old_result = $result;
        $result = PyTuple_New(1);
        PyTuple_SetItem($result, 0, old_result);
    }

    toadd = PyTuple_New(1);
    if (PyInt_AsLong (PyTuple_GetItem ($result, 0)) != 0) {
        Py_INCREF(Py_None);
        PyTuple_SetItem(toadd, 0, Py_None);
    } else {
        PyTuple_SetItem(toadd, 0, PyLong_FromLongLong (*$1));
    }

    old_result = $result;
    $result = PySequence_Concat(old_result, toadd);
    Py_DECREF(old_result);
    Py_DECREF(toadd);
}


%typemap(in, numinputs=0) (time_t *OUTPUT) {
   time_t tmp;
   $1 = &tmp;
}


%typemap(argout) (time_t *OUTPUT) {
    PyObject*   old_result;
    PyObject*   toadd;

    if (!PyTuple_Check($result)) {
        old_result = $result;
        $result = PyTuple_New(1);
        PyTuple_SetItem($result, 0, old_result);
    }

    toadd = PyTuple_New(1);
    if (PyInt_AsLong (PyTuple_GetItem ($result, 0)) != 0) {
        Py_INCREF(Py_None);
        PyTuple_SetItem(toadd, 0, Py_None);
    } else {
        PyTuple_SetItem(toadd, 0, PyLong_FromLong (*$1));
    }

    old_result = $result;
    $result = PySequence_Concat(old_result, toadd);
    Py_DECREF(old_result);
    Py_DECREF(toadd);
}


%typemap(in, numinputs=0) (gid_t *OUTPUT) {
   u_gid_t tmp;
   $1 = &tmp;
}


%typemap(argout) (gid_t *OUTPUT) {
    PyObject*   old_result;
    PyObject*   toadd;

    if (!PyTuple_Check($result)) {
        old_result = $result;
        $result = PyTuple_New(1);
        PyTuple_SetItem($result, 0, old_result);
    }

    toadd = PyTuple_New(1);
    if (PyInt_AsLong (PyTuple_GetItem ($result, 0)) != 0) {
        Py_INCREF(Py_None);
        PyTuple_SetItem(toadd, 0, Py_None);
    } else {
        PyTuple_SetItem(toadd, 0, PyLong_FromLong (*$1));
    }

    old_result = $result;
    $result = PySequence_Concat(old_result, toadd);
    Py_DECREF(old_result);
    Py_DECREF(toadd);
}


/* C/C++ declarations and apply statement */

%apply (int LENGTH, gid_t *INPUT) {(int nbgids, gid_t *req_gids)};
%apply (char *OUTPUT) {(char *actual_s_type)};
%apply (u_signed64 *OUTPUT) {(u_signed64 *actual_t_space)};
%apply (u_signed64 *OUTPUT) {(u_signed64 *actual_g_space)};
%apply (time_t *OUTPUT) {(time_t *actual_lifetime)};
%apply (char *TOKEN) {(char *s_token)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_reservespace(const char s_type, const char *u_token, const char ret_policy,
        const char ac_latency, u_signed64 req_t_space, u_signed64 req_g_space,
        time_t req_lifetime, int nbgids, gid_t *req_gids, const char *poolname,
        char *actual_s_type, u_signed64 *actual_t_space, u_signed64 *actual_g_space,
        time_t *actual_lifetime, char *s_token);

%clear (int nbgids, gid_t *req_gids);
%clear (char *actual_s_type);
%clear (u_signed64 *actual_t_space);
%clear (u_signed64 *actual_g_space);
%clear (time_t *actual_lifetime);
%clear (char *s_token);





/***********************************************
    Typemaps definition for dpm_rmfs
***********************************************/

/* C/C++ declarations */

extern int dpm_rmfs(char *server, char *fs);





/***********************************************
    Typemaps definition for dpm_rmpool
***********************************************/

/* C/C++ declarations */

extern int dpm_rmpool(char *poolname);





/***********************************************
    Typemaps definition for dpm_updatespace
***********************************************/

%typemap(in) u_signed64 {
    $1 = PyLong_AsLongLong ($input);
}


%typemap(in) time_t {
    $1 = PyLong_AsLong ($input);
}

%typemap(in) (int LENGTH, gid_t *INPUT){
   
  /* Empty list */
  if ($input == Py_None) {
      $1 = 0;
      $2 = NULL;
  }
   
  /* Check the input list */
  else if (!PyList_Check ($input)) {
    PyErr_SetString (PyExc_ValueError, "Expecting a list");
    return NULL;
  }
    
  /* No error */
  else {
    int i;
  
    /* Length of the python list */
    $1 = PyList_Size($input);
   
    $2 = (gid_t *) calloc ($1, sizeof (gid_t));
  
    for (i = 0; i < $1; ++i) {
        $2[i] = PyInt_AsLong (PyList_GetItem($input, i));  
    }
  }/* end of: No error */
    
}/* end of: typemap(in) */


/* Free the temporary array passed to the C function */
%typemap(freearg) (int LENGTH, gid_t *INPUT) {
  if ($2) {
     free ($2);
  }
} 


%typemap(argout) (u_signed64 *OUTPUT) {
    PyObject*   old_result;
    PyObject*   toadd;

    if (!PyTuple_Check($result)) {
        old_result = $result;
        $result = PyTuple_New(1);
        PyTuple_SetItem($result, 0, old_result);
    }

    toadd = PyTuple_New(1);
    if (PyInt_AsLong (PyTuple_GetItem ($result, 0)) != 0) {
        Py_INCREF(Py_None);
        PyTuple_SetItem(toadd, 0, Py_None);
    } else {
        PyTuple_SetItem(toadd, 0, PyLong_FromLongLong (*$1));
    }

    old_result = $result;
    $result = PySequence_Concat(old_result, toadd);
    Py_DECREF(old_result);
    Py_DECREF(toadd);
}


%typemap(in, numinputs=0) (time_t *OUTPUT) {
   time_t tmp;
   $1 = &tmp;
}


%typemap(argout) (time_t *OUTPUT) {
    PyObject*   old_result;
    PyObject*   toadd;

    if (!PyTuple_Check($result)) {
        old_result = $result;
        $result = PyTuple_New(1);
        PyTuple_SetItem($result, 0, old_result);
    }

    toadd = PyTuple_New(1);
    if (PyInt_AsLong (PyTuple_GetItem ($result, 0)) != 0) {
        Py_INCREF(Py_None);
        PyTuple_SetItem(toadd, 0, Py_None);
    } else {
        PyTuple_SetItem(toadd, 0, PyLong_FromLong (*$1));
    }

    old_result = $result;
    $result = PySequence_Concat(old_result, toadd);
    Py_DECREF(old_result);
    Py_DECREF(toadd);
}


/* C/C++ declarations and apply statement */

%apply (int LENGTH, gid_t *INPUT) {(int nbgids, gid_t *req_gids)};
%apply (u_signed64 *OUTPUT) {(u_signed64 *actual_t_space)};
%apply (u_signed64 *OUTPUT) {(u_signed64 *actual_g_space)};
%apply (time_t *OUTPUT) {(time_t *actual_lifetime)};

/* Redeclare the function (no harm) to be sure that the typemap is applied only to this function */
extern int dpm_updatespace(char *s_token, u_signed64 req_t_space, u_signed64 req_g_space, time_t req_lifetime,
        int nbgids, gid_t *req_gids, u_signed64 *actual_t_space, u_signed64 *actual_g_space, time_t *actual_lifetime);

%clear (int nbgids, gid_t *req_gids);
%clear (u_signed64 *actual_t_space);
%clear (u_signed64 *actual_g_space);
%clear (time_t *actual_lifetime);


#endif



/****************************
      Rest of declarations
****************************/

%include "dpns_api.h"
%include "Castor_limits.h"
%include "Cns_constants.h"
%include "Cns_struct.h"
%include "dpm_constants.h"
%include "dpm_struct.h"
struct dirent {
        long            d_ino;
        long            d_off;
        unsigned short  d_reclen;
        char            d_name[256];
};
typedef unsigned int gid_t;
typedef unsigned int mode_t;
typedef long int time_t;
typedef unsigned int uid_t;
typedef unsigned long long u_signed64;
typedef long long signed64;
struct utimbuf {
        time_t actime;
        time_t modtime;
};
#define R_OK 4
#define W_OK 2
#define X_OK 1
#define F_OK 0
#define _PROTO(a) a
#define EXTERN_C extern
#define DLL_DECL
%include "Cns_api.h"
%include "dpm_api.h"
EXTERN_C int DLL_DECL serrno;
EXTERN_C char DLL_DECL *sstrerror _PROTO((int));

