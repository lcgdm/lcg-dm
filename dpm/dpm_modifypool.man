.\" @(#)$RCSfile: dpm_modifypool.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2011 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_MODIFYPOOL 3 "$Date$" LCG "DPM Library Functions"
.SH NAME
dpm_modifypool \- modify a disk pool definition
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_modifypool (struct dpm_pool *" dpm_pool )
.SH DESCRIPTION
.B dpm_modifypool
modifies a disk pool definition.
.TP
.I dpm_pool
contains the new pool definition.
.PP
.nf
.ft CW
struct dpm_pool {
	char		poolname[CA_MAXPOOLNAMELEN+1];
	u_signed64	defsize;
	int		gc_start_thresh;
	int		gc_stop_thresh;
	int		def_lifetime;
	int		defpintime;
	int		max_lifetime;
	int		maxpintime;
	char		fss_policy[CA_MAXPOLICYLEN+1];
	char		gc_policy[CA_MAXPOLICYLEN+1];
	char		mig_policy[CA_MAXPOLICYLEN+1];
	char		rs_policy[CA_MAXPOLICYLEN+1];
	int		nbgids;
	gid_t		*gids;
	char		ret_policy;
	char		s_type;
	u_signed64	capacity;
	u_signed64	free;
	int		nbelem;
};
.ft
.fi
.TP
.I poolname
specifies the disk pool name.
.TP
.I defsize
specifies the default amount of space reserved for a file (in bytes).
If set to -1, the current value is unchanged.
.TP
.I gc_start_thresh
specifies the minimum free space in the pool.
If the percentage of free space goes below this value, the garbage collector
is started. If set to -1, the current value is unchanged.
.TP
.I gc_stop_thresh
specifies the percentage of free space in the pool above which the garbage
collector is stopped. If set to -1, the current value is unchanged.
.TP
.I def_lifetime
specifies the default time a space or volatile file is kept in the system (in seconds).
If set to -1, the current value is unchanged.
.TP
.I defpintime
specifies the default time a file is kept on a given disk (in seconds).
If set to -1, the current value is unchanged.
.TP
.I max_lifetime
specifies the maximum time a space or volatile file is kept in the system (in seconds).
If set to -1, the current value is unchanged.
.TP
.I maxpintime
specifies the maximum time a file is kept on a given disk (in seconds).
If set to -1, the current value is unchanged.
.TP
.I nbgids
is the size of the array of group ids
.IR gids .
If set to -1, the current set is unchanged.
.TP 
.I gids
The disk pool is restricted to this set of gids unless the group gid is zero.
.TP
.I ret_policy
specifies the retention policy supported by the disk pool. It can be
.BR R " (for Replica),"
.BR O " (for Output)"
or
.BR C " (for Custodial)."
If set to 0, the current value is unchanged.
.TP
.I s_type
indicates the type of space supported in the disk pool. It can be
.BR V " (for Volatile),"
.BR D " (for Durable),"
.BR P " (for Permanent)"
or
.BR - " (to accept any type)."
If set to 0, the current value is unchanged.
.LP
This function requires ADMIN privilege.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
This pool does not exist.
.TP
.B EACCES
The caller does not have ADMIN privilege.
.TP
.B EFAULT
.I dpm_pool
is a NULL pointer.
.TP
.B EINVAL
The length of
.I poolname
exceeds
.B CA_MAXPOOLNAMELEN
or the number of gids is too big.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
