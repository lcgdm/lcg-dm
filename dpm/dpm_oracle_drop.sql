--
--  Copyright (C) 2004-2008 by CERN/IT/GD/CT
--  All rights reserved
--
--       @(#)$RCSfile: dpm_oracle_drop.sql,v $ $Revision: 1.3 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/CT Jean-Philippe Baud
 
--     Drop Disk Pool Manager Oracle tables and constraints.

ALTER TABLE dpm_space_reserv
       DROP CONSTRAINT pk_s_token;
ALTER TABLE dpm_copy_filereq
       DROP CONSTRAINT pk_c_fullid;
ALTER TABLE dpm_put_filereq
       DROP CONSTRAINT pk_p_fullid;
ALTER TABLE dpm_get_filereq
       DROP CONSTRAINT pk_g_fullid;
ALTER TABLE dpm_req
       DROP CONSTRAINT pk_token;
ALTER TABLE dpm_pending_req
       DROP CONSTRAINT pk_p_token;
ALTER TABLE dpm_fs
       DROP CONSTRAINT pk_fs
       DROP CONSTRAINT fk_fs;
ALTER TABLE dpm_pool
       DROP CONSTRAINT pk_poolname;

DROP SEQUENCE dpm_unique_id;

DROP TABLE dpm_space_reserv;
DROP TABLE dpm_copy_filereq;
DROP TABLE dpm_put_filereq;
DROP TABLE dpm_get_filereq;
DROP TABLE dpm_req;
DROP TABLE dpm_pending_req;
DROP TABLE dpm_fs;
DROP TABLE dpm_pool;
DROP TABLE schema_version_dpm;
