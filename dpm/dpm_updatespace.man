.\" @(#)$RCSfile: dpm_updatespace.man,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005-2011 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH DPM_UPDATESPACE 3 "$Date$" LCG "DPM Library Functions"
.SH NAME
dpm_updatespace \- update space
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_updatespace (char *" s_token ,
.BI "u_signed64 " req_t_space ,
.BI "u_signed64 " req_g_space ,
.BI "time_t " req_lifetime ,
.BI "int " nbgids ,
.BI "gid_t *" req_gids ,
.BI "u_signed64 *" actual_t_space ,
.BI "u_signed64 *" actual_g_space ,
.BI "time_t *" actual_lifetime )
.SH DESCRIPTION
.B dpm_updatespace
updates space.
.LP
The input arguments are:
.TP
.I s_token
specifies the token returned by a previous reservespace request.
.TP
.I req_t_space
Total space requested in bytes.
If set to -1, the current value is unchanged.
.TP
.I req_g_space
Guaranteed space requested in bytes.
If set to -1, the current value is unchanged.
.TP
.I req_lifetime
specifies the requested space lifetime (in seconds) relative to the current time.
If set to -1, the current value is unchanged.
.TP
.I nbgids
is the size of the array of group ids
.IR req_gids .
If set to -1, the current value is unchanged.
.TP
.I req_gids
when zero, the space is reserved for the user if the requester has a proxy
without VOMS extension or for the group if the requester has a proxy with
VOMS extension.
If not zero, the space is reserved for this set of group ids.
If the caller does not belong to the group, it must have ADMIN privileges.
.LP
The output arguments are:
.TP
.I actual_t_space
Total space reserved in bytes (best effort).
.TP
.I actual_g_space
Guaranteed space reserved in bytes.
.TP
.I actual_lifetime
Lifetime of the reserved space (in seconds) relative to the current time.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I s_token
is a NULL pointer or
.I nbgids
is greater than 0 and
.I req_gids
is a NULL pointer.
.TP
.B EINVAL
.I s_token
is invalid (too long) or unknown or the space requested is not a positive value
or the number of gids is too big.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
