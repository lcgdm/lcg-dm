.\" @(#)$RCSfile: dpm_getstatus_copyreq.man,v $ $Revision: 1.3 $ $Date: 2006/12/20 15:55:16 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_GETSTATUS_COPYREQ 3 "$Date: 2006/12/20 15:55:16 $" LCG "DPM Library Functions"
.SH NAME
dpm_getstatus_copyreq \- get status for a dpm_copy request
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getstatus_copyreq (char *" r_token ,
.BI "int " nbsurls ,
.BI "char **" fromsurls ,
.BI "char **" tosurls ,
.BI "int *" nbreplies ,
.BI "struct dpm_copyfilestatus **" filestatuses )
.SH DESCRIPTION
.B dpm_getstatus_copyreq
gets status for a dpm_copy request.
.LP
The input arguments are:
.TP
.I r_token
specifies the token returned by a previous copy request.
.TP
.I nbsurls
specifies the number of files for which the status is requested. If zero,
the status of all files in the copy request is returned.
.TP
.I fromsurls
specifies the array of source file names.
.TP
.I tosurls
specifies the array of target file names.
.LP
The output arguments are:
.TP
.I nbreplies
will be set to the number of replies in the array of file statuses.
.TP
.I filestatuses
will be set to the address of an array of dpm_copyfilestatus structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_copyfilestatus {
	char		*from_surl;
	char		*to_surl;
	u_signed64	filesize;
	int		status;
	char		*errstring;
	time_t	f_lifetime;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I nbsurls
is strictly positive and
.I fromsurls
or
.I tosurls
is NULL or
.IR r_token ,
.I nbreplies
or
.I filestatuses
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for marshalling the request.
.TP
.B EINVAL
.I nbsurls
is not positive, the token is invalid/unknown or all file requests have errors.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
