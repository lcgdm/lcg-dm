/*
 * Copyright (C) 2006-2011 by CERN/IT/GD/ITR
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_replicate.c,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud";
#endif /* not lint */

/*      dpm_replicate - replicate a file given the SURL or the PFN */
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "dpm_api.h"
#include "dpns_api.h"
#include "rfio_api.h"
#include "serrno.h"
#define DEFPOLLINT 5
int DLL_DECL
dpm_replicate(const char *path, char f_type)
{
	return (dpm_replicatex (path, f_type, NULL, 0, NULL));
}

int DLL_DECL
dpm_replicatex(const char *path, char f_type, const char *s_token, time_t ltime, char *pfn)
{
	struct dpm_filestatus *filestatuses = NULL;
	char *from_turl = NULL;
	struct dpm_getfilereq getfilereq;
	struct dpm_getfilestatus *getfilestatus;
	int i;
	int nbreplies;
	char *p;
	static char *protocols[] = {"rfio"};
	struct dpm_putfilereq putfilereq;
	struct dpm_putfilestatus *putfilestatus;
	int r = 0;
	char r_tokeng[CA_MAXDPMTOKENLEN+1];
	char r_tokenp[CA_MAXDPMTOKENLEN+1];
	int save_serrno;
	char sfn[CA_MAXSFNLEN+1];
	struct Cns_filestatg statbuf;
	int status;
	char *surl;
	char *to_turl = NULL;
	char turl[CA_MAXSFNLEN+1];

	if (s_token && strlen (s_token) > CA_MAXDPMTOKENLEN) {
		serrno = ENAMETOOLONG;
		return (-1);
	}
	if (strncmp (path, "srm://", 6) == 0 ||
	    (strncmp (path, CNS_ROOT, sizeof(CNS_ROOT)-1) == 0 &&
	    (*(path+sizeof(CNS_ROOT)-1) == '\0' || *(path+sizeof(CNS_ROOT)-1) == '/'))) {
		surl = (char *) path;
		memset (&getfilereq, 0, sizeof(getfilereq));
		getfilereq.from_surl = surl;
		if ((status = dpm_get (1, &getfilereq, 1, protocols, NULL,
		    0, r_tokeng, &nbreplies, &getfilestatus)) < 0)
			return (-1);

		/* wait for request status "Done" or "Failed" */

		while (status == DPM_QUEUED || status == DPM_ACTIVE) {
			if (getfilestatus->from_surl)
				free (getfilestatus->from_surl);
			if (getfilestatus->turl)
				free (getfilestatus->turl);
			if (getfilestatus->errstring)
				free (getfilestatus->errstring);
			free (getfilestatus);
			sleep ((r++ == 0) ? 1 : DEFPOLLINT);
			if ((status = dpm_getstatus_getreq (r_tokeng, 0, NULL,
			    &nbreplies, &getfilestatus)) < 0)
				return (-1);
		}
		if (getfilestatus->turl)
			from_turl = getfilestatus->turl;
		if (getfilestatus->from_surl)
			free (getfilestatus->from_surl);
		if (getfilestatus->errstring)
			free (getfilestatus->errstring);
		free (getfilestatus);
	} else {
		if (Cns_statr (path, &statbuf) < 0)
			return (-1);
		strcpy (turl, "rfio://");
		strcpy (turl + 7, path);
		p = strchr (turl + 7, ':');
		*p = '/';
		from_turl = turl;
		if (Cns_getpath (NULL, statbuf.fileid, sfn) < 0)
			return (-1);
		surl = sfn;
	}

	/* get turl for replica */

	memset (&putfilereq, 0, sizeof(putfilereq));
	putfilereq.to_surl = surl;
	putfilereq.f_type = f_type;
	if (s_token)
		strcpy(putfilereq.s_token, s_token);
	putfilereq.f_lifetime = ltime;
	if ((status = dpm_put (1, &putfilereq, 1, protocols, NULL, 4,
	    0, r_tokenp, &nbreplies, &putfilestatus)) < 0)
		return (-1);

	/* wait for request status "Done" or "Failed" */

	r = 0;
	while (status == DPM_QUEUED || status == DPM_ACTIVE) {
		if (putfilestatus->to_surl)
			free (putfilestatus->to_surl);
		if (putfilestatus->turl)
			free (putfilestatus->turl);
		if (putfilestatus->errstring)
			free (putfilestatus->errstring);
		free (putfilestatus);
		sleep ((r++ == 0) ? 1 : DEFPOLLINT);
		if ((status = dpm_getstatus_putreq (r_tokenp, 0, NULL,
		    &nbreplies, &putfilestatus)) < 0)
			return (-1);
	}
	if (putfilestatus->turl)
		to_turl = putfilestatus->turl;
	if (putfilestatus->to_surl)
		free (putfilestatus->to_surl);
	if (putfilestatus->errstring)
		free (putfilestatus->errstring);
	free (putfilestatus);

	status = rfio_rcp (from_turl, to_turl, 0);
	save_serrno = serrno;
	if (from_turl != turl) {
		free (from_turl);
		(void) dpm_relfiles (r_tokeng, 1, &surl, 0, &nbreplies, &filestatuses);
		dpm_free_filest (nbreplies, filestatuses);
	}

	filestatuses = NULL;
	if (status < 0) {
		(void) dpm_abortfiles (r_tokenp, 1, &surl, &nbreplies, &filestatuses);
	} else {
		status = dpm_putdone (r_tokenp, 1, &surl, &nbreplies, &filestatuses);
		save_serrno = serrno;
		if (status < 0 && nbreplies == 1 && filestatuses && *filestatuses->errstring)
			dpm_errmsg (NULL, "%s", filestatuses->errstring);
	}
	dpm_free_filest (nbreplies, filestatuses);
	serrno = save_serrno;
	if (pfn != NULL && to_turl != NULL) {
		*pfn = '\0';
		if ((p = strstr (to_turl, "rfio://"))) {
			p += 7;
			strcpy (pfn, p);
			if ((p = strchr (pfn, '/')))
				*p = ':';
		}
		if (!*pfn) {
			status = -1;
			serrno = ENOENT;
		}
	}
	free (to_turl);
	return (status);
}
