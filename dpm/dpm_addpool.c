/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_addpool.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_addpool - define a new disk pool */

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_addpool(struct dpm_pool *dpm_pool)
{
	int c;
	char func[16];
	gid_t gid;
	int i;
	int msglen;
	char *p;
	char *q;
	char *sbp;
	char sendbuf[REQBUFSZ];
	struct dpm_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "dpm_addpool");
	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (! dpm_pool) {
		serrno = EFAULT;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC3);
	marshall_LONG (sbp, DPM_ADDPOOL);
	q = sbp;	/* save pointer. The next field will be updated */
	msglen = 3 * LONGSIZE;
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_STRING (sbp, dpm_pool->poolname);
	marshall_HYPER (sbp, dpm_pool->defsize);
	marshall_LONG (sbp, dpm_pool->gc_start_thresh);
	marshall_LONG (sbp, dpm_pool->gc_stop_thresh);
	marshall_LONG (sbp, dpm_pool->defpintime);
	marshall_STRING (sbp, dpm_pool->fss_policy);
	marshall_STRING (sbp, dpm_pool->gc_policy);
	marshall_STRING (sbp, dpm_pool->rs_policy);
	marshall_LONG (sbp, dpm_pool->nbgids > 0 ? dpm_pool->gids[0] : 0);
	marshall_BYTE (sbp, dpm_pool->s_type);
	marshall_STRING (sbp, dpm_pool->mig_policy);
	marshall_BYTE (sbp, dpm_pool->ret_policy);
	marshall_LONG (sbp, dpm_pool->def_lifetime);
	marshall_LONG (sbp, dpm_pool->max_lifetime);
	marshall_LONG (sbp, dpm_pool->maxpintime);
	marshall_LONG (sbp, dpm_pool->nbgids);
	for (i = 0; i < dpm_pool->nbgids; i++)
		marshall_LONG (sbp, dpm_pool->gids[i]);

	msglen = sbp - sendbuf;
	marshall_LONG (q, msglen);	/* update length field */

	c = send2dpm (NULL, sendbuf, msglen, NULL, 0, NULL, NULL);
	return (c);
}
