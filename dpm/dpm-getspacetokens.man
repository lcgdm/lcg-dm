.\" @(#)$RCSfile: dpm-getspacetokens.man,v $ $Revision: 1.1 $ $Date: 2008/01/09 10:16:23 $ CERN Jean-Philippe Baud
.\" Copyright (C) 2007 by CERN
.\" All rights reserved
.\"
.TH DPM-GETSPACETOKENS 1 "$Date: 2008/01/09 10:16:23 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-getspacetokens \- get list of space tokens
.SH SYNOPSIS
.B dpm-getspacetokens
[
.BI --token_desc " u_token"
]
.LP
.B dpm-getspacetokens
.B --help
.SH DESCRIPTION
.B dpm-getspacetokens
gets the list of space tokens.
.TP
.I u_token
specifies the user provided description associated with a previous reservespace
request. If the option is not specified, all the space tokens accessible by the
user will be listed.
.SH EXAMPLE
.nf
.ft CW
	setenv DPM_HOST dpmhost
	setenv DPNS_HOST dpnshost
.sp
	dpm-getspacetokens
.sp
	fe869590-b771-4002-b11a-8e7430d72911
	80e35f8b-7e4e-49a9-90b0-5d5a7ce7e8bc
	023a5ec1-03ea-464c-9af7-5246183ff5c5
.sp
	dpm-getspacetokens --token_desc myspace
.sp
	fe869590-b771-4002-b11a-8e7430d72911
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.B dpm-reservespace(1)
