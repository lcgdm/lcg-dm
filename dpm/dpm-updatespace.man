.\" @(#)$RCSfile: dpm-updatespace.man,v $ $Revision$ $Date$ CERN IT-GD/ITR Jean-Philippe Baud
.\" Copyright (C) 2007-2011 by CERN/IT/GD/ITR
.\" All rights updated
.\"
.TH DPM-UPDATESPACE 1 "$Date$" LCG "DPM Commands"
.SH NAME
dpm-updatespace \- update space
.SH SYNOPSIS
.B dpm-updatespace
.BI --space_token " s_token"
[
.BI --gid " group_ids"
] [
.BI --group " group_names"
] [
.BI --gspace " size_guaranteed_space"
] [
.B --help
] [
.BI --lifetime " space_lifetime"
] [
.B --si
]
.sp
.B dpm-updatespace
.BI --token_desc " u_token"
[
.BI --gid " group_ids"
] [
.BI --group " group_names"
] [
.BI --gspace " size_guaranteed_space"
] [
.B --help
] [
.BI --lifetime " space_lifetime"
] [
.B --si
]
.SH DESCRIPTION
.B dpm-updatespace
updates space.
.SH OPTIONS
.TP
.I s_token
specifies the token returned by a previous reservespace request.
.TP
.I u_token
specifies the user provided description associated with the reservespace request.
.TP
.I group_ids
The existing list can be reset using a comma separated list of group gids,
extended by prefixing the gid by + or reduced by prefixing the gid by -.
See examples.
.TP
.I group_names
The existing list can be reset using a comma separated list of group names,
extended by prefixing the name by + or reduced by prefixing the name by -.
.TP
.I size_guaranteed_space
new size of guaranteed space desired in bytes.
The number may also have a suffix k, M, G, T or P to indicate kB, MB, GB, TB or
PB respectively.
.TP
.I space_lifetime
specifies the new space lifetime relative to the current time.
It can be "Inf" (for infinite) or expressed in years (suffix 'y'), months
(suffix 'm'), days (suffix 'd'), hours (suffix 'h') or seconds (no suffix).
.TP
.B --si
use powers of 1000 not 1024 for sizes.
.SH EXAMPLE
.nf
.ft CW
dpm-updatespace --space_token fe869590-b771-4002-b11a-8e7430d72911 --lifetime 1m
.sp
dpm-updatespace --token_desc myspace --gspace 5G
.sp
dpm-updatespace --token_desc atlgrpspace --group -atlas/higgs
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm_updatespace(3)
