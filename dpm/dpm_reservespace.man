.\" @(#)$RCSfile: dpm_reservespace.man,v $ $Revision$ $Date$ CERN IT-GD/SC Jean-Philippe Baud
.\" Copyright (C) 2005-2011 by CERN/IT/GD/SC
.\" All rights reserved
.\"
.TH DPM_RESERVESPACE 3 "$Date$" LCG "DPM Library Functions"
.SH NAME
dpm_reservespace \- reserve space
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_reservespace (const char " s_type ,
.BI "const char *" u_token ,
.BI "const char " ret_policy ,
.BI "const char " ac_latency ,
.BI "u_signed64 " req_t_space ,
.BI "u_signed64 " req_g_space ,
.BI "time_t " req_lifetime ,
.BI "int " nbgids ,
.BI "gid_t *" req_gids ,
.BI "const char *" poolname,
.BI "char *" actual_s_type ,
.BI "u_signed64 *" actual_t_space ,
.BI "u_signed64 *" actual_g_space ,
.BI "time_t *" actual_lifetime ,
.BI "char *" s_token )
.SH DESCRIPTION
.B dpm_reservespace
reserves space.
.LP
The input arguments are:
.TP
.I s_type
specifies the type of space requested. It can be
.BR V " (for Volatile),"
.BR D " (for Durable),"
.BR P " (for Permanent)"
or
.BR - " (for any)."
.TP
.I u_token
specifies the user provided description associated with the request.
This argument may be NULL.
.TP
.I ret_policy
specifies the retention policy requested. It can be
.BR R " (for REPLICA),"
.BR O " (for OUTPUT)"
or
.BR C " (for CUSTODIAL)."
.TP
.I ac_latency
specifies the access latency requested. It can be
.BR O " (for ONLINE)"
or
.BR N " (for NEARLINE)."
.TP
.I req_t_space
Total space requested in bytes.
.TP
.I req_g_space
Guaranteed space requested in bytes.
.TP
.I req_lifetime
specifies the requested space lifetime (in seconds) relative to the current time.
.TP
.I nbgids
is the size of the array of group ids
.IR req_gids .
.TP
.I req_gids
when zero, the space is reserved for the user if the requester has a proxy
without VOMS extension or for the group if the requester has a proxy with
VOMS extension.
If not zero, the space is reserved for this set of group ids.
If the caller does not belong to the group, it must have ADMIN privileges.
.TP
.I poolname
If not NULL, reserves the space in that disk pool.
.LP
The output arguments are:
.TP
.I actual_s_type
Type of reserved space.
.TP
.I actual_t_space
Total space reserved in bytes (best effort).
.TP
.I actual_g_space
Guaranteed space reserved in bytes.
.TP
.I actual_lifetime
Lifetime of the reserved space (in seconds) relative to the current time.
.TP
.I s_token
Address of a buffer to receive the system allocated token.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EACCES
The caller does not belong to the group identified by
.I req_gid
and does not have ADMIN privileges.
.TP
.B EFAULT
.I nbgids
is greater than 0 and
.I req_gids
is a NULL pointer.
.TP
.B EINVAL
The space type is invalid or the length of the user space token description is
greater than 255, the space requested is not a positive value, the pool name is
longer than
.B CA_MAXPOOLNAMELEN
or unknown
or the number of gids is too big.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
