.\" @(#)$RCSfile: dpm_extendfilelife.man,v $ $Revision: 1.2 $ $Date: 2006/12/20 15:28:25 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004-2006 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_EXTENDFILELIFE 3 "$Date: 2006/12/20 15:28:25 $" LCG "DPM Library Functions"
.SH NAME
dpm_extendfilelife \- extend file lifetime
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_extendfilelife (char *" r_token ,
.BI "char *" surl ,
.BI "time_t " lifetime ,
.BI "time_t *" actual_lifetime )
.SH DESCRIPTION
.B dpm_extendfilelife
extends file lifetime.
.TP
.I r_token
specifies the token returned by a previous get, put or copy request.
.TP
.I surl
specifies the name of the file.
.TP
.I lifetime
specifies the new lifetime relative to the current time.
.TP
.I actual_lifetime
will receive the actual lifetime assigned by the server.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
File does not exist.
.TP
.B EFAULT
.I surl
is a NULL pointer.
.TP
.B EINVAL
The length of
.I r_token
exceeds
.B CA_MAXDPMTOKENLEN
or the token is unknown.
.TP
.B ENAMETOOLONG
The length of the surl exceeds
.BR CA_MAXSFNLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
