.\" @(#)$RCSfile: dpm-modifypool.man,v $ $Revision$ $Date$ CERN Jean-Philippe Baud
.\" Copyright (C) 2004-2010 by CERN
.\" All rights reserved
.\"
.TH DPM-MODIFYPOOL 1 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
dpm-modifypool \- modify a disk pool definition
.SH SYNOPSIS
.B dpm-modifypool
.BI --poolname " pool_name"
[
.BI --def_filesize " defsize"
] [
.BI --def_lifetime " def_lifetime"
] [
.BI --def_pintime " defpintime"
] [
.BI --gc_start_thresh " perc"
] [
.BI --gc_stop_thresh " perc"
] [
.BI --gid " pool_gids"
] [
.BI --group " pool_groups"
] [
.B --help
] [
.BI --max_lifetime " max_lifetime"
] [
.BI --max_pintime " maxpintime"
] [
.BI --ret_policy " retention_policy"
] [
.BI --s_type " space_type"
]
.SH DESCRIPTION
.B dpm-modifypool
modifies a disk pool definition.
.LP
This command requires ADMIN privilege.
.SH OPTIONS
.TP
.I pool_name
specifies the disk pool name.
It must be at most CA_MAXPOOLNAMELEN characters long.
.TP
.I defsize
specifies the default amount of space reserved for a file.
This is by default in bytes, but the number may also be post-fixed
with 'k', 'M' or 'G' for kilobyte, Megabyte and Gigabyte respectively.
.TP
.I def_lifetime
specifies the default time a space or volatile file is kept in the system (in seconds).
.TP
.I defpintime
specifies the default time a file is kept on a given disk (in seconds).
.TP
.I gc_start_thresh
specifies the minimum free space in the pool.
If the percentage of free space goes below this value, the garbage collector
is started.
.TP
.I gc_stop_thresh
specifies the percentage of free space in the pool above which the garbage
collector is stopped.
.TP
.I max_lifetime
specifies the maximum time a space or volatile file is kept in the system (in seconds).
.TP
.I maxpintime
specifies the maximum time a file is kept on a given disk (in seconds).
.TP 
.I pool_gids
The existing list can be reset using a comma separated list of group gids,
extended by prefixing the gid by + or reduced by prefixing the gid by -.
See examples.
.TP 
.I pool_groups
The existing list can be reset using a comma separated list of group names,
extended by prefixing the name by + or reduced by prefixing the name by -.
The keyword "ALL" means non-dedicated pool.
See examples.
.TP
.I retention_policy
specifies the retention policy supported by the disk pool. It can be
.BR R " (for Replica),"
.BR O " (for Output)"
or
.BR C " (for Custodial)."
.TP
.I space_type
indicates the type of space supported in the disk pool. It can be
.BR V " (for Volatile),"
.BR D " (for Durable),"
.BR P " (for Permanent)"
or
.BR - " (to accept any type)."
.SH EXAMPLE
.nf
.ft CW
	dpm-modifypool --poolname Volatile --def_filesize 100M \\
		--groups -dteam,ops,+atlas
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.B dpm_modifypool(3)
