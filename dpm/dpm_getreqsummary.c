/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_getreqsummary.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_getreqsummary - get summary for a set of requests */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/in.h>
#endif
#include "dpm_api.h"
#include "dpm.h"
#include "marshall.h"
#include "serrno.h"

int DLL_DECL
dpm_getreqsummary (int nbtokens, char **r_tokens, int *nbreplies, struct dpm_reqsummary **summaries)
{
	int c;
	char errstring[256];
	char func[18];
	gid_t gid;
	int i;
	int msglen;
	char *rbp;
	char repbuf[4+256];
	char *sbp;
	char *sendbuf;
	struct dpm_api_thread_info *thip;
	uid_t uid;

	strcpy (func, "dpm_getreqsummary");
	if (dpm_apiinit (&thip))
		return (-1);
	uid = geteuid();
	gid = getegid();
#if defined(_WIN32)
	if (uid < 0 || gid < 0) {
		dpm_errmsg (func, DP053);
		serrno = SENOMAPFND;
		return (-1);
	}
#endif

	if (nbtokens <= 0) {
		serrno = EINVAL;
		return (-1);
	}
	if (! r_tokens || ! nbreplies || ! summaries) {
		serrno = EFAULT;
		return (-1);
	}

	/* Compute size of send buffer */

	msglen = 6 * LONGSIZE;
	for (i = 0; i < nbtokens; i++) {
		msglen += strlen (r_tokens[i]) + 1;
	}

	/* Allocate send buffer */

	if ((sendbuf = malloc (msglen)) == NULL) {
		serrno = ENOMEM;
		return (-1);
	}

	/* Build request header */

	sbp = sendbuf;
	marshall_LONG (sbp, DPM_MAGIC);
	marshall_LONG (sbp, DPM_GETREQSUM);
	marshall_LONG (sbp, msglen);

	/* Build request body */

	marshall_LONG (sbp, uid);
	marshall_LONG (sbp, gid);
	marshall_LONG (sbp, nbtokens);
	for (i = 0; i < nbtokens; i++) {
		marshall_STRING (sbp, r_tokens[i]);
	}

	c = send2dpm (NULL, sendbuf, msglen, repbuf, sizeof(repbuf),
	    (void **)summaries, nbreplies);
	free (sendbuf);
	if (c == 0) { 
		rbp = repbuf;
		unmarshall_LONG (rbp, c);
		if (c) {
			serrno = c - DPM_FAILED;
			c = -1;
		}
		unmarshall_STRING (rbp, errstring);
		if (*errstring)
			dpm_errmsg (func, "%s\n", errstring);
	}
	return (c);
}
