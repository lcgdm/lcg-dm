/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_apiinit.c,v $ $Revision: 1.2 $ $Date: 2005/11/24 07:20:25 $ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

/*	dpm_apiinit - allocate thread specific or global structures */

#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include "Cglobals.h"
#include "dpm_api.h"
#include "serrno.h"
static int dpm_api_key = -1;

int DLL_DECL
dpm_apiinit(thip)
struct dpm_api_thread_info **thip;
{
	Cglobals_get (&dpm_api_key,
	    (void **) thip, sizeof(struct dpm_api_thread_info));
	if (*thip == NULL) {
		serrno = ENOMEM;
		return (-1);
	}
	if(! (*thip)->initialized) {
		(*thip)->initialized = 1;
	}
	return (0);
}

int DLL_DECL *
C__dpm_errno()
{
struct dpm_api_thread_info *thip;
	Cglobals_get (&dpm_api_key,
	    (void **) &thip, sizeof(struct dpm_api_thread_info));
	return (&thip->dp_errno);
}
