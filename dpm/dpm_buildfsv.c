/*
 * Copyright (C) 2011 by CERN/IT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_buildfsv.c $ $Revision$ $Date$ CERN IT Maarten Litmaath/Jean-Philippe Baud";
#endif /* not lint */

/*      dpm_buildfsv - build filesystem selection vector */
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "dpm.h"
#include "dpm_server.h"
#include "serrno.h"
/* New filesystem selection algorithm proposed by Maarten Litmaath */

/* The new algorithm allows to assign weights to filesystems and tries
   to avoid overloading servers or filesystems.
   To obtain the file system "vector" the code will first try to avoid
   selecting the same file server twice in a row and then try to avoid
   selecting the same file system twice in a row.
   At any time it will try to select the server and the file system
   that have the highest remaining weight and are compatible with
   the previous rule.

   A file system with a weight of N will appear N times in the vector.
   At any time the next file system to use is the next entry in the vector.
   When the last entry has been used, the DPM starts again with the first.
 */

dpm_build_fs_vector (int nbfs, struct dpm_fs *dpm_fs, int **fs_vector)
{
	int cur_fs = nbfs;
	int *fs_servers = NULL;		/* disk server ordinal for each fs */
	int fs_vector_len = 0;
	int *fs_weights = NULL;		/* weight for each fs */
	int *fsv;
	int i;
	int j = 0;

	if (nbfs == 0)
		return (0);
	if (nbfs > 1) {
		if ((fs_servers = malloc (nbfs * sizeof(int))) == NULL) {
			serrno = ENOMEM;
			return (-1);
		}
		if ((fs_weights = malloc (nbfs * sizeof(int))) == NULL) {
			free (fs_servers);
			serrno = ENOMEM;
			return (-1);
		}
		*fs_servers = 0;
		for (i = 1; i < nbfs; i++) {
			if (strcmp ((dpm_fs + i)->server, (dpm_fs + i - 1)->server))
				j++;
			*(fs_servers + i) = j;
		}
		/* vector length = sum of filesystem weights unless there is a single filesystem */
		for (i = 0; i < nbfs; i++) {
			if ((dpm_fs + i)->status == 0) {
				*(fs_weights + i) = (dpm_fs + i)->weight;
				fs_vector_len += (dpm_fs + i)->weight;
			} else
				*(fs_weights + i) = 0;
		}
	} else
		fs_vector_len = 1;
	if ((fsv = malloc (fs_vector_len * sizeof(int))) == NULL) {
		free (fs_servers);
		free (fs_weights);
		serrno = ENOMEM;
		return (-1);
	}
	if (nbfs > 1) {
		for (i = 0; i < fs_vector_len; i++) {
			cur_fs = dpm_compute_fs_vector_elem (cur_fs, fs_servers,
			    fs_weights, nbfs);
			*(fsv + i) = cur_fs;
			(*(fs_weights + cur_fs))--;
		}
	} else
		*fsv = 0;
	free (fs_servers);
	free (fs_weights);
	*fs_vector = fsv;
	return (fs_vector_len);
}

dpm_compute_fs_vector_elem (int cur_fs, int *fs_servers, int *fs_weights, int nbfs)
{
	int i;
	int j;
	int max_fs = nbfs;
	int max_server = nbfs;
	int max_weight = 0;
	int nxt_fs = nbfs;
	int nxt_server = nbfs;
	int nxt_weight = 0;
	int tmp_fs;
	int tmp_server;
	int tmp_weight;

	/* select disk server */

	for (i = 0; i < nbfs; ) {
		tmp_server = *(fs_servers + i);
		/* compute sum of weights for the current disk server */
		tmp_weight = 0;
		for (j = i; j < nbfs; j++) {
			if (*(fs_servers + j) != *(fs_servers + i)) break;
			tmp_weight += *(fs_weights + j);
		}

		/* find server with highest weight */
		if (max_weight < tmp_weight) {
			nxt_weight = max_weight;
			nxt_server = max_server;
			max_weight = tmp_weight;
			max_server = tmp_server;
		} else if (nxt_weight < tmp_weight) {
			nxt_weight = tmp_weight;
			nxt_server = tmp_server;
		}
		i = j;
	}

	/* select the server with highest priority, if possible different
	   from the previously selected server */

	if (cur_fs < nbfs 	/* not the first selection */
	    && max_server == *(fs_servers + cur_fs)	/* same server as last time */
	    && nxt_server < nbfs)	/* there exists another possible server */
		tmp_server = nxt_server;
	else
		tmp_server = max_server;

	/* select filesystem */

	max_weight = 0;
	nxt_weight = 0;
	for (i = 0; i < nbfs; i++) {
		if (*(fs_servers + i) != tmp_server) continue;
		/* find filesystem with highest weight */
		if (max_weight < *(fs_weights + i)) {
			nxt_weight = max_weight;
			nxt_fs = max_fs;
			max_weight = *(fs_weights + i);
			max_fs = i;
		} else if (nxt_weight < *(fs_weights + i)) {
			nxt_weight = *(fs_weights + i);
			nxt_fs = i;
		}
	}

	/* select the filesystem with highest priority, if possible different
	   from the previously selected filesystem */

	if (max_fs == cur_fs	/* same filesystem selected as last time */
	    && nxt_fs < nbfs)	/* there exists another possible fs */
		tmp_fs = nxt_fs;
	else
		tmp_fs = max_fs;
	return (tmp_fs);
}
