/*
 * Copyright (C) 2005-2008 by CERN/IT/GD/SC
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpm_getturl.c,v $ $Revision: 1.5 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/SC Jean-Philippe Baud";
#endif /* not lint */

/*      dpm_getturl - get TURL associated with a given SURL for rfio protocol */
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#ifndef _WIN32
#include <unistd.h>
#else
#ifndef O_ACCMODE
#define O_ACCMODE 3
#endif
#endif
#include "dpm_api.h"
#define DEFPOLLINT 5
char DLL_DECL *
dpm_getturl(const char *sfn, int oflag, u_signed64 maxsize, char *r_token)
{
	struct dpm_getfilereq getfilereq;
	struct dpm_getfilestatus *getfilestatus;
	int i;
	int nbreplies;
	static char *protocols[] = {"rfio"};
	struct dpm_putfilereq putfilereq;
	struct dpm_putfilestatus *putfilestatus;
	int r = 0;
	int status;
	char *turl;

	if ((oflag & O_ACCMODE) == 0) {
		memset (&getfilereq, 0, sizeof(getfilereq));
		getfilereq.from_surl = (char *) sfn;
		if ((status = dpm_get (1, &getfilereq, 1, protocols, NULL,
		    0, r_token, &nbreplies, &getfilestatus)) < 0)
			return (NULL);

		/* wait for request status "Done" or "Failed" */

		while (status == DPM_QUEUED || status == DPM_ACTIVE) {
			if (getfilestatus->from_surl)
				free (getfilestatus->from_surl);
			if (getfilestatus->turl)
				free (getfilestatus->turl);
			if (getfilestatus->errstring)
				free (getfilestatus->errstring);
			free (getfilestatus);
			sleep ((r++ == 0) ? 1 : DEFPOLLINT);
			if ((status = dpm_getstatus_getreq (r_token, 0, NULL,
			    &nbreplies, &getfilestatus)) < 0)
				return (NULL);
		}
		if (getfilestatus->turl)
			turl = getfilestatus->turl;
		if (getfilestatus->from_surl)
			free (getfilestatus->from_surl);
		if (getfilestatus->errstring)
			free (getfilestatus->errstring);
		free (getfilestatus);
	} else {
		memset (&putfilereq, 0, sizeof(putfilereq));
		putfilereq.to_surl = (char *) sfn;
		putfilereq.requested_size = maxsize;
		if ((status = dpm_put (1, &putfilereq, 1, protocols, NULL, 1,
		    0, r_token, &nbreplies, &putfilestatus)) < 0)
			return (NULL);

		/* wait for request status "Done" or "Failed" */

		while (status == DPM_QUEUED || status == DPM_ACTIVE) {
			if (putfilestatus->to_surl)
				free (putfilestatus->to_surl);
			if (putfilestatus->turl)
				free (putfilestatus->turl);
			if (putfilestatus->errstring)
				free (putfilestatus->errstring);
			free (putfilestatus);
			sleep ((r++ == 0) ? 1 : DEFPOLLINT);
			if ((status = dpm_getstatus_putreq (r_token, 0, NULL,
			    &nbreplies, &putfilestatus)) < 0)
				return (NULL);
		}
		if (putfilestatus->turl)
			turl = putfilestatus->turl;
		if (putfilestatus->to_surl)
			free (putfilestatus->to_surl);
		if (putfilestatus->errstring)
			free (putfilestatus->errstring);
		free (putfilestatus);
	}
	return (turl);
}
