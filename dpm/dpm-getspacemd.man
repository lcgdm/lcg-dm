.\" @(#)$RCSfile: dpm-getspacemd.man,v $ $Revision: 1.3 $ $Date: 2008/09/24 11:25:00 $ CERN Jean-Philippe Baud
.\" Copyright (C) 2007-2008 by CERN
.\" All rights reserved
.\"
.TH DPM-GETSPACEMD 1 "$Date: 2008/09/24 11:25:00 $" LCG "DPM Administrator Commands"
.SH NAME
dpm-getspacemd \- get space metadata
.SH SYNOPSIS
.B dpm-getspacemd
.BI --space_token " s_token"
[
.B --si
]
.LP
.B dpm-getspacemd
[
.BI --token_desc " u_token"
] [
.B --si
]
.LP
.B dpm-getspacemd
.B --help
.SH DESCRIPTION
.B dpm-getspacemd
gets the space metadata for a given space token or a space token description.
If none of the options is specified, the metadata for all the space tokens
accessible by the user will be listed.
For each token, it displays three lines. The first one gives the space token,
the user description and the poolname associated with it.
.LP
The second one gives the space usage restriction, either a DN or a list of VOMS
FQANs.
.LP
The third one gives the size of the space, the amount of free space, the
remaining space lifetime, the retention policy and the access latency.
.TP
.I s_token
specifies the space token returned by a previous reservespace request.
.TP
.I u_token
specifies the user provided description associated with a previous reservespace
request.
.SH OPTIONS
.TP
.B --si
use powers of 1000 not 1024 for sizes.
.SH EXAMPLE
.nf
.ft CW
	setenv DPM_HOST dpmhost
	setenv DPNS_HOST dpnshost
.sp
	dpm-getspacemd
.sp
	fe869590-b771-4002-b11a-8e7430d72911 myspace pool1
		dteam
		10.00G 9.00G 24.0h REPLICA ONLINE
	80e35f8b-7e4e-49a9-90b0-5d5a7ce7e8bc gilbert_1 pool1
		dteam
		146.48k 127.01k 1.1m REPLICA ONLINE
	023a5ec1-03ea-464c-9af7-5246183ff5c5 gilbert_2 pool1
		dteam
		146.48k 127.01k 1.1m REPLICA ONLINE
.sp
	dpm-getspacemd --token_desc myspace
.sp
	fe869590-b771-4002-b11a-8e7430d72911 myspace pool1
		dteam
		10.00G 9.00G 24.0h REPLICA ONLINE
.sp
	dpm-getspacemd --space_token fe869590-b771-4002-b11a-8e7430d72911
.sp
	fe869590-b771-4002-b11a-8e7430d72911 myspace pool1
		dteam
		10.00G 9.00G 24.0h REPLICA ONLINE
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR dpm(1) ,
.B dpm-reservespace(1)
