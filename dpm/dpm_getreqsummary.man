.\" @(#)$RCSfile: dpm_getreqsummary.man,v $ $Revision: 1.1.1.1 $ $Date: 2004/12/15 08:04:58 $ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2004 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH DPM_GETREQSUMMARY 3 "$Date: 2004/12/15 08:04:58 $" LCG "DPM Library Functions"
.SH NAME
dpm_getreqsummary \- get summary for a set of requests
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "dpm_api.h"\fR
.sp
.BI "int dpm_getreqsummary (int " nbtokens ,
.BI "char **" r_tokens ,
.BI "int *" nbreplies ,
.BI "struct dpm_reqsummary **" summaries )
.SH DESCRIPTION
.B dpm_getreqsummary
gets summary for a set of requests.
.LP
The input arguments are:
.TP
.I nbtokens
specifies the number of tokens in the request.
.TP
.I r_tokens
specifies an array of tokens returned by previous get/put/copy requests.
.LP
The output arguments are:
.TP
.I nbreplies
will be set to the number of replies in the array of request summaries.
.TP
.I summaries
will be set to the address of an array of dpm_reqsummary structures allocated
by the API. The client application is responsible for freeing the array when not
needed anymore.
.PP
.nf
.ft CW
struct dpm_reqsummary {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	char		r_type;
	int		nb_reqfiles;
	int		nb_queued;
	int		nb_finished;
	int		nb_progress;
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I r_tokens ,
.I nbreplies
or
.I summaries
is a NULL pointer.
.TP
.B ENOMEM
Memory could not be allocated for storing the reply.
.TP
.B EINVAL
.I nbtokens
is not strictly positive or the specified tokens are invalid/unknown.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SEINTERNAL
Database error.
.TP
.B SECOMERR
Communication error.
