/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dpmlogit.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <stdarg.h>
#include <syslog.h>
#include "Clogit.h"

dpmlogit(char *func, char *msg, ...)
{
	va_list args;

	va_start (args, msg);
	Cvlogit (LOG_INFO, func, msg, args);
	va_end (args);
	return (0);
}
