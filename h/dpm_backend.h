/*
 * $Id: dpm_backend.h,v 1.1 2008/01/14 10:06:10 szamsu Exp $
 */

/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpm_backend.h,v $ $Revision: 1.1 $ $Date: 2008/01/14 10:06:10 $ CERN IT-GD/ITR Jean-Philippe Baud
 */

#ifndef _DPM_BACKEND_H
#define _DPM_BACKEND_H

			/* DPM backend server constants */

#define	CHKCONFIGINT	600
#define	DPM_MAXWORKERS	20

			/* DPM backend server structures */

struct dpm_backend_req {
	int		r_ordinal;
	char		r_token[CA_MAXDPMTOKENLEN+1];
	uid_t		r_uid;
	gid_t		r_gid;
	char		client_dn[256];
	char		groups[256];
	char		clienthost[CA_MAXHOSTNAMELEN+1];
	time_t		retrytime;
	time_t		lifetime;
	char		s_token[CA_MAXDPMTOKENLEN+1];
	char		ret_policy;
	char		f_type;
	int		nbreqfiles;
	time_t		ctime;
	int		status;
};
#endif
