#ifndef LCGDM_DBPOOL_C_H
#define LCGDM_DBPOOL_C_H


void LcgdmDbPool_AddDbfd(void *dbfd);
void *LcgdmDbPool_GetDbfd();

#endif
