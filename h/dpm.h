/*
 * $Id$
 */

/*
 * Copyright (C) 2004-2010 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpm.h,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _DPM_H
#define _DPM_H

			/* Disk pool manager constants */

#include "dpm_constants.h"
#define DPM_MAGIC	0x04100D01
#define DPM_MAGIC2	0x04100D02
#define DPM_MAGIC3	0x04100D03
#define DPM_TIMEOUT	5	/* netread timeout while receiving a request */
#define DEFAULT_RETRYCNT     38880   /* Default retry count 38880*60 = 1 week  */
#define DEFAULT_CONNTIMEOUT  (2*60)  /* Default connect timeout limit = 2mins. */
#define	MAXRETRY 5
#define	RETRYI	60
#define LOGBUFSZ 1024
#define PRTBUFSZ  180
#define REPBUFSZ 4100	/* must be >= max disk pool manager reply size */
#define REQBUFSZ 2854	/* must be >= max disk pool manager request size */

			/* Disk pool manager request types */

#define DPM_ADDPOOL	 0
#define DPM_GETPOOLS	 1
#define DPM_MODPOOL	 2
#define DPM_RMPOOL	 3
#define DPM_ADDFS	 4
#define DPM_GETPOOLFS	 5
#define DPM_MODFS	 6
#define DPM_RMFS	 7
#define DPM_RSVSPACE	 8
#define DPM_RLSSPACE	 9
#define DPM_UPDSPACE	10
#define DPM_COMPSPACE	11
#define DPM_GETSPACEMD	12
#define DPM_CHSPACETYPE	13
#define DPM_GETSPACETKN	14
#define DPM_RM		15
#define DPM_GET		16
#define DPM_PUT		17
#define DPM_COPY	18
#define DPM_RELFILES	19
#define DPM_REMFILES	20
#define DPM_PUTDONE	21
#define DPM_ABORTREQ	22
#define DPM_ABORTFILES	23
#define DPM_GETSTSGET	24
#define DPM_GETSTSPUT	25
#define DPM_GETSTSCOPY	26
#define DPM_GETREQSUM	27
#define DPM_EXTENDLIFE	28
#define DPM_GETREQID	29
#define DPM_GETPROTO	30
#define DPM_SHUTDOWN	31
#define DPM_INCREQCTR	32
#define DPM_DELREPLICA	33
#define DPM_PING	34
#define DPM_UPDFILSTS	35
#define DPM_ACCESSR	36
#define DPM_PUTX	37

			/* Disk pool manager reply types */

#define	MSG_ERR		1
#define	MSG_DATA	2
#define	DPM_RC		3
#define	DPM_IRC		4
#define	MSG_GET		5
#define	MSG_PUT		6
#define	MSG_COPY	7
#define	MSG_SURLST	8
#define	MSG_FS		9
#define	MSG_POOL	10
#define	MSG_SUMMARY	11
#define	MSG_REQIDS	12
#define	MSG_SPCMD	13
#define	MSG_SPCTKN	14

			/* Disk pool manager messages */

#define DP000	"DP000 - disk pool manager not running on %s\n"
#define	DP002	"DP002 - %s error : %s\n"
#define DP003   "DP003 - illegal function %d\n"
#define DP004   "DP004 - error getting request, netread = %d\n"
#define	DP009	"DP009 - fatal configuration error: %s %s\n"
#define	DP023	"DP023 - %s is not accessible\n"
#define DP046	"DP046 - request too large (max. %d)\n"
#if defined(_WIN32)
#define	DP052	"DP052 - WSAStartup unsuccessful\n"
#define	DP053	"DP053 - you are not registered in the unix group/passwd mapping file\n"
#endif
#define	DP092	"DP092 - %s request by %s (%d,%s) from %s\n"
#define	DP098	"DP098 - %s\n"
#endif
