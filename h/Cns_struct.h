/*
 * $Id$
 */

/*
 * Copyright (C) 2003-2007 by CERN/IT/ADC/CA
 * All rights reserved
 */

/*
 * @(#)$RCSfile: Cns_struct.h,v $ $Revision$ $Date$ CERN IT-ADC/CA Jean-Philippe Baud
 */

#ifndef _CNS_STRUCT_H
#define _CNS_STRUCT_H

			/* structures common to Name server client and server */

struct Cns_acl {
	unsigned char	a_type;
	int		a_id;
	unsigned char	a_perm;
};

struct Cns_groupinfo {
	gid_t		gid;
	char		groupname[256];
	int		banned;
};

struct Cns_userinfo {
	uid_t		userid;
	char		username[256];
	char		user_ca[256];
	int		banned;
};
#endif
