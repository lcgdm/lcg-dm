/*
 * $Id: dpm_util.h,v 1.5 2009/11/13 10:03:12 dhsmith Exp $
 */

/*
 * Copyright (C) 2004-2007 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpm_util.h,v $ $Revision: 1.5 $ $Date: 2009/11/13 10:03:12 $ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _DPM_UTIL_H
#define _DPM_UTIL_H
int build_proxy_filename(char *, char *);
int get_req_retention_time(time_t *);
int is_redirect_to_headnode_with_sfn(const char *proto);
char *get_redirect_target(const char *proto);
int get_supported_protocols (char ***);
int is_surl_local (char *surl);
char *pfnfromturl (char *turl);
char *sfnfromsurl (char *surl);
char *status2str (int status, char *buf);
const char *csumtype2srmname (const char *csumtype);
char *pfr_to_turl(struct dpm_dbfd *dbfd, struct dpm_put_filereq *pfr_entry, char *turl, size_t bufsize);
char *gfr_to_turl(struct dpm_dbfd *dbfd, struct dpm_get_filereq *gfr_entry, char *turl, size_t bufsize);
#endif
