/*
 * Copyright (C) 1999-2008 by CERN/IT/PDP/DM
 * All rights reserved
 */

/*
 * $RCSfile: Cnetdb.h,v $ $Revision: 1.4 $ $Date: 2008/09/24 11:25:00 $ CERN IT-PDP/DM Olof Barring
 */


#ifndef _CNETDB_H
#define _CNETDB_H

#include <osdep.h>
#if defined(_WIN32)
#include <ws2tcpip.h>
#else
#include <netdb.h>
#endif

#if defined(_REENTRANT) || defined(_THREAD_SAFE)
#if defined(hpux) || defined(HPUX10) || defined(sgi) || defined(SOLARIS)
EXTERN_C int DLL_DECL *C__h_errno _PROTO((void));
#define h_errno (*C__h_errno())
#endif /* hpux || HPUX10 || sgi || SOLARIS */
#endif /* _REENTRANT || _THREAD_SAFE */

#define CNA_WANTLOOPBACK 0x1
#define CNA_NOFWDLOOKUP  0x2
#define CNA_FWDLOOKUP    0x4

EXTERN_C struct hostent DLL_DECL *Cgethostbyname _PROTO((CONST char *));
EXTERN_C struct hostent DLL_DECL *Cgethostbyaddr _PROTO((CONST void *, size_t, int));
EXTERN_C struct servent DLL_DECL *Cgetservbyname _PROTO((CONST char *, CONST char *));

EXTERN_C int DLL_DECL Cgetnameinfo
	_PROTO((CONST void *, size_t, char *, size_t, char *, size_t, int));

EXTERN_C int DLL_DECL Cgetaddrinfo
	_PROTO((CONST char *, CONST char *, CONST struct addrinfo *, struct addrinfo **));

EXTERN_C char DLL_DECL *Cgetnetaddress
        _PROTO((int, CONST void *, size_t, int *, CONST char **, CONST char **, int, int));

EXTERN_C CONST char DLL_DECL *Cgai_strerror _PROTO((int));

#endif /* _CNETDB_H */
