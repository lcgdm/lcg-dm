/*
 * Copyright (C) 2004-2007 by CERN
 * All rights reserved
 */

/*
 * @(#)$RCSfile: srm_server.h,v $ $Revision: 1.7 $ $Date: 2008/01/15 10:24:23 $ CERN Jean-Philippe Baud
 */

#ifndef _SRM_SERVER_H
#define _SRM_SERVER_H
#ifdef USE_MYSQL
#include <mysql.h>
#endif
#include "stdsoap2.h"

			/* srm server constants */

#define SRM_TIMEOUT 300 /* network timeout while receiving a request */
#define BACKLOG (100)   /* Max. request backlog */
#define LOGBUFSZ 1024
#define PRTBUFSZ 180
#define SRMV1_NBTHREADS 20
#define SRMV1_PORT 8443
#define SRMV2_NBTHREADS 20
#define SRMV2_PORT 8444
#define SRMV2_2_PORT 8446
#define RETURNSC(x,c) \
	{ \
	dpmlogit (func, "returns %d, statusCode=%s\n", (x), soap_ns1__TStatusCode2s(soap, c)); \
	if (thip->dbfd.tr_started) \
		if (x) \
			(void) dpm_abort_tr (&thip->dbfd); \
		else \
			(void) dpm_end_tr (&thip->dbfd); \
	return ((x)); \
	}

			/* srm server structures */

#define srm_srv_thread_info dpm_srv_thread_info

			/* srm server function prototypes */

EXTERN_C int get_client_full_id _PROTO((struct soap *, char *, char **, char ***, int *, uid_t *, gid_t *, int *, gid_t **));

			/* srm server exit codes */

#define USERR	1	/* user error */
#define SYERR	2	/* system error */
#define CONFERR	4	/* configuration error */

			/* srm server messages */

#define	SRM02	"SRM02 - %s error : %s\n"
#define	SRM09	"SRM09 - fatal configuration error: %s %s\n"
#define	SRM23	"SRM23 - %s is not accessible\n"
#define	SRM98	"SRM98 - %s\n"
#endif
