/*
 * $Id: dpm_dicom.h,v 1.2 2008/04/01 06:53:15 baud Exp $
 */

/*
 * Copyright (C) 2007 by CERN/IT/GD/ITR
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpm_dicom.h,v $ $Revision: 1.2 $ $Date: 2008/04/01 06:53:15 $ CERN IT-GD/ITR Jean-Philippe Baud
 */

#ifndef _DPM_DICOM_H
#define _DPM_DICOM_H

#include "dpm_server.h"
#include "osdep.h"

			/* DPM DICOM server structures */

struct dpm_dicom_filereq {
	char		r_token[CA_MAXDPMTOKENLEN+1];
	int		f_ordinal;
	char		surl[CA_MAXSFNLEN+1];
	char		dicom_fn[256];
	int		status;
};

                        /* DPM DICOM server function prototypes */

EXTERN_C int dpm_delete_dfr_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *));
EXTERN_C int dpm_delete_dicomreq_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *));
EXTERN_C int dpm_get_dfr_by_fullid _PROTO((struct dpm_dbfd *, char *, int, struct dpm_dicom_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_dfr_by_surl _PROTO((struct dpm_dbfd *, char *, char *, struct dpm_dicom_filereq *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_dicomreq_by_token _PROTO((struct dpm_dbfd *, char *, struct dpm_backend_req *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_get_next_dicomreq _PROTO((struct dpm_dbfd *, int, struct dpm_backend_req *, int, dpm_dbrec_addr *));
EXTERN_C int dpm_insert_dfr_entry _PROTO((struct dpm_dbfd *, struct dpm_dicom_filereq *));
EXTERN_C int dpm_insert_dicomreq_entry _PROTO((struct dpm_dbfd *, struct dpm_backend_req *));
EXTERN_C int dpm_list_dfr_by_surl _PROTO((struct dpm_dbfd *, int, char *, struct dpm_dicom_filereq *, int, dpm_dbrec_addr *, int, DBLISTPTR *));
EXTERN_C int dpm_list_pending_dicomreq _PROTO((struct dpm_dbfd *, int, struct dpm_backend_req *, int, DBLISTPTR *));
EXTERN_C int dpm_update_dfr_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_dicom_filereq *));
EXTERN_C int dpm_update_dicomreq_entry _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_backend_req *));
#endif
