/*
 * $Id$
 */

/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpm_constants.h,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
 */

#ifndef _DPM_CONSTANTS_H
#define _DPM_CONSTANTS_H
#include "Castor_limits.h"

			/* Disk pool manager constants */

#define DPM_PORT	5015
#define DEFAULT_LIFETIME	(7 * 86400)
#define DEFAULT_PINTIME	7200
#define DEFAULT_SPACE_TYPE	'V'
#define DEFAULT_WEIGHT	1
#define DEFAULT_FSS_POLICY	"maxfreespace"
#define DEFAULT_GC_POLICY	"lru"
#define DEFAULT_MIG_POLICY	"none"
#define DEFAULT_RS_POLICY	"fifo"
#define MAX_LIFETIME	(30 * 86400)
#define MAX_PINTIME	(12 * 3600)

			/* Filesystem statuses */

#define FS_DISABLED	1
#define FS_RDONLY	2

			/* File/Request statuses */

#define DPM_SUCCESS	00000
#define DPM_QUEUED	0x1000
#define DPM_ACTIVE	0x2000
#define DPM_READY	0x3000
#define DPM_RUNNING	0x4000
#define DPM_DONE	0x5000
#define DPM_FAILED	0x6000
#define DPM_ABORTED	0x7000
#define DPM_EXPIRED	0x8000
#define DPM_RELEASED	0x9000
#define DPM_TO_BE_MIGRATED	0xA000
#define DPM_TO_BE_RECALLED	0xB000
#define DPM_DICOM_RECALL	0xB001
#define DPM_QUEUED4COPY	(DPM_QUEUED|1)

			/* Long options for Disk pool manager utilities */

#define OPT_FS		1
#define OPT_FS_SERVER	2
#define OPT_GC_POLICY	3
#define OPT_POOL_GID	4
#define OPT_POOL_GROUP	5
#define OPT_POOL_NAME	6
#define OPT_FSS_POLICY	7
#define OPT_RS_POLICY	8
#define OPT_S_TYPE	9
#define OPT_STATUS	10
#define OPT_DEF_PTIME	11
#define OPT_PUT_RETENP	12
#define OPT_GC_START	13
#define OPT_GC_STOP	14
#define OPT_MIG_POLICY	15
#define OPT_RET_POLICY	16
#define OPT_AC_LATENCY	17
#define OPT_GSPACE_SZ	18
#define OPT_LIFETIME	19
#define OPT_U_DESC	20
#define OPT_DEF_LTIME	21
#define OPT_MAX_LTIME	22
#define OPT_MAX_PTIME	23
#define OPT_S_TOKEN	24
#define OPT_DEF_FSZ	25
#define OPT_DRAIN_SZ	26
#define OPT_F_TYPE	27
#define OPT_NBTHREADS	28
#define OPT_WEIGHT	29

			/* Disk pool manager utilities exit codes */

#define USERR	1	/* user error */
#define SYERR	2	/* system error */
#define CONFERR	4	/* configuration error */
#endif
