/*
 * $RCSfile: rfio_server.h $ $Revision$ $Date$ CERN IT-GS Jean-Philippe Baud
 */

/*
 * Copyright (C) 2009-2010 by CERN IT-GS
 * All rights reserved
 */

/*
 * rfio_server.h - Remote File I/O server definitions
 */

#ifndef _RFIO_SERVER_H
#define _RFIO_SERVER_H

			/* Remote File I/O server constants */

/* If daemonv3_rdmt is true, reading from disk will be multithreaded
   The circular buffer will have in this case daemonv3_rdmt_nbuf buffers of
   size daemonv3_rdmt_bufsize. Defaults values are defined below */
#define DAEMONV3_RDMT (1)
#define DAEMONV3_RDMT_NBUF (4) 
#define DAEMONV3_RDMT_BUFSIZE (256*1024)

/* If daemonv3_wrmt is true, reading from disk will be multithreaded
   The circular buffer will have in this case daemonv3_wrmt_nbuf buffers of
   size daemonv3_wrmt_bufsize. Defaults values are defined below */
#define DAEMONV3_WRMT (1)
#define DAEMONV3_WRMT_NBUF (4) 
#define DAEMONV3_WRMT_BUFSIZE (256*1024)

#if defined(_WIN32)
#define ECONNRESET WSAECONNRESET
#endif  /* WIN32 */

			/* Remote File I/O server structures */


			/* Remote File I/O server function prototypes */

EXTERN_C int rfio_alrm _PROTO((int, char *));
EXTERN_C void rfioacct _PROTO((int, uid_t, gid_t, int, int, int, int, int, struct rfiostat *, char *, char *));
#endif
