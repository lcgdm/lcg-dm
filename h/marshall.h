/*
 * $Id: marshall.h,v 1.1.1.1 2004/01/21 09:19:34 baud Exp $
 */

/*
 * @(#)$RCSfile: marshall.h,v $ $Revision: 1.1.1.1 $ $Date: 2004/01/21 09:19:34 $ CERN IT-PDP/DM  Fabrizio Cane
 */

/*
 * Copyright (C) 1990-2002 by CERN/IT/PDP/DM
 * All rights reserved
 */

/*  marshall.h   -   marshalling/unmarshalling definitions */

#ifndef _MARSHALL_H_INCLUDED_
#define _MARSHALL_H_INCLUDED_

#include <osdep.h>              /* Operating system dependencies        */
#include <memory.h>             /* memory operations definition         */
#include <time.h>
#include <arpa/inet.h>

#define SHORT			WORD
/* #define U_SHORT			U_WORD */
#define SHORTSIZE		WORDSIZE
#define SHORTADDR		WORDADDR

#define marshall_WORD		marshall_SHORT
#define unmarshall_WORD		unmarshall_SHORT		
#define unmarshall_NWORD		unmarshall_NSHORT

#define INC_PTR(ptr,n)		(ptr) = (char*)(ptr) + (n)
#define DIFF_PTR(ptr,base)      (char*)(ptr) - (char*)(base)

/*
 * BIT manipulation
 */

#define  BITSOFBYTE     8       /* number of bits in a byte             */

#define  bitsof(t)      sizeof(t)*BITSOFBYTE /* number of bits in a type*/

typedef  char*          bitvct; /* bit vector type definition           */

/*
 * Allocate enough memory for a 'bitvct' type variable containing
 * 'size' bits
 */

#define  bitalloc(size)		(bitvct)malloc(size/BITSOFBYTE + \
				((size%BITSOFBYTE) ? 1 : 0))

/*
 *  Set the bit 'bit-th' starting from the byte pointed to by 'ptr'
 */

#define  BIT_SET(ptr,bit)	{ char *p = (char*)(ptr) + (bit)/8; \
				  *p = *p | (1 << (7-(bit)%8)) ; \
				}

/*
 *  Clear the bit 'bit-th' starting from the byte pointed to by 'ptr'
 */

#define  BIT_CLR(ptr,bit)	{ char *p = (char*)(ptr) + (bit)/8; \
				  *p = *p & ~(1 << (7-(bit)%8)); \
				}

/*
 *  Test the bit 'bit-th' starting from the byte pointed to by 'ptr'
 */

#define  BIT_ISSET(ptr,bit)	(*(char*)((char*)(ptr)+(bit)/8) & (1 << (7-(bit)%8)))


/*
 *    B Y T E
 */

#define marshall_BYTE(ptr,n)	{ BYTE n_ = n; \
				  (void) memcpy((ptr),BYTEADDR(n_),BYTESIZE); \
				  INC_PTR(ptr,BYTESIZE); \
				} 

#define unmarshall_BYTE(ptr,n)  { BYTE n_; \
				  (void) memcpy(BYTEADDR(n_),(ptr),BYTESIZE); \
				  n = n_; \
				  INC_PTR(ptr,BYTESIZE); \
				} 

static inline BYTE _unmarshall_NBYTE(ptr, end)
char **ptr;
char *end;
{
	BYTE n;
	if (*ptr > end)
		return (0);
	if (end - *ptr < BYTESIZE) {
		*ptr = NULL;
		return (0);
	}
	unmarshall_BYTE(*ptr,n);
	return (n);
}
#define unmarshall_NBYTE(ptr,end,n) ((n)=_unmarshall_NBYTE(&(ptr),end),\
                                     ((ptr)==NULL)?((ptr)=(end),-1):(((ptr)>(end))?-1:0))

/*
 *    S H O R T
 */

#define marshall_SHORT(ptr,n)	{ SHORT n_ = htons((unsigned short)(n)); \
				  (void) memcpy((ptr),SHORTADDR(n_),SHORTSIZE); \
				  INC_PTR(ptr,SHORTSIZE); \
				}
                                                                 
#define unmarshall_SHORT(ptr,n)	{ SHORT n_ = 0;  \
				  (void) memcpy(SHORTADDR(n_),(ptr),SHORTSIZE); \
                                  n = ntohs((unsigned short)(n_)); \
				  if ( BIT_ISSET(ptr,0) && sizeof(SHORT)-SHORTSIZE > 0 ) \
					 (void) memset((char *)&n,255,sizeof(SHORT)-SHORTSIZE); \
				  INC_PTR(ptr,SHORTSIZE); \
				}

static inline SHORT _unmarshall_NSHORT(ptr, end)
char **ptr;
char *end;
{
	SHORT n;
	if (*ptr > end)
		return (0);
	if (end - *ptr < SHORTSIZE) {
		*ptr = NULL;
		return (0);
	}
	unmarshall_SHORT(*ptr,n);
	return (n);
}
#define unmarshall_NSHORT(ptr,end,n) ((n)=_unmarshall_NSHORT(&(ptr),end),\
                                      ((ptr)==NULL)?((ptr)=(end),-1):(((ptr)>(end))?-1:0))

/*
 *    L O N G
 */

#define marshall_LONG(ptr,n)	{ LONG n_ = htonl((unsigned long)(n)); \
				  (void) memcpy((ptr),LONGADDR(n_),LONGSIZE); \
				  INC_PTR(ptr,LONGSIZE); \
				}
                                                                 
#define unmarshall_LONG(ptr,n)	{ LONG n_ = 0;  \
				  (void) memcpy(LONGADDR(n_),(ptr),LONGSIZE); \
                                  n = ntohl((unsigned long)(n_)); \
				  if ( BIT_ISSET(ptr,0) && sizeof(LONG)-LONGSIZE > 0 ) \
					 (void) memset((char *)&n,255,sizeof(LONG)-LONGSIZE); \
				  INC_PTR(ptr,LONGSIZE); \
				}

static inline LONG _unmarshall_NLONG(ptr, end)
char **ptr;
char *end;
{
	LONG n;
	if (*ptr > end)
		return (0);
	if (end - *ptr < LONGSIZE) {
		*ptr = NULL;
		return (0);
	}
	unmarshall_LONG(*ptr,n);
	return (n);
}
#define unmarshall_NLONG(ptr,end,n) ((n)=_unmarshall_NLONG(&(ptr),end),\
                                     ((ptr)==NULL)?((ptr)=(end),-1):(((ptr)>(end))?-1:0))

/*
 *    S T R I N G
 */

#define  marshall_STRING(ptr,str)       {  (void) strcpy((char*)(ptr),(char*)(str)); \
					   INC_PTR(ptr,strlen(str)+1); \
					}

#define  unmarshall_STRING(ptr,str)     { (void) strcpy((char*)(str),(char*)(ptr)); \
					  INC_PTR(ptr,strlen(str)+1); \
					}

EXTERN_C int DLL_DECL _unmarshall_STRINGN _PROTO((char **, char*, int));
EXTERN_C int DLL_DECL _unmarshall_NSTRINGN _PROTO((char **, char*, char *, int));
#define  unmarshall_STRINGN(ptr,str,n)  _unmarshall_STRINGN(&ptr, str, n)
#define  unmarshall_NSTRINGN(ptr,end,str,n)  _unmarshall_NSTRINGN(&ptr, end, str, n)

/*
 *    H Y P E R   ( 6 4   B I T S )
 */

#if !defined(__alpha) && !defined(i386) && !defined(_WIN32) && !defined(__ia64__) && !defined(__x86_64)
#define  marshall_HYPER(ptr,n)          { U_HYPER u_ = n; \
					  LONG n_ = htonl(*((unsigned long *)&(u_))); \
					  (void) memcpy((ptr),LONGADDR(n_),LONGSIZE); \
					  INC_PTR(ptr,LONGSIZE); \
					  n_ = htonl(*((unsigned long *)((char *)&(u_)+LONGSIZE))); \
					  (void) memcpy((ptr),LONGADDR(n_),LONGSIZE); \
					  INC_PTR(ptr,LONGSIZE); \
					}

#define  unmarshall_HYPER(ptr,n)        { U_HYPER u_; \
					  LONG n_ = 0;  \
					  (void) memcpy(LONGADDR(n_),(ptr),LONGSIZE); \
					  *((LONG *)&(u_)) = ntohl((unsigned long)(n_)); \
					  INC_PTR(ptr,LONGSIZE); \
					  n_ = 0;  \
					  (void) memcpy(LONGADDR(n_),(ptr),LONGSIZE); \
					  *((LONG *)((char *)&(u_)+LONGSIZE)) = \
						ntohl((unsigned long)(n_)); \
					  INC_PTR(ptr,LONGSIZE); \
					  n = u_; \
					}
#else
#define  marshall_HYPER(ptr,n)          { U_HYPER u_ = n; \
					  LONG n_ = htonl(*((U_LONG *)((char *)&(u_)+LONGSIZE))); \
					  (void) memcpy((ptr),LONGADDR(n_),LONGSIZE); \
					  INC_PTR(ptr,LONGSIZE); \
					  n_ = htonl(*((U_LONG *)&(u_))); \
					  (void) memcpy((ptr),LONGADDR(n_),LONGSIZE); \
					  INC_PTR(ptr,LONGSIZE); \
					}

#define  unmarshall_HYPER(ptr,n)        { U_HYPER u_; \
					  LONG n_ = 0;  \
					  (void) memcpy(LONGADDR(n_),(ptr),LONGSIZE); \
					  *((LONG *)((char *)&(u_)+LONGSIZE)) = \
						ntohl((U_LONG)(n_)); \
					  INC_PTR(ptr,LONGSIZE); \
					  n_ = 0;  \
					  (void) memcpy(LONGADDR(n_),(ptr),LONGSIZE); \
					  *((LONG *)&(u_)) = ntohl((U_LONG)(n_)); \
					  INC_PTR(ptr,LONGSIZE); \
					  n = u_; \
					}
#endif

static inline U_HYPER _unmarshall_NHYPER(ptr, end)
char **ptr;
char *end;
{
	U_HYPER n;
	if (*ptr > end)
		return (0);
	if (end - *ptr < 2*LONGSIZE) {
		*ptr = NULL;
		return (0);
	}
	unmarshall_HYPER(*ptr,n);
	return (n);
}
#define unmarshall_NHYPER(ptr,end,n) ((n)=_unmarshall_NHYPER(&(ptr),end),\
                                      ((ptr)==NULL)?((ptr)=(end),-1):(((ptr)>(end))?-1:0))

/*
 *    O P A Q U E
 */

#define marshall_OPAQUE(ptr,raw,n)	{ (void) memcpy((ptr),(raw),(n)); \
					  INC_PTR(ptr,(n)); \
					}

#define unmarshall_OPAQUE(ptr,raw,n)	{ (void) memcpy((raw),(ptr),(n)); \
					  INC_PTR(ptr,(n)); \
					}

/*
 *    T I M E
 */

#define  marshall_TIME_T(ptr,n)       {  \
					   TIME_T _marshall_time_dummy = (TIME_T) n; \
					   marshall_HYPER(ptr,_marshall_time_dummy); \
					}

#define  unmarshall_TIME_T(ptr,n)     { \
					   TIME_T _unmarshall_time_dummy; \
					   unmarshall_HYPER(ptr,_unmarshall_time_dummy); \
					   n = (time_t) _unmarshall_time_dummy; \
					}

static inline TIME_T _unmarshall_NTIME_T(ptr, end)
char **ptr;
char *end;
{ 
	TIME_T n;
	if (*ptr > end)
		return (0);
	if (end - *ptr < 2*LONGSIZE) {
		*ptr = NULL;
		return (0);
	}
	unmarshall_TIME_T(*ptr,n);
	return (n);
}
#define unmarshall_NTIME_T(ptr,end,n) ((n)=_unmarshall_NTIME_T(&(ptr),end),\
                                       ((ptr)==NULL)?((ptr)=(end),-1):(((ptr)>(end))?-1:0))

#endif /* _MARSHALL_H_INCLUDED_ */
