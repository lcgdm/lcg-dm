/*
 * $Id: dpmcopy_server.h,v 1.1 2008/09/24 11:25:00 dhsmith Exp $
 */

/*
 * Copyright (C) 2007-2008 by CERN/IT/GD/ITR
 * All rights reserved
 */

/*
 * @(#)$RCSfile: dpmcopy_server.h,v $ $Revision: 1.1 $ $Date: 2008/09/24 11:25:00 $ CERN IT-GD/ITR Jean-Philippe Baud
 */

#ifndef _DPMCOPY_SERVER_H
#define _DPMCOPY_SERVER_H
#include "osdep.h"

			/* DPM COPY backend server function prototypes */

EXTERN_C int dpmcopy_local _PROTO((struct dpm_dbfd *, struct dpm_req *, struct dpm_copy_filereq *, int));
EXTERN_C int dpmcopy_pull _PROTO((struct dpm_dbfd *, struct dpm_req *, struct dpm_copy_filereq *, int));
EXTERN_C int dpmcopy_push _PROTO((struct dpm_dbfd *, struct dpm_req *, struct dpm_copy_filereq *, int));
EXTERN_C void dpmcopy_set_errstring _PROTO((char *, char *, char *, char *, int));
EXTERN_C int get_srm_endpoint _PROTO((char *, char *, int));
EXTERN_C int srmv2_abortfiles _PROTO((char *, int, char **, int *, struct dpm_filestatus **, int));
EXTERN_C int srmv2_get _PROTO((int, char **, int, char **, int, char **, int *, struct dpm_getfilestatus **));
EXTERN_C int srmv2_getfilesize _PROTO((char *, u_signed64 *, int));
EXTERN_C int srmv2_getstatus_getreq _PROTO((char *, int, char **, char *, int *, struct dpm_getfilestatus **, int));
EXTERN_C int srmv2_getstatus_putreq _PROTO((char *, int, char **, char *, int *, struct dpm_putfilestatus **, int));
EXTERN_C int srmv2_ping _PROTO((char *, int));
EXTERN_C int srmv2_put _PROTO((int, struct dpm_putfilereq *, int, char **, int, int, char **, int *, struct dpm_putfilestatus **));
EXTERN_C int srmv2_putdone _PROTO((char *, int, char **, int *, struct dpm_filestatus **, int));
EXTERN_C int srmv2_relfiles _PROTO((char *, int, char **, int *, struct dpm_filestatus **, int));
EXTERN_C int upd_cpr _PROTO((struct dpm_dbfd *, dpm_dbrec_addr *, struct dpm_copy_filereq *));
EXTERN_C int upd_cpr2 _PROTO((struct dpm_dbfd *, char *, char *, char *, u_signed64, int, char *, time_t, struct dpm_copy_filereq *));
#endif
