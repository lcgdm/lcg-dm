#
#  Copyright (C) 2004 by CERN/IT/GD/CT
#  All rights reserved
#
#       @(#)$RCSfile: oralink.mk,v $ $Revision: 1.1 $ $Date: 2006/12/01 08:03:39 $ CERN IT-GD/CT Jean-Philippe Baud
 
#    Link dpm with Oracle libraries.

ifdef ORACLE_SHARE
include $(ORACLE_SHARE)/demo_proc_ic.mk
else
include $(ORACLE_HOME)/precomp/lib/env_precomp.mk
endif

ifdef ORACLE_LIB
LIBHOME=$(ORACLE_LIB)
endif

PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL) $(STATICTTLIBS)
PROLDLIBS=$(LLIBCLNTSH) $(LLIBCLIENT) $(LLIBSQL)

srmv2.2: $(SRV_OBJS)
	$(CC) -o srmv2.2 $(CLDFLAGS) $(SRV_OBJS) $(LIBS) -L$(LIBHOME) $(PROLDLIBS)
