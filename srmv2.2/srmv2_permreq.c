/*
 * Copyright (C) 2004-2010 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2_permreq.c,v $ $Revision$ $Date$ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <sys/types.h>
#include <Cgrp.h>
#include "Cnetdb.h"
#include <Cpwd.h>
#include "dpm.h"
#include "dpm_server.h"
#include "dpm_util.h"
#include "dpns_api.h"
#include "serrno.h"
#include "srm_server.h"
#include "srmv2H.h"
extern char *decode_group(gid_t, gid_t *, char *);
extern char *decode_user(uid_t, uid_t *, char *);
static enum ns1__TPermissionMode f_modes[] = {NONE, X, W, WX, R, RX, RW, RWX};
static int cvt_entries(struct ns1__srmSetPermissionRequest *, struct Cns_acl *);
static int na_key = -1;

/*			Permission Functions				*/

int
ns1__srmSetPermission (struct soap *soap, struct ns1__srmSetPermissionRequest *req, struct ns1__srmSetPermissionResponse_ *rep)
{
	struct Cns_acl *acl;
	struct Cns_acl *aclp;
	char clientdn[256];
	const char *clienthost;
	int found;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	struct Cns_acl inpacl[CA_MAXACLENTRIES];
	int j;
	int k;
	int nb_inp_entries;
	int nb_tmp_entries;
	int nbfqans;
	int nbgids;
	int nentries;
	int nocheckid;
	int ng = 0;
	int nm = 0;
	int nu = 0;
	struct ns1__srmSetPermissionResponse *repp;
	char *sfn;
	struct Cns_acl *taclp;
	struct srm_srv_thread_info *thip = soap->user;
	struct Cns_acl tmpacl[CA_MAXACLENTRIES];
	uid_t uid;
	char *voname;

	strcpy (func, "SetPermission");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmSetPermissionResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmSetPermissionResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}

	if (! req->SURL) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "SURL is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}
	if ((sfn = sfnfromsurl (req->SURL)) == NULL) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREPATH;
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREPATH);
	}
	if ((nb_inp_entries = cvt_entries (req, inpacl)) <= 0) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	if (req->permissionType == ADD || req->permissionType == REMOVE ||
	    req->permissionType == CHANGE) {
		if ((nb_tmp_entries = Cns_getacl (sfn, CA_MAXACLENTRIES, tmpacl)) < 0) {
			if (*thip->errbuf) {
				thip->errbuf[strlen(thip->errbuf)-1] = '\0';
				repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
			} else
				repp->returnStatus->explanation = soap_strdup (soap, sstrerror (serrno));
			repp->returnStatus->statusCode = serrno2statuscode (serrno);
			RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
		}
		if (req->permissionType == REMOVE) {
			for (aclp = inpacl, j = 0; j < nb_inp_entries; aclp++, j++) {
				nocheckid = aclp->a_type == CNS_ACL_USER_OBJ ||
					    aclp->a_type == CNS_ACL_GROUP_OBJ ||
					    aclp->a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER_OBJ) ||
					    aclp->a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP_OBJ);
				for (taclp = tmpacl, k = 0; k < nb_tmp_entries; taclp++, k++) {
					if (aclp->a_type == taclp->a_type &&
					    (aclp->a_id == taclp->a_id ||
					    nocheckid)) {
						nb_tmp_entries--;
						if (k < nb_tmp_entries)
							memcpy (taclp, taclp + 1,
							    (nb_tmp_entries - k) * sizeof (struct Cns_acl));
						break;
					}
				}
			}
		} else {
			for (aclp = inpacl, j = 0; j < nb_inp_entries; aclp++, j++) {
				found = 0;
				nocheckid = aclp->a_type == CNS_ACL_USER_OBJ ||
					    aclp->a_type == CNS_ACL_GROUP_OBJ ||
					    aclp->a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER_OBJ) ||
					    aclp->a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP_OBJ);
				for (taclp = tmpacl, k = 0; k < nb_tmp_entries; taclp++, k++) {
					if ((aclp->a_type == taclp->a_type ||
					    (aclp->a_type == CNS_ACL_GROUP &&
					     taclp->a_type == CNS_ACL_GROUP_OBJ)) &&
					    (aclp->a_id == taclp->a_id ||
					    nocheckid)) {
						found++;
						taclp->a_perm = aclp->a_perm;
						break;
					}
				}
				if (! found) {
					if (req->permissionType != ADD) {
						repp->returnStatus->statusCode =
						    SRM_USCOREINVALID_USCOREREQUEST;
						RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
					}
					if (nb_tmp_entries >= CA_MAXACLENTRIES) {
						repp->returnStatus->explanation =
						    soap_strdup (soap, "Too many ACL entries");
						repp->returnStatus->statusCode =
						    SRM_USCOREFAILURE;
						RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
					}
					memcpy (taclp, aclp, sizeof(struct Cns_acl));
					nb_tmp_entries++;
				}
			}
		}
		acl = tmpacl;
		nentries = nb_tmp_entries;
	} else {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		RETURNSC (SOAP_OK, SRM_USCOREINVALID_USCOREREQUEST);
	}

	/* If there is any USER or GROUP entry, there must be a MASK entry.
	 * If there is no mask entry, generate one */

	for (aclp = acl, j = 0; j < nentries; aclp++, j++) {
		if (aclp->a_type == CNS_ACL_USER) nu++;
		else if (aclp->a_type == CNS_ACL_GROUP) ng++;
		else if (aclp->a_type == CNS_ACL_MASK) nm++;
	}
	if ((nu || ng) && nm != 1) {
		aclp->a_type = CNS_ACL_MASK;
		aclp->a_id = 0;
		aclp->a_perm = 07;
		nentries++;
	}

	if (Cns_setacl (sfn, nentries, acl)) {
		repp->returnStatus->explanation = soap_strdup (soap, sstrerror (serrno));
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
	} else
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
}

static int
cvt_entries (struct ns1__srmSetPermissionRequest *req, struct Cns_acl *acl)
{
	int i;
	int nentries = 0;

	if (req->ownerPermission) {
		acl->a_type = CNS_ACL_USER_OBJ;
		acl->a_id = 0;
		acl->a_perm = *req->ownerPermission;
		acl++;
		nentries++;
	}
	if (req->arrayOfUserPermissions) {
		for (i = 0; i < req->arrayOfUserPermissions->__sizeuserPermissionArray; i++) {
			if (! req->arrayOfUserPermissions->userPermissionArray[i])
				continue;
			acl->a_type = CNS_ACL_USER;
			if (! req->arrayOfUserPermissions->userPermissionArray[i]->userID ||
			    (acl->a_id = cvt_user (req->arrayOfUserPermissions->userPermissionArray[i]->userID)) < 0)
				return (-1);
			acl->a_perm = req->arrayOfUserPermissions->userPermissionArray[i]->mode;
			acl++;
			nentries++;
		}
	}
	if (req->arrayOfGroupPermissions) {
		for (i = 0; i < req->arrayOfGroupPermissions->__sizegroupPermissionArray; i++) {
			if (! req->arrayOfGroupPermissions->groupPermissionArray[i])
				continue;
			if (! req->arrayOfGroupPermissions->groupPermissionArray[i]->groupID)
				return (-1);
			if (strcmp (req->arrayOfGroupPermissions->groupPermissionArray[i]->groupID, "-") == 0) {
				acl->a_id = 0;
				acl->a_type = CNS_ACL_GROUP_OBJ;
			} else {
				if ((acl->a_id = cvt_group (req->arrayOfGroupPermissions->groupPermissionArray[i]->groupID)) < 0)
					return (-1);
				acl->a_type = CNS_ACL_GROUP;
			}
			acl->a_perm = req->arrayOfGroupPermissions->groupPermissionArray[i]->mode;
			acl++;
			nentries++;
		}
	}
	if (req->otherPermission) {
		acl->a_type = CNS_ACL_OTHER;
		acl->a_id = 0;
		acl->a_perm = *req->otherPermission;
		acl++;
		nentries++;
	}
	return (nentries);
}

cvt_group (char *p)
{
	gid_t gid;
	struct group *gr;

#ifdef VIRTUAL_ID
	if (strcmp (p, "root") == 0)
		gid = 0;
	else if (Cns_getgrpbynam (p, &gid) < 0) {
#else
	if ((gr = Cgetgrnam (p)))
		gid = gr->gr_gid;
	else {
#endif
		return (-1);
	}
	return (gid);
}

cvt_user (char *p)
{
	struct passwd *pwd;
	uid_t uid;

#ifdef VIRTUAL_ID
	if (strcmp (p, "root") == 0)
		uid = 0;
	else if (Cns_getusrbynam (p, &uid) < 0) {
#else
	if ((pwd = Cgetpwnam (p)))
		uid = pwd->pw_uid;
	else {
#endif
		return (-1);
	}
	return (uid);
}

int
ns1__srmCheckPermission (struct soap *soap, struct ns1__srmCheckPermissionRequest *req, struct ns1__srmCheckPermissionResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int mode;
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbsurls;
	struct ns1__TSURLPermissionReturn *repfilep;
	struct ns1__srmCheckPermissionResponse *repp;
	char *sfn;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "CheckPermission");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmCheckPermissionResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfPermissions = NULL;
	rep->srmCheckPermissionResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}

	if (! req->arrayOfSURLs) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfSiteURLs is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}

	nbsurls = req->arrayOfSURLs->__sizeurlArray;

	/* Allocate the array of file permissions */

	if ((repp->arrayOfPermissions =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTSURLPermissionReturn))) == NULL ||
	    (repp->arrayOfPermissions->surlPermissionArray =
	    soap_malloc (soap, nbsurls * sizeof(struct ns1__TSURLPermissionReturn *))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->arrayOfPermissions->__sizesurlPermissionArray = nbsurls;

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	for (i = 0; i < nbsurls; i++) {
		if ((repp->arrayOfPermissions->surlPermissionArray[i] = repfilep =
		    soap_malloc (soap, sizeof(struct ns1__TSURLPermissionReturn))) == NULL)
			RETURN (SOAP_EOM);
		memset (repfilep, 0, sizeof(struct ns1__TSURLPermissionReturn));
		if ((repp->arrayOfPermissions->surlPermissionArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repfilep->status, 0, sizeof(struct ns1__TReturnStatus));
		repfilep->surl = req->arrayOfSURLs->urlArray[i];
		if (! repfilep->surl) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			repfilep->status->explanation = soap_strdup (soap, "Pointer to SURL is NULL");
			nb_file_err++;
			continue;
		}
		if (strlen (repfilep->surl) > CA_MAXSFNLEN ||
		    (sfn = sfnfromsurl (repfilep->surl)) == NULL) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		}
		mode = 0;
		if (Cns_access (sfn, R_OK) < 0) {
			if (serrno != EACCES) {
				repfilep->status->statusCode = serrno2statuscode (serrno);
				nb_file_err++;
				continue;
			}
		} else
			mode |= 4;
		if (Cns_access (sfn, W_OK) < 0) {
			if (serrno != EACCES) {
				repfilep->status->statusCode = serrno2statuscode (serrno);
				nb_file_err++;
				continue;
			}
		} else
			mode |= 2;
		if (Cns_access (sfn, X_OK) < 0) {
			if (serrno != EACCES) {
				repfilep->status->statusCode = serrno2statuscode (serrno);
				nb_file_err++;
				continue;
			}
		} else
			mode |= 1;
		repfilep->permission = &f_modes[mode];
	}
	if (nb_file_err == 0)
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	else if (nb_file_err != nbsurls)
		repp->returnStatus->statusCode = SRM_USCOREPARTIAL_USCORESUCCESS;
	else
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
	RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
}

int
ns1__srmGetPermission (struct soap *soap, struct ns1__srmGetPermissionRequest *req, struct ns1__srmGetPermissionResponse_ *rep)
{
	struct Cns_acl acl[CA_MAXACLENTRIES];
	struct Cns_acl *aclp;
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int ig;
	int iu;
	int j;
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbsurls;
	int nentries;
	int ng;
	int nu;
	struct ns1__TPermissionReturn *repfilep;
	struct ns1__srmGetPermissionResponse *repp;
	gid_t sav_gid = -1;
	char sav_gidstr[256];
	uid_t sav_uid = -1;
	char sav_uidstr[256];
	char *sfn;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "GetPermission");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmGetPermissionResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfPermissionReturns = NULL;
	rep->srmGetPermissionResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURNSC (SOAP_OK, SRM_USCOREAUTHENTICATION_USCOREFAILURE);
	}

	if (! req->arrayOfSURLs) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfSiteURLs is required");
		RETURNSC (SOAP_OK, SRM_USCOREFAILURE);
	}

	nbsurls = req->arrayOfSURLs->__sizeurlArray;

	/* Allocate the array of file permissions */

	if ((repp->arrayOfPermissionReturns =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTPermissionReturn))) == NULL ||
	    (repp->arrayOfPermissionReturns->permissionArray =
	    soap_malloc (soap, nbsurls * sizeof(struct ns1__TPermissionReturn *))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->arrayOfPermissionReturns->__sizepermissionArray = nbsurls;

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	for (i = 0; i < nbsurls; i++) {
		if ((repp->arrayOfPermissionReturns->permissionArray[i] = repfilep =
		    soap_malloc (soap, sizeof(struct ns1__TPermissionReturn))) == NULL)
			RETURN (SOAP_EOM);
		memset (repfilep, 0, sizeof(struct ns1__TPermissionReturn));
		if ((repp->arrayOfPermissionReturns->permissionArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repfilep->status, 0, sizeof(struct ns1__TReturnStatus));
		repfilep->surl = req->arrayOfSURLs->urlArray[i];
		if (! repfilep->surl) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			repfilep->status->explanation = soap_strdup (soap, "Pointer to SURL is NULL");
			nb_file_err++;
			continue;
		}
		if (strlen (repfilep->surl) > CA_MAXSFNLEN ||
		    (sfn = sfnfromsurl (repfilep->surl)) == NULL) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		}
		if ((nentries = Cns_getacl (sfn, CA_MAXACLENTRIES, acl)) < 0) {
			repfilep->status->statusCode = serrno2statuscode (serrno);
			nb_file_err++;
			continue;
		}
		ng = 0;
		nu = 0;
		for (j = 0, aclp = acl; j < nentries; j++, aclp++) {
			if (aclp->a_type == CNS_ACL_USER) nu++;
			if (aclp->a_type == CNS_ACL_GROUP_OBJ) ng++;
			if (aclp->a_type == CNS_ACL_GROUP) ng++;
		}
		if (nu == 0 || (repfilep->arrayOfUserPermissions =
		      soap_malloc (soap, sizeof(struct ns1__ArrayOfTUserPermission))) == NULL ||
		    (repfilep->arrayOfUserPermissions->userPermissionArray =
		      soap_malloc (soap, nu * sizeof(struct ns1__TUserPermission *))) == NULL)
			repfilep->arrayOfUserPermissions = NULL;
		if ((repfilep->arrayOfGroupPermissions =
		      soap_malloc (soap, sizeof(struct ns1__ArrayOfTGroupPermission))) == NULL ||
		    (repfilep->arrayOfGroupPermissions->groupPermissionArray =
		      soap_malloc (soap, ng * sizeof(struct ns1__TGroupPermission *))) == NULL)
			repfilep->arrayOfGroupPermissions = NULL;
		ig = 0;
		iu = 0;
		for (j = 0, aclp = acl; j < nentries; j++, aclp++) {
			if (aclp->a_type == CNS_ACL_USER_OBJ) {
				repfilep->owner =
				    soap_strdup (soap, decode_user (aclp->a_id, &sav_uid, sav_uidstr));
				repfilep->ownerPermission = &f_modes[aclp->a_perm];
			} else if (aclp->a_type == CNS_ACL_USER) {
				if (repfilep->arrayOfUserPermissions &&
				    (repfilep->arrayOfUserPermissions->userPermissionArray[iu] =
				      soap_malloc (soap, sizeof(struct ns1__TUserPermission))) &&
				    (repfilep->arrayOfUserPermissions->userPermissionArray[iu]->userID =
				      soap_strdup (soap, decode_user (aclp->a_id, &sav_uid, sav_uidstr))))
					repfilep->arrayOfUserPermissions->userPermissionArray[iu++]->mode = aclp->a_perm;
			} else if (aclp->a_type == CNS_ACL_GROUP_OBJ ||
			           aclp->a_type == CNS_ACL_GROUP) {
				if (repfilep->arrayOfGroupPermissions &&
				    (repfilep->arrayOfGroupPermissions->groupPermissionArray[ig] =
				      soap_malloc (soap, sizeof(struct ns1__TGroupPermission))) &&
				    (repfilep->arrayOfGroupPermissions->groupPermissionArray[ig]->groupID =
				      soap_strdup (soap, decode_group (aclp->a_id, &sav_gid, sav_gidstr))))
					repfilep->arrayOfGroupPermissions->groupPermissionArray[ig++]->mode = aclp->a_perm;
			} else if (aclp->a_type == CNS_ACL_OTHER)
				repfilep->otherPermission = &f_modes[aclp->a_perm];
		}
		if (iu)
			repfilep->arrayOfUserPermissions->__sizeuserPermissionArray = iu;
		else
			repfilep->arrayOfUserPermissions = NULL;
		if (ig)
			repfilep->arrayOfGroupPermissions->__sizegroupPermissionArray = ig;
		else
			repfilep->arrayOfGroupPermissions = NULL;
	}
	if (nb_file_err == 0)
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	else if (nb_file_err != nbsurls)
		repp->returnStatus->statusCode = SRM_USCOREPARTIAL_USCORESUCCESS;
	else
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
	RETURNSC (SOAP_OK, repp->returnStatus->statusCode);
}
