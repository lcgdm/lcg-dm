/*
 * Copyright (C) 2004-2010 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srm_util.c,v $ $Revision$ $Date$ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <sys/types.h>
#include "Castor_limits.h"
#include "Cpwd.h"
#include "cgsi_plugin.h"
#include "dpns_api.h"
#include "srm_server.h"

get_client_full_id (struct soap *soap, char *clientdn, char **voname, char ***fqans, int *nbfqan, uid_t *uid, gid_t *gid, int *nbgids, gid_t **gids)
{
	char *user_ca;

	user_ca = get_client_ca (soap);
#ifndef VIRTUAL_ID
	if (get_client_id (soap, uid, gid) < 0)
		return (-1);
	*voname = NULL;
	*fqans = NULL;
	*nbfqan = 0;
	*nbgids = 1;
	*gids = gid;
#else
#ifdef USE_VOMS
	*voname = get_client_voname (soap);
	*fqans = get_client_roles (soap, nbfqan);
#else
	*voname = NULL;
	*fqans = NULL;
	*nbfqan = 0;
#endif
	/* must reset VOMS pointers in Cns client context */
	Cns_client_setAuthorizationId (-1, -1, "GSI", clientdn);

	*nbgids = *nbfqan ? *nbfqan : 1;
	if ((*gids = soap_malloc (soap, *nbgids * sizeof(gid_t))) == NULL)
		return (-1);

	if (*voname && *fqans && *nbfqan)
		Cns_client_setVOMS_data (*voname, *fqans, 1);
	if (Cns_getidmapc (clientdn, user_ca, *nbfqan, (const char **)*fqans,
	    uid, *gids) < 0)
		return (-1);
	*gid = **gids;
	if (*voname && *fqans)
		Cns_client_setVOMS_data (*voname, *fqans, *nbfqan);
	Cns_set_selectsrvr (CNS_SSRV_NOTPATH);
#endif
	return 0;
}

get_client_id (soap, uid, gid)
struct soap *soap;
uid_t *uid;
gid_t *gid;
{
	struct passwd *pw;
	char username[CA_MAXUSRNAMELEN+1];

	if (get_client_username (soap, username, sizeof(username)) < 0)
		return (-1);
	if ((pw = Cgetpwnam (username)) == NULL)
		return (-1);
	*uid = pw->pw_uid;
	*gid = pw->pw_gid;
	return (0);
}

/*	srm_logreq - log a request */

/*	Split the message into lines so they don't exceed LOGBUFSZ-1 characters
 *	A backslash is appended to a line to be continued
 *	A continuation line is prefixed by '+ '
 */
void
srm_logreq(func, logbuf)
char *func;
char *logbuf;
{
	int n1, n2;
	char *p;
	char savechrs1[2];
	char savechrs2[2];

	n1 = LOGBUFSZ - strlen (func) - 36;
	n2 = strlen (logbuf);
	p = logbuf;
	while (n2 > n1) {
		savechrs1[0] = *(p + n1);
		savechrs1[1] = *(p + n1 + 1);
		*(p + n1) = '\\';
		*(p + n1 + 1) = '\0';
		srmlogit (func, SRM98, p);
		if (p != logbuf) {
			*p = savechrs2[0];
			*(p + 1) = savechrs2[1];
		}
		p += n1 - 2;
		savechrs2[0] = *p;
		savechrs2[1] = *(p + 1);
		*p = '+';
		*(p + 1) = ' ';
		*(p + 2) = savechrs1[0];
		*(p + 3) = savechrs1[1];
		n2 -= n1;
	}
	srmlogit (func, SRM98, p);
	if (p != logbuf) {
		*p = savechrs2[0];
		*(p + 1) = savechrs2[1];
	}
}
