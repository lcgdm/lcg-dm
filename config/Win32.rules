/*
 * Copyright (C) 1997-2005 by CERN/IT/PDP/DM
 * All rights reserved
 */

/*
 * @(#)$RCSfile: Win32.rules,v $ $Revision: 1.2 $ $Date: 2005/03/31 14:58:06 $ CERN IT-PDP/DM   Jean-Philippe Baud
 */

.c.obj:
	$(CC) -c $*.c @<<
	$(CFLAGS)
<<

.SUFFIXES: .c .robj
.c.robj:
	$(CC) $(MTCCFLAGS) /Fo$@ -c $*.c @<<
	$(CFLAGS)
<<

#define DepSourceName(dirname,source) ..\ ## dirname\ ## source
#define FileName(dirname,basename) dirname\ ## basename

#define LibraryTargetName(libname) libname.lib

#define NormalLibraryTarget(libname,objects) \
LibraryTargetName(libname): objects			@@\
	$(AR) @<<					@@\
/out:$@ $**						@@\
<<

#define DepLibraryTargetName(dir,libname) ..\ ## dir\ ## LibraryTargetName(libname)
#define SharedLibraryTargetName(libname) libname.dll
#define DepSharedLibraryTargetName(dir,libname) ..\ ## dir\ ## SharedLibraryTargetName(libname)

#define MakeDepLibrary(dir,libname) \
DepLibraryTargetName(dir,libname):	FORCE		@@\
	pushd ..\ ## dir & $(MAKE) LibraryTargetName(libname) & popd

#define ProgramTargetName(target) target.exe

#define NormalProgramTarget(program,objects,deplibs,libs) \
ProgramTargetName(program): objects deplibs		@@\
        $(LD) @<<					@@\
/out:$@ $(LDFLAGS) objects libs				@@\
<<

#define MakeSubdirs(name,dirs) \
name:							@@\
	@for %i in ( dirs ) \				@@\
	do @(echo %i - & cd %i & \			@@\
		$(MAKE) $(MFLAGS) $@ & cd .. )

#define MakeCondSubdirs(name,dirs,cond) \
name: cond							@@\
	@for %i in ( dirs ) \				@@\
	do @(echo %i - & cd %i & \			@@\
		$(MAKE) $(MFLAGS) $@ & cd .. )

#define RemoveFile(file) if exist file $(RM) file

#define RemoveFiles(files) for %i in ( files ) do @RemoveFile(%i)

#define InstallTarget(file,owner,group,mode,dest)	@@\
FileName(dest,file): file				@@\
	@if exist $@ \					@@\
		( RemoveFile(FileName(dest,OLD$?)) & \	@@\
		ren $@ FileName(dest,OLD$?) )		@@\
	copy $? $@

#define InstallLibrary(libname,dest,owner,group,mode)	@@\
InstallTarget(LibraryTargetName(libname),owner,group,mode,dest)

#define InstallSharedLibrary(libname,rev,dest)		@@\
dest\SharedLibraryTargetName(libname): SharedLibraryTargetName(libname)	@@\
	copy $? $@.rev					@@\
	copy $? $@

#define MakeDir(dirname,owner,group,mode)		@@\
dirname:						@@\
	-mkdir $@

