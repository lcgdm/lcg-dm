#
# Copyright (c) Members of the EGEE Collaboration. 2006-2009.
# See http://public.eu-egee.org/partners/ for details on 
# the copyright holders.
# For license conditions see the license file or
# http://www.apache.org/licenses/LICENSE-2.0
#
# Authors: 
#      Akos Frohner <Akos.Frohner@cern.ch>
#      Jean-Philippe Baud <Jean-Philippe.Baud@cern.ch>
#
# Makefile to build all RPMs in one go.

default: all

SHELL = /bin/sh

ifndef VERSION
include ../VERSION
endif

ifndef RELEASE_SUFFIX
RELEASE_SUFFIX=sol10-x86
endif

ifeq ($(GLOBUS_LOCATION),)
GLOBUS_LOCATION=$(shell awk '/^.define.*GlobusLocation/ { print $$3 }' ../config/site.def)
endif

ifndef ORACLE_INSTANTCLIENT_LOCATION
ORACLE_INSTANTCLIENT_LOCATION=/usr
endif
ifndef ORACLE_INSTANTCLIENT_VERSION
ORACLE_INSTANTCLIENT_VERSION=10.2.0.3
endif
ORACLE_INSTANTCLIENT_LIBDIR=$(ORACLE_INSTANTCLIENT_LOCATION)/lib/oracle/$(ORACLE_INSTANTCLIENT_VERSION)/client/lib/

PYTHON_VERSION=$(shell python -c "import sys; print sys.version[:3]")

all: lcg-dm-common LFC-mysql DPM-mysql DPM-dicom-mysql
ifneq ($(PROC_INIT),)
all: LFC-oracle DPM-oracle
endif
all:
	mkdir -p ../RPMS
	cp build-*/RPMS/*-sol10-x86 ../RPMS/

client: lcg-dm-common LFC-client DPM-client
	mkdir -p ../RPMS
	cp build-*/RPMS/*-sol10-x86 ../RPMS/

check-proc:
	source $(PROC_INIT); which proc

SOURCE_DIR=LCG-DM-$(VERSION)
SOURCE_TAR=$(SOURCE_DIR).tar.gz

$(SOURCE_TAR):
	cd ..; \
	rm -rf $(SOURCE_DIR); \
	mkdir $(SOURCE_DIR); \
	cp -rp Imakefile Makefile.ini Makefile.ini.Win32 README VERSION \
		config imake test setosflags configure doc scripts \
		h lib shlib common ns rfio dpm srmv1 srmv2 srmv2.2 security \
		dli dicomcopy dpmcopy $(SOURCE_DIR); \
	find $(SOURCE_DIR) -name .svn -exec rm -rf {} \;; \
	gtar -czf $(SOURCE_TAR) $(SOURCE_DIR); \
	rm -rf $(SOURCE_DIR)
	mv ../$(SOURCE_TAR) .

source: $(SOURCE_TAR)

build-pkg: source
	mkdir -p build-pkg
	mkdir -p build-pkg/BUILD
	mkdir -p build-pkg/RPMS
	mkdir -p build-pkg/SOURCES
	cp $(SOURCE_TAR) build-pkg/SOURCES

###############################################################################
# common
###############################################################################

lcg-dm-common: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-client-only dm CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lcgdmcmn -v $(VERSION) VERSION=$(VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LCG-DM-common-$(VERSION)-$(RELEASE_SUFFIX) LCG-DM-common

###############################################################################
# LFC client
###############################################################################

LFC-client: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-client-only lfc CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcclient -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcifce -v $(VERSION) PYTHON_VERSION=$(PYTHON_VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcifce2 -v $(VERSION) PYTHON_VERSION=$(PYTHON_VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-client-$(VERSION)-$(RELEASE_SUFFIX) LFC-client
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-interfaces-$(VERSION)-$(RELEASE_SUFFIX) LFC-interfaces
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-interfaces2-$(VERSION)-$(RELEASE_SUFFIX) LFC-interfaces2

###############################################################################
# DPM client
###############################################################################

DPM-client: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-client-only dpm CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmclient -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmifce -v $(VERSION) PYTHON_VERSION=$(PYTHON_VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmifce2 -v $(VERSION) PYTHON_VERSION=$(PYTHON_VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-client-$(VERSION)-$(RELEASE_SUFFIX) DPM-client
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-interfaces-$(VERSION)-$(RELEASE_SUFFIX) DPM-interfaces
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-interfaces2-$(VERSION)-$(RELEASE_SUFFIX) DPM-interfaces2

###############################################################################
# LFC MySQL
###############################################################################

LFC-mysql: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-mysql lfc CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcclient -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcifce -v $(VERSION) PYTHON_VERSION=$(PYTHON_VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcifce2 -v $(VERSION) PYTHON_VERSION=$(PYTHON_VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcmys -v $(VERSION) VERSION=$(VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-client-$(VERSION)-$(RELEASE_SUFFIX) LFC-client
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-interfaces-$(VERSION)-$(RELEASE_SUFFIX) LFC-interfaces
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-interfaces2-$(VERSION)-$(RELEASE_SUFFIX) LFC-interfaces2
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-server-mysql-$(VERSION)-$(RELEASE_SUFFIX) LFC-server-mysql

###############################################################################
# DPM MySQL
###############################################################################

DPM-mysql: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-mysql dpm CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmclient -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmifce -v $(VERSION) PYTHON_VERSION=$(PYTHON_VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmifce2 -v $(VERSION) PYTHON_VERSION=$(PYTHON_VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmmys -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.copymys -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpnsmys -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.srmmys -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.rfioserver -v $(VERSION) VERSION=$(VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-client-$(VERSION)-$(RELEASE_SUFFIX) DPM-client
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-interfaces-$(VERSION)-$(RELEASE_SUFFIX) DPM-interfaces
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-interfaces2-$(VERSION)-$(RELEASE_SUFFIX) DPM-interfaces2
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-server-mysql-$(VERSION)-$(RELEASE_SUFFIX) DPM-server-mysql
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-copy-server-mysql-$(VERSION)-$(RELEASE_SUFFIX) DPM-copy-server-mysql
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-name-server-mysql-$(VERSION)-$(RELEASE_SUFFIX) DPM-name-server-mysql
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-srm-server-mysql-$(VERSION)-$(RELEASE_SUFFIX) DPM-srm-server-mysql
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-rfio-server-$(VERSION)-$(RELEASE_SUFFIX) DPM-rfio-server

###############################################################################
# DPM-DICOM MySQL
###############################################################################

DPM-dicom-mysql: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-mysql --with-dicom dpm CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmdicommys -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dicommys -v $(VERSION) VERSION=$(VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-dicom-server-mysql-$(VERSION)-$(RELEASE_SUFFIX) DPM-dicom-server-mysql
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-dicom-copyd-mysql-$(VERSION)-$(RELEASE_SUFFIX) DPM-dicom-copyd-mysql

###############################################################################
# LFC Oracle
###############################################################################

LFC-oracle: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(ORACLE_INSTANTCLIENT_LIBDIR):$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-oracle lfc CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcora -v $(VERSION) VERSION=$(VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-server-oracle-$(VERSION)-$(RELEASE_SUFFIX) LFC-server-oracle

###############################################################################
# DPM Oracle
###############################################################################

DPM-oracle: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(ORACLE_INSTANTCLIENT_LIBDIR):$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-oracle dpm CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmora -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.copyora -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpnsora -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.srmora -v $(VERSION) VERSION=$(VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-server-oracle-$(VERSION)-$(RELEASE_SUFFIX) DPM-server-oracle
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-copy-server-oracle-$(VERSION)-$(RELEASE_SUFFIX) DPM-copy-server-oracle
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-name-server-oracle-$(VERSION)-$(RELEASE_SUFFIX) DPM-name-server-oracle
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-srm-server-oracle-$(VERSION)-$(RELEASE_SUFFIX) DPM-srm-server-oracle

###############################################################################
# LFC PostgreSQL
###############################################################################

LFC-postgres: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-postgres lfc CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.lfcpsql -v $(VERSION) VERSION=$(VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/LFC-server-postgres-$(VERSION)-$(RELEASE_SUFFIX) LFC-server-postgres

###############################################################################
# DPM PostgreSQL
###############################################################################

DPM-postgres: build-pkg
	cd build-pkg/BUILD; \
	rm -rf $@-$(VERSION); \
	gtar -xzf ../SOURCES/$(SOURCE_TAR); \
	mv $(SOURCE_DIR) $@-$(VERSION)
	BUILD_ROOT=$(PWD)/build-pkg/BUILD/$@-$(VERSION)-root; \
	LD_LIBRARY_PATH=$(GLOBUS_LOCATION)/lib:$(LD_LIBRARY_PATH); export LD_LIBRARY_PATH; \
	(cd build-pkg/BUILD/$@-$(VERSION); \
	./configure --with-postgres dpm CC=gcc MAKE=gmake ${EXTRA_CONFIGURE_OPTIONS}; \
	gmake; \
	gmake install prefix=$$BUILD_ROOT; \
	gmake install.man prefix=$$BUILD_ROOT); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpmpsql -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.copypsql -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.dpnspsql -v $(VERSION) VERSION=$(VERSION); \
	pkgmk -o -b$$BUILD_ROOT -f prototype.srmpsql -v $(VERSION) VERSION=$(VERSION)
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-server-postgres-$(VERSION)-$(RELEASE_SUFFIX) DPM-server-postgres
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-copy-server-postgres-$(VERSION)-$(RELEASE_SUFFIX) DPM-copy-server-postgres
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-name-server-postgres-$(VERSION)-$(RELEASE_SUFFIX) DPM-name-server-postgres
	pkgtrans -o -s /var/spool/pkg $(PWD)/build-pkg/RPMS/DPM-srm-server-postgres-$(VERSION)-$(RELEASE_SUFFIX) DPM-srm-server-postgres

clean:
	-rm -rf ../RPMS build-pkg $(SOURCE_TAR)


