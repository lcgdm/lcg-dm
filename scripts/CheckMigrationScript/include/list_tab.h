
#define NB_TAB_TO_CHECK 5
#define NB_COL_TO_CHECK 19
static char DPM_UPGRADE_VERSION[10]="1.6.3";

static char _mysql_db_name[31]="dpm_db";
static char TABLE_ADD_COL[NB_TAB_TO_CHECK][31]={"dpm_pool","dpm_put_filereq","dpm_copy_filereq","dpm_space_reserv","dpm_get_filereq"};
static char TABLE_COL[NB_COL_TO_CHECK][31]={"def_lifetime","max_lifetime","maxpintime","mig_policy","ret_policy","END","f_lifetime","ret_policy","ac_latency","END","ret_policy","ac_latency","END","s_gid","ret_policy","ac_latency","END","ret_policy"};
static char TABLE_COL_DEF_VALUE[NB_COL_TO_CHECK][255]={"604800","2592000","43200","none","R","END","2147483647|86400|:lifetime","_","O","END","_","O","END","0","R","O","END","_"};
static char TABLE_COL_TYPE[NB_COL_TO_CHECK]={'I','I','I','C','C','E','I','C','C','E','C','C','E','I','C','C','E','C'};
static char TABLE_COL_UP_COND[NB_COL_TO_CHECK][200]={"N","N","N","N","N","END","f_type='P'|concat(f_type,status) not in ('V4096','D4096')|concat(f_type,status) in ('V4096','D4096')","N","N","END","N","N","END","N","N","N","END","N"};
