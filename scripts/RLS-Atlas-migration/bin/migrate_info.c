/*
 * Copyright (C) 1999-2004 by CERN/IT/PDP/DM
 * All rights reserved
 */

/* Takes a GUID with filesize and checksum and adds the
 * given info to the file entry in the LFC. UID and GID to be added
 * later.
 */

/* Usage: migrate_info -g guid [-s filesize] [-c MD5 checksum] [-t
   time]
 * Using the -t option changed the access and modification times of
 * the file to the given time. Time should be given in seconds since
 * 1970. Creation time is _not_ changed. If -s, -c, -t not specified,
 * values are left as they were.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <uuid/uuid.h>
#include <sys/types.h>
#include <errno.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "lfc_api.h"
#include "serrno.h"

#define MD5LEN 32

extern	char	*optarg;

main(argc, argv)
     int argc;
     char **argv;
{
  int c;
  char *gd = NULL;
  char *sz = NULL;
  char *ck = NULL;
  char *tm = NULL;
  u_signed64 size = 0;
  struct Cns_filestatg statg;
  char guid[CA_MAXGUIDLEN+1];
  char cksum[MD5LEN+1];
  char cktype[3];
  long newtime = 0;
  struct utimbuf time;
  Cns_list list;
  struct Cns_linkinfo* lp;
  int flags;

  while ((c = getopt (argc, argv, "g:s:c:t:")) != EOF) {
    switch (c) {
    case 'g':
      gd = optarg;
      break;
    case 's':
      size = strtol (optarg, &sz, 0);
      if (size < 0) {
	fprintf (stderr, "invalid value for option -s\n");
	exit(1);
     } 
      break;
    case 'c':
      ck = optarg;
      break;
    case 't':
      newtime = strtol (optarg, &tm, 0);
      break;
    default:
      break;
    }
  }
  
  /* statg to check the guid exists */
  if (gd) {
    sprintf(guid, "%s", gd);  
  }
  else {
    printf("ERROR: must supply guid!\n");
    exit(USERR);
  }
  if(lfc_statg(NULL, guid, &statg) < 0) {
    fprintf (stderr, "Cannot statg %s : %s\n", guid,
             sstrerror(serrno));
    exit(1);
  }

  // if no size given, leave as it was
  if (!size) 
   size = statg.filesize;

  sprintf(cksum, "");
  if (ck) {
    sprintf(cktype, "MD");
    sprintf(cksum, "%s", ck);
  }
  else {
    // if no checksum given, leave as it was
    sprintf(cktype, "%s", statg.csumtype);
    sprintf(cksum, "%s", statg.csumvalue);
  }

  /* give it the size and checksum info */
  if(lfc_setfsizeg(guid, size, cktype, cksum) < 0) {
    printf ("Cannot setfsizeg %s: %d : %s\n", guid, size,
             sstrerror(serrno));
    exit(1);
  }

  /* give it time info: this changed access and modification times
     only */
  if (tm) {
    time.actime = newtime;
    time.modtime = newtime;
    flags = CNS_LIST_BEGIN;
    lp = lfc_listlinks(NULL, guid, flags, &list);
    if (lfc_utime(lp->path, &time) < 0) {
      printf ("Cannot set time %s: %s\n", lp->path, newtime);
      exit(1);
    }
  }

  exit (0);
}
