#
# constants
#

package Atlas;

use Time::Local;
use DBI;

sub queryFromRLSAndUpdateLFC($$$$) {
    my ($dbh_lfc, $dbh_rmc, $lrc_user, $rmc_user) = @_;

    my $select;
    my $count = 0;

    my ($guid_guid, $alias_alias, $pfn_pfn, $dq_lcn, $dq_date, $dq_fsize, $dq_md5sum);
    $select= $dbh_rmc->prepare("
	SELECT grmc.guid_guid, alias_alias, pfn_pfn, \"ATTR_dq_lcn\", \"ATTR_dq_date\", alias.\"ATTR_dq_fsize\", alias.\"ATTR_dq_md5sum\" 
        FROM $rmc_user.guid grmc, $lrc_user.guid glrc, alias alias, $lrc_user.pfn
        WHERE grmc.guid_id = alias_gid
	AND alias_alias like 'rome%'
        AND glrc.guid_id = pfn_gid
        AND grmc.guid_guid = glrc.guid_guid
	ORDER BY guid_guid");
    $select->execute();
    while(($guid_guid, $alias_alias, $pfn_pfn, $dq_lcn, $dq_date, $dq_fsize, $dq_md5sum) = $select->fetchrow_array()) {

    # if $guid_guid starts with "guid:",  remove this part
    if ($guid_guid =~ /guid:/) {
        $guid_guid =~ s/guid://;
    }

    if ($alias_alias =~ /burke/ || $alias_alias =~/&/ ) {

        # don't migrate this entry, this is just a test entry.

    } elsif (defined($dq_lcn) && $dq_lcn ne "/") {

        # if $dq_lcn starts with //, replace by /
        if (index($dq_lcn, "/") == 0) {
                print "dq_lcn = $dq_lcn\n";
                $dq_lcn =~ s/\/{1}//;
                print "dq_lcn = $dq_lcn\n";
        }

        # create the directories where to put the file
        print "./migrate_path /grid/atlas/$dq_lcn \n";
        `./migrate_path /grid/atlas/$dq_lcn `;
        # create the guid (if it doesn't exist), the alias and the pfn
        print "./migrate_files -p $dq_lcn -a $alias_alias -g $guid_guid -r $pfn_pfn \n";
        `./migrate_files -p $dq_lcn -a $alias_alias -g $guid_guid -r $pfn_pfn `;

        # migrate the files size and its checksum
	if (defined($dq_fsize) && defined($dq_md5sum)) {
		print "./migrate_info -g $guid_guid -s $dq_fsize -c $dq_md5sum \n";
                `./migrate_info -g $guid_guid -s $dq_fsize -c $dq_md5sum`;
	} else {
		if (defined($dq_fsize)) {
                	print "./migrate_info -g $guid_guid -s $dq_fsize \n";
	                `./migrate_info -g $guid_guid -s $dq_fsize`;
        	}
	        if (defined($dq_md5sum)) {
        	        print "./migrate_info -g $guid_guid -c $dq_md5sum\n";
	                `./migrate_info -g $guid_guid -c $dq_md5sum`;
        	}
	}

        # Parse the dq_date and convert it to unix time
        if (defined($dq_date)) {
                print "dq_date = $dq_date\n";
                convertDqDateIntoUnixDate($dbh_lfc, $dq_date, $guid_guid);
        }

    } else {

        # create the guid (if it doesn't exist), the alias and the pfn
        print "./migrate_files -a $alias_alias -g $guid_guid -r $pfn_pfn\n";
        `./migrate_files -a $alias_alias -g $guid_guid -r $pfn_pfn`;
        
	# migrate the files size and its checksum
	if (defined($dq_fsize) && defined($dq_md5sum)) {
                print "./migrate_info -g $guid_guid -s $dq_fsize -c $dq_md5sum \n";
                `./migrate_info -g $guid_guid -s $dq_fsize -c $dq_md5sum`;
        } else {
        	if (defined($dq_fsize)) {
                	print "./migrate_info -g $guid_guid -s $dq_fsize \n";
	                `./migrate_info -g $guid_guid -s $dq_fsize`;
        	}
	        if (defined($dq_md5sum)) {
        	        print "./migrate_info -g $guid_guid -c $dq_md5sum\n";
                	`./migrate_info -g $guid_guid -c $dq_md5sum`;
        	}
	}

        # Parse the dq_date and convert it to unix time
        if (defined($dq_date)) {
                print "dq_date = $dq_date\n";
                convertDqDateIntoUnixDate($dbh_lfc, $dq_date, $guid_guid);
        }

    }


    $count = $count+1;
    if ($count%1000==0) {
        $time = localtime();
        print "$time : $count entries migrated\n";
    }

}

    $select->finish;
    return $count;

} 

sub convertDqDateIntoUnixDate($$$) {
        my ($dbh_lfc, $dq_date, $guid_guid) = @_;
        my ($year, $mon, $rest, $day, $time, $hour, $min, $sec);

        #print "dq_date = $dq_date\n";
        # Change the dates of the form "04/08/30T13:36:55+0200" or "04/06/24 20:19"
        # to be of the form "2004-08-30 13:36:55"
        $dq_date =~ s/T/ /;
        $dq_date =~ s/\+0200//;
        $dq_date =~ s/\//-/g;

        # Get the year, month, etc. values from the date of form "2004-08-13 08:29"
        ($year, $mon, $rest) = split(/\-/, $dq_date);
        ($day, $time) = split(/\ /, $rest);
        ($hour, $min, $sec) = split(/\:/, $time);

        # if year has only 2 digits, add 2000
        if (length($year) == 2) {
                $year = 2000 + $year;
        }

        # if no seconds are specified, give them "00" as a value
        if (!$sec) {
                $sec = "00";
        }

        # Convert the date and time into a unix time
        # N.B. : the month is from 0 to 11 in the timelocal function
        $dq_date = timelocal($sec,$min,$hour,$day,$mon-1,$year);

        print "New dq_date = $dq_date\n";

        # Update the LFC database
        Common::updateFieldInLFC($dbh_lfc, "Cns_file_metadata", "ctime", $dq_date, $guid_guid);
        Common::updateFieldInLFC($dbh_lfc, "Cns_file_metadata", "atime", $dq_date, $guid_guid);
        Common::updateFieldInLFC($dbh_lfc, "Cns_file_metadata", "mtime", $dq_date, $guid_guid);

	$dbh_lfc->commit;
}



1;
