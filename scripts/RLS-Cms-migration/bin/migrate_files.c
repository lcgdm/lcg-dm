/*
 * Copyright (C) 1999-2004 by CERN/IT/PDP/DM
 * All rights reserved
 */

/* Takes a filename, GUID and PFN which have 
 * been extracted from the EDG RLS and inserts the corresponding
 * entries into the LFC. If there is more than one alias of the same
 * guid, the other aliases are implemented as symlinks to the
 * "master" alias. UID and GUID of all LFC entries created are set
 * according to the experiment chosen with the -v option. If none is
 * chosen, or if an unknown VO name is given, the default is
 * atlas. Therefore, if a VO other than ATLAS is required, the -v
 * option _must_ be used!
 */

/* Usage:
 *      migrate_files [-v VO_NAME] [-p PATH] -a ALIAS -g GUID -r PFN
 * VO_NAME should be the subdirectory of /grid where you want files to
 * be written, e.g. /grid/lhcb. If not specified, the default is
 * atlas.
 * PATH is optional and should be taken from ATTR_DQ_LCN if it is
 * specified.
 * If PATH is not given, the directory structure (if any) deduced from
 * the ALIAS name is written directly into /grid/vo_name/. Otherwise
 * it will be written to /grid/vo_name/PATH/. If there is no apparent
 * directory structure in the alias name, the path used is
 * /grid/vo_name/test/.
 * ALIAS and GUID are compulsory. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uuid/uuid.h>
#include <sys/types.h>
#include <errno.h>
#if defined(_WIN32)
#include <winsock2.h>
#define F_OK 0
#else
#include <unistd.h>
#endif
#include "lfc_api.h"
#include "serrno.h"

#define LOWER(s) \
{\
 char * q; \
 for (q = s; *q; q++) \
			if (*q >= 'A' && *q <= 'Z') *q = *q - ('A' - 'a'); \
}


extern	char	*optarg;

main(argc, argv)
int argc;
char **argv;
{
  int c;
  char *i = NULL;
  char *pn = NULL;
  char *gd = NULL;
  char *ap = NULL;
  char *rp = NULL;
  char *vo = NULL;
  char *p;
  char *endp;
  struct Cns_filestat statbuf;
  struct Cns_filestatg statg;
  struct Cns_filestatg statg2;
  char server_name[CA_MAXHOSTNAMELEN];
  char path_to_file[CA_MAXPATHLEN+1];
  char path_inc_file[CA_MAXPATHLEN+1];
  char link_buf[CA_MAXPATHLEN+1];
  char alias[CA_MAXPATHLEN+1];
  char guid[CA_MAXGUIDLEN+1];
  char pfn[CA_MAXSFNLEN+1];
  char vo_name[6];
  int errflg = 0;
  uuid_t uuid;
  Cns_list list;
  struct Cns_linkinfo* lp;
  int flags;
  char uidbuf[5];
  char gidbuf[6];
  uid_t uid;
  gid_t gid;

  while ((c = getopt (argc, argv, "p:g:a:r:v:")) != EOF) {
    switch (c) {
    case 'p':
      pn = optarg;
      break;
    case 'g':
      gd = optarg;
      break;
    case 'a':
      ap = optarg;
      break;
    case 'r':
      rp = optarg;
      break;
    case 'v':
      vo = optarg;
      break;
    default:
      break;
    }
  }

  /* set up path base. Use path from ATTR_DQ_LCN field if given using -p
     option. Else use /grid/vo_name/ only and add path from alias. If
     vo_name not given use atlas as default. */
  if (vo) {
    sprintf(path_to_file, "/grid/%s", vo);
    sprintf(vo_name, "%s", vo);
  }
  else {
    sprintf(path_to_file, "/grid/cms");
    printf("VO not specified; using CMS as default\n");
    sprintf(vo_name, "cms");
  }  
  if (pn) {
    strcat(path_to_file, "/");
    strcat(path_to_file, pn);
  }

  /* check filename for absence of '/'. If
   * true, path becomes /grid/vo_name/test/filename UNLESS the
   * path has been specified with the -p option. Else,
   * path becomes /grid/vo_name/filename-split-into-directories.
   */
  sprintf(alias, "%s", ap);
  if (strstr(alias, "/") == NULL) {
    //paths of form dummy.txt
    if (!pn)
      strcat(path_to_file, "/test");
    strcpy(path_inc_file, path_to_file);
    strcat(path_inc_file, "/");
    strcat(path_inc_file, alias);    
  }
  else {
    //paths of form /.../.../.../
    strcat(path_to_file, "/");
    strcpy(path_inc_file, path_to_file);
    strcat(path_inc_file, alias);
    endp = strrchr(alias, '/');
    strncat(path_to_file, alias, strlen(alias)-strlen(endp));
  }
  
  /* set gid and uid to correct values for this VO */
  if (!strcmp(vo_name, "alice")) {
    sprintf(gidbuf, "1395"); 
    sprintf(uidbuf, "10417");
  }
  else if (!strcmp(vo_name, "lhcb")) {
    sprintf(gidbuf, "1470");
    sprintf(uidbuf, "12238");
  }
  else if (!strcmp(vo_name, "cms")) {
    sprintf(gidbuf, "1399");
    sprintf(uidbuf, "11410");
  }
  else {
    //default is cms
    sprintf(gidbuf, "1399");
    sprintf(uidbuf, "11410");
  }
  uid = strtol(uidbuf, NULL, 10);
  gid = strtol(gidbuf, NULL, 10);

  /* if path to file does not exist, create it */
  if (lfc_stat (path_to_file, &statbuf) < 0) {
    if (serrno == ENOENT) {
      endp = strrchr (path_to_file, '/');
      p = endp;
      while (p > path_to_file) {
	*p = '\0';
	c = lfc_access (path_to_file, F_OK);
	if (c == 0) break;
	p = strrchr (path_to_file, '/');
      }
      while (p <= endp) {
	*p = '/';
	uuid_generate(uuid);
	uuid_unparse(uuid, guid);
	c = lfc_mkdirg (path_to_file, guid, 0777);
	if (c < 0 && serrno != EEXIST) {
	  fprintf (stderr, "cannot create %s: %s\n",
		   path_to_file, sstrerror(serrno));
	  errflg++;
	  break;
	}
	if (lfc_chown(path_to_file, uid, gid) < 0) {
	  fprintf(stderr, "Cannot chown %s: %s\n", path_to_file, sstrerror(serrno));
	}
	p += strlen (p);
      }
    } else {
      fprintf (stderr, "%s: %s\n", path_inc_file, sstrerror(serrno));
      errflg++;
    }    
  }

  /* if guid does not exist, create new file with given alias & pfn */
  
  if (gd) {
    sprintf(guid, "%s", gd);  
    //if guid is uppercase, change to lowercase. This is not necessary
    //but helps with guid comparison later.
    LOWER(guid);
  }
  else {
    printf("ERROR: must supply guid!\n");
    exit(USERR);
  }

  if (lfc_statg(NULL, guid, &statg) < 0) {
    if (lfc_creatg (path_inc_file, guid, 0666) < 0) {
      printf("Error: %d\n", serrno);
      fprintf (stderr, "%s: %s\n", path_inc_file, sstrerror(serrno));
    }
    if (lfc_chown(path_inc_file, uid, gid) < 0) {
      fprintf(stderr, "Cannot chown %s: %s\n", path_inc_file, sstrerror(serrno));
    }
  }
  else {
    printf("Guid already exists\n");
    /* if this alias does not exist as the 'master' lfn to this guid,
       add it as a symlink to the master */
    if (lfc_statg(path_inc_file, guid, &statg2) < 0) {
      flags = CNS_LIST_BEGIN;
      lp = lfc_listlinks(NULL, guid, flags, &list);
      if (lfc_symlink(lp->path, path_inc_file) < 0) {
	fprintf (stderr, "Error in symlink: %s: %s\n", path_inc_file, sstrerror(serrno));
      }
      if (lfc_lchown(path_inc_file, uid, gid) < 0) {
	fprintf(stderr, "Cannot chown %s: %s\n", path_inc_file, sstrerror(serrno));
      }
    }
    else {
      if (strcmp(statg.guid, statg2.guid) != 0) {
	fprintf (stderr, "ERROR: alias exists with wrong guid! %s: %s\n", path_inc_file, sstrerror(serrno));
	exit(1);
      }
    }
  }
  /* if this pfn does not exist, add it as a new replica to this
     guid */
  if (rp) {
    	sprintf(pfn, "%s", rp);
  
  	if( lfc_statr(pfn, &statg) < 0) {
    	p = strchr(pfn, ':')+3;
    	endp = strchr(p,'/');
    	sprintf(server_name, "");
    	strncat(server_name, p, strlen(p) - strlen(endp));
    	if (lfc_addreplica(guid, NULL, server_name, pfn, '-', '\0', NULL, NULL) < 0) {
    	  fprintf(stderr, "Cannot addreplica %s : %s : %s\n", alias, pfn,
		      sstrerror(serrno));
	    }
	  }
	  else if( strcmp(statg.guid, guid) != 0) {
	    fprintf(stderr, "ERROR: Replica exists but for different guid! %s: %s\n", alias, pfn);
	    exit(1);
	  }
  }  

  exit (0);
}
