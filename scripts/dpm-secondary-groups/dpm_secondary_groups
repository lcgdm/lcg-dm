#!/usr/bin/perl

#
# Script that:
#	- updates the DPM database schema to support secondary groups
#	- updates the 'groups' field in the "dpm_req", "dpm_pending" and "dpm_pool" tables
#
# Author : Sophie Lemaitre (sophie.lemaitre@cern.ch)
# Date : 01/03/2007
#

use strict;
use warnings;
use Getopt::Long;
use DBI;
use Env qw(ORACLE_HOME);

use UpdateDpmDatabase;

sub usage($) {
  my $reason = shift(@_);
  print <<EOF and   die "\nWrong usage of the script: $reason.\n";
usage: $0 --db-vendor db_vendor --db db --user user --pwd-file pwd_file [--dpm-db dpm_db] [--verbose]

The dpm-db argument has to be specified only if the database backend is MySQL.

EOF
}

# Create arguments variables...
my ($db_vendor, $db, $user, $pwd_file, $dpm_db, $verbose);

# ... and read the arguments
GetOptions("db-vendor:s", => \$db_vendor,
           "db:s", => \$db,
           "user:s", => \$user,
           "pwd-file:s", => \$pwd_file,
           "dpm-db:s", => \$dpm_db,
	   "verbose" => \$verbose );

# check CLI consistency
usage("The database vendor must be specified. It can either be \'Oracle\' or \'MySQL\'") unless(defined $db_vendor);
usage("The DPNS/DPM database user must be specified") unless(defined $user);
usage("The file containing the DPNS/DPM database password must be specified") unless(defined $pwd_file);

if ($db_vendor eq "MySQL") {
        usage("The DPM MySQL database must be specified") unless(defined $dpm_db);
        usage("The MySQL server host must be specified") unless(defined $db);
}
elsif ($db_vendor eq "Oracle") {
        usage("The Oracle database SID must be specified") unless(defined $db);
}
else {
	usage("The database vendor can either be \'Oracle\' or \'MySQL\'");
}

# useful variables
my ($start_time, $time, $end_time);
my $pwd;
my ($dbh_dpm);
my $count;


#
# read database password from file
#

open(FILE, $pwd_file) or die("Unable to open password file");
my @data = <FILE>;
$pwd = $data[0];
$pwd =~ s/\n//;
close(FILE);

eval {

$start_time = localtime();
print "$start_time : Starting to update the DPNS/DPM database.\n"; 
print "Please wait...\n";

if ($db_vendor eq "Oracle") {
	$dpm_db = $user;

	#
	# Check ORACLE_HOME is defined
	#
	if (!defined($ORACLE_HOME) ) {
	    print STDERR "Error: ORACLE_HOME is not set! Check your Oracle installation.\n";
	    exit(1);
	}

	my @drivers = DBI->available_drivers;
	if ((my $result = grep  /Oracle/ , @drivers) == 0){
	    print STDERR "Error: Oracle DBD Module is not installed.\n";
	    exit(1);
	}

}

# connect to databases
$dbh_dpm = Common::connectToDatabase($dpm_db, $user, $pwd, $db_vendor, $db);

# update the database schema
if ($db_vendor eq "MySQL") {
	UpdateDpmDatabase::update_mysql($dbh_dpm);
}
if ($db_vendor eq "Oracle") {
	UpdateDpmDatabase::update_oracle($dbh_dpm);
}

$dbh_dpm->disconnect;


#
# The migration is over
#

$end_time = localtime();
print "$end_time : The update of the DPNS/DPM database is over\n";

if ($verbose) {
        print "db vendor = $db_vendor\n";
	print "db = $db\n";
	print "DPM database user = $user\n";
	print "DPM database password = xxxxxx\n";

	if ($db_vendor eq "MySQL") {
                print "DPM database name = $dpm_db\n";
	}
}

};
die ("failed to query and/or update the DPM database : $@") if ($@);
