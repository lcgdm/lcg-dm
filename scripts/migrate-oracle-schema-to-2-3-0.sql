
--
-- THIS SCRIPT WAS TO TEST THE MATERIALIZED VIEWS REPLICATION
--                DO NOT USE IN PRODUCTION
--

--
-- For an Oracle database backend : 
-- script to migrate the schema from version 2.2.0 to version 2.3.0 
--
-- Author : Sophie Lemaitre <Sophie.Lemaitre@cern.ch>
--


-- Change UNIQUE constraints to PRIMARY KEYS

ALTER TABLE Cns_file_replica DROP CONSTRAINT repl_sfn;
ALTER TABLE Cns_groupinfo DROP CONSTRAINT map_groupname;
ALTER TABLE Cns_userinfo DROP CONSTRAINT map_username;

DROP INDEX repl_sfn;
DROP INDEX map_groupname;
DROP INDEX map_username;

ALTER TABLE Cns_file_replica ADD CONSTRAINT pk_repl_sfn PRIMARY KEY (sfn);
ALTER TABLE Cns_groupinfo ADD CONSTRAINT pk_map_groupname PRIMARY KEY (groupname);
ALTER TABLE Cns_userinfo ADD CONSTRAINT pk_map_username PRIMARY KEY (username);


-- Update the schema version

DROP TABLE schema_version;
CREATE TABLE schema_version (major NUMBER(1), minor NUMBER(1), patch NUMBER(1));
INSERT INTO schema_version VALUES (2, 3, 0);
