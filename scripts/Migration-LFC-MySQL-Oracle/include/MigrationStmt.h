#include "Ora_GetMetadataInfo.h"
#include "mysql_info.h"
#include "getError.h"


int PrepareSelectForMysql(char* table_name, int col_list_len, char* colname_list,int& stmt_len, char* select_stmt,char* ErrMess);
int GetIndexForString_second(char*  element_List,int elt_List_len, char* element_searched);
int GetEltAt(char*  element_List,int elt_List_len, int elt_idx, char* element_searched);
int WriteCtlFile(char* object_name,char* ora_column_list,int len_columns,int datatype_len, char* datatype_list, int* dataprecision_list, char* ErrMess);
int CheckSchemasCompatibility(MYSQL* conn_temp,char * mysql_tables,int mysql_tab_len, char* ora_tables,int ora_tab_len,int ora_nb_tab, int* nb_of_elt_per_tab, char* ErrMess);
int ExportTableFromMySQL(MYSQL* conn_temp,char* table_name, int col_list_len, char* col_list, char* ErrMess);
int GenerateShellScript(char* source_file, int sql_load,char* ErrMess);
int DisEnableFkCst(char* table_name, int enable_cst,char* ErrMess);
int CheckImportData(MYSQL* conn_temp,char* mysql_tab_list,int table_list_len,int* nb_of_elt_per_tab, char* ErrMess);
int GetParamInfo(char* config_file,char* ErrMess);
int RecreateConfigFile(char* filename, char* ErrMess);







