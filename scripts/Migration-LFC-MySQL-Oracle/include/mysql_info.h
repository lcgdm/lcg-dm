#include <stdio.h>
#include <mysql.h>
#include <stdlib.h>


int mysql_DBConnect(char * host_name,char* user_name,char* password, char *db_name,unsigned int port_num, char * socket_name, unsigned int flags,char* ErrMess);
void mysql_disconnect();
int mysql_GetTableList(MYSQL* conn_temp,char* db_name, int& table_list_len, char* table_list, char* ErrMess);
int mysql_GetColList(MYSQL* conn_temp,char* table_name,char* db_name,int& column_list_len, char* column_list, char* ErrMess);
int mysql_ExportTab(MYSQL* conn_temp,char* select_stmt, char* table_name, char* ErrMess);
int mysql_GetNbOfrows(MYSQL* conn_temp,char* db_name, char* table_name,int &nb_of_rows,char* ErrMess);
int mysql_GetMaxValue(MYSQL * conn_temp,char* db_name, char* table_name,int &max_value,char* ErrMess);



