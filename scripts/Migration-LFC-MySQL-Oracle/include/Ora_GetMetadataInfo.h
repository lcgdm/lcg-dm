
int Ora_DBConnect(char* server,char* usr,char* pwd,char* ErrorMessage);
void Ora_DBDisconnect(char* ErrorMessage);
int Ora_GetColInfo(char* table_name,int &len_columns,char* column_list,int & len_nb_col, int* column_pos_list, int& datatype_len,char* datatype_list, int* dataprecision_list,char* ErrorMessage);
int Ora_GetListOfTables(int &len_array, char* tabname_list,int & nb_of_tab,char* ErrorMessage);
int Ora_GetFkConstraints(char* table_name,int& len_cst_list,char* cstname_list,char* ErrorMessage);
int Ora_DisEnableFkConstraint(char* table_name,int len_cst_list,char* cstname_list,int enable_cst,char* ErrorMessage);
int Ora_GetNbOfRows(char*table_name, int& nb_of_rows,char* ErrorMessage);
int Ora_GetObjectType(char* object_name,int & object_type_len, char* object_type,char* ErrorMessage);
int Ora_UpdateSequence(int new_value, char* sequence_name, char* ErrorMessage);







