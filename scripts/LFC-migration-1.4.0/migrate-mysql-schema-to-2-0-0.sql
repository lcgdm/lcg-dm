--
-- Scripts that migrates the Database Schema from 1.1.1 to 2.0.0
--
-- The 2.0.0 schema integrates :
--	- virtual uids/gids
--	- VOMS support
--

--
-- Create the new tables
--
CREATE TABLE Cns_groupinfo (
       rowid INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
       gid INTEGER,
       groupname VARCHAR(255) BINARY)
      TYPE = InnoDB;

CREATE TABLE Cns_userinfo (
       rowid INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
       userid INTEGER,
       username VARCHAR(255) BINARY)
      TYPE = InnoDB;

CREATE TABLE Cns_unique_gid (
       id INTEGER UNSIGNED)
      TYPE = InnoDB;

CREATE TABLE Cns_unique_uid (
       id INTEGER UNSIGNED)
       TYPE = InnoDB;

ALTER TABLE Cns_groupinfo
       ADD UNIQUE (groupname);
ALTER TABLE Cns_userinfo
       ADD UNIQUE (username);

--
-- Update the schema version
--
DROP TABLE IF EXISTS schema_version;
CREATE TABLE schema_version (
  major INTEGER NOT NULL,
  minor INTEGER NOT NULL,
  patch INTEGER NOT NULL
) TYPE=INNODB;

INSERT INTO schema_version (major, minor, patch)
  VALUES (2, 1, 0);
