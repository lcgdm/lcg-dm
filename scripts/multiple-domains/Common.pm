#
# constants
#

package Common;

use DBI;

#
# MODULE STARTUP 
#
sub installed {
    my $module = $_;
    
    my $check = 0;
    
    eval "use $module";
    if ($@) { 
	$check =0; 
    } else {
	$check = 1; # non-zero equals installed.
	my $version = 0;
	eval "\$version = \$$module\::VERSION";
	$check = $version if (!$@); # Set check to version number if it exist.
    }
    return $check;
}


sub connectToDatabase($$$$$) {
    my ($db, $dbuser, $dbpasswd, $db_vendor, $host) = @_;
    if ($db_vendor eq "Oracle") {
    	return DBI->connect("DBI:Oracle:$host", $dbuser, $dbpasswd, { RaiseError => 1, PrintError => 0, AutoCommit => 0 })
	|| die "Can't open database $host:$dbuser: $DBI::errstr\n";
    }
    if ($db_vendor eq "MySQL") {
        return DBI->connect("dbi:mysql:$db:$host", $dbuser, $dbpasswd, { RaiseError => 1, PrintError => 0, AutoCommit => 0 })
        || die "Can't open database $db:$dbuser: $DBI::errstr\n";
    }
}

sub updateFieldInDpmWhereNumber ($$$$$$) {
    my ($dbh, $table, $field, $value, $where_field, $where_field_value_number) = @_;

    my $update = "UPDATE $table SET $field = '$value' WHERE $where_field=$where_field_value_number";

    my $prepare = $dbh->prepare($update);
    if (!$prepare->execute) {
        warn $dbh->errstr;
    }

}

sub updateFieldInDpmWhereString ($$$$$$) {
    my ($dbh, $table, $field, $value, $where_field, $where_field_value_string) = @_;

    my $update = "UPDATE $table SET $field = '$value' WHERE $where_field='$where_field_value_string'";

    my $prepare = $dbh->prepare($update);
    if (!$prepare->execute) {
        warn $dbh->errstr;
    }

}

1;
