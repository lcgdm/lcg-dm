#
# Query the DPM database and updates the hostname
#

package UpdateDpmDatabase;

use DBI;
use Common;

sub parseAndUpdateServer($$) {
    my ($dbh_dpm, $domain_name) = @_;
    my $count;

    my $server;
    $select = $dbh_dpm->prepare("
	SELECT server
	FROM dpm_fs");
    $select->execute();

    my $new_server;
    while (($server) = $select->fetchrow_array()) {
	
	$new_server = parseHost($server, $domain_name);
	Common::updateFieldInDpmWhereString($dbh_dpm, "dpm_fs", "server", $new_server, "server", $server);

	$count = $count+1;
    }

    $select->finish;
    return $count;

}

sub parseHostAndSfnAndUpdateDpm($$) {
    my ($dbh_dpm, $domain_name) = @_;

    my $select;
    my $count = 0;

    my ($host, $sfn, $fileid);
    $select= $dbh_dpm->prepare("
	SELECT host, sfn, fileid 
        FROM Cns_file_replica");
    $select->execute();

    my ($new_host, $new_sfn);
    while(($host, $sfn, $fileid) = $select->fetchrow_array()) {

	$new_host = parseHost($host, $domain_name);
	$new_sfn = parseSfn($sfn, $domain_name);
	
	Common::updateFieldInDpmWhereNumber($dbh_dpm, "Cns_file_replica", "host", $new_host, "fileid", $fileid);
	Common::updateFieldInDpmWhereNumber($dbh_dpm, "Cns_file_replica", "sfn", $new_sfn, "fileid", $fileid);

	$count = $count+1;
	if ($count%1000==0) {
		$time = localtime();
		print "$time : $count entries migrated\n";
	}

}

    $select->finish;
    return $count;

} 

sub parseHost($$) {

	my ($host, $domain_name) = @_;
	my $new_host;

	if ($host =~ /$domain_name/) {
		# keep the host as is
		$new_host = $host;
	} else {
		# append the domain name
		$new_host = $host.".".$domain_name;
	}

	return $new_host;
}


sub parseSfn($$) {

	my ($sfn, $domain_name) = @_;
	my ($host, $index);

	$index = index($sfn, ":");
	$host = substr($sfn, 0, $index);

	if ($host =~ /$domain_name/) {
		# keep the sfn as is
	} else {
		# append the domain name
		$sfn =~ s/$host/$host.$domain_name/;
	}
	
	return $sfn;
}

1;
