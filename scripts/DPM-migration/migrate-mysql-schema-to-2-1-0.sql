--
-- Scripts that migrates the Database Schema from 1.1.1 to 2.1.0
--

--
-- Create the new indexes
--

CREATE INDEX G_SURL_IDX ON dpm_get_filereq(FROM_SURL(255));
CREATE INDEX P_SURL_IDX ON dpm_put_filereq(TO_SURL(255));
CREATE INDEX CF_SURL_IDX ON dpm_copy_filereq(FROM_SURL(255));
CREATE INDEX CT_SURL_IDX ON dpm_copy_filereq(TO_SURL(255));

--
-- Create the "schema_version" table
--

DROP TABLE IF EXISTS schema_version;
CREATE TABLE schema_version (
  major INTEGER NOT NULL,
  minor INTEGER NOT NULL,
  patch INTEGER NOT NULL
) TYPE=INNODB;

INSERT INTO schema_version (major, minor, patch)
  VALUES (2, 1, 0);
