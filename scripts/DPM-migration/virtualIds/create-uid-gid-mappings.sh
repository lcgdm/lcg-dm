#!/bin/sh

#
# This script creates the virtual id <-> group mappings in the DPM Name Server for the existing groups
#
# Author : 	- Sophie Lemaitre (sophie.lemaitre@cern.ch)
#		- James Casey (james.casey@cern.ch)
#
# Date : 27/10/2005
#

base=$(cd $(dirname $0); pwd)
TEMP=$(getopt -o omu:p:d:hv --long oracle,mysql,user:,password-file:,database:,host:,help,verbose -- "$@")
eval set -- "$TEMP"

VERBOSE='false'
DB_TYPE="mysql"
DB_USER=""
DB_DATABASE=""
DB_HOST=`hostname`

function usage() {
echo "Usage : $(basename $0) (--oracle|--mysql) --user user --password-file \"/path/to/password/file\""
echo "          --database database [--host mysql-server] [--help] [--verbose]"
echo 

exit 1

}

function max_insert_statement() {
  echo "INSERT INTO Cns_unique_$1 VALUES($2);"
  return 0
}

function call_database() {
    if [ "x$1" == "x" ] ; then # no file provided
      if [ $DB_TYPE == "mysql" ] ; then
	  mysql -N -u $DB_USER -p$DB_PASSWORD --database $DB_DATABASE --host $DB_HOST
      else
	  sqlplus $DB_USER/$DB_PASSWORD@$DB_DATABASE
      fi


    else 
      if [ ! -f $1 ] ; then
	  echo "[ERROR] : File does not exist : $1"
	  return 1
      fi
      if [ $DB_TYPE == "mysql" ] ; then
	  mysql -N -u $DB_USER -p$DB_PASSWORD --database $DB_DATABASE --host $DB_HOST < $1
      else
	  sqlplus -S $DB_USER/$DB_PASSWORD@$DB_DATABASE < $1
      fi
   fi
}

while true; do
    case "$1" in
        -o|--oracle)
            DB_TYPE='oracle'
            shift
            ;;
        -m|--mysql)
            BD_TYPE='mysql'
            shift
            ;;
        -u|--user)
            shift
            DB_USER="$1"
            shift
            ;;
        -p|--password-file)
            shift
            PASSWORD_FILE="$1"
            shift
            ;;
        -d|--database)
            shift
            DB_DATABASE="$1"
            shift
            ;;
        --host)
            shift
            DB_HOST="$1"
            shift
            ;;
        -v|--verbose)
            VERBOSE='true'
            shift
            ;;
        -h|--help)
            usage
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Unknown option '$1'"
            exit
            ;;
    esac
done


#
# Check for reqd variables
if [ "$DB_USER" == "" ] ||  [ "$PASSWORD_FILE" == "" ] || [ "$DB_DATABASE" == "" ] ; then
    usage "Database name, database user and file containing database password must be specified"
fi

#
# Read database user password from file
DB_PASSWORD=`cat $PASSWORD_FILE`

OUTPUT_FILE=`mktemp "/tmp/dpm_upgrade.XXXXXX"`
if [ $VERBOSE == 'true' ]; then
    echo "Using Output File : $OUTPUT_FILE"
fi

QUERY_FILE=`mktemp "/tmp/dpm_upgrade.XXXXXX"`
GROUP_MAP=`mktemp "/tmp/dpm_upgrade.XXXXXX"`
CHECK_FILE1=`mktemp "/tmp/dpm_upgrade.XXXXXX"`
CHECK_FILE2=`mktemp "/tmp/dpm_upgrade.XXXXXX"`

#
# find /dpm/cern.ch/home/ entry and generate group map from it's children
#
if [ "$DB_TYPE" == "oracle" ] ; then
    echo "SET ECHO OFF NEWP 0 SPA 0 PAGES 0 FEED OFF HEAD OFF TRIMS ON" > $QUERY_FILE
else
    echo "" > $QUERY_FILE
fi
echo "select fileid from Cns_file_metadata where parent_fileid = '4' and name = 'home';" >> $QUERY_FILE
GRID_ENTRY=$(call_database $QUERY_FILE)

if [ "$DB_TYPE" == "oracle" ] ; then
    echo "SET ECHO OFF NEWP 0 SPA 0 PAGES 0 FEED OFF HEAD OFF TRIMS ON" > $QUERY_FILE
    echo "COLUMN name FORMAT A32" >> $QUERY_FILE
else
    echo "" > $QUERY_FILE
fi

# check that all existing gids are different
echo "select count(distinct gid) from Cns_file_metadata where parent_fileid = $GRID_ENTRY;" >> $CHECK_FILE1
NUMBER1=$(call_database $CHECK_FILE1  | tr -d "\t ")
echo "select count(gid) from Cns_file_metadata where parent_fileid = $GRID_ENTRY;" >> $CHECK_FILE2
NUMBER2=$(call_database $CHECK_FILE2  | tr -d "\t ")

if [ $NUMBER2 != $NUMBER1 ] ; then
    echo "ERROR : different groups have the same group id.\n"
    echo "ERROR : change the permissions on the /dpm/domain.name/home/vo directories and rerun the script."
    exit 1;
fi

echo "select gid, ':', name from Cns_file_metadata where parent_fileid = $GRID_ENTRY;" >> $QUERY_FILE
$(call_database $QUERY_FILE  | tr -d "\t " > $GROUP_MAP)

#
# find users from db
#

if [ "$DB_TYPE" == "oracle" ] ; then
    echo "SET ECHO OFF NEWP 0 SPA 0 PAGES 0 FEED OFF HEAD OFF TRIMS ON" > $QUERY_FILE
else
    echo "" > $QUERY_FILE
fi
echo "select distinct owner_uid from Cns_file_metadata order by owner_uid;" >> $QUERY_FILE
UID_LIST=$(call_database $QUERY_FILE)
IFS='
'

MAX=-1
for uid in $UID_LIST ; do
  if [ $uid == "owner_uid" ]; then
    continue
  fi
  # strip whitespace
  uid=$(echo $uid | tr -d "\t ")
  username=`cut -d':' -f1,3,4 /etc/passwd|grep :$uid:|cut -d':' -f1`
  if [ "x$username" != "x" ] ; then
      echo "INSERT INTO Cns_userinfo(username, userid) VALUES('$username', $uid);" >> $OUTPUT_FILE
      if [ $VERBOSE == 'true' ] ; then
	    echo "Adding User  : $username (uid=$uid)"
	fi
      if [ $(($uid > $MAX)) == 1 ] ; then
        MAX=$uid
      fi

  fi
done

if [ $(($MAX > -1)) == 1 ] ; then
  MAX=`expr $MAX + 1`
  echo $(max_insert_statement "uid" $MAX) >> $OUTPUT_FILE
  if [ $VERBOSE == 'true' ] ; then
      echo "Setting Maximum User ID : $MAX"
  fi
else
  echo "[WARNING] No Users added"
fi


#
# get all the group names and gids used in the DPM Name Server
# We map the group names to VO names using the 'group-mappings' file
#

if [ "$DB_TYPE" == "oracle" ] ; then
    echo "SET ECHO OFF NEWP 0 SPA 0 PAGES 0 FEED OFF HEAD OFF TRIMS ON" > $QUERY_FILE
else
    echo "" > $QUERY_FILE
fi
echo "select distinct gid from Cns_file_metadata order by gid;" >> $QUERY_FILE
GID_LIST=$(call_database $QUERY_FILE)
IFS='
'
MAX=-1
for gid in $GID_LIST; do
    if [ "x$gid" == "xgid" ]; then
	continue
    fi
    # strip whitespace
    gid=$(echo $gid | tr -d "\t ")
    voname=`grep ^$gid: $GROUP_MAP| cut -d':' -f2`
    if [ "x$voname" != "x" ] ; then
        echo "INSERT INTO Cns_groupinfo(groupname,gid) VALUES('$voname', $gid);"  >> $OUTPUT_FILE
    	if [ $VERBOSE == 'true' ] ; then
	    echo "Adding Group : $voname (gid=$gid)"
	fi
    fi
    if [ $(($gid > $MAX )) == 1 ] ; then
        MAX=$gid
    fi
done

if [ $(($MAX > -1)) == 1 ] ; then
    echo $(max_insert_statement "gid" $MAX) >> $OUTPUT_FILE
  if [ $VERBOSE == 'true' ] ; then
      echo "Setting Maximum User ID : $MAX"
  fi

else
    echo "[WARNING] No groups added"
fi

if [ ! -s $OUTPUT_FILE ] ; then
   echo "[WARNING] No SQL to insert into database."
   exit 1;
fi

#/bin/rm $QUERY_FILE
#/bin/rm $GROUP_MAP

#
# stop the DPM Name Server
#
/sbin/service dpnsdaemon stop && sleep 10

#
# Migrate the database schema 
#
call_database $base/migrate-$DB_TYPE-schema-to-2-2-0.sql

#
# Create the existing user and group mappings
#
call_database $OUTPUT_FILE 


#
# Migration over...
#
/sbin/service dpnsdaemon start
#/bin/rm $OUTPUT_FILE
