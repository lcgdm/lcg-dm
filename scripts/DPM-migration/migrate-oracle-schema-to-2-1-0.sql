--
-- Scripts that migrates the Database Schema from 2.0.0 to 2.1.0
--

--
-- Create the new indexes
--

CREATE INDEX G_SURL_IDX ON dpm_get_filereq(FROM_SURL);
CREATE INDEX P_SURL_IDX ON dpm_put_filereq(TO_SURL);
CREATE INDEX CF_SURL_IDX ON dpm_copy_filereq(FROM_SURL);
CREATE INDEX CT_SURL_IDX ON dpm_copy_filereq(TO_SURL);

--
-- Create the "schema_version" table
--

DROP TABLE schema_version;
CREATE TABLE schema_version (major NUMBER(1), minor NUMBER(1), patch NUMBER(1));
INSERT INTO schema_version VALUES (2, 1, 0);
