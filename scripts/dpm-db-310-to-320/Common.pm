#
# constants
#

package Common;

use DBI;
use strict;
use warnings; 

#
# MODULE STARTUP 
#
sub installed {
    my $module = $_;
    
    my $check = 0;
    
    eval "use $module";
    if ($@) { 
	$check =0; 
    } else {
	$check = 1; # non-zero equals installed.
	my $version = 0;
	eval "\$version = \$$module\::VERSION";
	$check = $version if (!$@); # Set check to version number if it exist.
    }
    return $check;
}


sub connectToDatabase($$$$$) {
    my ($db, $dbuser, $dbpasswd, $db_vendor, $host) = @_;
    if ($db_vendor eq "Oracle") {
    	return DBI->connect("DBI:Oracle:$host", $dbuser, $dbpasswd, { RaiseError => 1, PrintError => 0, AutoCommit => 0 })
	|| die "Can't open database $host:$dbuser: $DBI::errstr\n";
    }
    if ($db_vendor eq "MySQL") {
        return DBI->connect("dbi:mysql:$db:$host", $dbuser, $dbpasswd, { RaiseError => 1, PrintError => 0, AutoCommit => 0 })
        || die "Can't open database $db:$dbuser: $DBI::errstr\n";
    }
}

sub querySchemaVersion($) {
    my ($dbh) = @_;
    my ($sth,$ret);
    my @data;
    eval {
      $sth = $dbh->prepare("SELECT major, minor, patch FROM schema_version_dpm");
      if ($sth) {
        $ret = $sth->execute();
      }
    };
    eval {
      if (!$ret) {
        $sth = $dbh->prepare("SELECT major, minor, patch FROM schema_version");
        if ($sth) {
          $ret = $sth->execute();
        }
      }
    };
    if (!$ret) {
      die "Could not query dpm schema version\n";
    }

    @data = $sth->fetchrow_array();
    print $data[0]."\t".$data[1]."\t".$data[2]."\n";
    $sth->finish;
}

1;
