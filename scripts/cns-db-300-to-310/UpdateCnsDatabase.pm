#
# Adds the column 'banned' to Cns_groupinfo and the columns
# 'user_ca' and 'banned' to Cns_userinfo
# Updates schema_version table
# i.e. the update from schema 3.0.0 to 3.1.0
#

package UpdateCnsDatabase;

use DBI;
use Common;
use strict;
use warnings; 

sub update_mysql($) {
	my ($dbh_cns) = @_;

	$dbh_cns->do ("ALTER TABLE Cns_groupinfo ADD banned INTEGER");
	$dbh_cns->do ("ALTER TABLE Cns_userinfo ADD user_ca VARCHAR(255) BINARY");
	$dbh_cns->do ("ALTER TABLE Cns_userinfo ADD banned INTEGER");
	$dbh_cns->do ("CREATE INDEX linkname_idx ON Cns_symlinks(linkname(255))");

	$dbh_cns->do ("UPDATE schema_version SET major = 3, minor = 1, patch = 0");

	$dbh_cns->commit;
}

sub update_oracle($) {
        my ($dbh_cns) = @_;

	$dbh_cns->do ("ALTER TABLE Cns_groupinfo ADD banned NUMBER(10)");
	$dbh_cns->do ("ALTER TABLE Cns_userinfo ADD user_ca VARCHAR2(255)");
	$dbh_cns->do ("ALTER TABLE Cns_userinfo ADD banned NUMBER(10)");
	$dbh_cns->do ("CREATE INDEX linkname_idx ON Cns_symlinks(linkname)");

	$dbh_cns->do ("UPDATE schema_version SET major = 3, minor = 1, patch = 0");

	$dbh_cns->commit;
}

1;
