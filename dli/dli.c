/*
 * Copyright (C) 2005-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: dli.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include "Castor_limits.h"
#include "Cinit.h"
#include "Clogit.h"
#include "Cnetdb.h"
#include "Cpool_api.h"
#include "dli_server.h"
#include "dliH.h"
#include "lfc_api.h"
#include "serrno.h"
#include "DataLocationInterface.nsmap"

struct dli_srv_thread_info dli_srv_thread_info[DLI_NBTHREADS];
char func[16];
static int na_key = -1;

dli_main(main_args)
struct main_args *main_args;
{
	int c;
	void *process_request(void *);
	char *getconfent();
	int i;
	int ipool;
	char *p;
	int port;
	struct soap soap;
	int thread_index;
	struct soap *tsoap;

	strcpy (func, "dli");
	Cinitlog ("dli", LOGFILE);

	/* process command line options if any */

	while ((c = getopt (main_args->argc, main_args->argv, "l:")) != EOF) {
		switch (c) {
		case 'l':
			if (Cinitlog ("dli", optarg) < 0) {
				dlilogit (func, "Invalid logfile: %s\n", optarg);
				exit (USERR);
			}
			break;
		}
	}

	dlilogit (func, "started\n");

	/* Create a pool of threads */

	if ((ipool = Cpool_create (DLI_NBTHREADS, NULL)) < 0) {
		dlilogit (func, DLI02, "Cpool_create", sstrerror (serrno));
		return (SYERR);
	}
	for (i = 0; i < DLI_NBTHREADS; i++) {
		dli_srv_thread_info[i].s = -1;
	}

#if ! defined(_WIN32)
	signal (SIGPIPE, SIG_IGN);
	signal (SIGXFSZ, SIG_IGN);
#endif

	soap_init (&soap);
	soap.recv_timeout = DLI_TIMEOUT;
	soap.bind_flags |= SO_REUSEADDR;

	if ((p = getenv ("DLI_PORT")) || (p = getconfent ("DLI", "PORT", 0))) {
		port = atoi (p);
	} else {
		port = DLI_PORT;
	}
	if (soap_bind (&soap, NULL, port, BACKLOG) < 0) {
		dlilogit (func, DLI02, "soap_bind", strerror (soap.errnum));
		soap_done (&soap);
		return (SYERR);
	}

	/* main loop */

	while (1) {
		if (soap_accept (&soap) < 0) {
			dlilogit (func, DLI02, "soap_accept", strerror (soap.errnum));
			soap_done (&soap);
			return (1);
		}
		if ((tsoap = soap_copy (&soap)) == NULL) {
			dlilogit (func, DLI02, "soap_copy", strerror (ENOMEM));
			soap_done (&soap);
			return (1);
		}
		if ((thread_index = Cpool_next_index (ipool)) < 0) {
			dlilogit (func, DLI02, "Cpool_next_index",
			    sstrerror (serrno));
			return (SYERR);
		}
		tsoap->user = &dli_srv_thread_info[thread_index];
		if (Cpool_assign (ipool, &process_request, tsoap, 1) < 0) {
			free (tsoap);
			dli_srv_thread_info[thread_index].s = -1;
			dlilogit (func, DLI02, "Cpool_assign", sstrerror (serrno));
			return (SYERR);
		}
	}
	soap_done (&soap);
	return (0);
}

main(argc, argv)
int argc;
char **argv;
{
#if ! defined(_WIN32)
	struct main_args main_args;

	if (Cinitdaemon ("dli", NULL) < 0)
		exit (SYERR);
	main_args.argc = argc;
	main_args.argv = argv;
	exit (dli_main (&main_args));
#else
	if (Cinitservice ("dli", &dli_main))
		exit (SYERR);
#endif
}

void *
process_request(void *soap)
{
	const char *clientip;
	const char *clientname;
	size_t errlen;  
	char *errstr;
	const char *s;
	struct dli_srv_thread_info *thip = ((struct soap *)soap)->user;

	if (soap_serve ((struct soap *)soap)) {
		(void) Cgetnetaddress (-1, &((struct soap *)soap)->peer,
		    ((struct soap *)soap)->peerlen, &na_key, &clientip, &clientname, 0, 0);
		if (clientip == NULL)
			clientip = "unknown";
		if (clientname == NULL)
			clientname = "unknown";
		s = *soap_faultstring ((struct soap *)soap);
		if (s == NULL)
			s = "unknown failure";
		errlen = strlen (clientip) + 5 + strlen (s) + 1;
		errlen += strlen (clientname) + 3;
		errstr = malloc (errlen);
		if (errstr != NULL) {
			snprintf (errstr, errlen, "[%s] (%s) : %s", clientip, clientname, s);
			dlilogit (func, DLI02, "soap_serve", errstr);
			free (errstr);
		}
	}

	thip->s = -1;
	soap_end ((struct soap *)soap);
	soap_done ((struct soap *)soap);
	free (soap);
	return (NULL);
}

/*	dli_logreq - log a request */

/*	Split the message into lines so they don't exceed LOGBUFSZ-1 characters
 *	A backslash is appended to a line to be continued
 *	A continuation line is prefixed by '+ '
 */
void
dli_logreq(func, logbuf)
char *func;
char *logbuf;
{
	int n1, n2;
	char *p;
	char savechrs1[2];
	char savechrs2[2];

	n1 = LOGBUFSZ - strlen (func) - 36;
	n2 = strlen (logbuf);
	p = logbuf;
	while (n2 > n1) {
		savechrs1[0] = *(p + n1);
		savechrs1[1] = *(p + n1 + 1);
		*(p + n1) = '\\';
		*(p + n1 + 1) = '\0';
		dlilogit (func, DLI98, p);
		if (p != logbuf) {
			*p = savechrs2[0];
			*(p + 1) = savechrs2[1];
		}
		p += n1 - 2;
		savechrs2[0] = *p;
		savechrs2[1] = *(p + 1);
		*p = '+';
		*(p + 1) = ' ';
		*(p + 2) = savechrs1[0];
		*(p + 3) = savechrs1[1];
		n2 -= n1;
	}
	dlilogit (func, DLI98, p);
	if (p != logbuf) {
		*p = savechrs2[0];
		*(p + 1) = savechrs2[1];
	}
}

int ns1__listReplicas (struct soap *soap, char *type, char *data, struct ns1__listReplicasResponse *rep)
{
	const char *clienthost;
	char func[16];
	char *guid = NULL;
	int i = 0;
	char *lfn = NULL;
	char logbuf[CA_MAXPATHLEN+19];
	int nbsurls = 0;
	struct Cns_filestatg statbuf;
	struct Cns_filereplica *rep_entries;

	strcpy (func, "listReplicas");
        clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
        if (!clienthost) clienthost = "(unknown)";
	dlilogit (func, "request from %s\n", clienthost);

	if (! type) {
		soap_sender_fault (soap, "InvalidInputDataType", NULL);
		RETURN (SOAP_FAULT);
	}
	if (! data) {
		soap_sender_fault (soap, "InputData", NULL);
		RETURN (SOAP_FAULT);
	}
	if (strcmp (type, "lfn") == 0) {
		lfn = data;
		if (strncmp (lfn, "lfn:", 4) == 0) lfn += 4;
	} else if (strcmp (type, "guid") == 0) {
		guid = data;
		if (strncmp (guid, "guid:", 5) == 0) guid += 5;
	} else {
		soap_sender_fault (soap, "InvalidInputDataType", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((lfn && strlen (lfn) > CA_MAXPATHLEN) ||
	    (guid && strlen (guid) > CA_MAXGUIDLEN)) {
		soap_sender_fault (soap, "InputData", NULL);
		RETURN (SOAP_FAULT);
	}
	sprintf (logbuf, "listReplicas %s:%s", type, lfn ? lfn : guid);
	dli_logreq (func, logbuf);

	serrno = 0;
	if (lfc_statg (lfn, guid, &statbuf) < 0) {
		if (serrno == SENOSHOST || serrno == SECOMERR)
			soap_sender_fault (soap, "Catalog", NULL);
		else
			soap_sender_fault (soap, "InputData", NULL);
		RETURN (SOAP_FAULT);
	}

	serrno = 0;
	if (lfc_getreplica(lfn, guid, NULL, &nbsurls, &rep_entries) < 0) {
		if (serrno == ENOMEM)
			RETURN (SOAP_EOM);
		if (serrno == SENOSHOST || serrno == SECOMERR)
			soap_sender_fault (soap, "Catalog", NULL);
		else
			soap_sender_fault (soap, "NoURLFound", NULL);
		RETURN (SOAP_FAULT);
	}

	if (nbsurls == 0) {
		soap_sender_fault (soap, "NoURLFound", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((rep->urlList = soap_malloc (soap, sizeof(struct ArrayOfstring))) == NULL ||
	    (rep->urlList->__ptritem = soap_malloc (soap, nbsurls * sizeof(char *))) == NULL) {
		free (rep_entries);
		RETURN (SOAP_EOM);
	}

	for (i = 0; i < nbsurls; i++) {
		if ((rep->urlList->__ptritem[i] = soap_strdup (soap, (rep_entries+i)->sfn)) == NULL) {
			free(rep_entries);
			RETURN (SOAP_EOM);
		}
	}
	rep->urlList->__size = nbsurls;
	free (rep_entries);
	RETURN (SOAP_OK);
}
