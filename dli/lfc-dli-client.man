.\" @(#)$RCSfile: lfc-dli-client.man,v $ $Revision: 1.3 $ $Date: 2005/12/05 15:47:44 $ CERN James Casey
.\" Copyright (C) 2005 by CERN
.\" All rights reserved
.\"
.TH LFC-DLI-CLIENT 1 "$Date: 2005/12/05 15:47:44 $" LCG "User Commands"
.SH NAME
lfc-dli-client \- lists the replicas for a given LFN, GUID via the DLI Interface
.SH SYNOPSIS
.B lfc-dli-client
[
.BI --endpoint " endpoint"
] [
.B -h
|| 
.B --help
] [
.B -v
||
.B --verbose
]
.I logical file
.SH DESCRIPTION
.B lcg-dli-client
lists the replicas for a given LFN or GUID using the Data Location Interface
(DLI) Web service for a LFC Server.
.TP
.I logical file
a URI which specifies the Logical File Name (
.B lfn:
) or the Grid Unique IDentifier (
.B guid:
).
.TP
.I endpoint
specifies a web service endpoint to connect to. This consists of a http URL
with a hostname and an optional port number. An example is 
.B http://lfc-dteam.cern.ch:8085/

.SH NOTE
The environment variable LFC_HOST may be set instead of specifying an
endpoint to use.  Also,  DLI_PORT may be used if the DLI is running on a
non-standard port.  If DLI_PORT is not specified, the client defaults to
port 8085.
.SH EXAMPLE
.nf
.ft CW
         lfc-dli-client -e http://lfc-dteam-test:8085/\\
                lfn:/grid/dteam/dli-test
          
         sfn://lxb1533.cern.ch/storage//dteam/generated/2005-12-05/\\
                filed88f9c0b-1fba-40a4-b20e-66866d757643
         srm://lxb0724.cern.ch/pnfs/cern.ch/data/dteam/generated/\\
                2005-12-05/file9fd93cae-6400-4798-b4e9-2605bf31a7ff
         srm://lxb1921.cern.ch/dpm/cern.ch/home/dteam/generated/\\
                2005-12-05/file73c497d2-deed-46c8-b747-cda06c7708cb
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR lcg_util(3), 
.B lcg-lr(3), 
.B lfc-dli(3)
