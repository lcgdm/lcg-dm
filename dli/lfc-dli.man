.\" @(#)$RCSfile: lfc-dli.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 2005-2011 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH LFC-DLI 1 "$Date$" LCG "LFC Administrator Commands"
.SH NAME
lfc-dli \- start the LFC Data Location Interface server
.SH SYNOPSIS
.B lfc-dli
[
.BI -l " log_file"
]
.SH DESCRIPTION
.LP
The
.B lfc-dli
command starts the LFC Data Location Interface server.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
This will create a pool of threads, look for requests and pass them to the LFC.
When a request has been completed, the thread becomes idle until it is allocated
to another request.
.LP
All error messages and statistical information are kept in a log.
.LP
The Data Location Interface server listen port number can be defined on client
hosts and on the server itself in either of the following ways:
.RS
.LP
setting an environment variable DLI_PORT
.RS
.HP
setenv DLI_PORT 8085
.RE
.LP
an entry in
.B /etc/shift.conf
like:
.RS
.HP
DLI	PORT	8085
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of DLI_PORT in dli_server.h.
.LP
In the log each entry has a timestamp.
For each user request there is one message giving information about
the requestor (hostname) and one message DLI98 giving the request
itself.
The completion code of the request is also logged.
.SH OPTIONS
.TP
.BI -l " log_file"
Specifies a different path for the LFC Data Location Interface server log file.
The special value
.B syslog
will send the log messages to the system logger syslogd.
.SH FILES
.TP 1.5i
.B /var/log/dli/log
.SH EXAMPLES
.TP
Here is a small log:
.nf
05/18 16:27:21  1942 dli: started
05/18 22:10:33  1942,0 listReplicas: request from lxb2016.cern.ch
05/18 22:10:33  1942,0 listReplicas: DLI98 - listReplicas lfn:/grid/dteam/foo888
05/18 22:10:34  1942,0 listReplicas: returns 0
05/18 22:12:10  1942,0 listReplicas: request from lxb2016.cern.ch
05/18 22:12:10  1942,0 listReplicas: DLI98 - listReplicas guid:41b20a76-2287-4f81-9750-7e2eb4d199c8
05/18 22:12:11  1942,0 listReplicas: returns 0
.fi
.SH SEE ALSO
.BR Clogit(3) ,
.B lfcdaemon(1)
