Summary: Disk Pool Manager
Name: DPM-mysql
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
License: Apache-2.0
Prefix: @PREFIX@
BuildRequires: @BUILDREQUIRES@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.

%package -n dpm
Summary: CLI for DPM/DPNS/RFIO
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0
AutoReqProv: yes
Obsoletes: DPM-client
%description -n dpm
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This provides the CLIs and the corresponding man pages.

%package -n dpm-libs
Summary: Client shared libraries for Disk Pool Manager
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0
AutoReqProv: yes
Obsoletes: DPM-client
%description -n dpm-libs
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This provides the client shared libraries. 

%package -n dpm-devel
Summary: Disk Pool Manager development libraries and header files
Group: grid/lcg
Requires: dpm-libs >= @VERSION@, lcgdm-devel >= 1.8.0
AutoReqProv: yes
Obsoletes: DPM-client
%description -n dpm-devel
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This provides the client header files, archive library and API man pages.

%package -n perl-dpm
Summary: Perl interface to Disk Pool Manager
Group: grid/lcg
BuildRequires: @REQUIRES.PERL@
Requires: dpm-libs >= @VERSION@, @REQUIRES.PERL@
AutoReqProv: yes
Obsoletes: DPM-interfaces, DPM-interfaces2
%description -n perl-dpm
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This is the perl interface to the DPM built with swig. 
It requires Perl 5.

%package -n python-dpm
Summary: Python interfaces to Disk Pool Manager
Group: grid/lcg
BuildRequires: python >= @PYTHON.VERSION@
Requires: dpm-libs >= @VERSION@, python >= @PYTHON.VERSION@
AutoReqProv: yes
Obsoletes: DPM-interfaces, DPM-interfaces2
%description -n python-dpm
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.
This is the Python interface to the DPM built with swig.
It requires Python >= 2.3.

%package -n DPM-server-mysql
Summary: DPM MySQL Server
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, @REQUIRES.MYSQL@, e2fsprogs
Conflicts: DPM-server-oracle
AutoReqProv: yes
%description -n DPM-server-mysql
DPM server with MySQL database backend

%package -n DPM-copy-server-mysql
Summary: DPM COPY MySQL Server
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, @REQUIRES.MYSQL@, dpm-libs >= @VERSION@, CGSI_gSOAP_2.7 >= 1.3.4, CGSI_gSOAP_2.7-voms >= 1.3.4
Conflicts: DPM-copy-server-oracle
AutoReqProv: yes
%description -n DPM-copy-server-mysql
DPM COPY server with MySQL database backend

%package -n DPM-name-server-mysql
Summary: DPNS MySQL Server
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, @REQUIRES.MYSQL@, @REQUIRES.PERL.DBD.MYSQL@, e2fsprogs
Conflicts: DPM-name-server-oracle
AutoReqProv: yes
%description -n DPM-name-server-mysql
DPNS server with MySQL database backend

%package -n DPM-srm-server-mysql
Summary: SRM MySQL Servers
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, @REQUIRES.MYSQL@, dpm-libs >= @VERSION@, CGSI_gSOAP_2.7 >= 1.3.4, CGSI_gSOAP_2.7-voms >= 1.3.4, e2fsprogs
Conflicts: DPM-srm-server-oracle
AutoReqProv: yes
%description -n DPM-srm-server-mysql
SRM servers with MySQL database backend

%package -n DPM-rfio-server
Summary: DPM RFIO server
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, dpm-libs >= @VERSION@
AutoReqProv: yes
%description -n DPM-rfio-server
RFIO server for the Disk Pool Manager

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-mysql dpm ${EXTRA_CONFIGURE_OPTIONS}
make


%install 
rm -rf $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}/var/log/dpns
mkdir -p ${RPM_BUILD_ROOT}/var/log/dpm
mkdir -p ${RPM_BUILD_ROOT}/var/log/dpmcopy
mkdir -p ${RPM_BUILD_ROOT}/etc/logrotate.d
mkdir -p ${RPM_BUILD_ROOT}/etc/init.d
mkdir -p ${RPM_BUILD_ROOT}/etc/sysconfig
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/etc
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/multiple-domains
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/DPM-migration
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/DPM-migration/virtualIds
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/change-dpm-name
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-support-srmv2.2
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-secondary-groups
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-db-310-to-320
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/cns-db-300-to-310
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM/upgrades
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/man/man3

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

# For upgrades (all components)
cp -p scripts/upgrades/* ${RPM_BUILD_ROOT}%{prefix}/share/DPM/upgrades

# For the DPM
cp -p dpm/dpm.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/dpm
cp -p dpm/rc.dpm ${RPM_BUILD_ROOT}/etc/init.d/dpm
cp -p dpm/dpm.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/dpm.templ

# For the DPM COPY backend
cp -p dpmcopy/dpmcopyd.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/dpmcopyd
cp -p dpmcopy/rc.dpmcopyd ${RPM_BUILD_ROOT}/etc/init.d/dpmcopyd
cp -p dpmcopy/dpmcopyd.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/dpmcopyd.templ

# For the DPNS
cp -p scripts/multiple-domains/README ${RPM_BUILD_ROOT}%{prefix}/share/DPM/multiple-domains/README
cp -p scripts/multiple-domains/updateDomainName ${RPM_BUILD_ROOT}%{prefix}/share/DPM/multiple-domains/updateDomainName
cp -p scripts/multiple-domains/UpdateDpmDatabase.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/multiple-domains/UpdateDpmDatabase.pm
cp -p scripts/multiple-domains/Common.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/multiple-domains/Common.pm
cp -p scripts/DPM-migration/virtualIds/README ${RPM_BUILD_ROOT}%{prefix}/share/DPM/DPM-migration/virtualIds/README
cp -p scripts/DPM-migration/virtualIds/create-uid-gid-mappings.sh ${RPM_BUILD_ROOT}%{prefix}/share/DPM/DPM-migration/virtualIds/create-uid-gid-mappings.sh
cp -p scripts/DPM-migration/virtualIds/migrate-mysql-schema-to-2-2-0.sql ${RPM_BUILD_ROOT}%{prefix}/share/DPM/DPM-migration/virtualIds/migrate-mysql-schema-to-2-2-0.sql
cp -p scripts/change-dpm-name/changeDpmName ${RPM_BUILD_ROOT}%{prefix}/share/DPM/change-dpm-name/changeDpmName
cp -p scripts/change-dpm-name/Common.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/change-dpm-name/Common.pm
cp -p scripts/change-dpm-name/UpdateDpmDatabase.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/change-dpm-name/UpdateDpmDatabase.pm
cp -p scripts/change-dpm-name/README ${RPM_BUILD_ROOT}%{prefix}/share/DPM/change-dpm-name/README
cp -p scripts/dpm-support-srmv2.2/Common.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-support-srmv2.2/Common.pm
cp -p scripts/dpm-support-srmv2.2/README ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-support-srmv2.2/README
cp -p scripts/dpm-support-srmv2.2/UpdateDpmDatabase.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-support-srmv2.2/UpdateDpmDatabase.pm
cp -p scripts/dpm-support-srmv2.2/dpm_support_srmv2.2 ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-support-srmv2.2/dpm_support_srmv2.2
cp -p scripts/dpm-secondary-groups/Common.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-secondary-groups/Common.pm
cp -p scripts/dpm-secondary-groups/README ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-secondary-groups/README
cp -p scripts/dpm-secondary-groups/UpdateDpmDatabase.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-secondary-groups/UpdateDpmDatabase.pm
cp -p scripts/dpm-secondary-groups/dpm_secondary_groups ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-secondary-groups/dpm_secondary_groups
cp -p scripts/dpm-db-310-to-320/Common.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-db-310-to-320/Common.pm
cp -p scripts/dpm-db-310-to-320/README ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-db-310-to-320/README
cp -p scripts/dpm-db-310-to-320/UpdateDpmDatabase.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-db-310-to-320/UpdateDpmDatabase.pm
cp -p scripts/dpm-db-310-to-320/dpm_db_310_to_320 ${RPM_BUILD_ROOT}%{prefix}/share/DPM/dpm-db-310-to-320/dpm_db_310_to_320
cp -p scripts/cns-db-300-to-310/Common.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/cns-db-300-to-310/Common.pm
cp -p scripts/cns-db-300-to-310/README ${RPM_BUILD_ROOT}%{prefix}/share/DPM/cns-db-300-to-310/README
cp -p scripts/cns-db-300-to-310/UpdateCnsDatabase.pm ${RPM_BUILD_ROOT}%{prefix}/share/DPM/cns-db-300-to-310/UpdateCnsDatabase.pm
cp -p scripts/cns-db-300-to-310/cns_db_300_to_310 ${RPM_BUILD_ROOT}%{prefix}/share/DPM/cns-db-300-to-310/cns_db_300_to_310
cp -p ns/dpnsdaemon.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/dpnsdaemon
cp -p ns/rc.dpnsdaemon ${RPM_BUILD_ROOT}/etc/init.d/dpnsdaemon
cp -p ns/dpnsdaemon.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/dpnsdaemon.templ

# For the SRM server
cp -p srmv1/srmv1.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/srmv1
cp -p srmv1/rc.srmv1 ${RPM_BUILD_ROOT}/etc/init.d/srmv1
cp -p srmv1/srmv1.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/srmv1.templ
cp -p srmv2/srmv2.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/srmv2
cp -p srmv2/rc.srmv2 ${RPM_BUILD_ROOT}/etc/init.d/srmv2
cp -p srmv2/srmv2.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/srmv2.templ
cp -p srmv2.2/srmv2.2.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/srmv2.2
cp -p srmv2.2/rc.srmv2.2 ${RPM_BUILD_ROOT}/etc/init.d/srmv2.2
cp -p srmv2.2/srmv2.2.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/srmv2.2.templ

# For RFIO
cp -p rfio/rfiod.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/rfiod
cp -p rfio/rc.rfiod ${RPM_BUILD_ROOT}/etc/init.d/rfiod
cp -p rfio/rfiod.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/rfiod.templ

# For client backward compatibility
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python
ln -sf ../python@PYTHON.VERSION@/site-packages/_dpm.so ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python/_dpm.so
ln -sf ../python@PYTHON.VERSION@/site-packages/dpm.py ${RPM_BUILD_ROOT}%{prefix}/%{_lib}/python/dpm.py

%clean

%files -n dpm
%defattr(-,root,root)
%{prefix}/bin/dpm-addfs
%{prefix}/bin/dpm-addpool
%{prefix}/bin/dpm-drain
%{prefix}/bin/dpm-getspacemd
%{prefix}/bin/dpm-getspacetokens
%{prefix}/bin/dpm-modifyfs
%{prefix}/bin/dpm-modifypool
%{prefix}/bin/dpm-ping
%{prefix}/bin/dpm-qryconf
%{prefix}/bin/dpm-register
%{prefix}/bin/dpm-releasespace
%{prefix}/bin/dpm-replicate
%{prefix}/bin/dpm-reservespace
%{prefix}/bin/dpm-rmfs
%{prefix}/bin/dpm-rmpool
%{prefix}/bin/dpm-updatespace
%{prefix}/bin/dpns-chgrp
%{prefix}/bin/dpns-chmod
%{prefix}/bin/dpns-chown
%{prefix}/bin/dpns-entergrpmap
%{prefix}/bin/dpns-enterusrmap
%{prefix}/bin/dpns-getacl
%{prefix}/bin/dpns-listgrpmap
%{prefix}/bin/dpns-listusrmap
%{prefix}/bin/dpns-ln
%{prefix}/bin/dpns-ls
%{prefix}/bin/dpns-mkdir
%{prefix}/bin/dpns-modifygrpmap
%{prefix}/bin/dpns-modifyusrmap
%{prefix}/bin/dpns-ping
%{prefix}/bin/dpns-rename
%{prefix}/bin/dpns-rm
%{prefix}/bin/dpns-rmgrpmap
%{prefix}/bin/dpns-rmusrmap
%{prefix}/bin/dpns-setacl
%{prefix}/bin/rfcat
%{prefix}/bin/rfchmod
%{prefix}/bin/rfcp
%{prefix}/bin/rfdf
%{prefix}/bin/rfdir
%{prefix}/bin/rfmkdir
%{prefix}/bin/rfrename
%{prefix}/bin/rfrm
%{prefix}/bin/rfstat
%{prefix}/share/man/man1/dpm-addfs.1
%{prefix}/share/man/man1/dpm-addpool.1
%{prefix}/share/man/man1/dpm-drain.1
%{prefix}/share/man/man1/dpm-getspacemd.1
%{prefix}/share/man/man1/dpm-getspacetokens.1
%{prefix}/share/man/man1/dpm-modifyfs.1
%{prefix}/share/man/man1/dpm-modifypool.1
%{prefix}/share/man/man1/dpm-ping.1
%{prefix}/share/man/man1/dpm-qryconf.1
%{prefix}/share/man/man1/dpm-register.1
%{prefix}/share/man/man1/dpm-releasespace.1
%{prefix}/share/man/man1/dpm-replicate.1
%{prefix}/share/man/man1/dpm-reservespace.1
%{prefix}/share/man/man1/dpm-rmfs.1
%{prefix}/share/man/man1/dpm-rmpool.1
%{prefix}/share/man/man1/dpm-updatespace.1
%{prefix}/share/man/man1/dpns-chgrp.1
%{prefix}/share/man/man1/dpns-chmod.1
%{prefix}/share/man/man1/dpns-chown.1
%{prefix}/share/man/man1/dpns-entergrpmap.1
%{prefix}/share/man/man1/dpns-enterusrmap.1
%{prefix}/share/man/man1/dpns-getacl.1
%{prefix}/share/man/man1/dpns-listgrpmap.1
%{prefix}/share/man/man1/dpns-listusrmap.1
%{prefix}/share/man/man1/dpns-ln.1
%{prefix}/share/man/man1/dpns-ls.1
%{prefix}/share/man/man1/dpns-mkdir.1
%{prefix}/share/man/man1/dpns-modifygrpmap.1
%{prefix}/share/man/man1/dpns-modifyusrmap.1
%{prefix}/share/man/man1/dpns-ping.1
%{prefix}/share/man/man1/dpns-rename.1
%{prefix}/share/man/man1/dpns-rm.1
%{prefix}/share/man/man1/dpns-rmgrpmap.1
%{prefix}/share/man/man1/dpns-rmusrmap.1
%{prefix}/share/man/man1/dpns-setacl.1
%{prefix}/share/man/man1/rfcat.1
%{prefix}/share/man/man1/rfchmod.1
%{prefix}/share/man/man1/rfcp.1
%{prefix}/share/man/man1/rfdf.1
%{prefix}/share/man/man1/rfdir.1
%{prefix}/share/man/man1/rfmkdir.1
%{prefix}/share/man/man1/rfrename.1
%{prefix}/share/man/man1/rfrm.1
%{prefix}/share/man/man1/rfstat.1

%files -n dpm-libs
%{prefix}/%{_lib}/libdpm.so.*

%files -n dpm-devel
%{prefix}/include/dpm/Castor_limits.h
%{prefix}/include/dpm/Cnetdb.h
%{prefix}/include/dpm/Cns_api.h
%{prefix}/include/dpm/Cns_constants.h
%{prefix}/include/dpm/Cns_struct.h
%{prefix}/include/dpm/dpm_api.h
%{prefix}/include/dpm/dpm_constants.h
%{prefix}/include/dpm/dpm_struct.h
%{prefix}/include/dpm/dpns_api.h
%{prefix}/include/dpm/osdep.h
%{prefix}/include/dpm/rfcntl.h
%{prefix}/include/dpm/rfio.h
%{prefix}/include/dpm/rfio_api.h
%{prefix}/include/dpm/rfio_constants.h
%{prefix}/include/dpm/rfio_errno.h
%{prefix}/include/dpm/serrno.h
%{prefix}/include/dpm/u64subr.h
%{prefix}/%{_lib}/libdpm.a
%{prefix}/%{_lib}/libdpm.so
%{prefix}/share/man/man3/dpm_abortfiles.3
%{prefix}/share/man/man3/dpm_abortreq.3
%{prefix}/share/man/man3/dpm_addfs.3
%{prefix}/share/man/man3/dpm_addpool.3
%{prefix}/share/man/man3/dpm_copy.3
%{prefix}/share/man/man3/dpm_delreplica.3
%{prefix}/share/man/man3/dpm_extendfilelife.3
%{prefix}/share/man/man3/dpm_get.3
%{prefix}/share/man/man3/dpm_getifcevers.3
%{prefix}/share/man/man3/dpm_getpoolfs.3
%{prefix}/share/man/man3/dpm_getpools.3
%{prefix}/share/man/man3/dpm_getprotocols.3
%{prefix}/share/man/man3/dpm_getreqid.3
%{prefix}/share/man/man3/dpm_getreqsummary.3
%{prefix}/share/man/man3/dpm_getspacemd.3
%{prefix}/share/man/man3/dpm_getspacetoken.3
%{prefix}/share/man/man3/dpm_getstatus_copyreq.3
%{prefix}/share/man/man3/dpm_getstatus_getreq.3
%{prefix}/share/man/man3/dpm_getstatus_putreq.3
%{prefix}/share/man/man3/dpm_modifyfs.3
%{prefix}/share/man/man3/dpm_modifypool.3
%{prefix}/share/man/man3/dpm_ping.3
%{prefix}/share/man/man3/dpm_put.3
%{prefix}/share/man/man3/dpm_putdone.3
%{prefix}/share/man/man3/dpm_releasespace.3
%{prefix}/share/man/man3/dpm_relfiles.3
%{prefix}/share/man/man3/dpm_reservespace.3
%{prefix}/share/man/man3/dpm_rm.3
%{prefix}/share/man/man3/dpm_rmfs.3
%{prefix}/share/man/man3/dpm_rmpool.3
%{prefix}/share/man/man3/dpm_seterrbuf.3
%{prefix}/share/man/man3/dpm_updatespace.3
%{prefix}/share/man/man3/dpns_aborttrans.3
%{prefix}/share/man/man3/dpns_access.3
%{prefix}/share/man/man3/dpns_accessr.3
%{prefix}/share/man/man3/dpns_addreplica.3
%{prefix}/share/man/man3/dpns_addreplicax.3
%{prefix}/share/man/man3/dpns_chdir.3
%{prefix}/share/man/man3/dpns_chmod.3
%{prefix}/share/man/man3/dpns_chown.3
%{prefix}/share/man/man3/dpns_closedir.3
%{prefix}/share/man/man3/dpns_creat.3
%{prefix}/share/man/man3/dpns_delete.3
%{prefix}/share/man/man3/dpns_delreplica.3
%{prefix}/share/man/man3/dpns_delreplicasbysfn.3
%{prefix}/share/man/man3/dpns_endsess.3
%{prefix}/share/man/man3/dpns_endtrans.3
%{prefix}/share/man/man3/dpns_entergrpmap.3
%{prefix}/share/man/man3/dpns_enterusrmap.3
%{prefix}/share/man/man3/dpns_getacl.3
%{prefix}/share/man/man3/dpns_getcwd.3
%{prefix}/share/man/man3/dpns_getgrpbygid.3
%{prefix}/share/man/man3/dpns_getgrpbygids.3
%{prefix}/share/man/man3/dpns_getgrpbynam.3
%{prefix}/share/man/man3/dpns_getgrpmap.3
%{prefix}/share/man/man3/dpns_getidmap.3
%{prefix}/share/man/man3/dpns_getifcevers.3
%{prefix}/share/man/man3/dpns_getreplica.3
%{prefix}/share/man/man3/dpns_getreplicax.3
%{prefix}/share/man/man3/dpns_getusrbynam.3
%{prefix}/share/man/man3/dpns_getusrbyuid.3
%{prefix}/share/man/man3/dpns_getusrmap.3
%{prefix}/share/man/man3/dpns_lchown.3
%{prefix}/share/man/man3/dpns_listrep4gc.3
%{prefix}/share/man/man3/dpns_listreplica.3
%{prefix}/share/man/man3/dpns_listreplicax.3
%{prefix}/share/man/man3/dpns_listrepset.3
%{prefix}/share/man/man3/dpns_lstat.3
%{prefix}/share/man/man3/dpns_mkdir.3
%{prefix}/share/man/man3/dpns_modifygrpmap.3
%{prefix}/share/man/man3/dpns_modifyusrmap.3
%{prefix}/share/man/man3/dpns_modreplica.3
%{prefix}/share/man/man3/dpns_modreplicax.3
%{prefix}/share/man/man3/dpns_opendir.3
%{prefix}/share/man/man3/dpns_opendirxg.3
%{prefix}/share/man/man3/dpns_ping.3
%{prefix}/share/man/man3/dpns_readdir.3
%{prefix}/share/man/man3/dpns_readdirg.3
%{prefix}/share/man/man3/dpns_readdirx.3
%{prefix}/share/man/man3/dpns_readdirxp.3
%{prefix}/share/man/man3/dpns_readdirxr.3
%{prefix}/share/man/man3/dpns_readlink.3
%{prefix}/share/man/man3/dpns_registerfiles.3
%{prefix}/share/man/man3/dpns_rename.3
%{prefix}/share/man/man3/dpns_rewinddir.3
%{prefix}/share/man/man3/dpns_rmdir.3
%{prefix}/share/man/man3/dpns_rmgrpmap.3
%{prefix}/share/man/man3/dpns_rmusrmap.3
%{prefix}/share/man/man3/dpns_setacl.3
%{prefix}/share/man/man3/dpns_setatime.3
%{prefix}/share/man/man3/dpns_seterrbuf.3
%{prefix}/share/man/man3/dpns_setfsize.3
%{prefix}/share/man/man3/dpns_setfsizec.3
%{prefix}/share/man/man3/dpns_setptime.3
%{prefix}/share/man/man3/dpns_setratime.3
%{prefix}/share/man/man3/dpns_setrltime.3
%{prefix}/share/man/man3/dpns_setrstatus.3
%{prefix}/share/man/man3/dpns_setrtype.3
%{prefix}/share/man/man3/dpns_startsess.3
%{prefix}/share/man/man3/dpns_starttrans.3
%{prefix}/share/man/man3/dpns_stat.3
%{prefix}/share/man/man3/dpns_statg.3
%{prefix}/share/man/man3/dpns_statr.3
%{prefix}/share/man/man3/dpns_symlink.3
%{prefix}/share/man/man3/dpns_umask.3
%{prefix}/share/man/man3/dpns_undelete.3
%{prefix}/share/man/man3/dpns_unlink.3
%{prefix}/share/man/man3/dpns_utime.3
%{prefix}/share/man/man3/rfio_access.3
%{prefix}/share/man/man3/rfio_chmod.3
%{prefix}/share/man/man3/rfio_chown.3
%{prefix}/share/man/man3/rfio_close.3
%{prefix}/share/man/man3/rfio_closedir.3
%{prefix}/share/man/man3/rfio_fchmod.3
%{prefix}/share/man/man3/rfio_fclose.3
%{prefix}/share/man/man3/rfio_feof.3
%{prefix}/share/man/man3/rfio_ferror.3
%{prefix}/share/man/man3/rfio_fflush.3
%{prefix}/share/man/man3/rfio_fileno.3
%{prefix}/share/man/man3/rfio_fopen.3
%{prefix}/share/man/man3/rfio_fopen64.3
%{prefix}/share/man/man3/rfio_fread.3
%{prefix}/share/man/man3/rfio_fseek.3
%{prefix}/share/man/man3/rfio_fseeko64.3
%{prefix}/share/man/man3/rfio_fstat.3
%{prefix}/share/man/man3/rfio_fstat64.3
%{prefix}/share/man/man3/rfio_ftell.3
%{prefix}/share/man/man3/rfio_ftello64.3
%{prefix}/share/man/man3/rfio_fwrite.3
%{prefix}/share/man/man3/rfio_lockf.3
%{prefix}/share/man/man3/rfio_lockf64.3
%{prefix}/share/man/man3/rfio_lseek.3
%{prefix}/share/man/man3/rfio_lseek64.3
%{prefix}/share/man/man3/rfio_lstat.3
%{prefix}/share/man/man3/rfio_lstat64.3
%{prefix}/share/man/man3/rfio_mkdir.3
%{prefix}/share/man/man3/rfio_mstat.3
%{prefix}/share/man/man3/rfio_mstat64.3
%{prefix}/share/man/man3/rfio_msymlink.3
%{prefix}/share/man/man3/rfio_munlink.3
%{prefix}/share/man/man3/rfio_open.3
%{prefix}/share/man/man3/rfio_open64.3
%{prefix}/share/man/man3/rfio_opendir.3
%{prefix}/share/man/man3/rfio_pclose.3
%{prefix}/share/man/man3/rfio_perror.3
%{prefix}/share/man/man3/rfio_popen.3
%{prefix}/share/man/man3/rfio_pread.3
%{prefix}/share/man/man3/rfio_preseek.3
%{prefix}/share/man/man3/rfio_preseek64.3
%{prefix}/share/man/man3/rfio_pwrite.3
%{prefix}/share/man/man3/rfio_rcp.3
%{prefix}/share/man/man3/rfio_read.3
%{prefix}/share/man/man3/rfio_readdir.3
%{prefix}/share/man/man3/rfio_readlink.3
%{prefix}/share/man/man3/rfio_rename.3
%{prefix}/share/man/man3/rfio_rewinddir.3
%{prefix}/share/man/man3/rfio_rmdir.3
%{prefix}/share/man/man3/rfio_serror.3
%{prefix}/share/man/man3/rfio_setbufsize.3
%{prefix}/share/man/man3/rfio_stat.3
%{prefix}/share/man/man3/rfio_stat64.3
%{prefix}/share/man/man3/rfio_statfs.3
%{prefix}/share/man/man3/rfio_statfs64.3
%{prefix}/share/man/man3/rfio_symlink.3
%{prefix}/share/man/man3/rfio_unlink.3
%{prefix}/share/man/man3/rfio_write.3
%{prefix}/share/man/man3/rfioreadopt.3
%{prefix}/share/man/man3/rfiosetopt.3

%files -n perl-dpm
%defattr(-,root,root)
%attr(755,root,root) @PERLDIR@/dpm.so
%attr(755,root,root) @PERLDIR@/dpm.pm

%files -n python-dpm
%dir %{prefix}/%{_lib}/python
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/_dpm.so
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/dpm.py
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/_dpm2.so
%attr(755,root,root) %{prefix}/%{_lib}/python@PYTHON.VERSION@/site-packages/dpm2.py
%attr(755,root,root) %{prefix}/%{_lib}/python/_dpm.so
%attr(755,root,root) %{prefix}/%{_lib}/python/dpm.py
%attr(755,root,root) %{prefix}/bin/dpm-listspaces
%attr(644,root,root) %{prefix}/share/man/man1/dpm-listspaces.1
%attr(644,root,root) %{prefix}/share/man/man3/dpm_python.3
%attr(644,root,root) %{prefix}/share/man/man3/dpm2_python.3

%files -n DPM-server-mysql
%defattr(-,root,root)
/var/log/dpm
%attr(755, root, root) %{prefix}/bin/dpm
%attr(755, root, root) %{prefix}/bin/dpm-buildfsv
%attr(755, root, root) %{prefix}/bin/dpm-shutdown
%attr(600, root, root) %{prefix}/etc/DPMCONFIG.templ
%{prefix}/share/DPM/create_dpm_tables_mysql.sql
%attr(755, root, root) %{prefix}/share/DPM/upgrades/dpm-db-300-to-310
%attr(755, root, root) %{prefix}/share/DPM/upgrades/dpm-db-310-to-320
%attr(755, root, root) %{prefix}/share/DPM/upgrades/dpm-db-320-to-330
%{prefix}/share/man/man1/dpm.1
%{prefix}/share/man/man1/dpm-buildfsv.1
%{prefix}/share/man/man1/dpm-shutdown.1
%attr(644, root, root) /etc/logrotate.d/dpm
%attr(755, root, root) /etc/init.d/dpm
%attr(644, root, root) /etc/sysconfig/dpm.templ

%files -n DPM-copy-server-mysql
%defattr(-,root,root)
/var/log/dpmcopy
%attr(755, root, root) %{prefix}/bin/dpmcopyd
%{prefix}/share/man/man1/dpmcopyd.1
%attr(644, root, root) /etc/logrotate.d/dpmcopyd
%attr(755, root, root) /etc/init.d/dpmcopyd
%attr(644, root, root) /etc/sysconfig/dpmcopyd.templ

%files -n DPM-name-server-mysql
%defattr(-,root,root)
/var/log/dpns
%attr(755, root, root) %{prefix}/bin/dpnsdaemon
%attr(755, root, root) %{prefix}/bin/dpns-arguspoll
%attr(755, root, root) %{prefix}/bin/dpns-shutdown
%attr(600, root, root) %{prefix}/etc/NSCONFIG.templ
%{prefix}/share/DPM/create_dpns_tables_mysql.sql
%{prefix}/share/DPM/multiple-domains/README
%{prefix}/share/DPM/multiple-domains/updateDomainName
%{prefix}/share/DPM/multiple-domains/UpdateDpmDatabase.pm
%{prefix}/share/DPM/multiple-domains/Common.pm
%{prefix}/share/DPM/DPM-migration/virtualIds/README
%{prefix}/share/DPM/DPM-migration/virtualIds/create-uid-gid-mappings.sh
%{prefix}/share/DPM/DPM-migration/virtualIds/migrate-mysql-schema-to-2-2-0.sql
%{prefix}/share/DPM/change-dpm-name/changeDpmName
%{prefix}/share/DPM/change-dpm-name/Common.pm
%{prefix}/share/DPM/change-dpm-name/UpdateDpmDatabase.pm
%{prefix}/share/DPM/change-dpm-name/README
%{prefix}/share/DPM/dpm-support-srmv2.2/Common.pm
%{prefix}/share/DPM/dpm-support-srmv2.2/README
%{prefix}/share/DPM/dpm-support-srmv2.2/UpdateDpmDatabase.pm
%attr(755, root, root) %{prefix}/share/DPM/dpm-support-srmv2.2/dpm_support_srmv2.2
%{prefix}/share/DPM/dpm-secondary-groups/Common.pm
%{prefix}/share/DPM/dpm-secondary-groups/README
%{prefix}/share/DPM/dpm-secondary-groups/UpdateDpmDatabase.pm
%attr(755, root, root) %{prefix}/share/DPM/dpm-secondary-groups/dpm_secondary_groups
%{prefix}/share/DPM/dpm-db-310-to-320/Common.pm
%{prefix}/share/DPM/dpm-db-310-to-320/README
%{prefix}/share/DPM/dpm-db-310-to-320/UpdateDpmDatabase.pm
%attr(755, root, root) %{prefix}/share/DPM/dpm-db-310-to-320/dpm_db_310_to_320
%{prefix}/share/DPM/cns-db-300-to-310/Common.pm
%{prefix}/share/DPM/cns-db-300-to-310/README
%{prefix}/share/DPM/cns-db-300-to-310/UpdateCnsDatabase.pm
%attr(755, root, root) %{prefix}/share/DPM/cns-db-300-to-310/cns_db_300_to_310
%{prefix}/share/DPM/upgrades/Common.pm
%attr(755, root, root) %{prefix}/share/DPM/upgrades/cns-db-300-to-310
%{prefix}/share/man/man1/dpnsdaemon.1
%{prefix}/share/man/man1/dpns-arguspoll.1
%{prefix}/share/man/man1/dpns-shutdown.1
%attr(644, root, root) /etc/logrotate.d/dpnsdaemon
%attr(755, root, root) /etc/init.d/dpnsdaemon
%attr(644, root, root) /etc/sysconfig/dpnsdaemon.templ

%files -n DPM-srm-server-mysql
%defattr(-,root,root)
%attr(755, root, root) %{prefix}/bin/srmv1
%attr(755, root, root) %{prefix}/bin/srmv2
%attr(755, root, root) %{prefix}/bin/srmv2.2
%{prefix}/share/man/man1/srmv1.1
%{prefix}/share/man/man1/srmv2.1
%{prefix}/share/man/man1/srmv2.2.1
%attr(644, root, root) /etc/logrotate.d/srmv1
%attr(755, root, root) /etc/init.d/srmv1
%attr(644, root, root) /etc/sysconfig/srmv1.templ
%attr(644, root, root) /etc/logrotate.d/srmv2
%attr(755, root, root) /etc/init.d/srmv2
%attr(644, root, root) /etc/sysconfig/srmv2.templ
%attr(644, root, root) /etc/logrotate.d/srmv2.2
%attr(755, root, root) /etc/init.d/srmv2.2
%attr(644, root, root) /etc/sysconfig/srmv2.2.templ

%files -n DPM-rfio-server
%defattr(-,root,root)
%{prefix}/bin/rfiod
%{prefix}/share/man/man1/rfiod.1
%attr(644, root, root) /etc/logrotate.d/rfiod
%attr(755, root, root) /etc/init.d/rfiod
%attr(644, root, root) /etc/sysconfig/rfiod.templ

%post -n dpm-libs
if [ `uname -m` != x86_64 -o \( `uname -m` = x86_64 -a "%{_lib}" = lib64 \) ]; then
   if [ `grep -c ^%{prefix}/%{_lib} /etc/ld.so.conf` = 0 ]; then
      echo "%{prefix}/%{_lib}" >> /etc/ld.so.conf
   fi
fi

[ -x "/sbin/ldconfig" ] && /sbin/ldconfig

%postun -n dpm-libs
[ -x "/sbin/ldconfig" ] && /sbin/ldconfig

%post -n DPM-server-mysql
/sbin/chkconfig --add dpm

echo "The DPM is now installed."
echo "Please use the <install_dir>/etc/DPMCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the DPM daemon, use /etc/sysconfig/dpm.templ to create the /etc/sysconfig/dpm file and modify it if necessary."
echo " "
echo "Then, to start/stop the DPM server, use the following command :"
echo " > service dpm start|stop"
echo " "

%post -n DPM-copy-server-mysql
/sbin/chkconfig --add dpmcopyd

echo "The DPM COPY backend server is now installed."
echo "Please use the <install_dir>/etc/DPMCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the DPMCOPYD daemon, use /etc/sysconfig/dpmcopyd.templ"
echo "to create the /etc/sysconfig/dpmcopyd file and modify it where needed."
echo " "
echo "To start/stop the DPMCOPYD daemon, use the following command :"
echo " > service dpmcopyd start|stop"
echo " "

%post -n DPM-name-server-mysql
/sbin/chkconfig --add dpnsdaemon

echo "The DPM Name Server is now installed."
echo "Please use the <install_dir>/etc/NSCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the DPM Name Server daemon, use /etc/sysconfig/dpnsdaemon.templ to create the /etc/sysconfig/dpnsdaemon file and modify it if necessary."
echo " "
echo "Then, to start/stop the DPM Name Server, use the following command :"
echo " > service dpnsdaemon start|stop"
echo " "

%post -n DPM-srm-server-mysql
/sbin/chkconfig --add srmv1
/sbin/chkconfig --add srmv2
/sbin/chkconfig --add srmv2.2

echo "The SRM servers are now installed."
echo "Please use the <install_dir>/etc/DPMCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the SRM servers, use /etc/sysconfig/srmv1.templ, /etc/sysconfig/srmv2.templ and /etc/sysconfig/srmv2.2.templ"
echo "to create the /etc/sysconfig/srmv1, /etc/sysconfig/srmv2 and /etc/sysconfig/srmv2.2 files and modify them where needed."
echo " "
echo "To start/stop the SRM servers, use the following commands :"
echo " > service srmv1 start|stop"
echo " > service srmv2 start|stop"
echo " > service srmv2.2 start|stop"
echo " "

%post -n DPM-rfio-server
/sbin/chkconfig --add rfiod

echo "The DPM RFIO server is now installed."
echo "Before running the DPM RFIO server, use /etc/sysconfig/rfiod to create the /etc/sysconfig/rfiod file and modify it if necessary."
echo " "
echo "To start/stop the DPM RFIO server, use the following command :"
echo " > service rfiod start|stop"
echo " "

