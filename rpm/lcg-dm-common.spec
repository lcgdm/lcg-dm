Summary: LCG Data Management common libraries and man pages. 
Name: lcg-dm-common
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
License: Apache-2.0
Prefix: @PREFIX@
BuildRequires: @BUILDREQUIRES@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
This package package contains common libraries that are useful for the LCG Data Management components, the LFC (LCG File Catalog) and the DPM (Disk Pool Manager).

%package -n lcgdm-libs
Summary: LCG Data Management common shared libraries
Group: grid/lcg
Requires: @REQUIRES.VOMS@
AutoReqProv: yes
Obsoletes: lcg-dm-common
%description -n lcgdm-libs
The lcgdm-libs package contains common libraries that are useful for the LCG Data Management components, the LFC (LCG File Catalog) and the DPM (Disk Pool Manager).

%package -n lcgdm-devel
Summary: LCG Data Management common development libraries and header files.
Group: grid/lcg
Requires: lcgdm-libs >= @VERSION@
AutoReqProv: yes
Obsoletes: lcg-dm-common
%description -n lcgdm-devel
The lcgdm-devel package contains common development libraries and man pages that are useful for the LCG Data Management components, the LFC (LCG File Catalog) and the DPM (Disk Pool Manager).

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-client-only dm ${EXTRA_CONFIGURE_OPTIONS}
make

%install 
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/%{_lib}

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

%clean
rm -rf ${RPM_BUILD_ROOT}

%files -n lcgdm-libs
%defattr(-,root,root)
%{prefix}/%{_lib}/libCsec_plugin_GSI*.so*
%{prefix}/%{_lib}/libCsec_plugin_ID.so*
%{prefix}/%{_lib}/liblcgdm.so.*

%files -n lcgdm-devel
%defattr(-,root,root)
%attr(644,root,root) %{prefix}/include/lcgdm/Castor_limits.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cgetopt.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cglobals.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cgrp.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cinit.h
%attr(644,root,root) %{prefix}/include/lcgdm/Clogit.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cmutex.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cnetdb.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cpool_api.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cpwd.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cregexp.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cregexp_magic.h
%attr(644,root,root) %{prefix}/include/lcgdm/Csched_api.h
%attr(644,root,root) %{prefix}/include/lcgdm/Csched_flags.h
%attr(644,root,root) %{prefix}/include/lcgdm/Csec_api.h
%attr(644,root,root) %{prefix}/include/lcgdm/Csec_common.h
%attr(644,root,root) %{prefix}/include/lcgdm/Csec_constants.h
%attr(644,root,root) %{prefix}/include/lcgdm/Csnprintf.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cthread_api.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cthread_env.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cthread_flags.h
%attr(644,root,root) %{prefix}/include/lcgdm/Cthread_typedef.h
%attr(644,root,root) %{prefix}/include/lcgdm/log.h
%attr(644,root,root) %{prefix}/include/lcgdm/marshall.h
%attr(644,root,root) %{prefix}/include/lcgdm/net.h
%attr(644,root,root) %{prefix}/include/lcgdm/osdep.h
%attr(644,root,root) %{prefix}/include/lcgdm/serrno.h
%attr(644,root,root) %{prefix}/include/lcgdm/socket_timeout.h
%attr(644,root,root) %{prefix}/include/lcgdm/trace.h
%attr(644,root,root) %{prefix}/include/lcgdm/u64subr.h
%{prefix}/%{_lib}/liblcgdm.a
%{prefix}/%{_lib}/liblcgdm.so
%attr(644,root,root) %{prefix}/share/man/man4/Castor_limits.4
%attr(644,root,root) %{prefix}/share/man/man3/Cnetdb.3
%attr(644,root,root) %{prefix}/share/man/man3/Cthread.3
%attr(644,root,root) %{prefix}/share/man/man3/serrno.3
%attr(644,root,root) %{prefix}/share/man/man3/Cgetopt.3
%attr(644,root,root) %{prefix}/share/man/man3/Cglobals.3
%attr(644,root,root) %{prefix}/share/man/man3/Cgrp.3
%attr(644,root,root) %{prefix}/share/man/man3/Cinitlog.3
%attr(644,root,root) %{prefix}/share/man/man3/Clogit.3
%attr(644,root,root) %{prefix}/share/man/man3/Cmutex.3
%attr(644,root,root) %{prefix}/share/man/man3/Cpool.3
%attr(644,root,root) %{prefix}/share/man/man3/Cpwd.3
%attr(644,root,root) %{prefix}/share/man/man3/Csched.3
%attr(644,root,root) %{prefix}/share/man/man3/Csec_api.3
%attr(644,root,root) %{prefix}/share/man/man3/Cvlogit.3
%attr(644,root,root) %{prefix}/share/man/man3/getconfent.3
%attr(644,root,root) %{prefix}/share/man/man3/log.3
%attr(644,root,root) %{prefix}/share/man/man3/netclose.3
%attr(644,root,root) %{prefix}/share/man/man3/netread.3
%attr(644,root,root) %{prefix}/share/man/man3/netwrite.3

%post -n lcgdm-libs
if [ `uname -m` != x86_64 -o \( `uname -m` = x86_64 -a "%{_lib}" = lib64 \) ]; then
   if [ `grep -c ^%{prefix}/%{_lib} /etc/ld.so.conf` = 0 ]; then
      echo "%{prefix}/%{_lib}" >> /etc/ld.so.conf
   fi
fi

[ -x "/sbin/ldconfig" ] && /sbin/ldconfig

%postun -n lcgdm-libs
[ -x "/sbin/ldconfig" ] && /sbin/ldconfig
