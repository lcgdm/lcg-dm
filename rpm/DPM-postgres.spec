Summary: Disk Pool Manager
Name: DPM-postgres
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
AutoReqProv: yes
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
License: Apache-2.0
Prefix: @PREFIX@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
Light weight Disk Pool Manager offering SRMv1, SRMv2 and socket interfaces.

%package -n DPM-server-postgres
Summary: DPM PostgreSQL Server
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, postgresql-libs, e2fsprogs
Conflicts: DPM-server-mysql
AutoReqProv: yes
%description -n DPM-server-postgres
DPM server with PostgreSQL database backend

%package -n DPM-copy-server-postgres
Summary: DPM COPY PostgreSQL Server
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, postgresql-libs, dpm-libs >= @VERSION@, CGSI_gSOAP_2.7 >= 1.3.4, CGSI_gSOAP_2.7-voms >= 1.3.4
Conflicts: DPM-copy-server-mysql
AutoReqProv: yes
%description -n DPM-copy-server-postgres
DPM COPY server with PostgreSQL database backend

%package -n DPM-name-server-postgres
Summary: DPNS PostgreSQL Server
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, postgresql-libs, e2fsprogs
Conflicts: DPM-name-server-mysql
AutoReqProv: yes
%description -n DPM-name-server-postgres
DPNS server with PostgreSQL database backend

%package -n DPM-srm-server-postgres
Summary: SRM PostgreSQL Servers
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, postgresql-libs, dpm-libs >= @VERSION@, CGSI_gSOAP_2.7 >= 1.3.4, CGSI_gSOAP_2.7-voms >= 1.3.4, e2fsprogs
Conflicts: DPM-srm-server-mysql
AutoReqProv: yes
%description -n DPM-srm-server-postgres
SRM servers with PostgreSQL database backend

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-postgres dpm ${EXTRA_CONFIGURE_OPTIONS}
make


%install 
rm -rf $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}/var/log/dpns
mkdir -p ${RPM_BUILD_ROOT}/var/log/dpm
mkdir -p ${RPM_BUILD_ROOT}/var/log/dpmcopy
mkdir -p ${RPM_BUILD_ROOT}/etc/logrotate.d
mkdir -p ${RPM_BUILD_ROOT}/etc/init.d
mkdir -p ${RPM_BUILD_ROOT}/etc/sysconfig
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/etc
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/DPM

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

# For the DPM
cp -p dpm/dpm.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/dpm
cp -p dpm/rc.dpm ${RPM_BUILD_ROOT}/etc/init.d/dpm
cp -p dpm/dpm.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/dpm.templ

# For the DPM COPY backend
cp -p dpmcopy/dpmcopyd.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/dpmcopyd
cp -p dpmcopy/rc.dpmcopyd ${RPM_BUILD_ROOT}/etc/init.d/dpmcopyd
cp -p dpmcopy/dpmcopyd.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/dpmcopyd.templ

# For the DPNS
cp -p ns/dpnsdaemon.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/dpnsdaemon
cp -p ns/rc.dpnsdaemon ${RPM_BUILD_ROOT}/etc/init.d/dpnsdaemon
cp -p ns/dpnsdaemon.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/dpnsdaemon.templ

# For the SRM server
cp -p srmv1/srmv1.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/srmv1
cp -p srmv1/rc.srmv1 ${RPM_BUILD_ROOT}/etc/init.d/srmv1
cp -p srmv1/srmv1.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/srmv1.templ
cp -p srmv2/srmv2.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/srmv2
cp -p srmv2/rc.srmv2 ${RPM_BUILD_ROOT}/etc/init.d/srmv2
cp -p srmv2/srmv2.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/srmv2.templ
cp -p srmv2.2/srmv2.2.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/srmv2.2
cp -p srmv2.2/rc.srmv2.2 ${RPM_BUILD_ROOT}/etc/init.d/srmv2.2
cp -p srmv2.2/srmv2.2.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/srmv2.2.templ

%clean
rm -rf ${RPM_BUILD_ROOT}

%files -n DPM-server-postgres
%defattr(-,root,root)
/var/log/dpm
%attr(755, root, root) %{prefix}/bin/dpm
%attr(755, root, root) %{prefix}/bin/dpm-buildfsv
%attr(755, root, root) %{prefix}/bin/dpm-shutdown
%attr(600, root, root) %{prefix}/etc/DPMCONFIG.templ
%{prefix}/share/DPM/create_dpm_tables_postgres.sql
%{prefix}/share/man/man1/dpm.1
%{prefix}/share/man/man1/dpm-buildfsv.1
%{prefix}/share/man/man1/dpm-shutdown.1
%attr(644, root, root) /etc/logrotate.d/dpm
%attr(755, root, root) /etc/init.d/dpm
%attr(644, root, root) /etc/sysconfig/dpm.templ

%files -n DPM-copy-server-postgres
%defattr(-,root,root)
/var/log/dpmcopy
%attr(755, root, root) %{prefix}/bin/dpmcopyd
%{prefix}/share/man/man1/dpmcopyd.1
%attr(644, root, root) /etc/logrotate.d/dpmcopyd
%attr(755, root, root) /etc/init.d/dpmcopyd
%attr(644, root, root) /etc/sysconfig/dpmcopyd.templ

%files -n DPM-name-server-postgres
%defattr(-,root,root)
/var/log/dpns
%attr(755, root, root) %{prefix}/bin/dpnsdaemon
%attr(755, root, root) %{prefix}/bin/dpns-arguspoll
%attr(755, root, root) %{prefix}/bin/dpns-shutdown
%attr(600, root, root) %{prefix}/etc/NSCONFIG.templ
%{prefix}/share/DPM/create_dpns_tables_postgres.sql
%{prefix}/share/man/man1/dpnsdaemon.1
%{prefix}/share/man/man1/dpns-arguspoll.1
%{prefix}/share/man/man1/dpns-shutdown.1
%attr(644, root, root) /etc/logrotate.d/dpnsdaemon
%attr(755, root, root) /etc/init.d/dpnsdaemon
%attr(644, root, root) /etc/sysconfig/dpnsdaemon.templ

%files -n DPM-srm-server-postgres
%defattr(-,root,root)
%attr(755, root, root) %{prefix}/bin/srmv1
%attr(755, root, root) %{prefix}/bin/srmv2
%attr(755, root, root) %{prefix}/bin/srmv2.2
%{prefix}/share/man/man1/srmv1.1
%{prefix}/share/man/man1/srmv2.1
%{prefix}/share/man/man1/srmv2.2.1
%attr(644, root, root) /etc/logrotate.d/srmv1
%attr(755, root, root) /etc/init.d/srmv1
%attr(644, root, root) /etc/sysconfig/srmv1.templ
%attr(644, root, root) /etc/logrotate.d/srmv2
%attr(755, root, root) /etc/init.d/srmv2
%attr(644, root, root) /etc/sysconfig/srmv2.templ
%attr(644, root, root) /etc/logrotate.d/srmv2.2
%attr(755, root, root) /etc/init.d/srmv2.2
%attr(644, root, root) /etc/sysconfig/srmv2.2.templ

%post -n DPM-server-postgres
/sbin/chkconfig --add dpm

echo "The DPM is now installed."
echo "Please use the <install_dir>/etc/DPMCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the DPM daemon, use /etc/sysconfig/dpm.templ to create the /etc/sysconfig/dpm file and modify it if necessary."
echo " "
echo "Then, to start/stop the DPM server, use the following command :"
echo " > service dpm start|stop"
echo " "

%post -n DPM-copy-server-postgres
/sbin/chkconfig --add dpmcopyd

echo "The DPM COPY backend server is now installed."
echo "Please use the <install_dir>/etc/DPMCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the DPMCOPYD daemon, use /etc/sysconfig/dpmcopyd.templ"
echo "to create the /etc/sysconfig/dpmcopyd file and modify it where needed."
echo " "
echo "To start/stop the DPMCOPYD daemon, use the following command :"
echo " > service dpmcopyd start|stop"
echo " "

%post -n DPM-name-server-postgres
/sbin/chkconfig --add dpnsdaemon

echo "The DPM Name Server is now installed."
echo "Please use the <install_dir>/etc/NSCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the DPM Name Server daemon, use /etc/sysconfig/dpnsdaemon.templ to create the /etc/sysconfig/dpnsdaemon file and modify it if necessary."
echo " "
echo "Then, to start/stop the DPM Name Server, use the following command :"
echo " > service dpnsdaemon start|stop"
echo " "

%post -n DPM-srm-server-postgres
/sbin/chkconfig --add srmv1
/sbin/chkconfig --add srmv2
/sbin/chkconfig --add srmv2.2

echo "The SRM servers are now installed."
echo "Please use the <install_dir>/etc/DPMCONFIG.templ template to create your own configuration file with the appropriate values."
echo " "
echo "Before running the SRM servers, use /etc/sysconfig/srmv1.templ, /etc/sysconfig/srmv2.templ and /etc/sysconfig/srmv2.2.templ"
echo "to create the /etc/sysconfig/srmv1, /etc/sysconfig/srmv2 and /etc/sysconfig/srmv2.2 files and modify them if necessary."
echo " "
echo "To start/stop the SRM servers, use the following commands :"
echo " > service srmv1 start|stop"
echo " > service srmv2 start|stop"
echo " > service srmv2.2 start|stop"
echo " "
