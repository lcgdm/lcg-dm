Summary: LCG File Catalog
Name: LFC-oracle
Version: @VERSION@
Release: @RELEASE@@SECURITY@
Source0: LCG-DM-%{version}.tar.gz
Group: grid/lcg
BuildRoot: %{_builddir}/%{name}-%{version}-root
License: Apache-2.0
Prefix: @PREFIX@

%define __spec_install_post %{nil}
%define debug_package %{nil}
%define _unpackaged_files_terminate_build  %{nil}

%description
The LCG File Catalog (LFC) allows to store files in a File System looking like structure.
It allows you to create symbolic links to any file or directory stored in the LFC, as well as replicas.

%package -n LFC-server-oracle
Summary: LFC Server for an Oracle database backend
Group: grid/lcg
Requires: lcgdm-libs >= 1.8.0, e2fsprogs
Conflicts: LFC-server-mysql
AutoReq: no
AutoProv: yes
Obsoletes: CSEC
#Provides: LFC-server
%description -n LFC-server-oracle
LFC server with Oracle database backend

%prep
# '%setup -q' with renaming the source directory
rm -rf %{name}-%{version}
tar -xzf %{SOURCE0}
mv LCG-DM-%{version} %{name}-%{version}
%setup -D -T

%build
./configure --with-oracle lfc ${EXTRA_CONFIGURE_OPTIONS}
make

%install 
rm -rf $RPM_BUILD_ROOT

make prefix=${RPM_BUILD_ROOT}%{prefix} install
make prefix=${RPM_BUILD_ROOT}%{prefix} install.man

mkdir -p ${RPM_BUILD_ROOT}/var/log/lfc
mkdir -p ${RPM_BUILD_ROOT}/var/log/dli
mkdir -p ${RPM_BUILD_ROOT}/etc/logrotate.d
cp ns/lfcdaemon.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/lfcdaemon
cp dli/lfc-dli.logrotate ${RPM_BUILD_ROOT}/etc/logrotate.d/lfc-dli

mkdir -p ${RPM_BUILD_ROOT}/etc/sysconfig
cp ns/lfcdaemon.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/lfcdaemon.templ
cp dli/lfc-dli.conf.templ ${RPM_BUILD_ROOT}/etc/sysconfig/lfc-dli.templ

mkdir -p ${RPM_BUILD_ROOT}/etc/init.d
cp ns/rc.lfcdaemon ${RPM_BUILD_ROOT}/etc/init.d/lfcdaemon
cp dli/rc.lfc-dli ${RPM_BUILD_ROOT}/etc/init.d/lfc-dli

mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/doc/LFC-server-oracle-%{version}
cp doc/lfc/README ${RPM_BUILD_ROOT}%{prefix}/share/doc/LFC-server-oracle-%{version}
cp doc/lfc/INSTALL-server-oracle ${RPM_BUILD_ROOT}%{prefix}/share/doc/LFC-server-oracle-%{version}

# schema is already copied by the 'install' target
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/LFC/db-deployment
cp scripts/README ${RPM_BUILD_ROOT}%{prefix}/share/LFC/db-deployment/README
cp scripts/create-tablespaces-lfc ${RPM_BUILD_ROOT}%{prefix}/share/LFC/db-deployment/create-tablespaces-lfc
cp scripts/create-user-lfc ${RPM_BUILD_ROOT}%{prefix}/share/LFC/db-deployment/create-user-lfc
cp scripts/create-schema-lfc ${RPM_BUILD_ROOT}%{prefix}/share/LFC/db-deployment/create-schema-lfc

mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/LFC/cns-db-300-to-310
cp -p scripts/cns-db-300-to-310/Common.pm ${RPM_BUILD_ROOT}%{prefix}/share/LFC/cns-db-300-to-310/Common.pm
cp -p scripts/cns-db-300-to-310/README ${RPM_BUILD_ROOT}%{prefix}/share/LFC/cns-db-300-to-310/README
cp -p scripts/cns-db-300-to-310/UpdateCnsDatabase.pm ${RPM_BUILD_ROOT}%{prefix}/share/LFC/cns-db-300-to-310/UpdateCnsDatabase.pm
cp -p scripts/cns-db-300-to-310/cns_db_300_to_310 ${RPM_BUILD_ROOT}%{prefix}/share/LFC/cns-db-300-to-310/cns_db_300_to_310

# LCG info provider
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/libexec
cp scripts/lcg-info-provider-lfc ${RPM_BUILD_ROOT}%{prefix}/libexec

# Upgrade scripts
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/share/LFC/upgrades
cp -p scripts/upgrades/* ${RPM_BUILD_ROOT}%{prefix}/share/LFC/upgrades

%clean
rm -rf ${RPM_BUILD_ROOT}

%files -n LFC-server-oracle
%defattr(-,root,root)
%attr(755,root,root) /var/log/lfc
%attr(755,root,root) /var/log/dli
%attr(755,root,root) %{prefix}/bin/lfcdaemon
%attr(755,root,root) %{prefix}/bin/lfc-arguspoll
%attr(755,root,root) %{prefix}/bin/lfc-shutdown
%attr(755,root,root) %{prefix}/bin/lfc-dli
%attr(755,root,root) %{prefix}/bin/lfc-dli-client
%attr(600,root,root) %{prefix}/etc/NSCONFIG.templ
%attr(644,root,root) %{prefix}/share/LFC/create_lfc_tables_oracle.sql
%attr(755,root,root) %{prefix}/share/LFC/db-deployment/create-tablespaces-lfc
%attr(755,root,root) %{prefix}/share/LFC/db-deployment/create-user-lfc
%attr(755,root,root) %{prefix}/share/LFC/db-deployment/create-schema-lfc
%attr(755,root,root) %{prefix}/share/LFC/db-deployment/README
%{prefix}/share/LFC/cns-db-300-to-310/Common.pm
%{prefix}/share/LFC/cns-db-300-to-310/README
%{prefix}/share/LFC/cns-db-300-to-310/UpdateCnsDatabase.pm
%attr(755,root,root) %{prefix}/share/LFC/cns-db-300-to-310/cns_db_300_to_310
%{prefix}/share/LFC/upgrades/Common.pm
%attr(755, root, root) %{prefix}/share/LFC/upgrades/cns-db-300-to-310
%attr(644,root,root) %{prefix}/share/doc/LFC-server-oracle-%{version}/README
%attr(644,root,root) %{prefix}/share/doc/LFC-server-oracle-%{version}/INSTALL-server-oracle
%attr(755,root,root) %{prefix}/libexec/lcg-info-provider-lfc
%attr(644,root,root) %{prefix}/share/man/man1/lfcdaemon.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-arguspoll.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-shutdown.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-dli.1
%attr(644,root,root) %{prefix}/share/man/man1/lfc-dli-client.1
%attr(644,root,root) /etc/logrotate.d/lfcdaemon
%attr(755,root,root) /etc/init.d/lfcdaemon
%attr(644,root,root) /etc/sysconfig/lfcdaemon.templ
%attr(644,root,root) /etc/logrotate.d/lfc-dli
%attr(755,root,root) /etc/init.d/lfc-dli
%attr(644,root,root) /etc/sysconfig/lfc-dli.templ

%post -n LFC-server-oracle
/sbin/chkconfig --add lfcdaemon
/sbin/chkconfig --add lfc-dli

