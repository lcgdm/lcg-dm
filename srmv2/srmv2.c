/*
 * Copyright (C) 2004-2011 by CERN/IT/GD/CT
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2.c,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud";
#endif /* not lint */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "Castor_limits.h"
#include "Cinit.h"
#include "Clogit.h"
#include "Cnetdb.h"
#include "Cpool_api.h"
#include "cgsi_plugin.h"
#include "dpm.h"
#include "dpm_server.h"
#include "patchlevel.h"
#include "serrno.h"
#include "srm_server.h"
#include "srmv2H.h"
#include "srmSoapBinding.nsmap"

struct srm_srv_thread_info srm_srv_thread_info[SRMV2_NBTHREADS];
char func[16];
char localdomain[CA_MAXHOSTNAMELEN+1];
char localhost[CA_MAXHOSTNAMELEN+1];
int nb_supported_protocols;
char **supported_protocols;
extern int Cdomainname (char *, int);
static int na_key = -1;

srm_main(main_args)
struct main_args *main_args;
{
	int c;
	void *process_request(void *);
	char *dpmconfigfile = NULL;
	int flags;
	char *getconfent();
	int i;
	int ipool;
	char *p;
	int port;
	struct soap soap;
	int thread_index;
	struct soap *tsoap;

	strcpy (func, "srmv2");
	Cinitlog ("srmv2", LOGFILE);

	/* process command line options if any */

	while ((c = getopt (main_args->argc, main_args->argv, "c:l:")) != EOF) {
		switch (c) {
		case 'c':
			dpmconfigfile = optarg;
			break;
		case 'l':
			if (Cinitlog ("srmv2", optarg) < 0) {
				srmlogit (func, "Invalid logfile: %s\n", optarg);
				exit (USERR);
			}
			break;
		}
	}

	srmlogit (func, "started (srmv2 %s-%d)\n", BASEVERSION, PATCHLEVEL);
	gethostname (localhost, CA_MAXHOSTNAMELEN+1);
	if (Cdomainname (localdomain, sizeof(localdomain)) < 0) {
		srmlogit (func, "Unable to get local domain name\n");
		return (SYERR);
	}
	if (strchr (localhost, '.') == NULL) {
		strcat (localhost, ".");
		strcat (localhost, localdomain);
	}

	/* Get list of supported protocols */

	if ((nb_supported_protocols = get_supported_protocols (&supported_protocols)) < 0) {
		srmlogit (func, SRM02, "get_supported_protocols", strerror (ENOMEM));
		return (SYERR);
	}

	/* get DB login info from the Disk Pool Manager server config file */

	if (dpm_get_dbconf (dpmconfigfile) < 0)
		return (CONFERR);

	(void) dpm_init_dbpkg ();

	/* Create a pool of threads */

	if ((ipool = Cpool_create (SRMV2_NBTHREADS, NULL)) < 0) {
		srmlogit (func, SRM02, "Cpool_create", sstrerror (serrno));
		return (SYERR);
	}
	for (i = 0; i < SRMV2_NBTHREADS; i++) {
		srm_srv_thread_info[i].s = -1;
		srm_srv_thread_info[i].dbfd.idx = i;
	}

#if ! defined(_WIN32)
	signal (SIGPIPE, SIG_IGN);
	signal (SIGXFSZ, SIG_IGN);
#endif

	soap_init (&soap);
	soap.recv_timeout = SRM_TIMEOUT;
	flags = CGSI_OPT_DELEG_FLAG;
#ifdef VIRTUAL_ID
	flags |= CGSI_OPT_DISABLE_MAPPING;
#endif
	soap_register_plugin_arg (&soap, server_cgsi_plugin, &flags);
	soap.bind_flags |= SO_REUSEADDR;

	if ((p = getenv ("SRMV2_PORT")) || (p = getconfent ("SRMV2", "PORT", 0))) {
		port = atoi (p);
	} else {
		port = SRMV2_PORT;
	}
	if (soap_bind (&soap, NULL, port, BACKLOG) < 0) {
		srmlogit (func, SRM02, "soap_bind", strerror (soap.errnum));
		soap_done (&soap);
		return (SYERR);
	}

	/* main loop */

	while (1) {
		if (soap_accept (&soap) < 0) {
			srmlogit (func, SRM02, "soap_accept", strerror (soap.errnum));
			soap_done (&soap);
			return (1);
		}
		if ((tsoap = soap_copy (&soap)) == NULL) {
			srmlogit (func, SRM02, "soap_copy", strerror (ENOMEM));
			soap_done (&soap);
			return (1);
		}
		if ((thread_index = Cpool_next_index (ipool)) < 0) {
			srmlogit (func, SRM02, "Cpool_next_index",
			    sstrerror (serrno));
			return (SYERR);
		}
		tsoap->user = &srm_srv_thread_info[thread_index];
		if (Cpool_assign (ipool, &process_request, tsoap, 1) < 0) {
			free (tsoap);
			srm_srv_thread_info[thread_index].s = -1;
			srmlogit (func, SRM02, "Cpool_assign", sstrerror (serrno));
			return (SYERR);
		}
	}
	soap_done (&soap);
	return (0);
}

main(argc, argv)
int argc;
char **argv;
{
#if ! defined(_WIN32)
	struct main_args main_args;

	if (Cinitdaemon ("srmv2", NULL) < 0)
		exit (SYERR);
	main_args.argc = argc;
	main_args.argv = argv;
	exit (srm_main (&main_args));
#else
	if (Cinitservice ("srmv2", &srm_main))
		exit (SYERR);
#endif
}

void *
process_request(void *soap)
{
	const char *clientip;
	const char *clientname;
	size_t errlen;
	char *errstr;
	const char *s;
	struct srm_srv_thread_info *thip = ((struct soap *)soap)->user;

	if (soap_serve ((struct soap *)soap)) {
		(void) Cgetnetaddress (-1, &((struct soap *)soap)->peer,
		    ((struct soap *)soap)->peerlen, &na_key, &clientip, &clientname, 0, 0);
		if (clientip == NULL)
			clientip = "unknown";
		if (clientname == NULL)
			clientname = "unknown";
		s = *soap_faultstring ((struct soap *)soap);
		if (s == NULL)
			s = "unknown failure";
		errlen = strlen (clientip) + 5 + strlen (s) + 1;
		errlen += strlen (clientname) + 3;
		errstr = malloc (errlen);
		if (errstr != NULL) {
			snprintf (errstr, errlen, "[%s] (%s) : %s", clientip, clientname, s);
			srmlogit (func, SRM02, "soap_serve", errstr);
			free (errstr);
		}
	}

	thip->s = -1;
	soap_end ((struct soap *)soap);
	soap_done ((struct soap *)soap);
	free (soap);
	return (NULL);
}

int
serrno2statuscode (int err)
{
	switch (err) {
	case ENOENT:
	case ENOTDIR:
	case EISDIR:
	case ENAMETOOLONG:
		return (SRM_USCOREINVALID_USCOREPATH);
	case EACCES:
		return (SRM_USCOREUNAUATHORIZED_USCOREACCESS);
	case EEXIST:
		return (SRM_USCOREDUPLICATION_USCOREERROR);
	case EINVAL:
		return (SRM_USCOREINVALID_USCOREREQUEST);
	case ENOSPC:
		return (SRM_USCORENO_USCOREFREE_USCORESPACE);
	case SETIMEDOUT:
	case SEINTERNAL:
	case SECOMERR:
		return (SRM_USCOREINTERNAL_USCOREERROR);
	case SEOPNOTSUP:
	case SEPROTONOTSUP:
		return (SRM_USCORENOT_USCORESUPPORTED);
	default:
		return (SRM_USCOREFAILURE);
	}
}
