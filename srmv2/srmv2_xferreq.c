/*
 * Copyright (C) 2004-2011 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2_xferreq.c,v $ $Revision$ $Date$ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <string.h>
#include <sys/types.h>
#include <uuid/uuid.h>
#include "Cnetdb.h"
#include "dpm.h"
#include "dpm_api.h"
#include "dpm_server.h"
#include "dpns_api.h"
#include "serrno.h"
#include "srm_server.h"
#include "srmv2H.h"
extern char localdomain[CA_MAXHOSTNAMELEN+1];
extern int nb_supported_protocols;
extern char **supported_protocols;
char *Cencode_groups (int, gid_t *, char *, size_t);
static int na_key = -1;

/*			Data Transfer Functions				*/

dpmstatus2statuscode (int status, int isfile, time_t pintime)
{
	switch (status & 0xF000) {
	case DPM_SUCCESS:
		return (SRM_USCORESUCCESS);
	case DPM_QUEUED:
		return (SRM_USCOREREQUEST_USCOREQUEUED);
	case DPM_ACTIVE:
		return (SRM_USCOREREQUEST_USCOREINPROGRESS);
	case DPM_READY:
	case DPM_RUNNING:
		return ((pintime > time (0)) ? SRM_USCOREFILE_USCOREPINNED :
		    SRM_USCOREFILE_USCOREIN_USCORECACHE);
	case DPM_DONE:
		if (isfile)
			return ((pintime > time (0)) ? SRM_USCOREFILE_USCOREPINNED :
			    SRM_USCOREFILE_USCOREIN_USCORECACHE);
		else
			return (SRM_USCOREDONE);
	case DPM_ABORTED:
		return (SRM_USCOREABORTED);
	case DPM_EXPIRED:
		return (SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED);
	case DPM_RELEASED:
		return (SRM_USCORERELEASED);
	case DPM_TO_BE_RECALLED:
		return (SRM_USCOREREQUEST_USCOREINPROGRESS);
	default:
		return (serrno2statuscode (status & 0xFFF));
	}
}

marshall_CPR (struct soap *soap, struct ns1__TCopyRequestFileStatus *repfilep, struct dpm_copy_filereq *cpr_entry)
{
	time_t current_time = time(0);

	if ((repfilep->fileSize = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
		repfilep->fileSize->value = cpr_entry->actual_size;
	if ((repfilep->fromSURL->value =
	    soap_strdup (soap, cpr_entry->from_surl)) == NULL)
		return (-1);
	if ((repfilep->remainingPinTime = soap_malloc (soap, sizeof(struct ns1__TLifeTimeInSeconds))))
		repfilep->remainingPinTime->value = (cpr_entry->f_lifetime > current_time) ?
		    cpr_entry->f_lifetime - current_time : 0;
	repfilep->status->statusCode =
	    dpmstatus2statuscode (cpr_entry->status, 1, cpr_entry->f_lifetime);
	if (*cpr_entry->errstring)
		repfilep->status->explanation = soap_strdup (soap, cpr_entry->errstring);
	if ((repfilep->toSURL->value =
	    soap_strdup (soap, cpr_entry->to_surl)) == NULL)
		return (-1);
	return (0);
}

marshall_GFR (struct soap *soap, struct dpm_dbfd *dbfd, struct ns1__TGetRequestFileStatus *repfilep, struct dpm_get_filereq *gfr_entry)
{
	time_t current_time = time(0);
	char *p;
	char turl[CA_MAXSFNLEN+1];

	if ((repfilep->fileSize = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
		repfilep->fileSize->value = gfr_entry->actual_size;
	if ((repfilep->fromSURLInfo->value =
	    soap_strdup (soap, gfr_entry->from_surl)) == NULL)
		return (-1);
	if ((repfilep->remainingPinTime = soap_malloc (soap, sizeof(struct ns1__TLifeTimeInSeconds))))
		repfilep->remainingPinTime->value = (gfr_entry->lifetime > current_time) ?
		    gfr_entry->lifetime - current_time : 0;
	repfilep->status->statusCode =
	    dpmstatus2statuscode (gfr_entry->status, 1, gfr_entry->lifetime);
	if (*gfr_entry->errstring)
		repfilep->status->explanation = soap_strdup (soap, gfr_entry->errstring);
	gfr_to_turl (dbfd, gfr_entry, turl, sizeof(turl));
	if (*turl) {
		if ((repfilep->transferURL = soap_malloc (soap, sizeof(struct ns1__TTURL))))
			repfilep->transferURL->value = soap_strdup (soap, turl);
	}
	return (0);
}

marshall_PFR (struct soap *soap, struct dpm_dbfd *dbfd, struct ns1__TPutRequestFileStatus *repfilep, struct dpm_put_filereq *pfr_entry)
{
	time_t current_time = time(0);
	char *p;
	char turl[CA_MAXSFNLEN+1];

	if ((repfilep->fileSize = soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
		repfilep->fileSize->value =
		    (pfr_entry->actual_size == LLONG_MAX) ? 0 : pfr_entry->actual_size;
	if ((repfilep->siteURL->value =
	    soap_strdup (soap, pfr_entry->to_surl)) == NULL)
		return (-1);
	if ((repfilep->remainingPinTime = soap_malloc (soap, sizeof(struct ns1__TLifeTimeInSeconds))))
		repfilep->remainingPinTime->value = (pfr_entry->lifetime > current_time) ?
		    pfr_entry->lifetime - current_time : 0;
	repfilep->status->statusCode =
	    dpmstatus2statuscode (pfr_entry->status, 1, pfr_entry->lifetime);
	if (*pfr_entry->errstring)
		repfilep->status->explanation = soap_strdup (soap, pfr_entry->errstring);
	pfr_to_turl (dbfd, pfr_entry, turl, sizeof(turl));
	if (*turl) {
		if ((repfilep->transferURL = soap_malloc (soap, sizeof(struct ns1__TTURL))))
			repfilep->transferURL->value = soap_strdup (soap, turl);
	}
	return (0);
}

int
ns1__srmPrepareToGet (struct soap *soap, struct ns1__srmPrepareToGetRequest *req, struct ns1__srmPrepareToGetResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	time_t curtime;
	struct dpm_req dpm_req;
	char **fqan;
	char func[16];
	struct dpm_get_filereq gfr_entry;
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	char logbuf[CA_MAXSFNLEN+25];
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbreqfiles;
	struct ns1__TGetRequestFileStatus *repfilep;
	struct ns1__srmPrepareToGetResponse *repp;
	struct ns1__TGetFileRequest *reqfilep;
	char selected_protocol[CA_MAXPROTOLEN+1];
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	u_signed64 unique_id;
	uuid_t uuid;
	char *voname;

	strcpy (func, "PrepareToGet");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	memset ((char *) &dpm_req, 0, sizeof(dpm_req));
	dpm_req.ctime = time (0);

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmPrepareToGetResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->requestToken = NULL;
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmPrepareToGetResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (req->arrayOfFileRequests)
		nbreqfiles = req->arrayOfFileRequests->__sizegetRequestArray;
	else {
		nbreqfiles = 0;
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "arrayOfFileRequests is required");
	}
	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTGetRequestFileStatus))) == NULL ||
	    (nbreqfiles > 0 && (repp->arrayOfFileStatuses->getStatusArray =
		soap_malloc (soap, nbreqfiles * sizeof(struct ns1__TGetRequestFileStatus *))) == NULL))
		RETURN (SOAP_EOM);

	repp->arrayOfFileStatuses->__sizegetStatusArray = nbreqfiles;
	for (i = 0; i < nbreqfiles; i++) {
		if ((repp->arrayOfFileStatuses->getStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TGetRequestFileStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfFileStatuses->getStatusArray[i], 0, sizeof(struct ns1__TGetRequestFileStatus));
		if ((repp->arrayOfFileStatuses->getStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		repp->arrayOfFileStatuses->getStatusArray[i]->status->explanation = NULL;
	}

	dpm_req.r_uid = uid;
	dpm_req.r_gid = gid;
	strcpy (dpm_req.client_dn, clientdn);
	if (! Cencode_groups (nbgids, gids, dpm_req.groups, sizeof(dpm_req.groups))) {
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "Too many FQANs in proxy");
	}
	strcpy (dpm_req.clienthost, clienthost);
	dpm_req.r_type = 'G';

	/* Negociate protocol */

	if (! req->arrayOfTransferProtocols)
		strcpy (selected_protocol, "gsiftp");
	else {
		*selected_protocol = '\0';
		for (i = 0; i < req->arrayOfTransferProtocols->__sizestringArray; i++) {
			if (! req->arrayOfTransferProtocols->stringArray[i]) continue;
			for (j = 0; j < nb_supported_protocols; j++) {
				if (strcmp (req->arrayOfTransferProtocols->stringArray[i], supported_protocols[j]) == 0) {
					strcpy (selected_protocol, req->arrayOfTransferProtocols->stringArray[i]);
					break;
				}
			}
			if (*selected_protocol) break;
		}
		if (! *selected_protocol) {
			dpm_req.status = DPM_FAILED | SEPROTONOTSUP;
			strcpy (dpm_req.errstring, "Protocol not supported");
		}
	}
	if (req->userRequestDescription) {
		if (strlen (req->userRequestDescription) >= 256) {
			dpm_req.status = DPM_FAILED | EINVAL;
			strcpy (dpm_req.errstring, "Invalid user request description");
		}
		strncpy (dpm_req.u_token, req->userRequestDescription, 255);
	}
	dpm_req.retrytime = req->totalRetryTime ? req->totalRetryTime->value : 0;

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->arrayOfFileStatuses = NULL;
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURN (SOAP_OK);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	if (dpm_unique_id (&thip->dbfd, &unique_id) < 0) {
		repp->arrayOfFileStatuses = NULL;
		repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
		repp->returnStatus->explanation = soap_strdup (soap, "Can't get req uniqueid");
		RETURN (SOAP_OK);
	}
	dpm_req.r_ordinal = unique_id & 0xFFFFFFFF;
	uuid_generate (uuid);
	uuid_unparse (uuid, dpm_req.r_token);
	sprintf (logbuf, "PrepareToGet %d %s", dpm_req.r_ordinal, dpm_req.r_token);
	srm_logreq (func, logbuf);

	dpm_req.ctime = time (0);
	if (! dpm_req.status)
		dpm_req.status = DPM_QUEUED;
	dpm_req.nbreqfiles = nbreqfiles;

	/* Start transaction */

	(void) dpm_start_tr (thip->s, &thip->dbfd);

	/* Build file requests */

	for (i = 0; i < nbreqfiles; i++) {
		memset (&gfr_entry, 0, sizeof(gfr_entry));
		strcpy (gfr_entry.r_token, dpm_req.r_token);
		gfr_entry.f_ordinal = i;
		gfr_entry.r_uid = uid;
		gfr_entry.status = DPM_QUEUED;
		reqfilep = req->arrayOfFileRequests->getRequestArray[i];
		if (! reqfilep->fromSURLInfo ||
		    ! reqfilep->fromSURLInfo->SURLOrStFN ||
		    ! reqfilep->fromSURLInfo->SURLOrStFN->value) {
			gfr_entry.status = DPM_FAILED | EINVAL;
			strcpy (gfr_entry.errstring, "Pointer to SURL is NULL");
		} else {
			if (strlen (reqfilep->fromSURLInfo->SURLOrStFN->value) > CA_MAXSFNLEN)
				gfr_entry.status = DPM_FAILED | SENAMETOOLONG;
			strncpy (gfr_entry.from_surl,
			    reqfilep->fromSURLInfo->SURLOrStFN->value, CA_MAXSFNLEN);
		}
		if (reqfilep->lifetime) {
			if (reqfilep->lifetime->value < 0) {
				gfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (gfr_entry.errstring, "Negative value for lifetime");
			} else
				gfr_entry.lifetime = reqfilep->lifetime->value;
		}
		if (reqfilep->fileStorageType) {
			if (*reqfilep->fileStorageType == Volatile_)
				gfr_entry.f_type = 'V';
			else if (*reqfilep->fileStorageType == Durable_)
				gfr_entry.f_type = 'D';
			else if (*reqfilep->fileStorageType == Permanent_)
				gfr_entry.f_type = 'P';
			else {
				gfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (gfr_entry.errstring, "Invalid file storage type");
			}
		} else
			gfr_entry.f_type = DEFAULT_SPACE_TYPE;
		if (reqfilep->spaceToken) {
			if (! reqfilep->spaceToken->value ||
			    strlen (reqfilep->spaceToken->value) > CA_MAXDPMTOKENLEN) {
				gfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (gfr_entry.errstring, "Invalid space token");
			} else
				strcpy (gfr_entry.s_token, reqfilep->spaceToken->value);
		}
		gfr_entry.ret_policy = '_';
		if (reqfilep->dirOption) {
			if (reqfilep->dirOption->allLevelRecursive ||
			    reqfilep->dirOption->isSourceADirectory ||
			    reqfilep->dirOption->numOfLevels) {
				gfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (gfr_entry.errstring, "dirOption is not supported");
			}
		}
		strcpy (gfr_entry.protocol, selected_protocol);
		sprintf (logbuf, "PrepareToGet %d %s", i, gfr_entry.from_surl);
		srm_logreq (func, logbuf);
		if (gfr_entry.status != DPM_QUEUED) {
			nb_file_err++;
			if (*gfr_entry.errstring)
				srmlogit (func, "file %d: %s\n", i, gfr_entry.errstring);
		}
		if (dpm_insert_gfr_entry (&thip->dbfd, &gfr_entry) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}

		repfilep = repp->arrayOfFileStatuses->getStatusArray[i];
		repfilep->fromSURLInfo = reqfilep->fromSURLInfo->SURLOrStFN;
		repfilep->status->statusCode =
		    (gfr_entry.status == DPM_QUEUED) ? SRM_USCOREREQUEST_USCOREQUEUED :
		    (gfr_entry.status == (DPM_FAILED | SENAMETOOLONG)) ? SRM_USCOREINVALID_USCOREPATH :
		    SRM_USCOREFAILURE;
		if (*gfr_entry.errstring)
			repfilep->status->explanation = soap_strdup (soap, gfr_entry.errstring);
	}

	if (dpm_req.status == DPM_QUEUED && nb_file_err == nbreqfiles) {
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "Failed for all SURLs");
	}

	if (dpm_req.status == DPM_QUEUED) {
		if (dpm_insert_pending_entry (&thip->dbfd, &dpm_req) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}
		dpm_end_tr (&thip->dbfd);
		if (dpm_inc_reqctr () < 0)
			srmlogit (func, "dpm_inc_reqctr failed: %s\n", sstrerror (serrno));
	} else {
		if (dpm_insert_xferreq_entry (&thip->dbfd, &dpm_req) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}
	}
	if ((repp->requestToken = soap_malloc (soap, sizeof(struct ns1__TRequestToken))))
		repp->requestToken->value = soap_strdup (soap, dpm_req.r_token);
	repp->returnStatus->statusCode =
	    dpmstatus2statuscode (dpm_req.status, 0, 0);
	if (*dpm_req.errstring)
		repp->returnStatus->explanation = soap_strdup (soap, dpm_req.errstring);
	RETURN (SOAP_OK);
}

int
ns1__srmPrepareToPut (struct soap *soap, struct ns1__srmPrepareToPutRequest *req, struct ns1__srmPrepareToPutResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	time_t curtime;
	struct dpm_req dpm_req;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	char logbuf[CA_MAXSFNLEN+25];
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbreqfiles;
	struct dpm_put_filereq pfr_entry;
	struct ns1__TPutRequestFileStatus *repfilep;
	struct ns1__srmPrepareToPutResponse *repp;
	struct ns1__TPutFileRequest *reqfilep;
	char selected_protocol[CA_MAXPROTOLEN+1];
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	u_signed64 unique_id;
	uuid_t uuid;
	char *voname;

	strcpy (func, "PrepareToPut");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	memset ((char *) &dpm_req, 0, sizeof(dpm_req));
	dpm_req.ctime = time (0);

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmPrepareToPutResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->requestToken = NULL;
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmPrepareToPutResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (req->arrayOfFileRequests)
		nbreqfiles = req->arrayOfFileRequests->__sizeputRequestArray;
	else {
		nbreqfiles = 0;
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "arrayOfFileRequests is required");
	}
	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTPutRequestFileStatus))) == NULL ||
	    (nbreqfiles > 0 && (repp->arrayOfFileStatuses->putStatusArray =
		soap_malloc (soap, nbreqfiles * sizeof(struct ns1__TPutRequestFileStatus *))) == NULL))
		RETURN (SOAP_EOM);

	repp->arrayOfFileStatuses->__sizeputStatusArray = nbreqfiles;
	for (i = 0; i < nbreqfiles; i++) {
		if ((repp->arrayOfFileStatuses->putStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TPutRequestFileStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfFileStatuses->putStatusArray[i], 0, sizeof(struct ns1__TPutRequestFileStatus));
		if ((repp->arrayOfFileStatuses->putStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		repp->arrayOfFileStatuses->putStatusArray[i]->status->explanation = NULL;
	}

	dpm_req.r_uid = uid;
	dpm_req.r_gid = gid;
	strcpy (dpm_req.client_dn, clientdn);
	if (! Cencode_groups (nbgids, gids, dpm_req.groups, sizeof(dpm_req.groups))) {
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "Too many FQANs in proxy");
	}
	strcpy (dpm_req.clienthost, clienthost);
	dpm_req.r_type = 'P';

	/* Negociate protocol */

	if (! req->arrayOfTransferProtocols)
		strcpy (selected_protocol, "gsiftp");
	else {
		*selected_protocol = '\0';
		for (i = 0; i < req->arrayOfTransferProtocols->__sizestringArray; i++) {
			if (! req->arrayOfTransferProtocols->stringArray[i]) continue;
			for (j = 0; j < nb_supported_protocols; j++) {
				if (strcmp (req->arrayOfTransferProtocols->stringArray[i], supported_protocols[j]) == 0) {
					strcpy (selected_protocol, req->arrayOfTransferProtocols->stringArray[i]);
					break;
				}
			}
			if (*selected_protocol) break;
		}
		if (! *selected_protocol) {
			dpm_req.status = DPM_FAILED | SEPROTONOTSUP;
			strcpy (dpm_req.errstring, "Protocol not supported");
		}
	}
	if (req->userRequestDescription) {
		if (strlen (req->userRequestDescription) >= 256) {
			dpm_req.status = DPM_FAILED | EINVAL;
			strcpy (dpm_req.errstring, "Invalid user request description");
		}
		strncpy (dpm_req.u_token, req->userRequestDescription, 255);
	}
	dpm_req.flags = req->overwriteOption ? *req->overwriteOption : 1;
	dpm_req.retrytime = req->totalRetryTime ? req->totalRetryTime->value : 0;

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->arrayOfFileStatuses = NULL;
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURN (SOAP_OK);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	if (dpm_unique_id (&thip->dbfd, &unique_id) < 0) {
		repp->arrayOfFileStatuses = NULL;
		repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
		repp->returnStatus->explanation = soap_strdup (soap, "Can't get req uniqueid");
		RETURN (SOAP_OK);
	}
	dpm_req.r_ordinal = unique_id & 0xFFFFFFFF;
	uuid_generate (uuid);
	uuid_unparse (uuid, dpm_req.r_token);
	sprintf (logbuf, "PrepareToPut %d %s", dpm_req.r_ordinal, dpm_req.r_token);
	srm_logreq (func, logbuf);

	dpm_req.ctime = time (0);
	if (! dpm_req.status)
		dpm_req.status = DPM_QUEUED;
	dpm_req.nbreqfiles = nbreqfiles;

	/* Start transaction */

	(void) dpm_start_tr (thip->s, &thip->dbfd);

	/* Build file requests */

	for (i = 0; i < nbreqfiles; i++) {
		memset (&pfr_entry, 0, sizeof(pfr_entry));
		strcpy (pfr_entry.r_token, dpm_req.r_token);
		pfr_entry.f_ordinal = i;
		pfr_entry.status = DPM_QUEUED;
		reqfilep = req->arrayOfFileRequests->putRequestArray[i];
		if (! reqfilep->toSURLInfo ||
		    ! reqfilep->toSURLInfo->SURLOrStFN ||
		    ! reqfilep->toSURLInfo->SURLOrStFN->value) {
			pfr_entry.status = DPM_FAILED | EINVAL;
			strcpy (pfr_entry.errstring, "Pointer to SURL is NULL");
		} else {
			if (strlen (reqfilep->toSURLInfo->SURLOrStFN->value) > CA_MAXSFNLEN)
				pfr_entry.status = DPM_FAILED | SENAMETOOLONG;
			strncpy (pfr_entry.to_surl,
			    reqfilep->toSURLInfo->SURLOrStFN->value, CA_MAXSFNLEN);
		}
		if (reqfilep->lifetime) {
			if (reqfilep->lifetime->value < 0) {
				pfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (pfr_entry.errstring, "Negative value for lifetime");
			} else
				pfr_entry.lifetime = reqfilep->lifetime->value;
		}
		if (reqfilep->fileStorageType) {
			if (*reqfilep->fileStorageType == Volatile_)
				pfr_entry.f_type = 'V';
			else if (*reqfilep->fileStorageType == Durable_)
				pfr_entry.f_type = 'D';
			else if (*reqfilep->fileStorageType == Permanent_)
				pfr_entry.f_type = 'P';
			else {
				pfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (pfr_entry.errstring, "Invalid file storage type");
			}
		} else
			pfr_entry.f_type = DEFAULT_SPACE_TYPE;
		if (reqfilep->spaceToken) {
			if (! reqfilep->spaceToken->value ||
			    strlen (reqfilep->spaceToken->value) > CA_MAXDPMTOKENLEN) {
				pfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (pfr_entry.errstring, "Invalid space token");
			} else
				strcpy (pfr_entry.s_token, reqfilep->spaceToken->value);
		}
		pfr_entry.ret_policy = '_';
		pfr_entry.ac_latency = 'O';
		if (reqfilep->knownSizeOfThisFile)
			pfr_entry.requested_size = reqfilep->knownSizeOfThisFile->value;
		if (pfr_entry.requested_size & INT64_NEG) {
			pfr_entry.status = DPM_FAILED | EINVAL;
			strcpy (pfr_entry.errstring, "Negative value for requested size");
		}
		strcpy (pfr_entry.protocol, selected_protocol);
		sprintf (logbuf, "PrepareToPut %d %s", i, pfr_entry.to_surl);
		srm_logreq (func, logbuf);
		if (pfr_entry.status != DPM_QUEUED) {
			nb_file_err++;
			if (*pfr_entry.errstring)
				srmlogit (func, "file %d: %s\n", i, pfr_entry.errstring);
		}
		if (dpm_insert_pfr_entry (&thip->dbfd, &pfr_entry) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}

		repfilep = repp->arrayOfFileStatuses->putStatusArray[i];
		repfilep->siteURL = reqfilep->toSURLInfo->SURLOrStFN;
		repfilep->status->statusCode =
		    (pfr_entry.status == DPM_QUEUED) ? SRM_USCOREREQUEST_USCOREQUEUED :
		    (pfr_entry.status == (DPM_FAILED | SENAMETOOLONG)) ? SRM_USCOREINVALID_USCOREPATH :
		    SRM_USCOREFAILURE;
		if (*pfr_entry.errstring)
			repfilep->status->explanation = soap_strdup (soap, pfr_entry.errstring);
	}

	if (dpm_req.status == DPM_QUEUED && nb_file_err == nbreqfiles) {
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "Failed for all SURLs");
	}

	if (dpm_req.status == DPM_QUEUED) {
		if (dpm_insert_pending_entry (&thip->dbfd, &dpm_req) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}
		dpm_end_tr (&thip->dbfd);
		if (dpm_inc_reqctr () < 0)
			srmlogit (func, "dpm_inc_reqctr failed: %s\n", sstrerror (serrno));
	} else {
		if (dpm_insert_xferreq_entry (&thip->dbfd, &dpm_req) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}
	}
	if ((repp->requestToken = soap_malloc (soap, sizeof(struct ns1__TRequestToken))))
		repp->requestToken->value = soap_strdup (soap, dpm_req.r_token);
	repp->returnStatus->statusCode =
	    dpmstatus2statuscode (dpm_req.status, 0, 0);
	if (*dpm_req.errstring)
		repp->returnStatus->explanation = soap_strdup (soap, dpm_req.errstring);
	RETURN (SOAP_OK);
}

int
ns1__srmCopy (struct soap *soap, struct ns1__srmCopyRequest *req, struct ns1__srmCopyResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	struct dpm_copy_filereq cpr_entry;
	time_t curtime;
	struct dpm_req dpm_req;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	char logbuf[2*CA_MAXSFNLEN+18];
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbreqfiles;
	char proxy_filename[sizeof(P_tmpdir)+CA_MAXDPMTOKENLEN+4];
	struct ns1__TCopyRequestFileStatus *repfilep;
	struct ns1__srmCopyResponse *repp;
	struct ns1__TCopyFileRequest *reqfilep;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	u_signed64 unique_id;
	uuid_t uuid;
	char *voname;

	strcpy (func, "Copy");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	memset ((char *) &dpm_req, 0, sizeof(dpm_req));
	dpm_req.ctime = time (0);

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmCopyResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->requestToken = NULL;
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmCopyResponse = repp;

#if 1
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	RETURN (SOAP_OK);
#else
	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (req->arrayOfFileRequests)
		nbreqfiles = req->arrayOfFileRequests->__sizecopyRequestArray;
	else {
		nbreqfiles = 0;
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "arrayOfFileRequests is required");
	}
	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTCopyRequestFileStatus))) == NULL ||
	    (nbreqfiles > 0 && (repp->arrayOfFileStatuses->copyStatusArray =
		soap_malloc (soap, nbreqfiles * sizeof(struct ns1__TCopyRequestFileStatus *))) == NULL))
		RETURN (SOAP_EOM);

	repp->arrayOfFileStatuses->__sizecopyStatusArray = nbreqfiles;
	for (i = 0; i < nbreqfiles; i++) {
		if ((repp->arrayOfFileStatuses->copyStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TCopyRequestFileStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfFileStatuses->copyStatusArray[i], 0, sizeof(struct ns1__TCopyRequestFileStatus));
		if ((repp->arrayOfFileStatuses->copyStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		repp->arrayOfFileStatuses->copyStatusArray[i]->status->explanation = NULL;
	}

	dpm_req.r_uid = uid;
	dpm_req.r_gid = gid;
	strcpy (dpm_req.client_dn, clientdn);
	if (! Cencode_groups (nbgids, gids, dpm_req.groups, sizeof(dpm_req.groups))) {
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "Too many FQANs in proxy");
	}
	strcpy (dpm_req.clienthost, clienthost);
	dpm_req.r_type = 'C';

	if (req->userRequestDescription) {
		if (strlen (req->userRequestDescription) >= 256) {
			dpm_req.status = DPM_FAILED | EINVAL;
			strcpy (dpm_req.errstring, "Invalid user request description");
		}
		strncpy (dpm_req.u_token, req->userRequestDescription, 255);
	}
	dpm_req.flags = req->overwriteOption ? *req->overwriteOption : 1;
	if (req->removeSourceFiles && *req->removeSourceFiles == true_)
		dpm_req.flags += 4;
	dpm_req.retrytime = req->totalRetryTime ? req->totalRetryTime->value : 0;

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->arrayOfFileStatuses = NULL;
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURN (SOAP_OK);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	if (dpm_unique_id (&thip->dbfd, &unique_id) < 0) {
		repp->arrayOfFileStatuses = NULL;
		repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
		repp->returnStatus->explanation = soap_strdup (soap, "Can't get req uniqueid");
		RETURN (SOAP_OK);
	}
	dpm_req.r_ordinal = unique_id & 0xFFFFFFFF;
	uuid_generate (uuid);
	uuid_unparse (uuid, dpm_req.r_token);
	sprintf (logbuf, "Copy %d %s", dpm_req.r_ordinal, dpm_req.r_token);
	srm_logreq (func, logbuf);

	dpm_req.ctime = time (0);
	if (! dpm_req.status)
		dpm_req.status = DPM_QUEUED;
	dpm_req.nbreqfiles = nbreqfiles;

	/* Start transaction */

	(void) dpm_start_tr (thip->s, &thip->dbfd);

	/* Build file requests */

	for (i = 0; i < nbreqfiles; i++) {
		memset (&cpr_entry, 0, sizeof(cpr_entry));
		strcpy (cpr_entry.r_token, dpm_req.r_token);
		cpr_entry.f_ordinal = i;
		cpr_entry.status = DPM_QUEUED;
		reqfilep = req->arrayOfFileRequests->copyRequestArray[i];
		if (! reqfilep->fromSURLInfo ||
		    ! reqfilep->fromSURLInfo->SURLOrStFN ||
		    ! reqfilep->fromSURLInfo->SURLOrStFN->value) {
			cpr_entry.status = DPM_FAILED | EINVAL;
			strcpy (cpr_entry.errstring, "Pointer to fromSURL is NULL");
		} else {
			if (strlen (reqfilep->fromSURLInfo->SURLOrStFN->value) > CA_MAXSFNLEN)
				cpr_entry.status = DPM_FAILED | SENAMETOOLONG;
			strncpy (cpr_entry.from_surl,
			    reqfilep->fromSURLInfo->SURLOrStFN->value, CA_MAXSFNLEN);
		}
		if (! reqfilep->toSURLInfo ||
		    ! reqfilep->toSURLInfo->SURLOrStFN ||
		    ! reqfilep->toSURLInfo->SURLOrStFN->value) {
			cpr_entry.status = DPM_FAILED | EINVAL;
			strcpy (cpr_entry.errstring, "Pointer to toSURL is NULL");
		} else {
			if (strlen (reqfilep->toSURLInfo->SURLOrStFN->value) > CA_MAXSFNLEN)
				cpr_entry.status = DPM_FAILED | SENAMETOOLONG;
			strncpy (cpr_entry.to_surl,
			    reqfilep->toSURLInfo->SURLOrStFN->value, CA_MAXSFNLEN);
		}
		if (reqfilep->lifetime)
			cpr_entry.f_lifetime = reqfilep->lifetime->value;
		if (reqfilep->fileStorageType) {
			if (*reqfilep->fileStorageType == Volatile_)
				cpr_entry.f_type = 'V';
			else if (*reqfilep->fileStorageType == Durable_)
				cpr_entry.f_type = 'D';
			else if (*reqfilep->fileStorageType == Permanent_)
				cpr_entry.f_type = 'P';
			else {
				cpr_entry.status = DPM_FAILED | EINVAL;
				strcpy (cpr_entry.errstring, "Invalid file storage type");
			}
		} else
			cpr_entry.f_type = '_';
		if (reqfilep->spaceToken) {
			if (! reqfilep->spaceToken->value ||
			    strlen (reqfilep->spaceToken->value) > CA_MAXDPMTOKENLEN) {
				cpr_entry.status = DPM_FAILED | EINVAL;
				strcpy (cpr_entry.errstring, "Invalid space token");
			} else
				strcpy (cpr_entry.s_token, reqfilep->spaceToken->value);
		}
		cpr_entry.ret_policy = '_';
		cpr_entry.ac_latency = '_';
		if (reqfilep->dirOption && reqfilep->dirOption->isSourceADirectory)
			cpr_entry.flags = 1;
		sprintf (logbuf, "Copy %d %s %s", i, cpr_entry.from_surl,
		    cpr_entry.to_surl);
		srm_logreq (func, logbuf);
		if (cpr_entry.status != DPM_QUEUED) {
			nb_file_err++;
			if (*cpr_entry.errstring)
				srmlogit (func, "file %d: %s\n", i, cpr_entry.errstring);
		}
		if (dpm_insert_cpr_entry (&thip->dbfd, &cpr_entry) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}

		repfilep = repp->arrayOfFileStatuses->copyStatusArray[i];
		repfilep->fromSURL = reqfilep->fromSURLInfo->SURLOrStFN;
		repfilep->status->statusCode =
		    (cpr_entry.status == DPM_QUEUED) ? SRM_USCOREREQUEST_USCOREQUEUED :
		    (cpr_entry.status == (DPM_FAILED | SENAMETOOLONG)) ? SRM_USCOREINVALID_USCOREPATH :
		    SRM_USCOREFAILURE;
		if (*cpr_entry.errstring)
			repfilep->status->explanation = soap_strdup (soap, cpr_entry.errstring);
		repfilep->toSURL = reqfilep->toSURLInfo->SURLOrStFN;
	}

	if (dpm_req.status == DPM_QUEUED && nb_file_err == nbreqfiles) {
		dpm_req.status = DPM_FAILED | EINVAL;
		strcpy (dpm_req.errstring, "Failed for all SURLs");
	}

	if (dpm_req.status == DPM_QUEUED) {
		if (! has_delegated_credentials (soap)) {
			dpm_req.status = DPM_FAILED | EINVAL;
			strcpy (dpm_req.errstring, "No delegated credential available");
		} else {
			(void) build_proxy_filename (proxy_filename, dpm_req.r_token);
			if (export_delegated_credentials (soap, proxy_filename) < 0) {
				dpm_req.status = DPM_FAILED | EINVAL;
				strcpy (dpm_req.errstring, "Could not export credentials");
			}
		}
	}

	if (dpm_req.status == DPM_QUEUED) {
		dpm_req.status = DPM_QUEUED4COPY;
		if (dpm_insert_pending_entry (&thip->dbfd, &dpm_req) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}
		dpm_end_tr (&thip->dbfd);
		if (dpmcopy_inc_reqctr () < 0)
			srmlogit (func, "dpmcopy_inc_reqctr failed: %s\n", sstrerror (serrno));
	} else {
		if (dpm_insert_xferreq_entry (&thip->dbfd, &dpm_req) < 0) {
			soap_sender_fault (soap, "DB insert error", NULL);
			RETURN (SOAP_FAULT);
		}
	}
	if ((repp->requestToken = soap_malloc (soap, sizeof(struct ns1__TRequestToken))))
		repp->requestToken->value = soap_strdup (soap, dpm_req.r_token);
	repp->returnStatus->statusCode =
	    dpmstatus2statuscode (dpm_req.status, 0, 0);
	if (*dpm_req.errstring)
		repp->returnStatus->explanation = soap_strdup (soap, dpm_req.errstring);
	RETURN (SOAP_OK);
#endif
}

int
ns1__srmRemoveFiles (struct soap *soap, struct ns1__srmRemoveFilesRequest *req, struct ns1__srmRemoveFilesResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[16];
	struct ns1__srmRemoveFilesResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "RemoveFiles");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmRemoveFilesResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	repp->arrayOfFileStatuses = NULL;
	rep->srmRemoveFilesResponse = repp;
	RETURN (SOAP_OK);
}

int
ns1__srmReleaseFiles (struct soap *soap, struct ns1__srmReleaseFilesRequest *req, struct ns1__srmReleaseFilesResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	struct dpm_req dpm_req;
	char **fqan;
	char func[16];
	struct dpm_get_filereq gfr_entry;
	gid_t gid;
	gid_t *gids;
	int i;
	char logbuf[CA_MAXSFNLEN+25];
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nbsurls;
	dpm_dbrec_addr rec_addr;
	struct ns1__TSURLReturnStatus *repfilep;
	struct ns1__srmReleaseFilesResponse *repp;
	int status;
	char *surl;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "ReleaseFiles");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmReleaseFilesResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmReleaseFilesResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	if (! req->requestToken || ! req->requestToken->value) {
		repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
		repp->returnStatus->explanation = soap_strdup (soap, "Current implementation requires token");
		RETURN (SOAP_OK);
	}
	if (strlen (req->requestToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "ReleaseFiles %s", req->requestToken->value);
	srm_logreq (func, logbuf);

	if (! req->siteURLs) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "siteURLs is required");
		RETURN (SOAP_OK);
	}

	if (dpm_get_req_by_token (&thip->dbfd, req->requestToken->value,
	    &dpm_req, 0, NULL) < 0) {
		if (serrno == ENOENT) {
			repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
			repp->returnStatus->explanation = soap_strdup (soap, "Unknown request token");
		} else {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB fetch error");
		}
		RETURN (SOAP_OK);
	}
	if (dpm_req.r_type != 'G') {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation =
		    soap_strdup (soap, "ReleaseFiles only valid on PrepareToGet req");
		RETURN (SOAP_OK);
	}

	nbsurls = req->siteURLs->__sizesurlArray;

	/* Allocate the reply file structures */

	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTSURLReturnStatus))) == NULL ||
	    (nbsurls > 0 && (repp->arrayOfFileStatuses->surlReturnStatusArray =
		soap_malloc (soap, nbsurls * sizeof(struct ns1__TSURLReturnStatus *))) == NULL))
		RETURN (SOAP_EOM);

	repp->arrayOfFileStatuses->__sizesurlReturnStatusArray = nbsurls;
	for (i = 0; i < nbsurls; i++) {
		if ((repp->arrayOfFileStatuses->surlReturnStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TSURLReturnStatus))) == NULL ||
		    (repp->arrayOfFileStatuses->surlReturnStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
	}

	for (i = 0; i < nbsurls; i++) {
		repfilep = repp->arrayOfFileStatuses->surlReturnStatusArray[i];
		surl = req->siteURLs->surlArray[i]->value;
		repfilep->surl = req->siteURLs->surlArray[i];
		if (! surl) {
			repfilep->status->statusCode = SRM_USCOREFAILURE;
			repfilep->status->explanation =
				soap_strdup (soap, "Pointer to SURL is NULL");
			nb_file_err++;
			continue;
		}
		repfilep->status->explanation = NULL;
		if (strlen (surl) > CA_MAXSFNLEN) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			nb_file_err++;
			continue;
		}
		sprintf (logbuf, "ReleaseFiles %d %s", i, surl);
		srm_logreq (func, logbuf);
		(void) dpm_start_tr (thip->s, &thip->dbfd);
		if (dpm_get_gfr_by_surl (&thip->dbfd, req->requestToken->value,
		    surl, &gfr_entry, 1, &rec_addr) < 0) {
			repfilep->status->statusCode = serrno2statuscode (serrno);
			(void) dpm_abort_tr (&thip->dbfd);
			nb_file_err++;
		} else if (dpm_relonefile (thip, &gfr_entry, &rec_addr, &status) < 0) {
			repfilep->status->statusCode = serrno2statuscode (status & 0xFFF);
			(void) dpm_abort_tr (&thip->dbfd);
			nb_file_err++;
		} else {
			repfilep->status->statusCode = SRM_USCOREFILE_USCOREIN_USCORECACHE;
			(void) dpm_end_tr (&thip->dbfd);
		}
	}
	repp->returnStatus->statusCode = (nb_file_err == nbsurls) ?
		SRM_USCOREFAILURE : SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmPutDone (struct soap *soap, struct ns1__srmPutDoneRequest *req, struct ns1__srmPutDoneResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	struct dpm_filestatus *filestatuses = NULL;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	char logbuf[CA_MAXSFNLEN+20];
	int nbfqans;
	int nbgids;
	int nbreplies = 0;
	int nbsurls;
	struct ns1__TSURLReturnStatus *repfilep;
	struct ns1__srmPutDoneResponse *repp;
	int status;
	char **surls = NULL;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "PutDone");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmPutDoneResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmPutDoneResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (! req->requestToken || ! req->requestToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "requestToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->requestToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "PutDone %s", req->requestToken->value);
	srm_logreq (func, logbuf);

	if (! req->arrayOfSiteURLs) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfSiteURLs is required");
		RETURN (SOAP_OK);
	}
	nbsurls = req->arrayOfSiteURLs->__sizesurlArray;

	/* Allocate the reply file structures */

	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTSURLReturnStatus))) == NULL ||
	    (nbsurls > 0 && ((repp->arrayOfFileStatuses->surlReturnStatusArray =
		soap_malloc (soap, nbsurls * sizeof(struct ns1__TSURLReturnStatus *))) == NULL ||
		(surls = soap_malloc (soap, nbsurls * sizeof (char *))) == NULL)))
		RETURN (SOAP_EOM);
	repp->arrayOfFileStatuses->__sizesurlReturnStatusArray = nbsurls;

	j = 0;
	for (i = 0; i < nbsurls; i++) {
		surls[j] = req->arrayOfSiteURLs->surlArray[i]->value;
		if ((repp->arrayOfFileStatuses->surlReturnStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TSURLReturnStatus))) == NULL ||
		    (repp->arrayOfFileStatuses->surlReturnStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		if (surls[j]) j++;
	}

	if (j > 0) {
		dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
		thip->errbuf[0] = '\0';
		dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
		if (voname && fqan)
			dpm_client_setVOMS_data (voname, fqan, nbfqans);

		if (dpm_putdone (req->requestToken->value, j, surls,
		    &nbreplies, &filestatuses) < 0 && ! filestatuses) {
			srmlogit (func, "dpm_putdone failed: %s\n", sstrerror (serrno));
			repp->arrayOfFileStatuses = NULL;
			repp->returnStatus->statusCode = serrno2statuscode (serrno);
			if (*thip->errbuf) {
				thip->errbuf[strlen(thip->errbuf)-1] = '\0';
				repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
			}
			RETURN (SOAP_OK);
		}
	}

	j = 0;
	for (i = 0; i < nbsurls; i++) {
		repfilep = repp->arrayOfFileStatuses->surlReturnStatusArray[i];
		repfilep->surl = req->arrayOfSiteURLs->surlArray[i];
		if (! req->arrayOfSiteURLs->surlArray[i]->value) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			repfilep->status->explanation =
				soap_strdup (soap, "Pointer to SURL is NULL");
			continue;
		}
		if ((filestatuses+j)->surl)
			free ((filestatuses+j)->surl);
		repfilep->status->statusCode =
			dpmstatus2statuscode ((filestatuses+j)->status, 0, 0);
		if ((filestatuses+j)->errstring) {
			repfilep->status->explanation =
				soap_strdup (soap, (filestatuses+j)->errstring);
			free ((filestatuses+j)->errstring);
		} else
			repfilep->status->explanation = NULL;
		j++;
	}
	free (filestatuses);
	repp->returnStatus->statusCode = j ? SRM_USCORESUCCESS : SRM_USCOREFAILURE;
	RETURN (SOAP_OK);
}

int
ns1__srmAbortRequest (struct soap *soap, struct ns1__srmAbortRequestRequest *req, struct ns1__srmAbortRequestResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	char logbuf[CA_MAXDPMTOKENLEN+14];
	int nbfqans;
	int nbgids;
	struct ns1__srmAbortRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "AbortRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmAbortRequestResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmAbortRequestResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (! req->requestToken || ! req->requestToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "requestToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->requestToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "AbortRequest %s", req->requestToken->value);
	srm_logreq (func, logbuf);

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (dpm_abortreq (req->requestToken->value) < 0) {
		srmlogit (func, "dpm_abortreq failed: %s\n", sstrerror (serrno));
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
	} else
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmAbortFiles (struct soap *soap, struct ns1__srmAbortFilesRequest *req, struct ns1__srmAbortFilesResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	struct dpm_filestatus *filestatuses = NULL;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	char logbuf[CA_MAXSFNLEN+20];
	int nbfqans;
	int nbgids;
	int nbreplies = 0;
	int nbsurls;
	struct ns1__TSURLReturnStatus *repfilep;
	struct ns1__srmAbortFilesResponse *repp;
	int status;
	char **surls = NULL;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "AbortFiles");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmAbortFilesResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmAbortFilesResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (! req->requestToken || ! req->requestToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "requestToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->requestToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "AbortFiles %s", req->requestToken->value);
	srm_logreq (func, logbuf);

	if (! req->arrayOfSiteURLs) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfSiteURLs is required");
		RETURN (SOAP_OK);
	}
	nbsurls = req->arrayOfSiteURLs->__sizesurlArray;

	/* Allocate the reply file structures */

	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTSURLReturnStatus))) == NULL ||
	    (nbsurls > 0 && ((repp->arrayOfFileStatuses->surlReturnStatusArray =
		soap_malloc (soap, nbsurls * sizeof(struct ns1__TSURLReturnStatus *))) == NULL ||
		(surls = soap_malloc (soap, nbsurls * sizeof (char *))) == NULL)))
		RETURN (SOAP_EOM);
	repp->arrayOfFileStatuses->__sizesurlReturnStatusArray = nbsurls;

	j = 0;
	for (i = 0; i < nbsurls; i++) {
		surls[j] = req->arrayOfSiteURLs->surlArray[i]->value;
		if ((repp->arrayOfFileStatuses->surlReturnStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TSURLReturnStatus))) == NULL ||
		    (repp->arrayOfFileStatuses->surlReturnStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		if (surls[j]) j++;
	}

	if (j > 0) {
		dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
		thip->errbuf[0] = '\0';
		dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
		if (voname && fqan)
			dpm_client_setVOMS_data (voname, fqan, nbfqans);

		if (dpm_abortfiles (req->requestToken->value, j, surls,
		    &nbreplies, &filestatuses) < 0 && ! filestatuses) {
			srmlogit (func, "dpm_abortfiles failed: %s\n", sstrerror (serrno));
			repp->arrayOfFileStatuses = NULL;
			repp->returnStatus->statusCode = serrno2statuscode (serrno);
			if (*thip->errbuf) {
				thip->errbuf[strlen(thip->errbuf)-1] = '\0';
				repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
			}
			RETURN (SOAP_OK);
		}
	}

	j = 0;
	for (i = 0; i < nbsurls; i++) {
		repfilep = repp->arrayOfFileStatuses->surlReturnStatusArray[i];
		repfilep->surl = req->arrayOfSiteURLs->surlArray[i];
		if (! req->arrayOfSiteURLs->surlArray[i]->value) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			repfilep->status->explanation =
				soap_strdup (soap, "Pointer to SURL is NULL");
			continue;
		}
		if ((filestatuses+j)->surl)
			free ((filestatuses+j)->surl);
		repfilep->status->statusCode =
			dpmstatus2statuscode ((filestatuses+j)->status, 0, 0);
		if ((filestatuses+j)->errstring) {
			repfilep->status->explanation =
				soap_strdup (soap, (filestatuses+j)->errstring);
			free ((filestatuses+j)->errstring);
		} else
			repfilep->status->explanation = NULL;
		j++;
	}
	free (filestatuses);
	repp->returnStatus->statusCode = j ? SRM_USCORESUCCESS : SRM_USCOREFAILURE;
	RETURN (SOAP_OK);
}

int
ns1__srmSuspendRequest (struct soap *soap, struct ns1__srmSuspendRequestRequest *req, struct ns1__srmSuspendRequestResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[16];
	struct ns1__srmSuspendRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "SuspendRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmSuspendRequestResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	rep->srmSuspendRequestResponse = repp;
	RETURN (SOAP_OK);
}

int
ns1__srmResumeRequest (struct soap *soap, struct ns1__srmResumeRequestRequest *req, struct ns1__srmResumeRequestResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char func[16];
	struct ns1__srmResumeRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "ResumeRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmResumeRequestResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
	rep->srmResumeRequestResponse = repp;
	RETURN (SOAP_OK);
}

int
ns1__srmStatusOfGetRequest (struct soap *soap, struct ns1__srmStatusOfGetRequestRequest *req, struct ns1__srmStatusOfGetRequestResponse_ *rep)
{
	int bol = 1;
	int c;
	char clientdn[256];
	const char *clienthost;
	time_t curtime;
	DBLISTPTR dblistptr;
	struct dpm_req dpm_req;
	char func[19];
	struct dpm_get_filereq gfr_entry;
	int i;
	char logbuf[CA_MAXSFNLEN+20];
	int nbfromsurls;
	int nbreplies;
	struct ns1__srmStatusOfGetRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "StatusOfGetRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmStatusOfGetRequestResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmStatusOfGetRequestResponse = repp;

	if (! req->requestToken || ! req->requestToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "requestToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->requestToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "StatusOfGetRequest %s", req->requestToken->value);
	srm_logreq (func, logbuf);

	if (req->arrayOfFromSURLs)
		nbfromsurls = req->arrayOfFromSURLs->__sizesurlArray;
	else
		nbfromsurls = 0;

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURN (SOAP_OK);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	/* Get the global request status */

	if (dpm_get_pending_req_by_token (&thip->dbfd, req->requestToken->value,
	    &dpm_req, 0, NULL) < 0 &&
	    dpm_get_req_by_token (&thip->dbfd, req->requestToken->value,
	    &dpm_req, 0, NULL) < 0) {
		if (serrno == ENOENT) {
			repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
			repp->returnStatus->explanation = soap_strdup (soap, "Unknown request token");
		} else {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB fetch error");
		}
		RETURN (SOAP_OK);
	}

	if (dpm_req.r_type != 'G') {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "request type mismatch");
		RETURN (SOAP_OK);
	}

	if (nbfromsurls == 0)	/* Get the number of file requests from the DB */
		nbreplies = dpm_req.nbreqfiles;
	else
		nbreplies = nbfromsurls;

	/* Allocate the reply file structures */

	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTGetRequestFileStatus))) == NULL ||
	    (nbreplies > 0 && (repp->arrayOfFileStatuses->getStatusArray =
		soap_malloc (soap, nbreplies * sizeof(struct ns1__TGetRequestFileStatus *))) == NULL))
		RETURN (SOAP_EOM);

	repp->arrayOfFileStatuses->__sizegetStatusArray = nbreplies;
	for (i = 0; i < nbreplies; i++) {
		if ((repp->arrayOfFileStatuses->getStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TGetRequestFileStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfFileStatuses->getStatusArray[i], 0, sizeof(struct ns1__TGetRequestFileStatus));
		if ((repp->arrayOfFileStatuses->getStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL ||
		    (repp->arrayOfFileStatuses->getStatusArray[i]->fromSURLInfo =
		    soap_malloc (soap, sizeof(struct ns1__TSURL))) == NULL)
			RETURN (SOAP_EOM);
		repp->arrayOfFileStatuses->getStatusArray[i]->status->explanation = NULL;
	}

	if (nbfromsurls == 0) {	/* return all file requests for this token */
		i = 0;
		while ((c = dpm_list_gfr_entry (&thip->dbfd, bol, req->requestToken->value,
		    &gfr_entry, 0, NULL, 0, &dblistptr)) == 0) {
			bol = 0;
			if (marshall_GFR (soap, &thip->dbfd, repp->arrayOfFileStatuses->getStatusArray[i],
			    &gfr_entry) < 0) {
				(void) dpm_list_gfr_entry (&thip->dbfd, bol,
				    req->requestToken->value, &gfr_entry,
				    0, NULL, 1, &dblistptr);	/* free res */
				RETURN (SOAP_EOM);
			}
			i++;
		}
		(void) dpm_list_gfr_entry (&thip->dbfd, bol, req->requestToken->value,
		    &gfr_entry, 0, NULL, 1, &dblistptr);	/* free res */
	} else {	/* return information about specified file requests */
		for (i = 0; i < nbfromsurls; i++) {
			if (! req->arrayOfFromSURLs->surlArray[i]->value) {
				memset (&gfr_entry, 0, sizeof(gfr_entry));
				gfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (gfr_entry.errstring, "Pointer to SURL is NULL");
			} else if (strlen (req->arrayOfFromSURLs->surlArray[i]->value) > CA_MAXSFNLEN) {
				memset (&gfr_entry, 0, sizeof(gfr_entry));
				strncpy (gfr_entry.from_surl, req->arrayOfFromSURLs->surlArray[i]->value, CA_MAXSFNLEN);
				gfr_entry.status = DPM_FAILED | SENAMETOOLONG;
			} else if (dpm_get_gfr_by_surl (&thip->dbfd, req->requestToken->value,
			    req->arrayOfFromSURLs->surlArray[i]->value, &gfr_entry, 0, NULL) < 0) {
				memset (&gfr_entry, 0, sizeof(gfr_entry));
				strcpy (gfr_entry.from_surl, req->arrayOfFromSURLs->surlArray[i]->value);
				gfr_entry.status = DPM_FAILED | serrno;
			}
			sprintf (logbuf, "StatusOfGetRequest %s", gfr_entry.from_surl);
			srm_logreq (func, logbuf);
			if (marshall_GFR (soap, &thip->dbfd, repp->arrayOfFileStatuses->getStatusArray[i],
			    &gfr_entry) < 0)
				RETURN (SOAP_EOM);
		}
	}
	repp->returnStatus->statusCode =
	    dpmstatus2statuscode (dpm_req.status, 0, 0);
	if (*dpm_req.errstring)
		repp->returnStatus->explanation = soap_strdup (soap, dpm_req.errstring);
	RETURN (SOAP_OK);
}

int
ns1__srmStatusOfPutRequest (struct soap *soap, struct ns1__srmStatusOfPutRequestRequest *req, struct ns1__srmStatusOfPutRequestResponse_ *rep)
{
	int bol = 1;
	int c;
	char clientdn[256];
	const char *clienthost;
	time_t curtime;
	DBLISTPTR dblistptr;
	struct dpm_req dpm_req;
	char func[19];
	struct dpm_put_filereq pfr_entry;
	int i;
	char logbuf[CA_MAXSFNLEN+20];
	int nbreplies;
	int nbtosurls;
	struct ns1__srmStatusOfPutRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "StatusOfPutRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmStatusOfPutRequestResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmStatusOfPutRequestResponse = repp;

	if (! req->requestToken || ! req->requestToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "requestToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->requestToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "StatusOfPutRequest %s", req->requestToken->value);
	srm_logreq (func, logbuf);

	if (req->arrayOfToSURLs)
		nbtosurls = req->arrayOfToSURLs->__sizesurlArray;
	else
		nbtosurls = 0;

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURN (SOAP_OK);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	/* Get the global request status */

	if (dpm_get_pending_req_by_token (&thip->dbfd, req->requestToken->value,
	    &dpm_req, 0, NULL) < 0 &&
	    dpm_get_req_by_token (&thip->dbfd, req->requestToken->value,
	    &dpm_req, 0, NULL) < 0) {
		if (serrno == ENOENT) {
			repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
			repp->returnStatus->explanation = soap_strdup (soap, "Unknown request token");
		} else {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB fetch error");
		}
		RETURN (SOAP_OK);
	}

	if (dpm_req.r_type != 'P') {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "request type mismatch");
		RETURN (SOAP_OK);
	}

	if (nbtosurls == 0)	/* Get the number of file requests from the DB */

		nbreplies = dpm_req.nbreqfiles;
	else
		nbreplies = nbtosurls;

	/* Allocate the reply file structures */

	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTPutRequestFileStatus))) == NULL ||
	    (nbreplies > 0 && (repp->arrayOfFileStatuses->putStatusArray =
		soap_malloc (soap, nbreplies * sizeof(struct ns1__TPutRequestFileStatus *))) == NULL))
		RETURN (SOAP_EOM);

	repp->arrayOfFileStatuses->__sizeputStatusArray = nbreplies;
	for (i = 0; i < nbreplies; i++) {
		if ((repp->arrayOfFileStatuses->putStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TPutRequestFileStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfFileStatuses->putStatusArray[i], 0, sizeof(struct ns1__TPutRequestFileStatus));
		if ((repp->arrayOfFileStatuses->putStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL ||
		    (repp->arrayOfFileStatuses->putStatusArray[i]->siteURL =
		    soap_malloc (soap, sizeof(struct ns1__TSURL))) == NULL)
			RETURN (SOAP_EOM);
		repp->arrayOfFileStatuses->putStatusArray[i]->status->explanation = NULL;
	}

	if (nbtosurls == 0) {	/* return all file requests for this token */
		i = 0;
		while ((c = dpm_list_pfr_entry (&thip->dbfd, bol, req->requestToken->value,
		    &pfr_entry, 0, NULL, 0, &dblistptr)) == 0) {
			bol = 0;
			if (marshall_PFR (soap, &thip->dbfd, repp->arrayOfFileStatuses->putStatusArray[i],
			    &pfr_entry) < 0) {
				(void) dpm_list_pfr_entry (&thip->dbfd, bol,
				    req->requestToken->value, &pfr_entry,
				    0, NULL, 1, &dblistptr);	/* free res */
				RETURN (SOAP_EOM);
			}
			i++;
		}
		(void) dpm_list_pfr_entry (&thip->dbfd, bol, req->requestToken->value,
		    &pfr_entry, 0, NULL, 1, &dblistptr);	/* free res */
	} else {	/* return information about specified file requests */
		for (i = 0; i < nbtosurls; i++) {
			if (! req->arrayOfToSURLs->surlArray[i]->value) {
				memset (&pfr_entry, 0, sizeof(pfr_entry));
				pfr_entry.status = DPM_FAILED | EINVAL;
				strcpy (pfr_entry.errstring, "Pointer to SURL is NULL");
			} else if (strlen (req->arrayOfToSURLs->surlArray[i]->value) > CA_MAXSFNLEN) {
				memset (&pfr_entry, 0, sizeof(pfr_entry));
				strncpy (pfr_entry.to_surl, req->arrayOfToSURLs->surlArray[i]->value, CA_MAXSFNLEN);
				pfr_entry.status = DPM_FAILED | SENAMETOOLONG;
			} else if (dpm_get_pfr_by_surl (&thip->dbfd, req->requestToken->value,
			    req->arrayOfToSURLs->surlArray[i]->value, &pfr_entry, 0, NULL) < 0) {
				memset (&pfr_entry, 0, sizeof(pfr_entry));
				strcpy (pfr_entry.to_surl, req->arrayOfToSURLs->surlArray[i]->value);
				pfr_entry.status = DPM_FAILED | serrno;
			}
			sprintf (logbuf, "StatusOfPutRequest %s", pfr_entry.to_surl);
			srm_logreq (func, logbuf);
			if (marshall_PFR (soap, &thip->dbfd, repp->arrayOfFileStatuses->putStatusArray[i],
			    &pfr_entry) < 0)
				RETURN (SOAP_EOM);
		}
	}
	repp->returnStatus->statusCode =
	    dpmstatus2statuscode (dpm_req.status, 0, 0);
	if (*dpm_req.errstring)
		repp->returnStatus->explanation = soap_strdup (soap, dpm_req.errstring);
	RETURN (SOAP_OK);
}

int
ns1__srmStatusOfCopyRequest (struct soap *soap, struct ns1__srmStatusOfCopyRequestRequest *req, struct ns1__srmStatusOfCopyRequestResponse_ *rep)
{
	int bol = 1;
	int c;
	char clientdn[256];
	const char *clienthost;
	struct dpm_copy_filereq cpr_entry;
	time_t curtime;
	DBLISTPTR dblistptr;
	struct dpm_req dpm_req;
	char func[20];
	int i;
	char logbuf[2*CA_MAXSFNLEN+22];
	int nbreplies;
	int nbsurls;
	struct ns1__srmStatusOfCopyRequestResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "StatusOfCopyRequest");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmStatusOfCopyRequestResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmStatusOfCopyRequestResponse = repp;

	if (! req->requestToken || ! req->requestToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "requestToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->requestToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "StatusOfCopyRequest %s", req->requestToken->value);
	srm_logreq (func, logbuf);

	if (req->arrayOfFromSURLs)
		nbsurls = req->arrayOfFromSURLs->__sizesurlArray;
	else
		nbsurls = 0;

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURN (SOAP_OK);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	/* Get the global request status */

	if (dpm_get_pending_req_by_token (&thip->dbfd, req->requestToken->value,
	    &dpm_req, 0, NULL) < 0 &&
	    dpm_get_req_by_token (&thip->dbfd, req->requestToken->value,
	    &dpm_req, 0, NULL) < 0) {
		if (serrno == ENOENT) {
			repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
			repp->returnStatus->explanation = soap_strdup (soap, "Unknown request token");
		} else {
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB fetch error");
		}
		RETURN (SOAP_OK);
	}

	if (dpm_req.r_type != 'C') {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "request type mismatch");
		RETURN (SOAP_OK);
	}

	if (nbsurls == 0)	/* Get the number of file requests from the DB */

		nbreplies = dpm_req.nbreqfiles;
	else
		nbreplies = nbsurls;

	/* Allocate the reply file structures */

	if ((repp->arrayOfFileStatuses =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTCopyRequestFileStatus))) == NULL ||
	    (nbreplies > 0 && (repp->arrayOfFileStatuses->copyStatusArray =
		soap_malloc (soap, nbreplies * sizeof(struct ns1__TCopyRequestFileStatus *))) == NULL))
		RETURN (SOAP_EOM);

	repp->arrayOfFileStatuses->__sizecopyStatusArray = nbreplies;
	for (i = 0; i < nbreplies; i++) {
		if ((repp->arrayOfFileStatuses->copyStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TCopyRequestFileStatus))) == NULL)
			RETURN (SOAP_EOM);
		memset (repp->arrayOfFileStatuses->copyStatusArray[i], 0, sizeof(struct ns1__TCopyRequestFileStatus));
		if ((repp->arrayOfFileStatuses->copyStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL ||
		    (repp->arrayOfFileStatuses->copyStatusArray[i]->fromSURL =
		    soap_malloc (soap, sizeof(struct ns1__TSURL))) == NULL ||
		    (repp->arrayOfFileStatuses->copyStatusArray[i]->toSURL =
		    soap_malloc (soap, sizeof(struct ns1__TSURL))) == NULL)
			RETURN (SOAP_EOM);
		repp->arrayOfFileStatuses->copyStatusArray[i]->status->explanation = NULL;
	}

	if (nbsurls == 0) {	/* return all file requests for this token */
		i = 0;
		while ((c = dpm_list_cpr_entry (&thip->dbfd, bol, req->requestToken->value,
		    &cpr_entry, 0, NULL, 0, &dblistptr)) == 0) {
			bol = 0;
			if (marshall_CPR (soap, repp->arrayOfFileStatuses->copyStatusArray[i],
			    &cpr_entry) < 0) {
				(void) dpm_list_cpr_entry (&thip->dbfd, bol,
				    req->requestToken->value, &cpr_entry,
				    0, NULL, 1, &dblistptr);	/* free res */
				RETURN (SOAP_EOM);
			}
			i++;
		}
		(void) dpm_list_cpr_entry (&thip->dbfd, bol, req->requestToken->value,
		    &cpr_entry, 0, NULL, 1, &dblistptr);	/* free res */
	} else {	/* return information about specified file requests */
		for (i = 0; i < nbsurls; i++) {
			if (! req->arrayOfFromSURLs->surlArray[i]->value ||
			    ! req->arrayOfToSURLs->surlArray[i]->value) {
				memset (&cpr_entry, 0, sizeof(cpr_entry));
				cpr_entry.status = DPM_FAILED | EINVAL;
				strcpy (cpr_entry.errstring, "Pointer to SURL is NULL");
			} else if (strlen (req->arrayOfFromSURLs->surlArray[i]->value) > CA_MAXSFNLEN ||
			    strlen (req->arrayOfToSURLs->surlArray[i]->value) > CA_MAXSFNLEN) {
				memset (&cpr_entry, 0, sizeof(cpr_entry));
				strncpy (cpr_entry.from_surl, req->arrayOfFromSURLs->surlArray[i]->value, CA_MAXSFNLEN);
				strncpy (cpr_entry.to_surl, req->arrayOfToSURLs->surlArray[i]->value, CA_MAXSFNLEN);
				cpr_entry.status = DPM_FAILED | SENAMETOOLONG;
			} else if (dpm_get_cpr_by_surls (&thip->dbfd, req->requestToken->value,
			    req->arrayOfFromSURLs->surlArray[i]->value,
			    req->arrayOfToSURLs->surlArray[i]->value, &cpr_entry, 0, NULL) < 0) {
				memset (&cpr_entry, 0, sizeof(cpr_entry));
				strcpy (cpr_entry.from_surl, req->arrayOfFromSURLs->surlArray[i]->value);
				strcpy (cpr_entry.to_surl, req->arrayOfToSURLs->surlArray[i]->value);
				cpr_entry.status = DPM_FAILED | serrno;
			}
			sprintf (logbuf, "StatusOfCopyRequest %s %s",
			    cpr_entry.from_surl, cpr_entry.to_surl);
			srm_logreq (func, logbuf);
			if (marshall_CPR (soap, repp->arrayOfFileStatuses->copyStatusArray[i], &cpr_entry) < 0)
				RETURN (SOAP_EOM);
		}
	}
	repp->returnStatus->statusCode =
	    dpmstatus2statuscode (dpm_req.status, 0, 0);
	if (*dpm_req.errstring)
		repp->returnStatus->explanation = soap_strdup (soap, dpm_req.errstring);
	RETURN (SOAP_OK);
}

int
ns1__srmGetRequestSummary (struct soap *soap, struct ns1__srmGetRequestSummaryRequest *req, struct ns1__srmGetRequestSummaryResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	time_t curtime;
	char func[18];
	int i;
	int j;
	int nb_errors = 0;
	int nb_failed;
	int nb_progress;
	int nb_queued;
	int nbreqfiles;
	int nbtokens;
	int r_status;
	char *r_token;
	char r_type;
	struct ns1__srmGetRequestSummaryResponse *repp;
	struct ns1__TRequestSummary *reptokenp;
	static enum ns1__TRequestType rtypes[] = {PrepareToGet, PrepareToPut, Copy};
	struct srm_srv_thread_info *thip = soap->user;

	strcpy (func, "GetRequestSummary");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmGetRequestSummaryResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfRequestSummaries = NULL;
	rep->srmGetRequestSummaryResponse = repp;

	if (! req->arrayOfRequestTokens) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfRequestTokens is required");
		RETURN (SOAP_OK);
	}
	nbtokens = req->arrayOfRequestTokens->__sizerequestTokenArray;

	/* Allocate the array of request summaries */

	if ((repp->arrayOfRequestSummaries =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTRequestSummary))) == NULL ||
	    (repp->arrayOfRequestSummaries->summaryArray =
	    soap_malloc (soap, nbtokens * sizeof(struct ns1__TRequestSummary *))) == NULL)
		RETURN (SOAP_EOM);
	repp->arrayOfRequestSummaries->__sizesummaryArray = nbtokens;
	for (i = 0; i < nbtokens; i++) {
		if ((repp->arrayOfRequestSummaries->summaryArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TRequestSummary))) == NULL)
			RETURN (SOAP_EOM);
	}

	/* Connect to the database if not done yet */

	if (! thip->db_open_done) {
		if (dpm_opendb (&thip->dbfd) < 0) {
			repp->arrayOfRequestSummaries = NULL;
			repp->returnStatus->statusCode = SRM_USCOREINTERNAL_USCOREERROR;
			repp->returnStatus->explanation = soap_strdup (soap, "DB open error");
			RETURN (SOAP_OK);
		}
		thip->db_open_done = 1;
		thip->last_db_use = time (0);
	} else {
		if ((curtime = time (0)) > thip->last_db_use + DPM_DBPINGI)
			(void) dpm_pingdb (&thip->dbfd);
		thip->last_db_use = curtime;
	}

	j = 0;
	for (i = 0; i < nbtokens; i++) {
		if ( ! req->arrayOfRequestTokens->requestTokenArray[i]->value) {
			nb_errors++;
			continue;
		}
		r_token = req->arrayOfRequestTokens->requestTokenArray[i]->value;
		if (strlen (r_token) > CA_MAXDPMTOKENLEN) {
			nb_errors++;
			continue;
		}
		if (dpm_getonereqsummary (thip, r_token, &r_type, &r_status,
		    &nbreqfiles, &nb_queued, &nb_progress, &nb_failed) < 0) {
			nb_errors++;
			continue;
		}
		reptokenp = repp->arrayOfRequestSummaries->summaryArray[j];
		reptokenp->isSuspended = false_;
		reptokenp->numOfFinishedRequests = nbreqfiles - (nb_queued + nb_progress);
		reptokenp->numOfProgressingRequests = nb_progress;
		reptokenp->numOfQueuedRequests = nb_queued;
		reptokenp->requestToken = req->arrayOfRequestTokens->requestTokenArray[i];
		if (r_type == 'G')
			reptokenp->requestType = &rtypes[PrepareToGet];
		else if (r_type == 'P')
			reptokenp->requestType = &rtypes[PrepareToPut];
		else if (r_type == 'C')
			reptokenp->requestType = &rtypes[Copy];
		reptokenp->totalFilesInThisRequest = nbreqfiles;
		j++;
	}
	repp->arrayOfRequestSummaries->__sizesummaryArray = nbtokens - nb_errors;
	if (nb_errors == nbtokens) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid/unknown request token(s)");
	} else
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmExtendFileLifeTime (struct soap *soap, struct ns1__srmExtendFileLifeTimeRequest *req, struct ns1__srmExtendFileLifeTimeResponse_ *rep)
{
	time_t actual_lifetime;
	int c;
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[19];
	gid_t gid;
	gid_t *gids;
	char logbuf[CA_MAXDPMTOKENLEN+CA_MAXSFNLEN+21];
	int nbfqans;
	int nbgids;
	struct ns1__srmExtendFileLifeTimeResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "ExtendFileLifeTime");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmExtendFileLifeTimeResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->newTimeExtended = NULL;
	rep->srmExtendFileLifeTimeResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	if (! req->requestToken || ! req->requestToken->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "requestToken is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->requestToken->value) > CA_MAXDPMTOKENLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
		repp->returnStatus->explanation = soap_strdup (soap, "Invalid request token");
		RETURN (SOAP_OK);
	}
	if (! req->siteURL || ! req->siteURL->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "siteURL is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->siteURL->value) > CA_MAXSFNLEN) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREPATH;
		RETURN (SOAP_OK);
	}

	sprintf (logbuf, "ExtendFileLifeTime %s %s", req->requestToken->value,
	    req->siteURL->value);
	srm_logreq (func, logbuf);

	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if ((c = dpm_extendfilelife (req->requestToken->value, req->siteURL->value,
	    req->newLifeTime ? req->newLifeTime->value : 0, &actual_lifetime)) != 0) {
		if (c < 0) {
			srmlogit (func, "dpm_extendfilelife failed: %s\n",
			    sstrerror (serrno));
			repp->returnStatus->statusCode = serrno2statuscode (serrno);
		} else
			repp->returnStatus->statusCode =
			    dpmstatus2statuscode (c, 1, 0);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURN (SOAP_OK);
	}
	if ((repp->newTimeExtended =
	    soap_malloc (soap, sizeof(struct ns1__TLifeTimeInSeconds))) == NULL)
		RETURN (SOAP_EOM);
	repp->newTimeExtended->value = actual_lifetime;
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmGetRequestID (struct soap *soap, struct ns1__srmGetRequestIDRequest *req, struct ns1__srmGetRequestIDResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	char logbuf[269];
	int nbfqans;
	int nbgids;
	int nbreplies = 0;
	struct ns1__srmGetRequestIDResponse *repp;
	struct srm_srv_thread_info *thip = soap->user;
	struct dpm_tokeninfo *tokeninfos = NULL;
	uid_t uid;
	char *voname;

	strcpy (func, "GetRequestID");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate the reply structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmGetRequestIDResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfRequestTokens = NULL;
	rep->srmGetRequestIDResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}
	dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	thip->errbuf[0] = '\0';
	dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		dpm_client_setVOMS_data (voname, fqan, nbfqans);

	if (req->userRequestDescription) {
		if (strlen (req->userRequestDescription) >= 256) {
			repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREREQUEST;
			repp->returnStatus->explanation =
			    soap_strdup (soap, "Invalid user request description");
			RETURN (SOAP_OK);
		}
	}
	sprintf (logbuf, "GetRequestID %s", req->userRequestDescription ?
	    req->userRequestDescription : "");
	srm_logreq (func, logbuf);

	if (dpm_getreqid (req->userRequestDescription, &nbreplies, &tokeninfos) < 0) {
		srmlogit (func, "dpm_getreqid failed: %s\n", sstrerror (serrno));
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
		if (tokeninfos)
			free (tokeninfos);
		if (*thip->errbuf) {
			thip->errbuf[strlen(thip->errbuf)-1] = '\0';
			repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
		}
		RETURN (SOAP_OK);
	}

	/* Allocate the array of request tokens */

	if ((repp->arrayOfRequestTokens =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTRequestToken))) == NULL ||
	    (repp->arrayOfRequestTokens->requestTokenArray =
	    soap_malloc (soap, nbreplies * sizeof(struct ns1__TRequestToken *))) == NULL) {
		free (tokeninfos);
		RETURN (SOAP_EOM);
	}

	repp->arrayOfRequestTokens->__sizerequestTokenArray = nbreplies;
	for (i = 0; i < nbreplies; i++) {
		if ((repp->arrayOfRequestTokens->requestTokenArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TRequestToken))) == NULL ||
		    (repp->arrayOfRequestTokens->requestTokenArray[i]->value =
		    soap_strdup (soap, (tokeninfos+i)->r_token)) == NULL) {
			free (tokeninfos);
			RETURN (SOAP_EOM);
		}
	}
	free (tokeninfos);
	repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}
