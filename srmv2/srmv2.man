.\" @(#)$RCSfile: srmv2.man,v $ $Revision$ $Date$ CERN IT-GD/CT Jean-Philippe Baud
.\" Copyright (C) 1999-2011 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH SRMV2 8 "$Date$" LCG "DPM Administrator Commands"
.SH NAME
srmv2 \- start the SRM v2 server
.SH SYNOPSIS
.B srmv2
[
.BI -c " config_file"
] [
.BI -l " log_file"
]
.SH DESCRIPTION
.LP
The
.B srmv2
command starts the SRM v2 server.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
This will read the Disk Pool Manager "request" database configuration file,
create a pool of threads and look for requests.
Each of them is processed in a thread which opens a connection to the
database server if necessary.
When a request has been completed, the thread becomes idle until it is allocated
to another request.
The connection to the database server is kept open between 2 requests.
.LP
All error messages and statistical information are kept in a log.
.LP
The Disk Pool Manager listen port number can be defined on client hosts and
on the server itself in either of the following ways:
.RS
.LP
setting an environment variable SRMV2_PORT
.RS
.HP
setenv SRMV2_PORT 8444
.RE
.LP
an entry in
.B /etc/shift.conf
like:
.RS
.HP
SRMV2	PORT	8444
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of SRMV2_PORT in srm_server.h.
.LP
The Disk Pool Manager "request" database keeps the requests and their status
even after completion.
.LP
The Disk Pool Manager configuration file contains password information for the
database and must be readable/writable only by root.
It contains a single line in the format:
.HP
.RS 
username/password@server
.RE 
or
.RS
username/password@server/dbname
.RE
.sp 
where 'username' and 'password' are the credentials to login to the database
instance identified by 'server'. If 'dbname' is not specified, "dpm_db" is used.
.LP
In the log each entry has a timestamp.
All entries corresponding to one request have the same request id.
For each user command there is one message giving information about
the requestor (hostname, dn) and one message SRM98 giving the command
itself.
The completion code of the command is also logged.
.SH OPTIONS
.TP
.BI -c " config_file"
Specifies a different path for the Disk Pool Manager configuration file.
.TP
.BI -l " log_file"
Specifies a different path for the Disk Pool Manager log file.
The special value
.B syslog
will send the log messages to the system logger syslogd.
.SH FILES
.TP 1.5i
.B /etc/DPMCONFIG
configuration file
.TP
.B /var/log/srmv2/log
.SH EXAMPLES
.TP
Here is a small log:
.nf
12/03 15:54:51 17419 srmv2: started
12/03 16:54:54 17419,0 PrepareToPut: request by xxx from lxb1908.cern.ch
12/03 16:54:54 17419,0 PrepareToPut: SRM98 - PrepareToPut 3 bfc51f99-17bd-4a45-b440-0fc33ec7a8d4
12/03 16:54:54 17419,0 PrepareToPut: SRM98 - PrepareToPut 0 srm://lxb0722.cern.ch:8444//dpm/dteam/srm_test_suite_file.17702.19410
.fi
.SH SEE ALSO
.BR Clogit(3) ,
.B dpm(1)
