/*
 * Copyright (C) 2004-2008 by CERN
 * All rights reserved
 */

#ifndef lint
static char sccsid[] = "@(#)$RCSfile: srmv2_dirreq.c,v $ $Revision: 1.25 $ $Date: 2009/08/27 13:04:10 $ CERN Jean-Philippe Baud";
#endif /* not lint */

#include <sys/stat.h>
#include "Cgrp.h"
#include "Cnetdb.h"
#include "Cpwd.h"
#include "dpm.h"
#include "dpm_api.h"
#include "dpm_server.h"
#include "dpm_util.h"
#include "dpns_api.h"
#include "serrno.h"
#include "srm_server.h"
#include "srmv2H.h"
static int na_key = -1;

/*			Directory Functions				*/

int
ns1__srmMkdir (struct soap *soap, struct ns1__srmMkdirRequest *req, struct ns1__srmMkdirResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	char logbuf[CA_MAXSFNLEN+7];
	int nbfqans;
	int nbgids;
	struct ns1__srmMkdirResponse *repp;
	char *sfn;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "Mkdir");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate response structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmMkdirResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmMkdirResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}

	if (! req->directoryPath || ! req->directoryPath->SURLOrStFN ||
	    ! req->directoryPath->SURLOrStFN->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "directoryPath is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->directoryPath->SURLOrStFN->value) > CA_MAXSFNLEN ||
	    (sfn = sfnfromsurl (req->directoryPath->SURLOrStFN->value)) == NULL) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREPATH;
		RETURN (SOAP_OK);
	}
	snprintf (logbuf, sizeof(logbuf), "Mkdir %s", req->directoryPath->SURLOrStFN->value);
	srm_logreq (func, logbuf);

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	if (Cns_mkdir (sfn, 0775) < 0) {
		repp->returnStatus->explanation = soap_strdup (soap, sstrerror (serrno));
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
	} else
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmRmdir (struct soap *soap, struct ns1__srmRmdirRequest *req, struct ns1__srmRmdirResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	char logbuf[CA_MAXSFNLEN+7];
	int nbfqans;
	int nbgids;
	struct ns1__srmRmdirResponse *repp;
	char *sfn;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "Rmdir");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate response structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmRmdirResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmRmdirResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}

	if (! req->directoryPath || ! req->directoryPath->SURLOrStFN ||
	    ! req->directoryPath->SURLOrStFN->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "directoryPath is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->directoryPath->SURLOrStFN->value) > CA_MAXSFNLEN ||
	    (sfn = sfnfromsurl (req->directoryPath->SURLOrStFN->value)) == NULL) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREPATH;
		RETURN (SOAP_OK);
	}
	if (req->recursive && *req->recursive == true_) {
		repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
		repp->returnStatus->explanation = soap_strdup (soap, "recursive option is not supported");
		RETURN (SOAP_OK);
	}
	snprintf (logbuf, sizeof(logbuf), "Rmdir %s", req->directoryPath->SURLOrStFN->value);
	srm_logreq (func, logbuf);

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	if (Cns_rmdir (sfn) < 0) {
		repp->returnStatus->explanation = soap_strdup (soap, sstrerror (serrno));
		if (serrno == EEXIST)
			repp->returnStatus->statusCode = SRM_USCORENON_USCOREEMPTY_USCOREDIRECTORY;
		else
			repp->returnStatus->statusCode = serrno2statuscode (serrno);
	} else
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}

int
ns1__srmRm (struct soap *soap, struct ns1__srmRmRequest *req, struct ns1__srmRmResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	struct dpm_filestatus *filestatuses = NULL;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	int j;
	int nbfqans;
	int nbgids;
	int nbreplies = 0;
	int nbsurls;
	struct ns1__TSURLReturnStatus *repfilep;
	struct ns1__srmRmResponse *repp;
	char **surls = NULL;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "Rm");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate response structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmRmResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	repp->arrayOfFileStatuses = NULL;
	rep->srmRmResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}

	if (! req->arrayOfFilePaths) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "arrayOfFilePaths is required");
		RETURN (SOAP_OK);
	}
	nbsurls = req->arrayOfFilePaths->__sizesurlInfoArray;

	/* Allocate the array of file statuses */

	if ((repp->arrayOfFileStatuses =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTSURLReturnStatus))) == NULL ||
	    (repp->arrayOfFileStatuses->surlReturnStatusArray =
	    soap_malloc (soap, nbsurls * sizeof(struct ns1__TSURLReturnStatus *))) == NULL ||
	    (surls = soap_malloc (soap, nbsurls * sizeof (char *))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->arrayOfFileStatuses->__sizesurlReturnStatusArray = nbsurls;

	j = 0;
	for (i = 0; i < nbsurls; i++) {
		surls[j] = req->arrayOfFilePaths->surlInfoArray[i]->SURLOrStFN->value;
		if ((repp->arrayOfFileStatuses->surlReturnStatusArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TSURLReturnStatus))) == NULL ||
		    (repp->arrayOfFileStatuses->surlReturnStatusArray[i]->status =
		    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
			RETURN (SOAP_EOM);
		if (surls[j]) j++;
	}

	if (j > 0) {
		dpm_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
		thip->errbuf[0] = '\0';
		dpm_client_setAuthorizationId (uid, gid, "GSI", clientdn);
		if (voname && fqan)
			dpm_client_setVOMS_data (voname, fqan, nbfqans);

		if (dpm_rm (j, surls, &nbreplies, &filestatuses) < 0 && ! filestatuses) {
			repp->arrayOfFileStatuses = NULL;
			repp->returnStatus->statusCode = serrno2statuscode (serrno);
			if (*thip->errbuf) {
				thip->errbuf[strlen(thip->errbuf)-1] = '\0';
				repp->returnStatus->explanation = soap_strdup (soap, thip->errbuf);
			}
			RETURN (SOAP_OK);
		}
	}

	/* Copy the status (and error string) for each file */

	for (i = 0; i < req->arrayOfFilePaths->__sizesurlInfoArray; i++) {
		repfilep = repp->arrayOfFileStatuses->surlReturnStatusArray[i];
		repfilep->surl = req->arrayOfFilePaths->surlInfoArray[i]->SURLOrStFN;
		if (! req->arrayOfFilePaths->surlInfoArray[i]->SURLOrStFN->value) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			repfilep->status->explanation =
				soap_strdup (soap, "Pointer to SURL is NULL");
			continue;
		}
		if ((filestatuses+i)->surl)
			free ((filestatuses+i)->surl);
		repfilep->status->statusCode =
			dpmstatus2statuscode ((filestatuses+i)->status, 0, 0);
		if ((filestatuses+i)->errstring) {
			repfilep->status->explanation =
				soap_strdup (soap, (filestatuses+i)->errstring);
			free ((filestatuses+i)->errstring);
		} else
			repfilep->status->explanation = NULL;
	}
	free (filestatuses);
	repp->returnStatus->statusCode = j ? SRM_USCORESUCCESS : SRM_USCOREFAILURE;
	RETURN (SOAP_OK);
}

char *
decode_group(gid_t gid, gid_t *sav_gid, char *sav_gidstr)
{
	struct group *gr;

	if (gid != *sav_gid) {
#ifdef VIRTUAL_ID
		if (gid == 0)
			return ("root");
		*sav_gid = gid;
		if (Cns_getgrpbygid (*sav_gid, sav_gidstr) < 0)
#else
		*sav_gid = gid;
		if (gr = getgrgid (*sav_gid))
			strcpy (sav_gidstr, gr->gr_name);
		else
#endif
			return (NULL);
	}
	return (sav_gidstr);
}

char *
decode_user(uid_t uid, uid_t *sav_uid, char *sav_uidstr)
{
	struct passwd *pw;

	if (uid != *sav_uid) {
#ifdef VIRTUAL_ID
		if (uid == 0)
			return ("root");
		*sav_uid = uid;
		if (Cns_getusrbyuid (*sav_uid, sav_uidstr) < 0)
#else
		*sav_uid = uid;
		if (pw = getpwuid (*sav_uid))
			strcpy (sav_uidstr, pw->pw_name);
		else
#endif
			return (NULL);
	}
	return (sav_uidstr);
}

listdir (struct soap *soap, char *dir, int nbfiles, int fflag, int nblevels, int level, struct ns1__TMetaDataPathDetail *repfilep, gid_t *sav_gid, char *sav_gidstr, uid_t *sav_uid, char *sav_uidstr)
{
	Cns_DIR *dirp;
	struct Cns_direnstatg *dxp;
	int i = 0;
	char path[CA_MAXPATHLEN+1];

	if ((repfilep->subPaths =
	    soap_malloc (soap, sizeof(struct ns1__ArrayOfTMetaDataPathDetail))) == NULL ||
	    (nbfiles > 0 && (repfilep->subPaths->pathDetailArray =
	    soap_malloc (soap, nbfiles * sizeof(struct ns1__TMetaDataPathDetail *))) == NULL))
		return (-2);
	if ((dirp = Cns_opendir (dir)) == NULL) {
		repfilep->subPaths = NULL;
		return (-1);
	}
	while ((dxp = Cns_readdirg (dirp)) != NULL) {
		if (i > nbfiles) break;
		if ((repfilep->subPaths->pathDetailArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TMetaDataPathDetail))) == NULL)
			return (-2);
		snprintf (path, sizeof(path), "%s/%s", dir, dxp->d_name);
		listentry (soap, path, dxp, fflag,
		    nblevels, level, repfilep->subPaths->pathDetailArray[i++],
		    sav_gid, sav_gidstr, sav_uid, sav_uidstr);
	}
	(void) Cns_closedir (dirp);
	repfilep->subPaths->__sizepathDetailArray = i;
	return (0);
}

listentry (struct soap *soap, char *path, struct Cns_filestatg *statbuf, int fflag, int nblevels, int level, struct ns1__TMetaDataPathDetail *repfilep, gid_t *sav_gid, char *sav_gidstr, uid_t *sav_uid, char *sav_uidstr)
{
	struct Cns_acl acl[CA_MAXACLENTRIES];
	struct Cns_acl *aclp;
	static enum ns1__TFileType ftypes[] = {File, Directory, Link};
	char *gr_name;
	int ig;
	int iu;
	int j;
	int nbfiles;
	int nentries;
	int ng;
	int nu;
	char *pw_name;
	char *sfn;
	struct Cns_filestatg st;

	memset (repfilep, 0, sizeof(struct ns1__TMetaDataPathDetail));
	if ((repfilep->status =
	    soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL)
		return (-2);
	memset (repfilep->status, 0, sizeof(struct ns1__TReturnStatus));
	if (! statbuf) {
		if (! path) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			repfilep->status->explanation = soap_strdup (soap, "Pointer to SURL is NULL");
			return (-1);
		}
		if (strlen (path) >= CA_MAXSFNLEN ||
		    (sfn = sfnfromsurl (path)) == NULL) {
			repfilep->status->statusCode = SRM_USCOREINVALID_USCOREPATH;
			return (-1);
		}
		repfilep->path = sfn;
		if (Cns_statg (sfn, NULL, &st) < 0) {
			repfilep->status->explanation = soap_strdup (soap, sstrerror (serrno));
			repfilep->status->statusCode = serrno2statuscode (serrno);
			return (-1);
		}
	} else {
		if ((repfilep->path = soap_strdup (soap, path)) == NULL)
			return (-2);
		memcpy (&st, statbuf, sizeof(struct Cns_filestatg));
	}
	if ((repfilep->size =
	    soap_malloc (soap, sizeof(struct ns1__TSizeInBytes))))
		repfilep->size->value = (st.filemode & S_IFDIR) ? 0 : st.filesize;
	if (st.filemode & S_IFDIR)
		repfilep->type = &ftypes[Directory];
	else if ((st.filemode & S_IFLNK) == S_IFLNK)
		repfilep->type = &ftypes[Link];
	else
		repfilep->type = &ftypes[File];
	if (fflag == 0)
		goto end_entry;
	if ((repfilep->ownerPermission =
	    soap_malloc (soap, sizeof(struct ns1__TOwnerPermission))))
		repfilep->ownerPermission->mode = (st.filemode >> 6) & 7;
	if ((nentries = Cns_getacl (repfilep->path, CA_MAXACLENTRIES, acl)) > 3) {
		ng = 0;
		nu = 0;
		for (j = 0, aclp = acl; j < nentries; j++, aclp++) {
			if (aclp->a_type == CNS_ACL_USER) nu++;
			if (aclp->a_type == CNS_ACL_GROUP_OBJ) ng++;
			if (aclp->a_type == CNS_ACL_GROUP) ng++;
		}
		if (nu == 0 || (repfilep->userPermissions =
		      soap_malloc (soap, sizeof(struct ns1__ArrayOfTUserPermission))) == NULL ||
		    (repfilep->userPermissions->userPermissionArray =
		      soap_malloc (soap, nu * sizeof(struct ns1__TUserPermission *))) == NULL)
			repfilep->userPermissions = NULL;
		if ((repfilep->groupPermissions =
		      soap_malloc (soap, sizeof(struct ns1__ArrayOfTGroupPermission))) == NULL ||
		    (repfilep->groupPermissions->groupPermissionArray =
		      soap_malloc (soap, ng * sizeof(struct ns1__TGroupPermission *))) == NULL)
			repfilep->groupPermissions = NULL;
		ig = 0;
		iu = 0;
		for (j = 0, aclp = acl; j < nentries; j++, aclp++) {
			if (aclp->a_type == CNS_ACL_USER &&
			    (pw_name = decode_user (aclp->a_id, sav_uid, sav_uidstr)) &&
			    repfilep->userPermissions &&
			    (repfilep->userPermissions->userPermissionArray[iu] =
			      soap_malloc (soap, sizeof(struct ns1__TUserPermission))) &&
			    (repfilep->userPermissions->userPermissionArray[iu]->userID =
			      soap_malloc (soap, sizeof(struct ns1__TUserID))) &&
			    (repfilep->userPermissions->userPermissionArray[iu]->userID->value =
			      soap_strdup (soap, pw_name)))
				repfilep->userPermissions->userPermissionArray[iu++]->mode = aclp->a_perm;
			if ((aclp->a_type == CNS_ACL_GROUP_OBJ ||
			    aclp->a_type == CNS_ACL_GROUP) &&
			    (gr_name = decode_group (aclp->a_id, sav_gid, sav_gidstr)) &&
			    repfilep->groupPermissions &&
			    (repfilep->groupPermissions->groupPermissionArray[ig] =
			      soap_malloc (soap, sizeof(struct ns1__TGroupPermission))) &&
			    (repfilep->groupPermissions->groupPermissionArray[ig]->groupID =
			      soap_malloc (soap, sizeof(struct ns1__TGroupID))) &&
			    (repfilep->groupPermissions->groupPermissionArray[ig]->groupID->value =
			      soap_strdup (soap, gr_name)))
				repfilep->groupPermissions->groupPermissionArray[ig++]->mode = aclp->a_perm;
		}
		if (iu)
			repfilep->userPermissions->__sizeuserPermissionArray = iu;
		else
			repfilep->userPermissions = NULL;
		if (ig)
			repfilep->groupPermissions->__sizegroupPermissionArray = ig;
		else
			repfilep->groupPermissions = NULL;
	} else {
		if ((gr_name = decode_group (st.gid, sav_gid, sav_gidstr)) &&
		    (repfilep->groupPermissions =
		      soap_malloc (soap, sizeof(struct ns1__ArrayOfTGroupPermission))) &&
		    (repfilep->groupPermissions->groupPermissionArray =
		      soap_malloc (soap, sizeof(struct ns1__TGroupPermission *))) &&
		    (repfilep->groupPermissions->groupPermissionArray[0] =
		      soap_malloc (soap, sizeof(struct ns1__TGroupPermission))) &&
		    (repfilep->groupPermissions->groupPermissionArray[0]->groupID =
		      soap_malloc (soap, sizeof(struct ns1__TGroupID))) &&
		    (repfilep->groupPermissions->groupPermissionArray[0]->groupID->value =
		      soap_strdup (soap, gr_name))) {
			repfilep->groupPermissions->groupPermissionArray[0]->mode = (st.filemode >> 3) & 7;
			repfilep->groupPermissions->__sizegroupPermissionArray = 1;
		} else
			repfilep->groupPermissions = NULL;
	}
	if ((repfilep->otherPermission =
	    soap_malloc (soap, sizeof(struct ns1__TOtherPermission))))
		repfilep->otherPermission->mode = st.filemode & 7;
	if ((repfilep->createdAtTime =
	    soap_malloc (soap, sizeof(struct ns1__TGMTTime)))) {
		if ((repfilep->createdAtTime->value =
		    soap_strdup (soap, soap_dateTime2s (soap, st.ctime))) == NULL)
			repfilep->createdAtTime = NULL;
	}
	if ((repfilep->lastModificationTime =
	    soap_malloc (soap, sizeof(struct ns1__TGMTTime)))) {
		if ((repfilep->lastModificationTime->value =
		    soap_strdup (soap, soap_dateTime2s (soap, st.mtime))) == NULL)
			repfilep->lastModificationTime = NULL;
	}
	if ((pw_name = decode_user (st.uid, sav_uid, sav_uidstr)) &&
	    (repfilep->owner =
	    soap_malloc (soap, sizeof(struct ns1__TUserID)))) {
		if ((repfilep->owner->value =
		    soap_strdup (soap, pw_name)) == NULL)
			repfilep->owner = NULL;
	}
	if (*st.csumtype &&
	    (repfilep->checkSumType =
	    soap_malloc (soap, sizeof(struct ns1__TCheckSumType)))) {
		if ((repfilep->checkSumType->value =
		    soap_strdup (soap, csumtype2srmname (st.csumtype))) == NULL)
			repfilep->checkSumType = NULL;
	}
	if (*st.csumvalue &&
	    (repfilep->checkSumValue =
	    soap_malloc (soap, sizeof(struct ns1__TCheckSumValue)))) {
		if ((repfilep->checkSumValue->value =
		    soap_strdup (soap, st.csumvalue)) == NULL)
			repfilep->checkSumValue = NULL;
	}
end_entry:
	if ((st.filemode & S_IFDIR) == 0 || level >= nblevels || st.nlink == 0)
		return (0);
	return (listdir (soap, repfilep->path, st.nlink, fflag, nblevels, level + 1, repfilep,
	    sav_gid, sav_gidstr, sav_uid, sav_uidstr));
}

int
ns1__srmLs (struct soap *soap, struct ns1__srmLsRequest *req, struct ns1__srmLsResponse_ *rep)
{
	int c;
	char clientdn[256];
	const char *clienthost;
	int fflag;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	int i;
	char logbuf[CA_MAXSFNLEN+4];
	int nb_file_err = 0;
	int nbfqans;
	int nbgids;
	int nblevels;
	int nbsurls;
	struct ns1__srmLsResponse *repp;
	gid_t sav_gid = -1;
	char sav_gidstr[256];
	uid_t sav_uid = -1;
	char sav_uidstr[256];
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "Ls");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate response structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmLsResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->details = NULL;
	repp->returnStatus->explanation = NULL;
	rep->srmLsResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}

	if (! req->paths) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "paths is required");
		RETURN (SOAP_OK);
	}
	if (req->fileStorageType) {
		repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
		repp->returnStatus->explanation = soap_strdup (soap, "filtering by fileStorageType is not supported");
		RETURN (SOAP_OK);
	}
	if ((req->allLevelRecursive && *req->allLevelRecursive) ||
	    (req->numOfLevels && *req->numOfLevels > 1)) {
		repp->returnStatus->statusCode = SRM_USCORENOT_USCORESUPPORTED;
		repp->returnStatus->explanation = soap_strdup (soap, "recursive option is not supported");
		RETURN (SOAP_OK);
	}
	if (req->numOfLevels)
		nblevels = *req->numOfLevels;
	else
		nblevels = 1;
	if (req->fullDetailedList && *req->fullDetailedList == true_)
		fflag = 1;
	else
		fflag = 0;

	nbsurls = req->paths->__sizesurlInfoArray;
	if ((repp->details =
		soap_malloc (soap, sizeof(struct ns1__ArrayOfTMetaDataPathDetail))) == NULL ||
	    (nbsurls > 0 && (repp->details->pathDetailArray =
		soap_malloc (soap, nbsurls * sizeof(struct ns1__TMetaDataPathDetail *))) == NULL))
		RETURN (SOAP_EOM);

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	repp->details->__sizepathDetailArray = nbsurls;
	for (i = 0; i < nbsurls; i++) {
		char *path = NULL;
		if (req->paths->surlInfoArray[i] && req->paths->surlInfoArray[i]->SURLOrStFN)
			path = req->paths->surlInfoArray[i]->SURLOrStFN->value;
		if (path != NULL) {
			snprintf (logbuf, sizeof(logbuf), "Ls %s", path);
		} else {
			snprintf (logbuf, sizeof(logbuf), "Ls");
		}
		srm_logreq (func, logbuf);
		if ((repp->details->pathDetailArray[i] =
		    soap_malloc (soap, sizeof(struct ns1__TMetaDataPathDetail))) == NULL)
			RETURN (SOAP_EOM);
		c = listentry (soap, path, NULL, fflag, nblevels, 0,
		    repp->details->pathDetailArray[i], &sav_gid, sav_gidstr, &sav_uid, sav_uidstr);
		if (c == -2)
			RETURN (SOAP_EOM);
		if (c == -1)
			nb_file_err++;
	}
	repp->returnStatus->statusCode = (nb_file_err != nbsurls) ?
		SRM_USCORESUCCESS : (nbsurls == 1) ?
		repp->details->pathDetailArray[0]->status->statusCode : SRM_USCOREFAILURE;
	RETURN (SOAP_OK);
}

int
ns1__srmMv (struct soap *soap, struct ns1__srmMvRequest *req, struct ns1__srmMvResponse_ *rep)
{
	char clientdn[256];
	const char *clienthost;
	char **fqan;
	char func[16];
	gid_t gid;
	gid_t *gids;
	char logbuf[2*CA_MAXSFNLEN+5];
	int nbfqans;
	int nbgids;
	struct ns1__srmMvResponse *repp;
	char *sfn1;
	char *sfn2;
	struct srm_srv_thread_info *thip = soap->user;
	uid_t uid;
	char *voname;

	strcpy (func, "Mv");
	get_client_dn (soap, clientdn, sizeof(clientdn));
	clienthost = Cgetnetaddress (-1, &soap->peer, soap->peerlen, &na_key, NULL, NULL, 0, 0);
	if (!clienthost) clienthost = "(unknown)";
	srmlogit (func, "request by %s from %s\n", clientdn, clienthost);
	if (! req) {
		soap_sender_fault (soap, "NULL request", NULL);
		RETURN (SOAP_FAULT);
	}

	/* Allocate response structure */

	if ((repp = soap_malloc (soap, sizeof(struct ns1__srmMvResponse))) == NULL ||
	    (repp->returnStatus = soap_malloc (soap, sizeof(struct ns1__TReturnStatus))) == NULL) {
		RETURN (SOAP_EOM);
	}
	repp->returnStatus->explanation = NULL;
	rep->srmMvResponse = repp;

	if (get_client_full_id (soap, clientdn, &voname, &fqan, &nbfqans, &uid, &gid, &nbgids, &gids) < 0) {
		repp->returnStatus->statusCode = SRM_USCOREAUTHENTICATION_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "Could not get user mapping");
		RETURN (SOAP_OK);
	}

	if (! req->fromPath || ! req->fromPath->SURLOrStFN ||
	    ! req->fromPath->SURLOrStFN->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "fromPath is required");
		RETURN (SOAP_OK);
	}
	if (! req->toPath || ! req->toPath->SURLOrStFN ||
	    ! req->toPath->SURLOrStFN->value) {
		repp->returnStatus->statusCode = SRM_USCOREFAILURE;
		repp->returnStatus->explanation = soap_strdup (soap, "toPath is required");
		RETURN (SOAP_OK);
	}
	if (strlen (req->fromPath->SURLOrStFN->value) > CA_MAXSFNLEN ||
	    strlen (req->toPath->SURLOrStFN->value) > CA_MAXSFNLEN ||
	    (sfn1 = sfnfromsurl (req->fromPath->SURLOrStFN->value)) == NULL ||
	    (sfn2 = sfnfromsurl (req->toPath->SURLOrStFN->value)) == NULL) {
		repp->returnStatus->statusCode = SRM_USCOREINVALID_USCOREPATH;
		RETURN (SOAP_OK);
	}
	snprintf (logbuf, sizeof(logbuf), "Mv %s %s", req->fromPath->SURLOrStFN->value,
	    req->toPath->SURLOrStFN->value);
	srm_logreq (func, logbuf);

	Cns_seterrbuf (thip->errbuf, sizeof(thip->errbuf));
	Cns_client_setAuthorizationId (uid, gid, "GSI", clientdn);
	if (voname && fqan)
		Cns_client_setVOMS_data (voname, fqan, nbfqans);

	if (Cns_rename (sfn1, sfn2) < 0) {
		repp->returnStatus->explanation = soap_strdup (soap, sstrerror (serrno));
		repp->returnStatus->statusCode = serrno2statuscode (serrno);
	} else
		repp->returnStatus->statusCode = SRM_USCORESUCCESS;
	RETURN (SOAP_OK);
}
