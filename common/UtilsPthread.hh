#ifndef __UTILS_PTHREAD__
#define __UTILS_PTHREAD__

/******************************************************************************
 *UtilsPthread
 *A set of utilities that wrap the pthread library for use with C++
 *By Fabrizio Furano (CERN IT/GT, Sept 2011)
 *
 *Originally taken from XrdSysPthread, by A.Hanushevsky, with a few additions
 ******************************************************************************/


#include <errno.h>
#ifdef WIN32
#define HAVE_STRUCT_TIMESPEC 1
#endif
#include <pthread.h>
#include <signal.h>
#ifdef AIX
#include <sys/sem.h>
#else
#include <semaphore.h>
#endif


// UptCondVar implements the standard POSIX-compliant condition variable.
//            Methods correspond to the equivalent pthread condvar functions.

class UptCondVar {
public:

	inline void  Lock()           {pthread_mutex_lock(&cmut);}

	inline void  Signal()         {
		if (relMutex) pthread_mutex_lock(&cmut);
		pthread_cond_signal(&cvar);
		if (relMutex) pthread_mutex_unlock(&cmut);
	}

	inline void  Broadcast()      {if (relMutex) pthread_mutex_lock(&cmut);
		pthread_cond_broadcast(&cvar);
		if (relMutex) pthread_mutex_unlock(&cmut);
	}

	inline void  UnLock()         {pthread_mutex_unlock(&cmut);}

	int   Wait();
	int   Wait(int sec);
	int   WaitMS(int msec);

	UptCondVar(      int   relm=1, // 0->Caller will handle lock/unlock
			 const char *cid=0   // ID string for debugging only
			 ) {
		pthread_cond_init(&cvar, NULL);
		pthread_mutex_init(&cmut, NULL);
		relMutex = relm; condID = (cid ? cid : "unk");
	}

     ~UptCondVar() {
	     pthread_cond_destroy(&cvar);
	     pthread_mutex_destroy(&cmut);
     }
private:

	pthread_cond_t  cvar;
	pthread_mutex_t cmut;
	int             relMutex;
	const char     *condID;
};



// UptCondVarHelper is used to implement monitors with the Lock of a a condvar.
//                     Monitors are used to lock
//                     whole regions of code (e.g., a method) and automatically
//                     unlock with exiting the region (e.g., return). The
//                     methods should be self-evident.
  
class UptCondVarHelper {
public:

	inline void   Lock(UptCondVar *CndVar) {
		if (cnd) {if (cnd != CndVar) cnd->UnLock();
			else return;
		}
		CndVar->Lock();
		cnd = CndVar;
	};

	inline void UnLock() {if (cnd) {cnd->UnLock(); cnd = 0;}}

	UptCondVarHelper(UptCondVar *CndVar=0) {
		if (CndVar) CndVar->Lock();
		cnd = CndVar;
	}

	UptCondVarHelper(UptCondVar &CndVar) {
		CndVar.Lock();
		cnd = &CndVar;
	}

	~UptCondVarHelper() {
		if (cnd) UnLock();
	}

private:
	UptCondVar *cnd;
};









// UptMutex implements the standard POSIX mutex. The methods correspond
//             to the equivalent pthread mutex functions.
  
class UptMutex
{
public:

inline int CondLock()
       {if (pthread_mutex_trylock( &cs )) return 0;
        return 1;
       }

inline void   Lock() {pthread_mutex_lock(&cs);}

inline void UnLock() {pthread_mutex_unlock(&cs);}

        UptMutex() {pthread_mutex_init(&cs, NULL);}
       ~UptMutex() {pthread_mutex_destroy(&cs);}

protected:

pthread_mutex_t cs;
};







// UptRecMutex implements the recursive POSIX mutex. The methods correspond
//             to the equivalent pthread mutex functions.
  
class UptRecMutex: public UptMutex
{
public:

UptRecMutex();

};




// UptMutexHelper us ised to implement monitors. Monitors are used to lock
//                   whole regions of code (e.g., a method) and automatically
//                   unlock with exiting the region (e.g., return). The
//                   methods should be self-evident.
  
class UptMutexHelper
{
public:

inline void   Lock(UptMutex *Mutex)
                  {if (mtx) {if (mtx != Mutex) mtx->UnLock();
                                else return;
                            }
                   Mutex->Lock();
                   mtx = Mutex;
                  };

inline void UnLock() {if (mtx) {mtx->UnLock(); mtx = 0;}}

            UptMutexHelper(UptMutex *mutex=0)
                 {if (mutex) mutex->Lock();
                  mtx = mutex;
                 }
            UptMutexHelper(UptMutex &mutex) {
	         mutex.Lock();
		 mtx = &mutex;
                 }

           ~UptMutexHelper() {if (mtx) UnLock();}
private:
UptMutex *mtx;
};












// UptRWLock implements the standard POSIX wrlock mutex. The methods correspond
//             to the equivalent pthread wrlock functions.
  
class UptRWLock
{
public:

inline int CondReadLock()
       {if (pthread_rwlock_tryrdlock( &lock )) return 0;
        return 1;
       }
inline int CondWriteLock()
       {if (pthread_rwlock_trywrlock( &lock )) return 0;
        return 1;
       }

inline void  ReadLock() {pthread_rwlock_rdlock(&lock);}
inline void  WriteLock() {pthread_rwlock_wrlock(&lock);}

inline void UnLock() {pthread_rwlock_unlock(&lock);}

        UptRWLock() {pthread_rwlock_init(&lock, NULL);}
       ~UptRWLock() {pthread_rwlock_destroy(&lock);}

protected:

pthread_rwlock_t lock;
};











// UptWRLockHelper : helper class for UptRWLock
  
class UptRWLockHelper
{
public:

inline void   Lock(UptRWLock *lock, bool rd = 1)
                  {if (lck) {if (lck != lock) lck->UnLock();
                                else return;
                            }
                   if (rd) lock->ReadLock();
                      else lock->WriteLock();
                   lck = lock;
                  };

inline void UnLock() {if (lck) {lck->UnLock(); lck = 0;}}

            UptRWLockHelper(UptRWLock *l=0, bool rd = 1)
                 { if (l) {if (rd) l->ReadLock();
                              else l->WriteLock();
                          }
                   lck = l;
                 }
            UptRWLockHelper(UptRWLock &l, bool rd = 1)
                 { if (rd) l.ReadLock();
                      else l.WriteLock();
                   lck = &l;
                 }

           ~UptRWLockHelper() {if (lck) UnLock();}
private:
UptRWLock *lck;
};









// UptSemaphore implements the classic counting semaphore. The methods
//                 should be self-evident. Note that on certain platforms
//                 semaphores need to be implemented based on condition
//                 variables since no native implementation is available.
  
#ifdef __macos__
class UptSemaphore
{
public:

       int  CondWait();

       void Post();

       void Wait();

  UptSemaphore(int semval=1,const char *cid=0) : semVar(0, cid)
                                  {semVal = semval; semWait = 0;}
 ~UptSemaphore() {}

private:

UptCondVar semVar;
int           semVal;
int           semWait;
};

#else

class UptSemaphore
{
public:

inline int  CondWait()
       {while(sem_trywait( &h_semaphore ))
             {if (errno == EAGAIN) return 0;
              if (errno != EINTR) { throw "sem_CondWait() failed";}
             }
        return 1;
       }

inline void Post() {if (sem_post(&h_semaphore))
                       {throw "sem_post() failed";}
                   }

inline void Wait() {while (sem_wait(&h_semaphore))
                          {if (EINTR != errno) 
                              {throw "sem_wait() failed";}
                          }
                   }

  UptSemaphore(int semval=1, const char * =0)
                               {if (sem_init(&h_semaphore, 0, semval))
				   {throw "sem_init() failed";}
                               }
 ~UptSemaphore() {if (sem_destroy(&h_semaphore))
                       {throw "sem_destroy() failed";}
                   }

private:

sem_t h_semaphore;
};
#endif




  
// Basic, lightweight pthread functions wrapper (syntactic sugar)
// Look above for a more advanced version of this


// Options to Run()
//
// BIND creates threads that are bound to a kernel thread.
//
#define UPTTHREAD_BIND 0x001

// HOLD creates a thread that needs to be joined to get its ending value.
//      Otherwise, a detached thread is created.
//
#define UPTTHREAD_HOLD 0x002

class UptThread {
public:
	
	static int          Cancel(pthread_t tid) {return pthread_cancel(tid);}

	static int          Detach(pthread_t tid) {return pthread_detach(tid);}


	static  int  SetCancelOff() {
		return pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, 0);
	};

	static  int  Join(pthread_t tid, void **ret) {
		return pthread_join(tid, ret);
	};

	static  int  SetCancelOn() {
		return pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);
	};

	static  int  SetCancelAsynchronous() {
		return pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, 0);
	};

	static int  SetCancelDeferred() {
		return pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, 0);
	};

	static void  CancelPoint() {
		pthread_testcancel();
	};


	static pthread_t    ID(void)              {return pthread_self();}

	static int          Kill(pthread_t tid)   {return pthread_cancel(tid);}

	static unsigned long Num(void) {
		if (!initDone) doInit();
		return (unsigned long)pthread_getspecific(threadNumkey);
	}

	static int          Run(pthread_t *, void *(*proc)(void *), void *arg, 
				int opts=0, const char *desc = 0);

	static int          Same(pthread_t t1, pthread_t t2) {
		return pthread_equal(t1, t2);
	}

	static void         setStackSize(size_t stsz) {stackSize = stsz;}

	static int          Signal(pthread_t tid, int snum) {
		return pthread_kill(tid, snum);
	}
 
	static int          Wait(pthread_t tid);

	UptThread() {}
	~UptThread() {}

private:
	static void          doInit(void);

	static pthread_key_t threadNumkey;
	static size_t        stackSize;
	static int           initDone;
};








//
// UptSemWait
// A counting semaphore with timed out wait primitive
//
class UptSemWait {
public:

	int  CondWait() {

		int rc = 0;
		// Decrease the semaphore value until the sempahore value is positive
		// otherwise return nonzero, without blocking

		// This will not be starvation
		// free is the OS implements an unfair mutex;
		// Returns 0 if signalled, non-0 if would block
		//

		semVar.Lock();
		if (semVal > 0) semVal--;
		else {
			rc = 1;
		}

		semVar.UnLock();

		return rc;

	};
   
	void Post() {
		// Add one to the semaphore counter. If we the value is > 0 and there is a
		// thread waiting for the sempagore, signal it to get the semaphore.
		//
		semVar.Lock();

		if (semWait > 0) {
			semVar.Signal();
			semWait--;
		}
		else
			semVal++;
      
		semVar.UnLock();
	};
   
	void Wait()   {
		// Wait until the sempahore value is positive. This will not be starvation
		// free is the OS implements an unfair mutex;
		//

		semVar.Lock();
		if (semVal > 0) semVal--;
		else {
			semWait++;
			semVar.Wait();
		}

		semVar.UnLock();

	};

	int Wait(int secs)  {
		int rc = 0;
		// Wait until the sempahore value is positive. This will not be starvation
		// free is the OS implements an unfair mutex;
		// Returns 0 if signalled, non-0 if timeout
		//

		semVar.Lock();
		if (semVal > 0) semVal--;
		else {
			semWait++;
			rc = semVar.Wait(secs);
			if (rc) semWait--;
		}

		semVar.UnLock();

		return rc;
	};

	UptSemWait(int semval=1,const char *cid=0) : semVar(0, cid) {
		semVal = semval; semWait = 0;
	}

	~UptSemWait() {}

private:

	UptCondVar semVar;
	int           semVal;
	int           semWait;
};











void * UptThreadFancyDispatcher(void * arg);




class UptThreadFancy {
private:
   pthread_t fThr;

   typedef void *(*VoidRtnFunc_t)(void *, UptThreadFancy *);
   VoidRtnFunc_t ThreadFunc;

   friend void *UptThreadFancyDispatcher(void *);

 public:
   struct UptThreadFancyArgs {
      void *arg;
      UptThreadFancy *threadobj;
   } fArg;


   UptThreadFancy(VoidRtnFunc_t fn) {
#ifndef WIN32
      fThr = 0;
#endif
      ThreadFunc = fn;
   };

   virtual ~UptThreadFancy() {

//      Cancel();
   };

   int Cancel() {
      return UptThread::Cancel(fThr);
   };

   int Run(void *arg = 0) {
      fArg.arg = arg;
      fArg.threadobj = this;
      return UptThread::Run(&fThr, UptThreadFancyDispatcher, (void *)&fArg,
                               UPTTHREAD_HOLD, "");
   };

   int Detach() {
      return UptThread::Detach(fThr);
   };

   int Join(void **ret = 0) {
      return UptThread::Join(fThr, ret);
   };

   // these funcs are to be called only from INSIDE the thread loop
   int     SetCancelOn() {
      return UptThread::SetCancelOn();
   };
   int     SetCancelOff() {
      return UptThread::SetCancelOff();
   };
   int     SetCancelAsynchronous() {
      return UptThread::SetCancelAsynchronous();
   };
   int     SetCancelDeferred() {
      return UptThread::SetCancelDeferred();
   };
void     CancelPoint() {
      UptThread::CancelPoint();
   };

   int MaskSignal(int snum = 0, bool block = 1);
};

#endif
