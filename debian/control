Source: lcg-dm
Section: net
Priority: optional
Maintainer: project-eu-egee-glite-middleware@cern.ch
Build-Depends: debhelper (>= 5.0.22), python-dev (>= 2.3), perl (>= 5.6), libc6-dev
Standards-Version: 3.7.2

Package: liblcgdm-dev
Section: libs
Architecture: i386 amd64
Depends: liblcgdm1 (>= ${binary:Version}), glite-security-voms-api-cpp (>= 1.7.24)
Description: Development files for common LCG DM components (LFC / DPM)

Package: liblcgdm1
Section: libs
Architecture: i386 amd64
Depends: glite-security-voms-api-cpp (>= 1.7.24), ${shlibs:Depends}
Description: Common libraries for LFC and DPM
 The liblcgdm package contains common libraries and man pages that are
 useful for the LCG Data Management components, the LFC (LCG File Catalog) and
 the DPM (Disk Pool Manager)

Package: lfc-client
Section: utils
Architecture: i386 amd64
Depends: liblfc1 (>= ${binary:Version})
Description: Command line utilities for LFC

Package: liblfc1
Section: libs
Architecture: i386 amd64
Depends: liblcgdm1 (>= 1.7.0), ${shlibs:Depends}
Description: Libraries for LFC

Package: liblfc-dev
Section: libdevel
Architecture: i386 amd64
Depends: liblcgdm-dev (>= 1.7.0), liblfc1 (>= ${binary:Version}), glite-security-voms-api (>= 1.7.24)
Description: Development files for LFC

Package: liblfc-perl
Section: libs
Architecture: i386 amd64
Depends: liblfc1 (>= 1.7.0), ${perl:Depends}
Description: Perl and Python interfaces for LFC
 The LCG File Catalog (LFC) allows to store files in a File System looking like
 structure.  It allows you to create symbolic links to any file or directory
 stored in the LFC, as well as replicas.  This is the perl
 interface to the LFC built with swig.

Package: python-lfc
Section: libs
Architecture: i386 amd64
Depends: liblfc1 (>= 1.7.0), ${python:Depends}
Description: Python interface for LFC
 The LCG File Catalog (LFC) allows to store files in a File System looking like
 structure.  It allows you to create symbolic links to any file or directory
 stored in the LFC, as well as replicas.  This is the python
 interface to the LFC built with swig.
 
Package: dpm-client
Section: utils
Architecture: i386 amd64
Depends: libdpm1 (>= ${binary:Version})
Description: Command line utilities for DPM

Package: libdpm1
Section: libs
Architecture: i386 amd64
Depends: liblcgdm1 (>= 1.7.0), ${shlibs:Depends}
Description: Library for DPM

Package: libdpm-dev
Section: libdevel
Architecture: i386 amd64
Depends: liblcgdm-dev (>= 1.7.0), libdpm1 (>= ${binary:Version}), glite-security-voms-api (>= 1.7.24)
Description: Development files for DPM

Package: libdpm-perl
Section: libs
Architecture: i386 amd64
Depends: libdpm1 (>= 1.7.0), perl (>= 5.10)
Description: Light weight Disk Pool Manager offering SRMv1, SRMv2 
             and socket interfaces. This is the perl 
             interface to the DPM built with swig.

Package: python-dpm
Section: libs
Architecture: i386 amd64
Depends: libdpm1 (>= 1.7.0), python (>= 2.3)
Description: Light weight Disk Pool Manager offering SRMv1, SRMv2 
             and socket interfaces. This is the python 
             interface to the DPM built with swig.
